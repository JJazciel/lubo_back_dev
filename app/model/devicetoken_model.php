<?php
namespace App\Model;

use App\Lib\Response,
    App\Lib\Cifrado;

class DevicetokenModel
{
    private $db;
    private $tbtoken = 'tbtoken';
    private $tbPersona = 'tbpersona';
    private $response;

    public function __CONSTRUCT($db)
    {
        $this->db = $db;
        $this->response = new Response();
    }

    public function altaToken($data){
        $idPersona = $data['idPersona'];
        $buscarPersona = $this->db->from($this->tbPersona)
            ->select('COUNT(*) Total')
            ->where('id_tbPersona',$idPersona)
            ->fetch()
            ->Total;

        if ($buscarPersona > 0) {
            $idToken = $this->db->from($this->tbtoken)
                ->select('COUNT(*) Total')
                ->where('idPersona',$idPersona)
                ->fetch()
                ->Total;

            if ($idToken > 0) {
                $actualizatoken = $this->db->update($this->tbtoken, $data)
                    ->where('idPersona',$idPersona)
                    ->execute();

                       $this->response->result = "$actualizatoken";
                return $this->response->SetResponse(true);
            }else{
                $altatoken=$this->db->insertInto($this->tbtoken, $data)
                                ->execute();

                       $this->response->result = $altatoken;
                return $this->response->SetResponse(true);
            }
        }
        else{
                   $this->response->errors='El usuario al que hace referencia no existe';
            return $this->response->SetResponse(false);
        }
    }

    public function eliminar($id){
        $eliminar = $this->db->deleteFrom($this->tbtoken)
                             ->where('idToken',$id)
                             ->execute();

               $this->response->result = $eliminar;
        return $this->response->SetResponse(true);
    }
}
?>