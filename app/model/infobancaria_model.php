<?php
namespace App\Model;

use App\Lib\Response;

class InfobancriaModel{
    private $db;
    private $table = "tbdatosBancarios";
    private $response;

    public function __CONSTRUCT($db){
        $this->db = $db;
        $this->response = new Response();
    }

    public function listar($idPersona){
        $data = $this->db->from($this->table)
                         ->where('idPersona',$idPersona)
                         ->fetch();
               if (is_object($data)) {
                        $this->response->result=$data;
                    return $this->response->SetResponse(true);
               } else {
                   return $this->response->SetResponse(false);
               }
    }

    public function registrar($data){
        $buscar = $this->db->from($this->table)
                           ->where('idPersona',$data['idPersona'])
                           ->fetch();

        if(!is_object($buscar)){
            $insertarInfoBancaria = $this->db->insertInto($this->table,$data)
                                             ->execute();
                    $this->response->result=$insertarInfoBancaria;
            return  $this->response->SetResponse(true);
        }else{
                    $this->response->errors = "Ya hay un metodo registrado";
            return  $this->response->SetResponse(false);
        }
    }

    public function actualizar($id,$data)
    {
        $actulizarInfoBancaria = $this->db->update($this->table,$data)
                                          ->where('idDatosBancarios',$id)
                                          ->execute();
        if ($actulizarInfoBancaria > 0) {
                    $this->response->result=$actulizarInfoBancaria;
            return  $this->response->SetResponse(true);
        }else{
                    $this->response->result=$actulizarInfoBancaria;
                    $this->response->errors = "No se actualizo el registro";
            return  $this->response->SetResponse(false);
        }                               
    }
}