<?php
namespace App\Model;

use App\Lib\Response,
    App\Lib\Cifrado,
    App\Lib\CURL;

class CalificacionModel{
  private $db;
  private $tbCalificacion = 'tbcalificacion';
  private $tbPersona = 'tbpersona';
  private $tbViaje = 'tbviaje';
  private $response;

  public function __CONSTRUCT($db){
    $this->db = $db;
    $this->response = new Response();
  }

  public function listarcalificacion($id){
    $calificaciones = $this->db->from($this->tbCalificacion)
                               ->select(null)
                               ->select('tbcalificacion.Calificacion, tbcalificacion.Comentario, tbpersona.Nombre')
                               ->leftJoin('tbpersona ON tbpersona.id_tbPersona = tbcalificacion.Califica')
                               ->where('tbcalificacion.Calificado', $id)
                               ->orderBy('id_tbCalificacion DESC')
                               ->fetchAll();
    
    if ($calificaciones != false) {
      $totalCalificaciones = count($calificaciones);

      $promedioCalificacion = $this->db->from($this->tbCalificacion)
                                       ->select('IFNULL(AVG(Calificacion), 0) as Promedio')
                                       ->where('tbcalificacion.Calificado', $id)
                                       ->fetch()
                                       ->Promedio;

             $this->response->result=[
                                      'data'  => $calificaciones,
                                      'total' => floatval($totalCalificaciones),
                                      'promedio' => round($promedioCalificacion, 2)
                                    ];
      return $this->response->SetResponse(true);
    }else{
             $this->response->result=[
                                      'data'  => [],
                                      'total' => floatval(0),
                                      'promedio' => floatval(5)
                                    ];
      return $this->response->SetResponse(true);
    }
   }

  public function registrarCalificacion($data){
    $persona = $this->db->from($this->tbPersona)
                        ->select(null)
                        ->select('
                          Tipo_de_usuario, 
                          Round(AVG(Calificacion), 2) AS miPromedio
                          ')
                        ->leftJoin('tbcalificacion ON tbcalificacion.Calificado = tbpersona.id_tbPersona')
                        ->where('id_tbPersona', $data['idPersona'])
                        ->fetchAll();

    if ($persona != false) {
      $id_viaje = $data['idViaje'];
      $viaje = $this->db->from($this->tbViaje)
                        ->select(null)
                        ->select('
                          tbviaje.idViaje,
                          tbviaje.idFirebase, 
                          tbviaje.idConductor, 
                          tbviaje.idUsuario, 
                          tbviaje.CalificadoDriver, 
                          tbviaje.CalificadoUsuario, 
                          tbpersona.idZona, 
                          tbpersona.Tipo_de_usuario
                          ')
                        ->leftJoin('tbpersona ON tbpersona.id_tbPersona = tbviaje.idConductor')
                        ->where('idViaje', $id_viaje)
                        ->fetch();

      if ($viaje != false) {
        $tipo_usuario = $persona[0]->Tipo_de_usuario; #Tipo de usuario de quién calificado

        switch ($tipo_usuario) {
          case 2: #Usuario califica a driver
            if ($viaje->CalificadoUsuario != 1) {
              $name = $viaje->idFirebase;
              $zonaDriver = $viaje->idZona;
              $calificado = $viaje->idConductor;
              $Driver = $viaje->idConductor;

              #Actualiza el campo CalificadoUsuario
              $updateCalificado = $this->db->update($this->tbViaje)
                                           ->set('CalificadoUsuario', 1)
                                           ->Where('idViaje', $id_viaje)
                                           ->execute();

              $dataCalificacion = array(
                'idViaje' => $data['idViaje'],
                'Califica' => $data['idPersona'],
                'Calificado' => $calificado,
                'Calificacion' => $data['Calificacion'], 
                'Comentario' => $data['Comentario']
              );

              $insertCalificacion = $this->db->insertInto($this->tbCalificacion, $dataCalificacion)
                                             ->execute();

              #Obtener el promedio de la calificación de quien se está calificando
              $promedioCalificado = $this->db->from($this->tbCalificacion)
                                             ->select('AVG(Calificacion) AS PromedioCalificacion')
                                             ->where('Calificado', $calificado)
                                             ->fetchAll();

              $qualification = round(($promedioCalificado[0]->PromedioCalificacion), 2);

              $actualiza_promedio_conductor = CURL::updateCalification($Driver, $qualification, $zonaDriver);
            } else {
                     $this->response->errors = 'No puedes calificar el mismo viaje dos veces, si tienes algún problema comunícate a Soporte Lubo: lubo.com.mx para más detalles.';
              return $this->response->SetResponse(false);
            }
            break;

          case 3: #Conductor califica a usuario
            if ($viaje->CalificadoDriver != 1) {
              $name = $viaje->idFirebase;
              $zonaDriver = $viaje->idZona;
              $calificado = $viaje->idUsuario;
              $Driver = $viaje->idConductor;

              $updateCalificado = $this->db->update($this->tbViaje)
                                           ->set('CalificadoDriver', 1)
                                           ->Where('idViaje', $id_viaje)
                                           ->execute();

              $dataCalificacion = array(
                'idViaje' => $data['idViaje'],
                'Califica' => $data['idPersona'],
                'Calificado' => $calificado,
                'Calificacion' => $data['Calificacion'], 
                'Comentario' => $data['Comentario']
              );

              $insertCalificacion = $this->db->insertInto($this->tbCalificacion, $dataCalificacion)
                                             ->execute();

              $delete = CURL::deleteIds($Driver, $zonaDriver); #Libera al conductor del viaje en Firebase

              $driver = $this->db->update($this->tbPersona) #Libera al conductor del viaje en MySQL
                                 ->set('tbpersona.Status', 1)
                                 ->where('id_tbPersona', $Driver)
                                 ->execute();

              #Obtener el promedio de la calificación de quien se está calificando
              $promedioCalificado = $this->db->from($this->tbCalificacion)
                                             ->select('AVG(Calificacion) AS PromedioCalificacion')
                                             ->where('Calificado', $calificado)
                                             ->fetchAll();

              $qualification = round(($promedioCalificado[0]->PromedioCalificacion), 2);

              #Actualizar el contador de viajes
              $viajes = CURL::ObtenerViajesDriver($Driver, $zonaDriver);
              $totalActual = ($viajes->totalTravels) + 1; 
              $totalActualizado = CURL::updateTotalTravels($Driver, $totalActual, $zonaDriver);
            } else {
                     $this->response->errors = 'No puedes calificar el mismo viaje dos veces, si tienes algún problema comunícate a Soporte Lubo: lubo.com.mx para más detalles.';
              return $this->response->SetResponse(false);
            }
            break;
          
          default:
                   $this->response->errors = 'Tenemos problemas al registrar la calificación.';
            return $this->response->SetResponse(false);
            break;
        }

        $actualiza_calificacion = CURL::Calificado($name, $tipo_usuario); #Calificar el viaje en Firebase

        $informacion_viaje = CURL::getInfoTravel($name);

        if ($informacion_viaje != false) {

          if (($informacion_viaje->calificadoDriver == 1) && ($informacion_viaje->calificadoUsuario == 1) && ($informacion_viaje->pagado == 1)) {
            $viajeEliminado = CURL::deleteTravel($name);
          }
        }

               $this->response->result=['idCalificacion' => $insertCalificacion,
                                        'UsuarioCalificado' => $calificado,
                                        'PromedioCalificado' => $qualification,
                                        'miPromedio' => round($persona[0]->miPromedio, 2)];
        return $this->response->SetResponse(true);
      }else{
               $this->response->errors='Este viaje no existe';
        return $this->response->SetResponse(false);
      }
    }else{
             $this->response->errors='El usuario al que hace referencia no existe.';
      return $this->response->SetResponse(false);
    }
  }
}
?>