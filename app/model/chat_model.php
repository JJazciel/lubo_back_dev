<?php 
namespace App\Model;

use App\Lib\Response,
    App\Lib\Push;

class ChatModel{
	private $db;
    private $tmensaje = 'message';
    private $tpersona = 'tbpersona';
    private $ttoken = 'tbtoken';
    private $response;

    public function __CONSTRUCT($db){
        $this->db = $db;
        $this->response = new Response();
    }

    public function altaMensaje($data){
        //Array que se ingresa a la tabla de message
        $fecha = date('Y-m-d H:i:s');
    	$dataMensaje = array(
    						'Mensaje' => $data['Mensaje'],
    						'de_idPersona' => $data['idDe'],
    						'para_idPersona'=> $data['idPara'],
    						'Fecha' => $fecha);

    	$insertaMensaje = $this->db->insertInto($this->tmensaje,$dataMensaje)
    							   ->execute();

		if ($insertaMensaje > 0) {
			$token = $this->db->from($this->ttoken)
							  ->where('idPersona',$data['idPara'])
							  ->fetch();

			$json = array('Mensaje'=>$data['Mensaje'],'de'=>$data['idDe'],'para'=>$data['idPara'],'Fecha'=>$fecha);
			$json = json_encode($json);
			$data = array('Mensaje'=>$data['Mensaje'],'de'=>$data['idDe'],'para'=>$data['idPara'],'Fecha'=>$fecha);
			$mensaje = Push::FMC('Mensaje',$json,$token->Token,$data);
            $mensaje = json_decode($mensaje);
			
            if ($mensaje->failure == '1') {
                       $this->response->errors='El mensaje no se pudo enviar al destinario';
                return $this->response->SetResponse(false,'Se guardó en la base de datos');
            }else{
                       $this->response->result=$mensaje;
                return $this->response->SetResponse(true,'Se envió al destinario');
            }
		}
    }

    //$id => Id de la persona
    public function actualizarToken($id,$data){
    	$busca = $this->db->from($this->ttoken)
    					  ->where('idPersona',$id)
    					  ->fetch();

    	if ($busca > 0) {
    		$updateToken = $this->db->update($this->ttoken,$data)
    								->where('idPersona',$id)
    								->execute();

            if ($updateToken == 0) {
                       $this->response->result=$updateToken;
                return $this->response->setResponse(true,'El token ya está actualizado');
            }else{
                       $this->response->result=$updateToken;
                return $this->response->setResponse(true,'Se actualizó el token');
            }   
    	}else{
    		$newData = array(
                          'idPersona' => $id,
                          'Token' => $data['Token'],
    					  'Plataforma' => $data['Plataforma']
                         );

    		$altaToken = $this->db->insertInto($this->ttoken,$newData)
    							  ->execute();

    			   $this->response->result=$altaToken;
    		return $this->response->SetResponse(true,'Token agregado');
    	}
    }

    //Lista los mensajes de las dos personas idCliente y el idConductor
    public function historialMensajes($data){
        $lista = $this->db->from($this->tmensaje)
                          ->where('(de_idPersona = :de AND para_idPersona = :para)', array(':de' => $data['idDe'], ':para' => $data['idPara']))
                          ->fetchAll();

        if ($lista != false) {
                   $this->response->result=$lista;
            return $this->response->SetResponse(true);
        }else{
                   $this->response->errors='No hay mensajes disponibles';
            return $this->response->SetResponse(false);
        }
    }
}

 ?>