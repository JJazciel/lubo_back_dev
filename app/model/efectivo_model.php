<?php 
namespace App\Model;
    use App\Lib\Response,
    App\Lib\MontoCk,
    App\Lib\Cifrado;

    class EfectivoModel
    {
    	private $db;
    	private $tablePersona = 'tbpersona';
    	private $tableTransaccion = 'tbtransaccion';
    	private $response;
    	
    	public function __CONSTRUCT($db)
    	{
    		$this->db = $db;
      	    $this->response = new Response();
    	}
    public function registrar($data,$id)
    {
    	$monto = $data['Monto'];
        $idServicio = $data['idViaje'];
        $transaccion=$data['idTransaccionConekta'];
        $status = $data['Status'];
        $buscarUser = $this->db->from($this->tablePersona)
                    ->where('id_tbPersona',$id)
                    ->fetch();
        if ($buscarUser != false) {
            //return $idCustomer;
            $montoMascomision = MontoCk::calcular($monto);
            /*$montoConekta = $montoMascomision * 100;
            $montoMsjConekta = $montoMascomision/100;*/
             $array = array('idTarjeta' => $id,
                             'IdViaje' => $idServicio,
                             'Monto'      => $montoMascomision,
                             'idTransaccionConekta' => $transaccion,
                             'Status'     => $status
                            );
              #$idOrden = $order->id;
              #$status = $order->payment_status
                $insertarTran = $this->db->insertInto($this->tableTransaccion, $array)
                                      ->execute();
                $this->response->result=$insertarTran;
                return $this->response->SetResponse(true);
            }else{
            $this->response->errors = ['Lo sentimos no se pudo realizar el pago'];
            return $this->response->SetResponse(false);
        }
    }
}     
?>