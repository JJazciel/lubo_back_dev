<?php
    namespace App\Model;

    use App\Lib\Response,
    App\Lib\Cifrado;

class DocumentosModel
{
    private $db;
    private $table = 'tbdocumentos';
    private $table1 = 'tbpersona';
    private $tbtipo = 'tbtipo_documento';
    private $response;

    public function __CONSTRUCT($db)
    {
        $this->db = $db;
        $this->response = new Response();
    }

    public function listar($l,$p)
    {
        $data = $this->db->from($this->table)
                         ->limit($l)
                         ->offset($p)
                         ->orderBy('id_tbDocumentos DESC')
                         ->fetchAll();//para mas de un registro

        $total = $this->db->from($this->table)
                          ->select('COUNT(*) Total')
                          ->fetch()
                          ->Total;

        return [
            'data'  => $data,
            'total' => $total
        ];
    }
    public function listartipo($l,$p)
    {
        $data = $this->db->from($this->tbtipo)
                         ->limit($l)
                         ->offset($p)
                         ->orderBy('id_tipo_documento DESC')
                         ->fetchAll();//para mas de un registro

        $total = $this->db->from($this->tbtipo)
                          ->select('COUNT(*) Total')
                          ->fetch()
                          ->Total;

        return [
            'data'  => $data,
            'total' => $total
        ];
    }

    public function obtener($id)
    {
      $obtener = $this->db->from($this->table)
                     ->where('id_tbDocumentos', $id)
                    ->fetch();
                    if ($obtener !=false) {
                        $this->response->result=$obtener;
                 return $this->response->SetResponse(true);
                        # code...
                    }else{
                        $this->response->errors='este id no existe';
                        return $this->response->SetResponse(false);
                    }

    }

    public function registrarD($data)
    {
        $id_tbPersonas=$data['id_tbPersona'];
        $buscar = $this->db->from($this->table1)
                                   ->select('COUNT(*) Total')
                                   ->where('id_tbPersona',$id_tbPersonas)
                                   ->fetch()
                                   ->Total;
        if ($buscar <= 0) {
            $this->response->errors='este usuario no existe';
            return $this->response->SetResponse(false);
        }
        else{
            $insertDocumentos=$this->db->insertInto($this->table, $data)
                   ->execute();
                   $this->response->result=$insertDocumentos;
             return $this->response->SetResponse(true);
        }
    } 

    public function actualizar($data,$id)
    {
      $actualizar = $this->db->update($this->table, $data)
                     ->where('id_tbDocumentos',$id)
                     ->execute();
                $this->response->result = $actualizar;
        return $this->response->SetResponse(true);
    }

    public function eliminar($id)
    {
      $eliminar = $this->db->deleteFrom($this->table)
                 ->where('id_tbDocumentos',$id)
                 ->execute();
                 $this->response->result = $eliminar;

        return $this->response->SetResponse(true);
    }
}
?>