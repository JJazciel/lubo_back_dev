<?php
 namespace App\Model;

use App\Lib\Response,
    App\Lib\Cifrado;

class ConductoresModel{
    private $db;
    private $tbPersona = 'tbpersona';
    private $tbCalificacion = 'tbcalificacion';
    private $response;

    public function __CONSTRUCT($db){
        $this->db = $db;
        $this->response = new Response();
    }

    public function listar(){
        $data = $this->db->from($this->tbPersona)
                         ->select(null)
                         ->select("`id_tbPersona`,`Tipo_de_usuario`,`Nombre`,`Apellidos`,`Email`,`Password`,`Telefono`,`Imagen`,`Sexo`,`Fecha_Registro`,tbpersona.Status,tbregistro.Status as Registro")
                         ->where('Tipo_de_usuario = 3 AND tbpersona.Status != 2')
                         ->leftJoin("tbregistro ON tbregistro.IdPersona = tbpersona.id_tbPersona")
                         ->fetchAll();
        return[
            "data" => $data
        ];
    }
    public function listarxzona($idZona){
        $data = $this->db->from($this->tbPersona)
                         ->select(null)
                         ->select("`id_tbPersona`,`Tipo_de_usuario`,`Nombre`,`Apellidos`,`Email`,`Password`,`Telefono`,`Imagen`,`Sexo`,`Fecha_Registro`,tbpersona.Status,tbregistro.Status as Registro")
                         ->where('Tipo_de_usuario = 3 AND tbpersona.Status != 2')
                         ->where('idZona',$idZona)
                         ->leftJoin("tbregistro ON tbregistro.IdPersona = tbpersona.id_tbPersona")
                         ->fetchAll();
        return[
            "data" => $data
        ];
    }
    #Informacion de los conductores
    public function infoDriver($idDriver){
        $driver = $this->db->from($this->tbPersona)
                           ->where('id_tbPersona', $idDriver)
                           ->where('Tipo_de_usuario',3)
                           ->fetch();

        if ($driver != false) {
            $consulta = $this->db->from($this->tbPersona)
                                 ->select('IFNULL(AVG(Calificacion),0) AS Promedio, tbvehiculo.Modelo')
                                 ->leftJoin('tbcalificacion ON tbcalificacion.idPersona = tbpersona.id_tbPersona')
                                 ->leftJoin('tbvehiculo ON tbvehiculo.idPersona = tbpersona.id_tbPersona')
                                 ->where('tbpersona.id_tbPersona', $idDriver)
                                 ->fetchAll();

            $prom = floatval($consulta[0]->Promedio);
            $modelo = $consulta[0]->Modelo;

                   $this->response->result=['Promedio' => $prom,
                                            'Modelo' => $modelo];
            return $this->response->SetResponse(true);
        }else{
                   $this->response->errors='Este usuario no es un conductor';
            return $this->response->SetResponse(false);
        }
    }
}
