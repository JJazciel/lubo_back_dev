<?php
    namespace App\Model;

    use App\Lib\Response,
    App\Lib\Cifrado;

class RegistroModel
{
    private $db;
    private $table = 'tbregistro';
    // private $table2 = 'tbpersona';
    private $response;

    public function __CONSTRUCT($db)
    {
        $this->db = $db;
        $this->response = new Response();
    }

    public function listar()
    {
        $data = $this->db->from($this->table)
                        //  ->limit($l)
                        //  ->offset($p)
                         ->orderBy('idRegistro DESC')
                         ->fetchAll();//para mas de un registro

        $total = $this->db->from($this->table)
                          ->select('COUNT(*) Total')
                          ->fetch()
                          ->Total;

        return [
            'data'  => $data,
            'total' => $total
        ];
    }

    public function obtener($id)
    { 

      $obtener = $this->db->from($this->table)
                     ->where('IdPersona', $id)
                    ->fetch();
                    if ($obtener !=false) {
                        $this->response->result=$obtener;
                 return $this->response->SetResponse(true);
                        # code...
                    }else{
                        $this->response->errors='este id no existe';
                        return $this->response->SetResponse(false);
                    }
            

    }
    public function registrar($data)
    {
      $insertSR = $this->db->insertInto($this->table,$data)
                           ->execute();
      $this->response->result = $insertSR;
      return $this->response->SetResponse(true);
    } 

    public function actualizar($data,$id)
    {
       $actualizar = $this->db->update($this->table, $data)
                     ->where('IdPersona',$id)
                     ->execute();
                $this->response->result = $actualizar;
        return $this->response->SetResponse(true);
    }

    public function eliminar($id)
    {
        $eliminar = $this->db->deleteFrom($this->table)
                 ->where('IdPersona',$id)
                 ->execute();
                 $this->response->result = $eliminar;

        return $this->response->SetResponse(true);
    }
}
?>