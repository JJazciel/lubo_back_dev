<?php
namespace App\Model;

use App\Lib\Response,
    App\Lib\Push,
    App\Lib\Cifrado;

Class TokenServicesModel{
    private $db;
    private $tbPersona = 'tbpersona';
    private $devicetoken = 'tbtoken';
    private $response;

    public function __CONSTRUCT($db){
        $this->db = $db;
        $this->response = new Response();
    }
    
    public function sendPush($id, $mensaje){
    	$buscar = $this->db->from($this->devicetoken)
    					   ->select(NULL)
    					   ->select('tbtoken.Token')
    					   ->where('idPersona',$id)
    					   ->fetch();
    
    	if ($buscar != false) {
            $to = $buscar->Token;
            $data = array('data' => 0);
            $titulo = "Lubo";
            $push = Push::FMC($titulo,$mensaje,$to,$data);
    
                   $this->response->result=$push;
            return $this->response->SetResponse(true);
        }else{
       		       $this->response->errors='Este usuario no existe';
            return $this->response->SetResponse(false);
        }

    }
}
?>