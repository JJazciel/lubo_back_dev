<?php
namespace App\Model;

use App\Lib\Response;

class ContactosModel{
  private $db;
  private $tbContactos = 'tbcontactos';
  private $tbPersona = 'tbpersona';
  private $response;

  public function __CONSTRUCT($db){
    $this->db = $db;
    $this->response = new Response();
  }

  public function listar($id){
    $listaContactos = $this->db->from($this->tbContactos)
               				         ->where('idPersona', $id)
                               ->fetchAll();

    if ($listaContactos != false) {
      $totalContactos = count($listaContactos);

             $this->response->result = [
              'data'  => $listaContactos,
              'total' => $totalContactos
             ];
      return $this->response->SetResponse(true);
    } else {
      return $this->response->SetResponse(false, 'No tienes contactos registrados.');
    }
  }

  public function obtener($id){
    $contacto = $this->db->from($this->tbContactos)
                         ->where('id', $id)
                         ->fetch();

    if ($contacto != false) {
             $this->response->result=$contacto;
      return $this->response->SetResponse(true);
    }else{
             $this->response->errors='Este contacto no existe.';
      return $this->response->SetResponse(false);
    }
  }

  public function registrar($data){ 
    $idUsuario = $data['idPersona'];
    $telefono = $data['telefono'];	
    $codPais = $data['codigoPais'];

    $contactosRegistrados = $this->db->from($this->tbContactos)  
                  					         ->select('COUNT(*) as  Total')
                  					         ->where('idPersona', $idUsuario)
                  					         ->fetch()
                  					         ->Total;

    if ($contactosRegistrados < '3' ){
      $buscarContacto = $this->db->from($this->tbContactos)
                                 ->where('idPersona', $idUsuario)
                                 ->where('telefono', $telefono)
                                 ->fetch();

      if ($buscarContacto != false)  {
               $this->response->errors='Ya has registrado este número, intenta de con otro.';
        return $this->response->SetResponse(false);
      }else{
        $registrarContacto = $this->db->insertInto($this->tbContactos, $data)
                                      ->execute();
        
        if ($registrarContacto != 0) {
                 $this->response->result=$registrarContacto;
          return $this->response->SetResponse(true, 'Contacto agregado.');
        } else {
          return $this->response->SetResponse(false, 'No se pudo agregar el contacto, reintente.');
        }
      }
    }else{
      		   $this->response->errors='Has alcanzado el límite de contactos registrados.';
      return $this->response->SetResponse(false);
  	}
  }

  public function actualizar($data, $id){
    $actualizarContacto = $this->db->update($this->tbContactos, $data)
                                   ->where('id', $id)
                                   ->execute();

    if ($actualizarContacto !=false) {
             $this->response->result=$actualizarContacto;
      return $this->response->SetResponse(true);
    }else{
             $this->response->errors='No se pudo actualizar el contacto, reintente.';
      return $this->response->SetResponse(false);
    }
  }

  public function eliminar($id){
    $contactoEliminado = $this->db->delete($this->tbContactos)
                                  ->where('id', $id)
                                  ->execute();

    if ($contactoEliminado != false) {
             $this->response->result=$contactoEliminado;
      return $this->response->SetResponse(true);
    } else {
      return $this->response->SetResponse(false, 'No se ha podido eliminar, reintente.');
    }
  }

  public function defecto($id, $idPersona){
    $limpiarDefecto = $this->db->update($this->tbContactos)
                               ->set('Defecto', 0)
                               ->where('idPersona', $idPersona)
                               ->execute();

    $actualizarDefecto = $this->db->update($this->tbContactos)
                                  ->set('Defecto', 1)
                                  ->where('id',$id)
                                  ->execute();

    if ($actualizarDefecto != false) {
             $this->response->result=$actualizarDefecto;
      return $this->response->SetResponse(true);
    } else {
      return $this->response->SetResponse(false, 'No se pudo actualizar tu contacto por defecto, reintente');
    }
  }
}
?>