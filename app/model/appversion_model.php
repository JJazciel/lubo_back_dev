<?php
namespace App\Model;

use App\Lib\Response;

class AppVersionModel
{
    private $db;
    private $tbversion = 'appversion';
    private $response;

    public function __CONSTRUCT($db)
    {
        $this->db = $db;
        $this->response = new Response();
    }

    public function listar($l, $p)
    {
        $data = $this->db->from($this->tbversion)
                         ->limit($l)
                         ->offset($p)
                         ->orderBy('id DESC')
                         ->fetchAll();//para mas de un registro

        $total = $this->db->from($this->tbversion)
                          ->select('COUNT(*) Total')
                          ->fetch()
                          ->Total;

        return [
            'data'  => $data,
            'total' => $total
        ];
    }

    public function obtener($id)
    {
      $obtener=$this->db->from($this->tbversion,$id)
                        ->where('id', $id)
                        ->fetch();//para un solo dato o linea

      if ($obtener !=false) {
               $this->response->result=$obtener;
        return $this->response->SetResponse(true);
        }else{
              $this->response->errors='este id no existe';
       return $this->response->SetResponse(false);
          }

    }

    public function registrar($data)
    {                
      $registrar = $this->db->insertInto($this->tbversion,$data)
                             ->execute();
                    

               $this->response->result = $registrar;
        return $this->response->SetResponse(true);
    }

    public function actualizar($data,$id)
    {
       $actualizar = $this->db->update($this->tbversion, $data, $id)
                              ->where('id',$id)
                              ->execute();

       if ($actualizar !=false) {
             $this->response->result=$actualizar;
      return $this->response->SetResponse(true);
     }else{
               $this->response->errors='Nose pudo actualizar la version de la app intente mas tarde';
        return $this->response->SetResponse(false);
         }
    }

    public function eliminar($id)
    {
        $eliminar=$this->db->deleteFrom($this->tbversion,$id)
                           ->where('id',$id)
                           ->execute();

               $this->response->result = $eliminar;
        return $this->response->SetResponse(true);
    }
}
?>