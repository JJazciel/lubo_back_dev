<?php
  	namespace App\Model;

    use App\Lib\Response,
    App\Lib\Cifrado;
    
Class HistorialViajesModel{
	private $db;
    private $table = 'tbviaje';
    private $table2 = 'tbpersona';
    private $table3 = 'tbvehiculo';
    private $table4 = 'tbtransaccion';
    private $response;

    public function __CONSTRUCT($db){
    	$this->db = $db;
        $this->response = new Response();
    }
     public function listarViaje($l,$p,$id)
    {
        $data = $this->db->from($this->table)
                         ->select('tbviaje.id_tbViaje,tbviaje.Fecha,tbviaje.Hora,tbvehiculo.Modelo,tbvehiculo.Placa,tbtransaccion.Monto,tbviaje.idOrigen,tbviaje.idDestino,CONCAT(dirOrg.Latitud,",",dirOrg.Longitud) AS ORIGEN ,CONCAT(dirDes.Latitud,",",dirDes.Longitud) AS DESTINO')
                         ->leftJoin('tbvehiculo ON tbvehiculo.id_tbVehiculo = tbviaje.id_tbVehiculo')
                         ->leftJoin('tbtransaccion ON tbtransaccion.idViaje = tbviaje.id_tbViaje')
                         ->leftJoin('tbdireccion as dirOrg ON dirOrg.id_tbDireccion = tbviaje.idOrigen')
                         ->leftJoin('tbdireccion as dirDes ON dirDes.id_tbDireccion = tbviaje.idDestino')
                         ->where('tbviaje.idUsuarios',$id)
                         ->where('tbviaje.Status',3)
                         ->limit($l)
                         ->offset($p)
                         ->orderBy('id_tbViaje DESC')
                         ->fetchAll();//para mas de un registro

        $total = $this->db->from($this->table)
                          ->select('COUNT(*) Total')
                          ->leftJoin('tbvehiculo ON tbvehiculo.id_tbVehiculo = tbviaje.id_tbVehiculo')
                         ->leftJoin('tbtransaccion ON tbtransaccion.idViaje = tbviaje.id_tbViaje')
                         ->where('tbviaje.idUsuarios',$id)
                         ->where('tbviaje.Status',3)
                          ->fetch()
                          ->Total;

        return [
            'data'  => $data,
            'total' => $total
        ];
    }
}
?>