<?php
  	namespace App\Model;

    use App\Lib\Response,
    App\Lib\Cifrado;
    
Class DetalleTarifaModel{
	private $db;
    private $table = 'tbtarifa';
    private $table2 = 'tbviaje';
    private $response;

    public function __CONSTRUCT($db){
    	$this->db = $db;
        $this->response = new Response();
    }
     public function listarTarifa($l,$p,$id)
    {
        $data = $this->db->from($this->table)
        				 ->select('tbviaje.id_tbViaje')
                         ->leftJoin('tbviaje ON tbtarifa.id_lugar = tbviaje.idViaje')
        				 ->where('tbtarifa.id_Lugar',$id)
                         ->limit($l)
                         ->offset($p)
                         ->orderBy('id_tbTarifa DESC')
                         ->fetchAll();//para mas de un registro
             $total = $this->db->from($this->table)
                          ->select('COUNT(*) Total')
                          ->fetch()
                          ->Total;

    return [
            'data'  => $data,
            'total' => $total
    ];            
        if ($data !=false) {
                    $this->response->result=$data;
            return $this->response->SetResponse(true);
                        # code...
        }else{
               $this->response->errors='este id no existe';
       return $this->response->SetResponse(false);
        }

        
    }
}
?>