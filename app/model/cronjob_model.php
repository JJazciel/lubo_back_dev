<?php
namespace App\Model;

use App\Lib\Response,
	App\Lib\CURL;

class CronJob{
	private $db;

	public function __CONSTRUCT($db){
        $this->db = $db;
        $this->response = new Response();
    }

	public function restartEarnigs(){
		$informacion_drivers = $this->db->from('tbpersona')
										->select(null)
										->select('id_tbPersona, idZona')
										->where('Tipo_de_usuario = 3 AND RegistroFirebase = 1')
										->fetchAll();

		foreach ($informacion_drivers as $key => $value) {
			CURL::reiniciarGanancias($value->id_tbPersona, $value->idZona);
		}
	}
}
?>