<?php
namespace App\Model;
use App\Lib\Response,
    App\Lib\Push;

Class ActualizacionModel
{
	private $db;
  private $tbPersona = 'tbpersona';
  private $tbpersonaUpdate = 'tbvalidarinfo';
	private $response;
	private $tbToken = 'tbtoken';

	public function __CONSTRUCT($db){
    $this->db = $db;
    $this->response = new Response();
  }

  public function verinfo($idPersona){
    $obtener = $this->db->from($this->tbpersonaUpdate)
                        ->where('idUsuario', $idPersona)
                        ->where('status = 0')
                        ->limit(1)
                        ->orderBy('id DESC')
                        ->fetch();

   	if ($obtener != false) {
             $this->response->result=$obtener;
      return $this->response->SetResponse(true);
   	}else{
             $this->response->errors='este usuario aun no tiene datos para actualizar';
      return $this->response->SetResponse(false);
    }
  }

  public function actualizar($data) {
     $id = $data['idUsuario'];

      $token = $this->db->from($this->tbToken)
                        ->select('Token')
                        ->where('idPersona',$id)
                        ->fetch()
                        ->Token;

      $insertarUser = $this->db->insertInto($this->tbpersonaUpdate,$data)
                               ->execute();

    if ($insertarUser != false) {
    $dataPush = array('lubo' => 'push');
    $push = Push::FMC('Lubo','Tus datos han sido enviados para su aprobación, esta acción puede tomar entre 24 o 48 hrs en días hábiles',$token,$dataPush);
    
    			  $this->response->result = $insertarUser;
     return $this->response->SetResponse(true, 'Tus datos han sido enviados para su aprobación, esta acción puede tomar entre 24 o 48 hrs en días hábiles');
    }else{
               $this->response->result ->errors = 'Lo sentimos!!! No ha sido posible actualizar los datos ingresados';
        return $this->response->SetResponse(false);
          }

  }

  public function datosCorrectos($id,$validacion){
    switch ($validacion) {
    	case 1:

      $token = $this->db->from($this->tbToken)
                        ->select('Token')
                        ->where('idPersona',$id)
                        ->fetch()
                        ->Token;

      $actualizar = $this->db->from($this->tbpersonaUpdate)
                             ->where('idUsuario',$id)
                             ->where('status = 0')
                             ->limit(1)
                             ->orderBy('id DESC')
                             ->fetch();

       

	     if ($actualizar != false) {

           $dataPush = array('lubo' => 'push');
           $push = Push::FMC('Lubo','Felicidades!!! tus datos han sido validados correctamente por favor vuelve a iniciar secion en Lubo',$token,$dataPush);

       		$data = [];
          if ($actualizar->Nombre != null || $actualizar->Nombre != "" || $actualizar->Nombre.length != 0){
            $data[]=["Nombre",$actualizar->Nombre];
          }else{
                 $this->response->errors='el campo Nombre viene vacio';
          return $this->response->SetResponse(false);
          }
          if ($actualizar->Apellidos != null || $actualizar->Apellidos != "" || $actualizar->Apellidos.length != 0) {
             $data[]=["Apellidos",$actualizar->Apellidos];
          }else{
                 $this->response->errors='El campo Apellidos viene vacio';
          return $this->response->SetResponse(false);
          } 
          if ($actualizar->Sexo != null || $actualizar->Sexo != "" || $actualizar->Sexo.length != 0) {
             $data[]=["Sexo",$actualizar->Sexo];
          }

        //}
        $data = array('Nombre' =>$actualizar->Nombre, 'Apellidos'=>$actualizar->Apellidos);
        $UPDATETBPERSONA = $this->db->update($this->tbPersona,$data)
 		                                ->where('id_tbPersona',$id)
                                    ->execute();

        $updateSTATUS = $this->db->update($this->tbpersonaUpdate)
                                 ->set('status', 0)
                                 ->where('idUsuario',$id)
                                 ->where('status = 1')
                                 ->execute();

        $updateSTATUS = $this->db->update($this->tbpersonaUpdate)
	            				           ->set('status', 1)
	                               ->where('idUsuario',$id)
                                 ->where('status = 0')
                                 ->limit(1)
                                 ->orderBy('id DESC')
                                 ->execute();

 	      	     $this->response->result = $data;
   		  return $this->response->SetResponse(true,'Tus datos han sido aprobados para actualizar');
          //}
	    }else{
               $this->response->errors='no se ha podido realizar el cambio';
        return $this->response->SetResponse(false);
      }
    break;

    case 2:
     	$rechazado = array('status' => $validacion);

      $token = $this->db->from($this->tbToken)
                        ->select('Token')
                        ->where('idPersona',$id)
                        ->fetch()
                        ->Token;

     	$actualizar = $this->db->from($this->tbpersonaUpdate,$data)
                           	 ->where('idUsuario',$id)
                             ->where('status = 0')
                             ->limit(1)
                             ->orderBy('id DESC')
       						  		     ->fetch();

     	if ($actualizar != false) {

        $dataPush = array('lubo' => 'push');
        $push = Push::FMC('Lubo','!Lo sentimos!!! No ha sido posible actualizar los datos ingresados ya que no cumplen con los estandares para Lubo',$token,$dataPush);

    	  $data = array('status' =>'2'); 		  
 		    $insertar=$this->db->update($this->tbpersonaUpdate,$data)
	   		                   ->where('idUsuario',$id)
                           ->where('status = 0')
                           ->limit(1)
                           ->orderBy('id DESC')
	                         ->execute();

	    	       $this->response->result = $data;
	      return $this->response->SetResponse(true,'Lo sentimos!!! No ha sido posible actualizar los datos ingresados');
	    }else{
	             $this->response->errors='no se ha podido realizar el cambio';
	      return $this->response->SetResponse(false);
	    }         
    break;
    }  
  }      
}
?>