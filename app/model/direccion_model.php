<?php
    namespace App\Model;

    use App\Lib\Response,
    App\Lib\Cifrado;

class DireccionModel
{
    private $db;
    private $tbDireccion = 'tbdireccion';
    private $tbPersona = 'tbpersona';
    private $response;

    public function __CONSTRUCT($db){
      $this->db = $db;
      $this->response = new Response();
    }

    //Direccion de casa
    public function defecto($idDireccion, $idPersona){
      $buscar = $this->db->from($this->tbDireccion)
                         ->where('id_tbDireccion',$idDireccion)
                         ->where('id_Persona',$idPersona)
                         ->fetch();

      if ($buscar->Defecto == 2) {
               $this->response->errors='Esta dirección ya está asignada como trabajo';
        return $this->response->SetResponse(false);
      }else{
        $limpiar = $this->db->update($this->tbDireccion)
                            ->set('Defecto',0)
                            ->where('id_Persona',$idPersona)
                            ->where('Defecto != 2')
                            ->execute();

        $actualizar = $this->db->update($this->tbDireccion)
                               ->set('Defecto',1)
                               ->where('id_tbDireccion',$idDireccion)
                               ->execute();

               $this->response->result=$actualizar;
        return $this->response->SetResponse(true);
      }
    }

    //Direccion del trabajo
    public function dirTrabajo($idDireccion, $idPersona){
      $buscar = $this->db->from($this->tbDireccion)
                         ->where('id_tbDireccion',$idDireccion)
                         ->where('id_Persona',$idPersona)
                         ->fetch();

      if ($buscar->Defecto == 1) {
               $this->response->errors='Esta dirección ya está asignada como casa';
        return $this->response->SetResponse(false);
      }else{
        $limpiar = $this->db->update($this->tbDireccion)
                          ->set('Defecto',0)
                          ->where('id_Persona',$idPersona)
                          ->where('Defecto != 1')
                          ->execute();

        $actualizar = $this->db->update($this->tbDireccion)
                               ->set('Defecto',2)
                               ->where('id_tbDireccion',$idDireccion)
                               ->execute();

               $this->response->result=$actualizar;
        return $this->response->SetResponse(true);
      }
    }

    public function listar($idPersona){
      $data = $this->db->from($this->tbDireccion)
                        ->where('id_Persona',$idPersona)
                        ->where('Guardado',1)
                        ->orderBy('id_tbDireccion DESC')
                        ->fetchAll();//para mas de un registro

      if ($data != false) {
        $total = $this->db->from($this->tbDireccion)
                          ->select('COUNT(*) Total')
                          ->where('id_Persona',$idPersona)
                          ->where('Guardado',1)
                          ->fetch()
                          ->Total;

                $this->response->result = [
                                            'data'  => $data,
                                            'total' => $total ];
          return $this->response->SetResponse(true);
        } else {
                 $this->response->errors = "No hay direcciones guardadas para este usuario";
          return $this->response->SetResponse(false);
        }
    }

    public function obtener($id){
      $obtener = $this->db->from($this->tbDireccion)
                          ->where('id_tbDireccion', $id)
                          ->fetch();

      if ($obtener != false) {
               $this->response->result=$obtener;
        return $this->response->SetResponse(true);
      }else{
               $this->response->errors='El id no existe';
        return $this->response->SetResponse(false);
      }
    }

    public function registrarD($data){
      $idPersona = $data['id_Persona'];
      $buscar = $this->db->from($this->tbPersona)
                         ->select('COUNT(*) Total')
                         ->where('id_tbPersona',$idPersona)
                         ->fetch()
                         ->Total;

      if ($buscar <= 0) {
               $this->response->errors = 'El id del usuario no está registrado';
        return $this->response->SetResponse(false);
      }else{
        $insertaDireccion = $this->db->insertInto($this->tbDireccion, $data)
                                    ->execute();

               $this->response->result=$insertaDireccion;
        return $this->response->SetResponse(true,'La direccion se ha guardado correctamente');
      }
    }

    public function actualizar($idDireccion, $idPersona, $data){
      $buscarDireccion = $this->db->from($this->tbDireccion)
                                  ->where('id_tbDireccion',$idDireccion)
                                  ->select('COUNT(*) Num')
                                  ->fetch()
                                  ->Num;

      if ($buscarDireccion > 0) {
        $buscarPersona = $this->db->from($this->tbPersona)
                                  ->where('id_tbPersona',$idPersona)
                                  ->select('COUNT(*) Num')
                                  ->fetch()
                                  ->Num;
        if ($buscarPersona > 0) {
          $actualizar = $this->db->update($this->tbDireccion,$data)
                                 ->where('id_tbDireccion',$idDireccion)
                                 ->where('id_Persona', $idPersona)
                                 ->execute();

                 $this->response->result=$actualizar;
          return $this->response->SetResponse(true);
        }else{
                 $this->response->errors = 'Este usuario no existe';
          return $this->response->SetResponse(false);
        }
      }else{
               $this->response->errors = 'Este viaje no existe';
        return $this->response->SetResponse(false);
      }
    }

    public function eliminar($id){
        $eliminar = $this->db->deleteFrom($this->tbDireccion)
                             ->where('id_tbDireccion',$id)
                             ->execute();
                             
               $this->response->result = $eliminar;
        return $this->response->SetResponse(true);
    }
}
?>