<?php
 namespace App\Model;

    use App\Lib\Response,
    App\Lib\Cifrado;
    class CodigoPromoModel
{
    private $db;
    private $table = 'tbcodigopromo';
    private $table1 = 'tbpersona';
    private $response;

    public function __CONSTRUCT($db)
    {
        $this->db = $db;
        $this->response = new Response();
    }

    public function listar($l,$p)
    {
        $data = $this->db->from($this->table)
                         ->limit($l)
                         ->offset($p)
                         ->orderBy('id_tbCodigoPromo DESC')
                         ->fetchAll();//para mas de un registro

        $total = $this->db->from($this->table)
                          ->select('COUNT(*) Total')
                          ->fetch()
                          ->Total;

        return [
            'data'  => $data,
            'total' => $total
        ];
    }

    public function obtener($id)
    {
     $obtener = $this->db->from($this->table)
                     ->where('id_tbCodigoPromo', $id)
                    ->fetch();
                    if ($obtener !=false) {
                        $this->response->result=$obtener;
                 return $this->response->SetResponse(true);
                        # code...
                    }else{
                        $this->response->errors='este id no existe';
                        return $this->response->SetResponse(false);
                    }
    }

    public function registrarCod($data)
    {
        $id_tbPersona=$data['id_tbPersona'];
        $buscar = $this->db->from($this->table1)
                                   ->select('COUNT(*) Total')
                                   ->where('id_tbPersona',$id_tbPersona)
                                   ->fetch()
                                   ->Total;
        if ($buscar <= 0) {
            $this->response->errors='este usuario no existe';
            return $this->response->SetResponse(false);
        }
    }
       
     public function actualizar($data,$id)
    {

       $actualizar = $this->db->update($this->table, $data)
                     ->where('id_tbCodigoPromo',$id)
                     ->execute();
                $this->response->result = $actualizar;
        return $this->response->SetResponse(true);
    }

    public function eliminar($id)
    {
       $eliminar = $this->db->deleteFrom($this->table)
                 ->where('id_tbCodigoPromo',$id)
                 ->execute();
                 $this->response->result = $eliminar;

        return $this->response->SetResponse(true);
    }
}
?>