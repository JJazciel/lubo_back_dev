<?php
 namespace App\Model;

use App\Lib\Response,
    App\Lib\Cifrado;

class AdministradoresModel{
    private $db;
    private $tbPersona = 'tbpersona';
    private $tbCalificacion = 'tbcalificacion';
    private $response;

    public function __CONSTRUCT($db){
        $this->db = $db;
        $this->response = new Response();
    }

    public function listar(){
        $data = $this->db->from($this->tbPersona)
                         ->select(null)
                         ->select("`id_tbPersona`,`Tipo_de_usuario`,`Nombre`,`Apellidos`,`Email`,`Password`,`Telefono`,`Imagen`,`Sexo`,`Fecha_Registro`,tbpersona.idZona, tbzona.Descripcion")
                         ->leftJoin('tbzona on tbpersona.idZona = tbzona.idZona')
                         ->where('Tipo_de_usuario = 1 AND tbpersona.Status != 2')
                        //  ->leftJoin("tbregistro ON tbregistro.IdPersona = tbpersona.id_tbPersona")
                         ->fetchAll();
        return[
            "data" => $data
        ];
    }
    #Informacion de los Administradores
    public function infoAdmin($idDriver){
        $driver = $this->db->from($this->tbPersona)
                           ->where('id_tbPersona', $idDriver)
                           ->where('Tipo_de_usuario',1)
                           ->fetch();

        if ($driver != false) {
            $consulta = $this->db->from($this->tbPersona)
                                 ->select('`id_tbPersona`,`Tipo_de_usuario`,`Nombre`,`Apellidos`,`Email`,`Password`,`Telefono`,`Imagen`,`Sexo`,`Fecha_Registro`,tbpersona.idZona')
                                //  ->leftJoin('tbcalificacion ON tbcalificacion.idPersona = tbpersona.id_tbPersona')
                                //  ->leftJoin('tbvehiculo ON tbvehiculo.idPersona = tbpersona.id_tbPersona')
                                 ->where('tbpersona.id_tbPersona', $idDriver)
                                 ->fetch();

                   $this->response->result=$consulta;
            return $this->response->SetResponse(true);
        }else{
                   $this->response->errors='Este usuario no es un Administrador';
            return $this->response->SetResponse(false);
        }
    }

    public function actualizar($id,$data)
    {
      if ($data['Password'] != '') {
        $data['Password'] = Cifrado::Sha512($data['Password']);
      }else{
          unset($data['Password']);
      }

        $update = $this->db->update($this->tbPersona, $data)
                           ->where('id_tbPersona',$id)
                           ->execute();
               $this->response->result = $update;
        return $this->response->SetResponse(true);
    }
}
