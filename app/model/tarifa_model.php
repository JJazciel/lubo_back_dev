<?php
namespace App\Model;

use App\Lib\Response,
   	App\Lib\Cifrado;

class TarifaModel{
	private $db;
    private $tbTarifa = 'tbtarifa';
    private $tbCiudad = 'tbciudad';
    private $tbPersona = 'tbpersona';
    private $response;
    
	public function __CONSTRUCT($db){
        $this->db = $db;
        $this->response = new Response();
    }

    public function obtener($id){
     	$obtener = $this->db->from($this->tbTarifa)
                     	 	->where('idTarifa', $id)
                    	 	->fetch();

    	if ($obtener !=false) {
               	$this->response->result=$obtener;
        	return $this->response->SetResponse(true);
    	}else{
               	$this->response->errors='este id no existe';
        	return $this->response->SetResponse(false); 
        }
    }

	public function GetDrivingDistance($data){
		$zona = $data['idZona'];
		$modos_trabajo = $this->db->from($this->tbPersona)
			->select(null)
			->select('tbvehiculo.idModoTrabajo')
			->leftJoin('tbvehiculo On tbvehiculo.idPersona = tbpersona.id_tbPersona')
			->where('tbpersona.idZona', $zona)
			->where('tbpersona.Status = 1 AND tbpersona.Tipo_de_usuario = 3')
			->groupBy('tbvehiculo.idModoTrabajo')
			->fetchAll();

		$total_modos_trabajo = count($modos_trabajo);
		if ($total_modos_trabajo > 0) {
			$taxi = 0;
			$estandar = 0;
			$deluxe = 0;

			switch ($total_modos_trabajo) {
				case 1: $taxi = $modos_trabajo[0]->idModoTrabajo; $estandar = $modos_trabajo[0]->idModoTrabajo; $deluxe = $modos_trabajo[0]->idModoTrabajo; break;
				
				case 2:$taxi = $modos_trabajo[0]->idModoTrabajo; $estandar = $modos_trabajo[1]->idModoTrabajo;$deluxe = $modos_trabajo[0]->idModoTrabajo; break;

				case 3: $taxi = $modos_trabajo[0]->idModoTrabajo; $estandar = $modos_trabajo[1]->idModoTrabajo; $deluxe = $modos_trabajo[2]->idModoTrabajo; break;
			}

			$ciudad = $data['idCiudad'];
			$tarifa = $this->db->from($this->tbTarifa)
				->select('
					tbmodotrabajo.Nombre AS TipoServicio,
					tbmodotrabajo.Descripcion AS DescripcionServicio,
					tbmodotrabajo.Imagen AS ImagenServicio
				')
				->leftJoin('tbmodotrabajo ON tbmodotrabajo.idModoTrabajo = tbtarifa.idModoTrabajo')
				->where('
					tbtarifa.idCiudad = :ciudad 
					AND ((tbtarifa.idModoTrabajo = :taxi) OR (tbtarifa.idModoTrabajo = :estandar) OR (tbtarifa.idModoTrabajo = :deluxe))',
					array(
						':ciudad' => $ciudad, 
						':taxi' => $taxi, 
						':estandar' => $estandar, 
						':deluxe' => $deluxe
					)
				)
				->fetchAll();

			if ($tarifa != false) {
				$url_consulta = 'https://maps.googleapis.com/maps/api/distancematrix/json?origins='.$data['LatitudOrigen'].','.$data['LongitudOrigen'].'&destinations='.$data['LatitudDestino'].','.$data['LongitudDestino'].'&key=AIzaSyDg4SngbNDrJz0nrpgbwurPnrcQEXwFf-4';

				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url_consulta);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
				$response = curl_exec($ch);
				curl_close($ch);
				$response_a = json_decode($response, true);
				$dist = $response_a['rows'][0]['elements'][0]['distance']['value'];
				$time = $response_a['rows'][0]['elements'][0]['duration']['text'];
				$newtime = explode(" ", $time);

				if (count($newtime) > 2) {
					$minutosPorHora = ((floatval($newtime[0])) * 60);
					$tiempoMinutos = ($minutosPorHora + floatval($newtime[2])); #En minutos
				} else {
					$tiempoMinutos = floatval($newtime[0]); #En minutos
				}

				$distanciaFinal = round(($dist/1000),1); #En Kilometros

				$output = array();
				foreach ($tarifa as $tf){
					$distanciaMinima = $tf->DistanciaMinima;#1.8
					$tarifaMinima = $tf->TarifaMinima;		#35

					$tipoTarifa = $tf->idTarifa;
					$tarifaBase = $tf->TarifaBase;          #7
					$costoKm = $tf->Costo_por_Km;           #3.57
					$costoMin = $tf->Costo_por_minuto;      #1.57
					$largaDistancia = $tf->LargaDistancia;  #0.8
					$cuotaPeticion = $tf->CuotaPeticion;    #0.16
					$tarifaNocturna = $tf->TarifaNocturna;  #30%
					$iva = $tf->IVA;                        #16
					$impuesto = $tf->Impuesto;              #0
					
					$imagen = $tf->ImagenServicio;
					$nombre = $tf->TipoServicio;
					$descripcion = $tf->DescripcionServicio;
					$saldoLargaDistancia = 0;
					$saldoTarifaNocturna = 0;

					$horaConsulta = date('H:i:s');
					$inicioTarifaNocturna = '22:00:00';
					$finTarifaNocturna = '06:00:00';

					if ($distanciaFinal <= $distanciaMinima) { #Distancia minima en 1.8
						$total = $tarifaMinima; #45
						$ivaBase = $total - ($total / (1 + $iva)); #6.21
						$subTotal = $total - $ivaBase; #38.8
						$parcial = $subTotal - ($subTotal / (1 + $cuotaPeticion)); #5.92
						$subParcial = $subTotal - $parcial; #32.88
						$saldoKm = (($subParcial - $tarifaBase) / 2); # 12.94
						$saldoMin = (($subParcial - $tarifaBase) / 2); #12.94

					}else{ #Mayor a la distancia mínima
					    $saldoKm = round($distanciaFinal * $costoKm, 2); #X Km
					    $saldoMin = round($tiempoMinutos * $costoMin, 2); #X Min

						$saldoBase = round($tarifaBase + $saldoKm + $saldoMin, 2);

						if ($distanciaFinal > 10) {
							$kmExtra = $distanciaFinal - 10;
							$saldoLargaDistancia = round($kmExtra * $largaDistancia, 2);
						}

						$base = $saldoBase + $saldoLargaDistancia;
						$saldoCuotaPeticion = $base * $cuotaPeticion;

						if (($horaConsulta > $inicioTarifaNocturna) || ($horaConsulta < $finTarifaNocturna)) {
							$saldoTarifaNocturna = round((($base + $saldoCuotaPeticion) * $tarifaNocturna), 2);
						}
						
						$subTotal = $base + $saldoCuotaPeticion + $saldoTarifaNocturna;
						$baseImpuesto = $subTotal * $impuesto;
						$ivaBase = $subTotal * $iva;
						$total = $subTotal + $baseImpuesto + $ivaBase;
					}
					
					if ($distanciaFinal <= $distanciaMinima) { #Falta verificar la distancia minima
						if ($tipoTarifa == 3) {
							$parcial = $parcial + 0.1;
						}

						$output[] = [
							'idTipo' => $tipoTarifa,
							'Nombre' => $nombre,
							'Descripcion' => $descripcion,
							'Distancia' => $distanciaFinal, 
							'Tiempo' => $tiempoMinutos,
							'Cotizacion' => 1, #round($total, 2),
							'Imagen' => $imagen,
					    	'Detalle' => [
					    		'TarifaBase' => floatval($tarifaBase),
					    		'xKm' => round($saldoKm, 1),
					    		'xMin' => round($saldoMin, 1),
					    		'LargaDistancia' => $saldoLargaDistancia,
								'CuotaPeticion' => round($parcial, 1),
								'TarifaNocturna' => 0,
								'IVA' => round($ivaBase, 1),
								'Impuesto' 
					    ]];
					}else{
						$output[] = [
							'idTipo' => $tipoTarifa,
							'Nombre' => $nombre,
							'Descripcion' => $descripcion,
							'Distancia' => $distanciaFinal,
							'Tiempo' => $tiempoMinutos,
							'Cotizacion' => round($total, 2),
							'Imagen' => $imagen,
					    	'Detalle' => [
					    		'TarifaBase' => floatval($tarifaBase),
					    		'xKm' => $saldoKm,
					    		'xMin' => $saldoMin,
					    		'LargaDistancia' => round($saldoLargaDistancia, 2),
								'CuotaPeticion' => round($saldoCuotaPeticion, 2),
								'TarifaNocturna' => $saldoTarifaNocturna,
								'IVA' => round($ivaBase, 2),
								'Impuesto' => round($baseImpuesto, 2)
					    ]];
					}
				}

					   $this->response->result = $output;
				return $this->response->SetResponse(true);
			} else {
					   $this->response->errors = 'No existen tarifas en esta ciudad.';
				return $this->response->SetResponse(false);
			}
		} else {
				   $this->response->errors = 'Lo sentimos, no hay conductores disponibles en este momento, espera unos minutos y vuelve a solicitar tu viaje.';
			return $this->response->SetResponse(false);
		}
	}

	public function NormasTarifa($data){
		$tipo = $data['idTipo'];
		$ciudad = $data['idCiudad'];

		$tarifa = $this->db->from($this->tbTarifa)
						   ->select('tbmodotrabajo.Nombre, tbmodotrabajo.Descripcion, tbmodotrabajo.Imagen')
						   ->leftJoin('tbmodotrabajo ON tbmodotrabajo.idModoTrabajo = tbtarifa.idTarifa')
						   ->where('tbtarifa.idModoTrabajo', $tipo)
						   ->where('tbtarifa.idCiudad', $ciudad)
		                   ->fetch();

		$normas = array(
						'id' => $tarifa->idTarifa,
				   		'Descripcion' => $tarifa->Descripcion,
				   		'NombreServicio' => $tarifa->Nombre,
				   		'Imagen' => $tarifa->Imagen,
				   		'TarifaMinima' => floatval($tarifa->TarifaMinima),
				   		'TarifaBase' => floatval($tarifa->TarifaBase),
				   		'Costo_por_minuto' => floatval($tarifa->Costo_por_minuto),
				   		'Costo_por_Km' => floatval($tarifa->Costo_por_Km),
				   		'Impuesto' => floatval($tarifa->Impuesto),
				   		'Cuota' => floatval($tarifa->CuotaPeticion)
				   		);

	   		   $this->response->result=$normas;
		return $this->response->SetResponse(true);
	}

	public function poligonoCiudad(){
		$ciudades = $this->db->from($this->tbCiudad)
							 ->fetchAll();

		$detalles = array();
		foreach ($ciudades as $key) {
			$detalles[] = $key->idCiudad;
			$detalles[] = $key->Descripcion;
			$detalles[] = json_decode($key->Poligono);
		}

			   $this->response->result=$ciudades;
		return $this->response->setResponse(true);
	}
}?>