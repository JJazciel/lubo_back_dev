<?php
namespace App\Model;

use App\Lib\Response;

class InfoBalanceModel{
  private $db;
  private $tbinfobalance = 'tbinfobalance';
  private $tbtransaccion = 'tbtransaccion';
  private $response;

  public function __CONSTRUCT($db){
    $this->db = $db;
    $this->response = new Response();
  }

  public function listar($idbalance){
    $data = $this->db->from($this->tbinfobalance)
                     ->where('idBalance',$idbalance)
                     ->fetchAll();
                       
    $total = $this->db->from($this->tbinfobalance)
                     ->select('COUNT(*) Total')
                     ->where('idBalance',$idbalance)
                     ->fetch()
                     ->Total;

           $this->response->result = [
             'data'  => $data,
             'total' => $total
           ];
    return $this->response->SetResponse(true);
  }

  public function obtener($id){
    $obtener=$this->db->from($this->tbinfobalance,$id)
                      ->where('id', $id)
                      ->fetch();//para un solo dato o linea

    if ($obtener !=false) {
             $this->response->result=$obtener;
      return $this->response->SetResponse(true);
    }else{
             $this->response->errors='este id no existe';
      return $this->response->SetResponse(false);
    }
  }

  //se reciben parametros de fecha de inicio y fin en un lapso de 7 dias laborados
  public function registrar($fechaInicio,$fechaFin,$id){  
    $idConductores=$data['idUsuario'];

    $obtener = $this->db->from($this->tbtransaccion)
                        ->select(null)
                        ->select('tbtransaccion.idViaje')
                        ->leftJoin('tbviaje ON tbtransaccion.idViaje = tbviaje.idViaje')
                        ->where('tbviaje.idConductor', $idConductores)
    
                        ->fetchAll();  
       /* $obtener = $this->db->from($this->tbtransaccion)
                                        ->select(null)
                                        ->select('MontoTotal*0.85 as MontoConductor, MontoTotal*0.15 AS MontoEmpresa, MontoTotal')
                                        ->where('Status = 2') #filtrar por conductor
                                        ->where('(tbtransaccion.idUsuario = :id) AND (tbtransaccion.FechaPago BETWEEN :i AND :f)', array(':id' => $id, ':i' => $fechaInicio, ':f' => $fechaFin))
                                        ->fetchAll();

        if ($obtener != false) {
          $montoDriver = 0;
          $montoEmpresa = 0;
          $montoTotal = 0;
          foreach ($obtener as $key => $value) {
            $montoDriver = $montoDriver + $value->MontoConductor;
            $montoEmpresa = $montoEmpresa + $value->MontoEmpresa;
            $montoTotal = $montoTotal + $value->MontoTotal;
          }

          $totalefectivo = $this->db->from($this->tbtransaccion)
                              ->select(null)
                              ->select('SUM(MontoTotal) as MontoEfectivo')
                              ->where('MetodoPago = 1')
                              ->where('(tbtransaccion.idUsuario = :id) AND (tbtransaccion.FechaAlta BETWEEN :i AND :f)', array(':id' => $id, ':i' => $fechaInicio, ':f' => $fechaFin))
                              ->fetchAll();                          

          $totaltarjeta = $this->db->from($this->tbtransaccion)
                              ->select(null)
                              ->select('IFNull(SUM(MontoTotal),0) as MontoTarjeta')
                              ->where('MetodoPago = 2')
                              ->where('(tbtransaccion.idUsuario = :id) AND (tbtransaccion.FechaAlta BETWEEN :i AND :f)', array(':id' => $id, ':i' => $fechaInicio, ':f' => $fechaFin))
                              ->fetchAll();

          $data_Balance = array(
            'idConductor' => $id,
            'gananciasEmpresa' => $montoEmpresa,
            'totalEfectivo' => $totalefectivo[0]->MontoEfectivo,
            'totalTarjeta' => $totaltarjeta[0]->MontoTarjeta,
            'balance' => ($totaltarjeta[0]->MontoTarjeta) - ($montoEmpresa),
            'total' => $montoTotal);

          $add_Balance = $this->db->insertInto($this->tbinfobalance, $data_Balance)
                                  ->execute();          

                 $this->response->result = $obtener;
          return $this->response->SetResponse(true);
        }else{
                 $this->response->errors='';
          return $this->response->SetResponse(false);
        }*/

           $this->response->result = $obtener;
    return $this->response->SetResponse(true);
  }

  public function actualizar($data,$id){
    $actualizar = $this->db->update($this->tbinfobalance, $data, $id)
                           ->where('id',$id)
                           ->execute();

    if ($actualizar !=false) {
             $this->response->result=$actualizar;
      return $this->response->SetResponse(true);
    }else{
             $this->response->errors='Nose pudo actualizar la informacion del balance intente mas tarde';
      return $this->response->SetResponse(false);
    }
  }

  public function eliminar($id){
    $eliminar=$this->db->deleteFrom($this->tbinfobalance,$id)
                       ->where('id',$id)
                       ->execute();

           $this->response->result = $eliminar;
    return $this->response->SetResponse(true);
  }
}
?>