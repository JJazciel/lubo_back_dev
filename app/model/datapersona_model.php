<?php
    namespace App\Model;
    use App\Lib\Response,
        App\Lib\Cifrado;

class DataPersonaModel
{
	private $db;
	private $tbViaje = 'tbviaje';
  private $tbpersona ='tbpersona';
	private $response;

	public function __CONSTRUCT($db){
    $this->db = $db;
    $this->response = new Response();
  }

  public function obtener($id){
    $tipo = $this->db->from($this->tbpersona)
                     ->select('
                      `id_tbPersona`,
                      `Tipo_de_usuario`
                      ')
                     ->where('id_tbPersona', $id)
                     ->fetch()
                     ->Tipo_de_usuario;

    if ($tipo != false) {
      switch ($tipo) {
        case 2: #Usuario normal
          $obtenerViaje = $this->db->from($this->tbViaje)
            ->select(null)
            ->select('
              idViaje, 
              IFNULL(idFirebase, "") AS idFirebase, 
              Status AS StatusViaje,
              Pagado,
              Cancelado,
              Excedido,
              CalificadoUsuario
              ')
            ->where('
              (Status >= 1 AND Status != 2 AND Status <= 7) 
              AND 
              ((Status <= 4 AND (Cancelado = 9 AND Pagado = 0 AND Excedido = 1)) OR (Status = 7  AND (CalificadoUsuario = 0 OR Pagado = 0)) OR (Status <= 7 AND (CalificadoUsuario = 0 AND Pagado = 0 AND Cancelado = 0)))
              
            ')
            ->where('(idUsuario = :idUser OR idConductor = :idDriver)', array(':idUser'=> $id, ':idDriver' => $id))
            ->limit('1')
            ->orderBy('idViaje DESC')
            ->fetch();

          if ($obtenerViaje != false) {
                   $this->response->result = [
                      'idViaje' => $obtenerViaje->idViaje,
                      'Name' => $obtenerViaje->idFirebase,
                      'StatusViaje' => $obtenerViaje->StatusViaje,
                      'CanceladoUsuario' => $obtenerViaje->Cancelado,
                      'Excedido' => $obtenerViaje->Excedido,
                      'CalificadoUsuario' => $obtenerViaje->CalificadoUsuario,
                      'Pagado' => $obtenerViaje->Pagado
                      ];
            return $this->response->SetResponse(true, 1);
          } else {
                   $this->response->errors = 'No hay viajes pendientes';
            return $this->response->SetResponse(false, 0);
          }

          break;

        case 3: #Driver
          $obtenerViaje = $this->db->from($this->tbViaje)
            ->select(null)
            ->select('
              idViaje, 
              IFNULL(idFirebase, "") AS idFirebase, 
              Status AS StatusViaje, 
              CalificadoDriver
              ')
            ->where('
              ((Status >= 1 AND Status != 2 AND Status <=7) AND 
              ((Status <= 7 AND (Cancelado = 0 AND CalificadoDriver = 0)))
              )
            ')
            ->where('(idUsuario = :idUser OR idConductor = :idDriver)', array(':idUser'=> $id, ':idDriver' => $id))
            ->limit('1')
            ->orderBy('idViaje DESC')
            ->fetch();

          if ($obtenerViaje != false) {
                   $this->response->result = [
                      'idViaje' => $obtenerViaje->idViaje,
                      'Name' => $obtenerViaje->idFirebase,
                      'StatusViaje' => $obtenerViaje->StatusViaje,
                      'CalificadoDriver' => $obtenerViaje->CalificadoDriver
                    ];
            return $this->response->SetResponse(true, 1);
          } else {
                   $this->response->errors = 'No hay viajes pendientes';
            return $this->response->SetResponse(false, 0);
          }

          break;
      }
    } else {
             $this->response->errors = 'No es un usuario válido.';
      return $this->response->SetResponse(false);
    }
  }

}
?>