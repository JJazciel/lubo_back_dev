<?php

namespace App\Model;

require_once __DIR__ . '/../lib/conekta-php-master/lib/Conekta.php';
// \Conekta\Conekta::setApiKey("key_yawNuPVcAsFs2bfJsCpVCQ");#key_yawNuPVcAsFs2bfJsCpVCQ nuevo - key_hiyJP38jUQyqLAC15xobvw anterior #dev
\Conekta\Conekta::setApiKey("key_bFgzHopChrHtt4u4rf7uqg");#prod
\Conekta\Conekta::setApiVersion("2.0.0");

use App\Lib\Response,
    App\Lib\MontoCk,
    App\Lib\Cifrado,
    App\Lib\Pago,
    App\lib\CURL;
use Conekta\Customer;

class TarjetaModel{
    private $db;
    private $table = 'tbconekta';
    private $tbtarjetas = 'tbtarjetas';
    private $tablaPerfilInpuestos = 'tbperfilImpuestos';
    private $tablePersona = 'tbpersona';
    private $tableTransaccion = 'tbtransaccion';
    private $tbViaje = 'tbviaje';
    private $response;

    public function __CONSTRUCT($db){
        $this->db = $db;
        $this->response = new Response();
    }

    #listar tarjetas del cliente
    public function listar($idCliente){
        $buscarUser = $this->db->from($this->table)
                               ->where('idPersona',$idCliente)
                               ->fetch();

        if ($buscarUser != false) {
            $idCustomer = $buscarUser->idClienteConekta;
            $customer = \Conekta\Customer::find($idCustomer);
            //echo $customer->payment_sources;//informacion del metodo de pago

            $max = count($customer->payment_sources);
            if ($max > 0) {
                $result = json_decode($customer->payment_sources);

                       $this->response->result = $result;
                return $this->response->SetResponse(true);
            }else{
                       $this->response->errors = 'No hay metodos de pago';
                return $this->response->SetResponse(false);
            }
        }else{
                   $this->response->errors = 'No hay tarjetas para este cliente';
            return $this->response->SetResponse(false);
        }
    }

    #obtener informacion de tarjeta
    public function obtener($idCliente,$idTarjeta){
        $buscarUser = $this->db->from($this->table)
                               ->where('',$idCliente)
                               ->fetch();

        if ($buscarUser != false) {
            $idCustomer = $buscarUser->idClienteConekta;
            $customer = \Conekta\Customer::find($idCustomer);
            $count = count($customer->payment_sources);
            for ($i=0; $i < $count; $i++) { 
                if ($idTarjeta === $customer->payment_sources[$i]->id) {
                    $tar = $customer->payment_sources[$i];
                }
            }

            if ($tar != null) {
                       $this->response->result = $tar;
                return $this->response->SetResponse(true);
            }else{
                       $this->response->errors='Tarjeta no encontrada';
                return $this->response->SetResponse(false); 
            }    
        }else{
                $this->response->errors='No hay tarjetas para este cliente';
                return $this->response->SetResponse(false);
        }
    }

    #registro de tarjetas y de usuario
    public function registrar($tokenCard,$id){
        $buscarUser =  $this->db->from($this->tablePersona)
                                ->where('id_tbPersona',$id)
                                ->fetch();

        if ($buscarUser != false) {
            $buscarTar=$this->db->from($this->table)
                                ->where('idPersona',$id)
                                ->fetch();

            if ($buscarTar != false) {
                #agregar tarjeta al usuario
                // return "#agregar tarjeta al usuario";
                $idCustomer = $buscarTar->idClienteConekta;
                //return $idCustomer;
                $customer = \Conekta\Customer::find($idCustomer);

                // max de 5 tarjetas
                if (count($customer->payment_sources) <= 4 ) {
                    # code...
                    $source = $customer->createPaymentSource(array(
                        "type" => "card",
                        "token_id" => $tokenCard
                    ));
                    
                    //verificar existencia de tarjeta
                    #variables de validacion
                    $last4 = $source->last4;
                    $bin = $source->bin;
                    $exp_month  = $source->exp_month;
                    $exp_year = $source->exp_year;
                    $brand = $source->brand;
                    $max = count($customer->payment_sources);
                    $i = 0;
                    $dupli =0 ;
                    while ( $i< $max) {
                        $sourceActu = $customer->payment_sources[$i];
                        #
                        $last4Actu = $sourceActu->last4;
                        $binActu = $sourceActu->bin;
                        $exp_monthActu  = $sourceActu->exp_month;
                        $exp_yearActu = $sourceActu->exp_year;
                        $brandActu = $sourceActu->brand;
                        #
                        if(($last4 == $last4Actu )AND($bin == $binActu)AND($brand == $brandActu)){
                            $dupli = $dupli + 1;
                        }
                        $i++;
                    }
                    
                    if ($dupli >= 2) {
                        $source->delete();
                        $this->response->errors='Esta tarjeta ya se encuentra registrada como método de pago.';
                        return $this->response->SetResponse(false); 
                    }else{
                        $dataAlta = [
                            "idPersona"=>$id,
                            "idClienteConekta"=>$source->parent_id,
                            "idTarjetaConekta"=>$source->id,
                            "last4"=>$source->last4,
                            "brand"=>$source->brand
                        ];
                        $altaTarjeta = $this->db->insertInto($this->tbtarjetas,$dataAlta)
                                            ->execute();
                        $this->response->result=$source;
                        return $this->response->SetResponse(true,"Metodo de pago dado de alta");
                    }
                }else{
                    $this->response->errors='Supera el nuemro de tarjetas agregadas, maximos 5.';
                        return $this->response->SetResponse(false); 
                }
            }else{
                $Nombre = $buscarUser->Nombre.' '.$buscarUser->Apellidos;
                $Email = $buscarUser->Email;
                $Telefono = $buscarUser->Telefono;
                //return $Nombre.' '.$Email.' '.$Telefono;
                try {
                    $customer = \Conekta\Customer::create(
                        array(
                            "name" => $Nombre,
                            "email" => $Email,
                            "phone" => $Telefono,
                            "payment_sources" => array(
                                array(
                                    "type" => "card",
                                    "token_id" => $tokenCard
                                )
                            )//payment_sources
                        )
                               //customer
                    );
                    //echo $customer->id." id customer";
                    $idCustomer = $customer->id; //id del cliente
                    $customer = \Conekta\Customer::find($idCustomer);
                    $source = $customer->payment_sources[0];
                    //$idCard = $source->id;
                    $data = ['idPersona'    => $id, 'IdClienteConekta' =>$idCustomer];
                    
                    if ($idCustomer!="") {
                        $dataAlta = [
                            "idPersona"=>$id,
                            "idClienteConekta"=>$source->parent_id,
                            "idTarjetaConekta"=>$source->id,
                            "last4"=>$source->last4,
                            "brand"=>$source->brand
                        ];
                        $altaTarjeta = $this->db->insertInto($this->tbtarjetas,$dataAlta)
                                            ->execute();
                        $altaCustomer = $this->db->insertInto($this->table, $data)
                                                 ->execute();
                               $this->response->result=['idCustomer'=>$altaCustomer, 'paymentSources'=>$source];
                        return $this->response->SetResponse(true,"Metodo de pago dado de alta");
                    }else{
                               $this->response->errors='Error al encontrar a cliente';
                        return $this->response->SetResponse(false);
                    }
                } catch (\Conekta\ProccessingError $error){
                      //echo $error->getMesage();
                           $this->response->errors=$error->getMessage();
                    return $this->response->SetResponse(false);
                } catch (\Conekta\ParameterValidationError $error){
                      // echo $error->getMessage();
                           $this->response->errors=$error->getMessage();
                    return $this->response->SetResponse(false);
                } catch (\Conekta\Handler $error){
                      // echo $error->getMessage();
                           $this->response->errors=$error->getMessage();
                    return $this->response->SetResponse(false);
                }
            }
        }else{
                   $this->response->errors='El Cliente no se encuentra';
            return $this->response->SetResponse(false); 
        }
    }

    #actualizar tarjeta de cliente   
    public function actualizar($data,$id,$idTarjeta){
        $nombre = $data['Nombre'];
        $mes = $data['mes'];
        $year = $data['year'];

        $buscarUser = $this->db->from($this->table)
                               ->where('idPersona',$id)
                               ->fetch();

        if ($buscarUser != false) {
            $idCustomer = $buscarUser->idClienteConekta;
            $customer = \Conekta\Customer::find($idCustomer);
            $count = count($customer->payment_sources);
            for ($i=0; $i < $count; $i++) { 
                if ($idTarjeta === $customer->payment_sources[$i]->id) {
                    $customer->payment_sources[$i]->update(array(
                        'name'=>$nombre,
                        'exp_month'=>$mes,
                        'exp_year'=>$year
                    ));
                    $tar = $customer->payment_sources[$i];
                }
            }
        
            if ($tar != null) {
                       $this->response->result = $tar;
                return $this->response->SetResponse(true);
            }else{
                       $this->response->errors='Tarjeta no encontrada';
                return $this->response->SetResponse(false); 
            }
        }else{
                   $this->response->errors='No hay tarjetas para este cliente';
            return $this->response->SetResponse(false);
        }
    }

    #eliminar tarjeta de cliente
    public function eliminar($id,$idTarjeta){
        $buscarUser = $this->db->from($this->table)
                               ->where('idPersona',$id)
                               ->fetch();

        if ($buscarUser != false) {
            $idCustomer = $buscarUser->idClienteConekta;
            $customer = \Conekta\Customer::find($idCustomer);
            $count = count($customer->payment_sources);
            for ($i=0; $i < $count; $i++) { 
                if ($idTarjeta === $customer->payment_sources[$i]->id) {
                    $customer->payment_sources[$i]->delete();
                    $tar = $customer->payment_sources[$i];
                }
            }
    
            if ($tar != null) {
                
                $this->db->update($this->tbtarjetas)
                            ->set('status',0)
                            ->where('idPersona',$id)
                            ->where('idTarjetaConekta',$idTarjeta)
                            ->execute();
                       
                //buscar perfil
                $idPerfilImpuestos = $this->db->from($this->tablaPerfilInpuestos)
                                              ->select('idtbperfilImpuestos')
                                              ->where('idPersona',$id)
                                              ->where('idTarjeta',$idTarjeta)
                                              ->fetch()
                                              ->idtbperfilImpuestos;
                // si exite se elimina 
                if ($idPerfilImpuestos != false) {
                    $this->db->update($this->tablaPerfilInpuestos)
                            ->set('status',0)
                            ->where('idtbperfilImpuestos',$idPerfilImpuestos)
                            ->execute();
                }
            //
                       $this->response->result = $tar;
                return $this->response->SetResponse(true);
            }else{
                       $this->response->errors='Tarjeta no encontrada';
                return $this->response->SetResponse(false); 
            }
        }else{
                   $this->response->errors='No hay tarjetas para este cliente';
            return $this->response->SetResponse(false);
        }
    }

    #metodo de pago por default
    public function defaultPaymentSource($id,$idTarjeta){
        $buscarUser = $this->db->from($this->table)
                               ->where('idPersona',$id)
                               ->fetch();

        if ($buscarUser != false) {
            $idCustomer = $buscarUser->idClienteConekta;
            $customer = \Conekta\Customer::find($idCustomer);
            $count = count($customer->payment_sources);
            for ($i=0; $i < $count; $i++) { 
            if ($idTarjeta === $customer->payment_sources[$i]->id) {
                #$customer->payment_sources[$i]->delete();
                $tar = $customer->payment_sources[$i];
                $customer->update(array(default_payment_source_id => $tar->id));
                $lugar = $i;
                }
            }
        
            if ($tar != null) {
                       $this->response->result = $customer->payment_sources[$lugar];
                return $this->response->SetResponse(true);
            }else{
                       $this->response->errors='Tarjeta no encontrada';
                return $this->response->SetResponse(false); 
            }
        }else{
                   $this->response->errors='No hay tarjetas para este cliente';
            return $this->response->SetResponse(false);
        }
    }
}
?>