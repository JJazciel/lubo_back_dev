<?php
namespace App\Model;

use App\Lib\Response,
    App\Lib\EnviarTicked;

class BalanceModel{
  private $db;
  private $tbbalance = 'tbbalance';
  private $tbPersona = 'tbpersona';
  private $tbViaje = 'tbviaje';
  private $tbTransaccion = 'tbtransaccion';
  private $tbzona ='tbzona';
  private $tbSemana = 'tbsemana';
  private $tbInformacionbalance = 'tbinformacionbalance';
  private $tbBalancetransaccion = 'balance_transaccion';
  private $tbMetodoPago = 'tbmetodo_de_pago';
  private $response;

  public function __CONSTRUCT($db){
    $this->db = $db;
    $this->response = new Response();
  }

  public function listar($l, $p) {
    $data = $this->db->from($this->tbbalance)
                      ->limit($l)
                      ->offset($p)
                      ->orderBy('id DESC')
                      ->fetchAll();//para mas de un registro

    $total = $this->db->from($this->tbbalance)
                      ->select('COUNT(*) Total')
                      ->fetch()
                      ->Total;

    return [
      'data'  => $data,
      'total' => $total
    ];
  }

  public function obtener($id) {
    $obtener=$this->db->from($this->tbbalance,$id)
                      ->where('id', $id)
                      ->fetch();//para un solo dato o linea

    if ($obtener !=false) {
             $this->response->result=$obtener;
      return $this->response->SetResponse(true);
    }else{
             $this->response->errors='este id no existe';
      return $this->response->SetResponse(false);
    }
  }

  public function registrar($data) { 
      /* $obtener = $this->db->from($this->tbzona)
                                       ->select(null)
                                       ->select('idZona')
                                       ->fetchAll(); 
        if ($obtener != false) {
          foreach ($obtener as $o) {
            $dataidzona = array('idZona'=>$o->idZona);
            //hacer mergue
            $data = array_merge($data, $dataidzona);
            $registrar = $this->db->insertInto($this->tbbalance, $data)
                                  ->execute();         
          }

          $fechaInicio =$data['inicio'];
          $fechaFin = $data['fin'];
         
         $obtenertransaccion = $this->db->from($this->tbtransaccion)
                                        ->select(null)
                                        ->select('MontoTotal*0.85 as MontoConductor, MontoTotal*0.15 AS MontoEmpresa, MontoTotal')
                                        ->where('Status = 2') #filtrar por conductor
                                        ->where('(tbtransaccion.idUsuario = :id) AND (tbtransaccion.FechaPago BETWEEN :i AND :f)', array(':id' => $id, ':i' => $fechaInicio, ':f' => $fechaFin))
                                        ->fetchAll();

        if ($obtenertransaccion != false) {

          $montoDriver = 0;
          $montoEmpresa = 0;
          $montoTotal = 0;
          foreach ($obtenertransaccion as $key => $value) {
            $montoDriver = $montoDriver + $value->MontoConductor;
            $montoEmpresa = $montoEmpresa + $value->MontoEmpresa;
            $montoTotal = $montoTotal + $value->MontoTotal;
          }

          $totalefectivo = $this->db->from($this->tbtransaccion)
                              ->select(null)
                              ->select('SUM(MontoTotal) as MontoEfectivo')
                              ->where('MetodoPago = 1')
                              ->where('(tbtransaccion.idUsuario = :id) AND (tbtransaccion.FechaAlta BETWEEN :i AND :f)', array(':id' => $id, ':i' => $fechaInicio, ':f' => $fechaFin))
                              ->fetchAll();                          

          $totaltarjeta = $this->db->from($this->tbtransaccion)
                              ->select(null)
                              ->select('IFNull(SUM(MontoTotal),0) as MontoTarjeta')
                              ->where('MetodoPago = 2')
                              ->where('(tbtransaccion.idUsuario = :id) AND (tbtransaccion.FechaAlta BETWEEN :i AND :f)', array(':id' => $id, ':i' => $fechaInicio, ':f' => $fechaFin))
                              ->fetchAll();

          $data_Balance = array(
            'idBalance ' =>$data->id
            'idConductor' => $id,
            'gananciasEmpresa' => $montoEmpresa,
            'totalEfectivo' => $totalefectivo[0]->MontoEfectivo,
            'totalTarjeta' => $totaltarjeta[0]->MontoTarjeta,
            'balance' => ($totaltarjeta[0]->MontoTarjeta) - ($montoEmpresa),
            'total' => $montoTotal);

          $add_Balance = $this->db->insertInto($this->tbinfobalance, $data_Balance)
                                  ->execute(); 

                        $this->response->result = $data;
                 return $this->response->SetResponse(true);
        }else{
               $this->response->errors='El balance no se registro intente mas tarde';
        return $this->response->SetResponse(false);
      }
      /* $registrar = $this->db->insertInto($this->tbbalance, $data)
                             ->execute();*/
  }

  public function actualizar($data,$id){
    $actualizar = $this->db->update($this->tbbalance, $data, $id)
                          ->where('id',$id)
                          ->execute();

    if ($actualizar !=false) {
             $this->response->result=$actualizar;
      return $this->response->SetResponse(true);
    }else{
             $this->response->errors='El balance no se actualizo intente mas tarde';
      return $this->response->SetResponse(false);
    }
  }

  public function eliminar($id){
    $eliminar=$this->db->deleteFrom($this->tbbalance,$id)
                       ->where('id',$id)
                       ->execute();

           $this->response->result = $eliminar;
    return $this->response->SetResponse(true);
  }

  #Lista las semanas para realizar los balances --> web
  public function listaSemanas(){
    $semanas = $this->db->from($this->tbSemana)
                        ->select(null)
                        ->select('idSemana, InicioSemana, FinSemana, Semana')
                        ->fetchAll();

    if (count($semanas > 0)) {
             $this->response->result = $semanas;
      return $this->response->SetResponse(true);
    } else {
             $this->response->errors = 'No se ha podido obtener la lista de semanas disponibles.';
      return $this->response->SetResponse(false);
    }
  }

  #Recopila información para poder visualizar el estado de los balances en web
  public function InformacionBalance($idSemana, $limite, $paginacion){
    $semana_activa = $this->db->from($this->tbSemana)
      ->select(null)
      ->select('idSemana, InicioSemana, FinSemana')
      ->where('idSemana', $idSemana)
      ->fetch();

    if ($semana_activa != false) {
      $inicioSemana = $semana_activa->InicioSemana;
      $finSemana = $semana_activa->FinSemana;

      $informacion_driver = $this->db->from($this->tbPersona)
        ->select(null)
        ->select('
          id_tbPersona AS idPersona, 
          CONCAT(tbpersona.Nombre, " ", Apellidos) AS NombreConductor, 
          idZona,
          IFNULL(tbmodotrabajo.Nombre, "Sin detalle") AS TipoServicio')
        ->leftJoin('tbvehiculo ON tbvehiculo.idPersona = tbpersona.id_tbPersona')
        ->leftJoin('tbmodotrabajo ON tbmodotrabajo.idModoTrabajo = tbvehiculo.idModoTrabajo')
        ->where('Tipo_de_usuario = 3 AND Status != 3')
        ->limit($limite)
        ->offset($paginacion)
        ->fetchAll();

      $total_conductores = $this->db->from($this->tbPersona)
        ->select('COUNT(*) AS Total')
        ->where('Tipo_de_usuario = 3 AND Status != 3')
        ->fetch()
        ->Total;

      if ($informacion_driver != false) {
        $efectivoTotal = 0;
        $efectivoivaTotal = 0;
        $tarjetaTotal = 0;
        $tarjetaivaTotal = 0;
        $conductorTotal = 0;
        $luboTotal = 0;
        foreach ($informacion_driver as $key => $value) {
          $id_conductor = $value->idPersona;

          $semana_pagada = $this->db->from('tbinformacionbalance')
            ->select(null)
            ->select('idBalance, StatusBalance')
            ->where('idSemana = :semana AND idConductor = :conductor',
              array(':semana' => $idSemana, ':conductor' => $id_conductor))
            ->fetch();

          if ($semana_pagada != false) {
            $status_balance = $semana_pagada->StatusBalance;

            if ($status_balance != 1) {
              $pagado = 'No pagado';
            } else {
              $pagado = 'Pagado';
            }
          } else {
            $pagado = 'No pagado';
          }

          $total_efectivo = $this->db->from($this->tbTransaccion)
            ->select(null)
            ->select('IFNULL(SUM(Round(tbtransaccion.Monto, 2)), 0) AS Total_dia')
            ->leftJoin('tbviaje ON tbviaje.idViaje = tbtransaccion.idViaje')
            ->where('
              tbviaje.idConductor = :idDriver AND tbtransaccion.FechaAlta BETWEEN :inicio AND :fin', 
              array(':idDriver' => $id_conductor, ':inicio' => $inicioSemana, ':fin' => $finSemana))
            ->where('tbtransaccion.Status = 2 AND tbtransaccion.MetodoPago = 1 AND tbviaje.Pagado = 1')
            ->fetchAll();

          $total_tarjeta = $this->db->from($this->tbTransaccion)
            ->select(null)
            ->select('
              IFNULL(SUM(Round(tbtransaccion.Monto, 2)), 0) AS Total_dia,
              IFNULL(SUM(Round(tbtransaccion.Propina, 2)), 0) AS Total_propina_dia
              ')
            ->leftJoin('tbviaje ON tbviaje.idViaje = tbtransaccion.idViaje')
            ->where('
              tbviaje.idConductor = :idDriver AND tbtransaccion.FechaAlta BETWEEN :inicio AND :fin', 
              array(':idDriver' => $id_conductor, ':inicio' => $inicioSemana, ':fin' => $finSemana))
            ->where('tbtransaccion.Status = 2 AND tbtransaccion.MetodoPago = 2 AND tbviaje.Pagado = 1')
            ->fetchAll();

          $total_viajes = $this->db->from($this->tbTransaccion)
            ->select('COUNT(*) AS Total')
            ->leftJoin('tbviaje ON tbviaje.idViaje = tbtransaccion.idViaje')
            ->where('
              tbviaje.idConductor = :idDriver AND tbtransaccion.FechaAlta BETWEEN :inicio AND :fin', 
              array(':idDriver' => $id_conductor, ':inicio' => $inicioSemana, ':fin' => $finSemana))
            ->where('tbtransaccion.Status = 2 AND tbviaje.Pagado = 1')
            ->fetch()
            ->Total;

          $ganancias_efectivo_globales = floatval($total_efectivo[0]->Total_dia);
          $ganancias_tarjeta_globales = floatval($total_tarjeta[0]->Total_dia);
          $ganancias_propina_globales = floatval($total_tarjeta[0]->Total_propina_dia);

          #Conductor
          $ganancias_efectivo_conductor = Round(($ganancias_efectivo_globales * 0.85), 2);
          $ganancias_tarjeta_conductor = Round(($ganancias_tarjeta_globales * 0.85), 2);
          $porcentaje_conductor = $ganancias_efectivo_conductor + $ganancias_tarjeta_conductor;

          #Lubo
          $ganancias_efectivo_lubo = $ganancias_efectivo_globales * 0.15;
          $ganancias_tarjeta_lubo = $ganancias_tarjeta_globales * 0.15;
          $porcentaje_lubo = Round(($ganancias_efectivo_lubo + $ganancias_tarjeta_lubo), 2);

          $iva_efectivo = Round($ganancias_efectivo_globales - ($ganancias_efectivo_globales / 1.16), 2);
          $iva_tarjeta = Round($ganancias_tarjeta_globales - ($ganancias_tarjeta_globales / 1.16), 2);

          if ($ganancias_tarjeta_globales > $ganancias_efectivo_globales) {
            #Se le tiene que dar al conductor
            $balance = Round((($porcentaje_lubo - $ganancias_tarjeta_globales)), 2);
          } else {
            #Se le tiene que dar a lubo
            $balance = Round((($ganancias_efectivo_globales - $porcentaje_conductor)), 2);
          }

          $efectivoTotal = $efectivoTotal + Round(($ganancias_efectivo_globales - $iva_efectivo));
          $efectivoivaTotal = $efectivoivaTotal + $iva_efectivo;
          $tarjetaTotal = $tarjetaTotal + Round(($ganancias_tarjeta_globales - $iva_tarjeta));
          $tarjetaivaTotal = $tarjetaivaTotal + round($iva_tarjeta,2);
          $conductorTotal = $conductorTotal + $porcentaje_conductor;
          $luboTotal = $luboTotal + $porcentaje_lubo;

          $informacion_conductor[] = [
            'idConductor' => floatval($value->idPersona),
            'Nombre' => $value->NombreConductor,
            'TipoServicio' => $value->TipoServicio,
            'ZonaConductor' => floatval($value->idZona),
            'TotalViajesConductor' => floatval($total_viajes),
            'GananciasEfectivo' => Round(($ganancias_efectivo_globales - $iva_efectivo), 2),
            'IVAEfectivo' => floatval($iva_efectivo),
            #'TOTALEFECTIVO' => floatval($ganancias_efectivo_globales),
            'GananciasTarjeta' => Round(($ganancias_tarjeta_globales - $iva_tarjeta), 2),
            'IVATarjeta' => floatval($iva_tarjeta),
            #'TOTALTARJETA' => floatval($ganancias_tarjeta_globales),
            'TotalPropinas' => floatval($ganancias_propina_globales),
            'Conductor+IVA' => $porcentaje_conductor,
            'ComisionLubo+IVA' => $porcentaje_lubo,
            'Balance' => $balance,
            'StatusBalance' => $pagado,
          ];
        }
               $this->response->result = [
                'BalanceConductores' => $informacion_conductor,
                'Totales' => [
                  'TotalEfectivo' =>$efectivoTotal,
                  'TotalEfectivoIva' => $efectivoivaTotal,
                  'TotalTarjeta' => $tarjetaTotal,
                  'TotalTrajetaIva' => round($tarjetaivaTotal,2),
                  'ConductorTotal' => $conductorTotal,
                  'LuboTotal' => round($luboTotal,2)
                ],
                'TotalConductores' => floatval($total_conductores)];
        return $this->response->SetResponse(true);
      } else {
               $this->response->errors = 'No hay conductores registrados o disponibles en este momento.';
        return $this->response->SetResponse(false);
      }
    } else {
             $this->response->errors = 'Aún no se crea la información de esta semana, sé paciente.';
      return $this->response->SetResponse(false);
    }
  }

  public function corteSemanaConductor($idConductor, $idSemana){
    $semana_activa = $this->db->from('tbsemana')
      ->select(null)
      ->select('InicioSemana, FinSemana')
      ->where('idSemana', $idSemana)
      ->fetch();

    if ($semana_activa != false) {
      $fechaPeticion = substr($semana_activa->InicioSemana, 0, 10);

      $peticion = strtotime($fechaPeticion); #fecha de peticion para calcular dias de la semana
      $numeroMes = substr($fechaPeticion, -5, 2); #Obtener el número del mes

      switch ($numeroMes){
        case 1: $mes = "Enero"; break;
        case 2: $mes = "Febrero"; break;
        case 3: $mes = "Marzo"; break;
        case 4: $mes = "Abril"; break;
        case 5: $mes = "Mayo"; break;
        case 6: $mes = "Junio"; break;
        case 7: $mes = "Julio"; break;
        case 8: $mes = "Agosto"; break;
        case 9: $mes = "Septiembre"; break;
        case 10: $mes = "Octubre"; break;
        case 11: $mes = "Noviembre"; break;
        case 12: $mes = "Diciembre"; break;
      }
      
      $dias_semana = array(); #dias de la semana empezando con domingo

      if (date('w', $peticion) == '1') { #Lunes
        for ($i=0; $i < 7; $i++) {
          $dias_semana[] = date("Y-m-d", strtotime("$fechaPeticion, + $i days"));
        }
      }else{
        $inicio = strtotime('sunday this week -1 week', $peticion);
        $inicioSemana = date('Y-m-d', $inicio); #fecha de inicio de esa semana
        for ($i=1; $i < 8; $i++) {
          $dias_semana[] = date("Y-m-d", strtotime("$inicioSemana + $i day"));
        }
      }

      for ($i=0; $i < 7; $i++) {
        $total_efectivo = $this->db->from($this->tbTransaccion)
          ->select(null)
          ->select('IFNULL(SUM(Round(tbtransaccion.MontoTotal, 2)), 0) AS Total_dia')
          ->leftJoin('tbviaje ON tbviaje.idViaje = tbtransaccion.idViaje')
          ->where('tbviaje.idConductor', $idConductor)
          ->where('tbtransaccion.FechaAlta LIKE ?','%'.$dias_semana[$i].'%')
          ->where('tbtransaccion.Status = 2 AND tbviaje.Pagado = 1 AND tbtransaccion.MetodoPago = 1')
          ->fetchAll();

        $total_tarjeta = $this->db->from($this->tbTransaccion)
          ->select(null)
          ->select('
            IFNULL(SUM(Round(tbtransaccion.Monto, 2)), 0) AS Total_dia,
            IFNULL(SUM(Round(tbtransaccion.Propina, 2)), 0) AS Total_propina_dia
            ')
          ->leftJoin('tbviaje ON tbviaje.idViaje = tbtransaccion.idViaje')
          ->where('tbviaje.idConductor', $idConductor)
          ->where('tbtransaccion.FechaAlta LIKE ?','%'.$dias_semana[$i].'%')
          ->where('tbtransaccion.Status = 2 AND tbviaje.Pagado = 1 AND tbtransaccion.MetodoPago = 2')
          ->fetchAll();

        $total_viajes = $this->db->from($this->tbTransaccion)
          ->select('COUNT(*) AS Total')
          ->leftJoin('tbviaje ON tbviaje.idViaje = tbtransaccion.idViaje')
          ->where('tbviaje.idConductor', $idConductor)
          ->where('tbtransaccion.FechaAlta LIKE ?','%'.$dias_semana[$i].'%')
          ->where('tbtransaccion.Status = 2 AND tbviaje.Pagado = 1')
          ->fetch()
          ->Total;

        $ganancias_efectivo = $total_efectivo[0]->Total_dia;
        $ganancias_tarjeta = $total_tarjeta[0]->Total_dia;
        $ganancias_propina = $total_tarjeta[0]->Total_propina_dia;

        $comision_lubo = Round((($ganancias_efectivo + $ganancias_tarjeta) * 0.15), 2);
        $ganancias_conductor = Round((($ganancias_efectivo + $ganancias_tarjeta) * 0.85) , 2);
        #Conductor
        $iva_ganancias_conductor = Round(($ganancias_conductor - ($ganancias_conductor / 1.16)), 2);
        $ganancias_conductor_sin_iva = Round(($ganancias_conductor - $iva_ganancias_conductor), 2);
        #lubo
        $iva_comision_lubo = Round(($comision_lubo - ($comision_lubo / 1.16)), 2);
        $comision_lubo_sin_iva = Round($comision_lubo - $iva_comision_lubo,2);

        $total_dia = Round(($comision_lubo + $ganancias_conductor), 2);

        switch ($i) {
          case 0:
            $semana[] = [
              'Dia' => $mes.' '.substr($dias_semana[$i], -2),
              'Viajes' => floatval($total_viajes),
              'Propina' => floatval($ganancias_propina),
              'Monto' => $ganancias_conductor_sin_iva,
              'IvaMonto' => $iva_ganancias_conductor,
              'Lubo' => $comision_lubo_sin_iva,
              'IvaLubo' => $iva_comision_lubo,
              'Total' => $total_dia
            ];
            break;
          
          case 1:
            $semana[] = [
              'Dia' => $mes.' '.substr($dias_semana[$i], -2),
              'Viajes' => floatval($total_viajes),
              'Propina' => floatval($ganancias_propina),
              'Monto' => $ganancias_conductor_sin_iva,
              'IvaMonto' => $iva_ganancias_conductor,
              'Lubo' => $comision_lubo_sin_iva,
              'IvaLubo' => $iva_comision_lubo,
              'Total' => $total_dia
            ];
            break;

          case 2:
            $semana[] = [
              'Dia' => $mes.' '.substr($dias_semana[$i], -2),
              'Viajes' => floatval($total_viajes),
              'Propina' => floatval($ganancias_propina),
              'Monto' => $ganancias_conductor_sin_iva,
              'IvaMonto' => $iva_ganancias_conductor,
              'Lubo' => $comision_lubo_sin_iva,
              'IvaLubo' => $iva_comision_lubo,
              'Total' => $total_dia
            ];
            break;

          case 3:
            $semana[] = [
              'Dia' => $mes.' '.substr($dias_semana[$i], -2),
              'Viajes' => floatval($total_viajes),
              'Propina' => floatval($ganancias_propina),
              'Monto' => $ganancias_conductor_sin_iva,
              'IvaMonto' => $iva_ganancias_conductor,
              'Lubo' => $comision_lubo_sin_iva,
              'IvaLubo' => $iva_comision_lubo,
              'Total' => $total_dia
            ];
            break;

          case 4:
            $semana[] = [
              'Dia' => $mes.' '.substr($dias_semana[$i], -2),
              'Viajes' => floatval($total_viajes),
              'Propina' => floatval($ganancias_propina),
              'Monto' => $ganancias_conductor_sin_iva,
              'IvaMonto' => $iva_ganancias_conductor,
              'Lubo' => $comision_lubo_sin_iva,
              'IvaLubo' => $iva_comision_lubo,
              'Total' => $total_dia
            ];
            break;

          case 5:
            $semana[] = [
              'Dia' => $mes.' '.substr($dias_semana[$i], -2),
              'Viajes' => floatval($total_viajes),
              'Propina' => floatval($ganancias_propina),
              'Monto' => $ganancias_conductor_sin_iva,
              'IvaMonto' => $iva_ganancias_conductor,
              'Lubo' => $comision_lubo_sin_iva,
              'IvaLubo' => $iva_comision_lubo,
              'Total' => $total_dia
            ];
            break;

          case 6:
            $semana[] = [
              'Dia' => $mes.' '.substr($dias_semana[$i], -2),
              'Viajes' => floatval($total_viajes),
              'Propina' => floatval($ganancias_propina),
              'Monto' => $ganancias_conductor_sin_iva,
              'IvaMonto' => $iva_ganancias_conductor,
              'Lubo' => $comision_lubo_sin_iva,
              'IvaLubo' => $iva_comision_lubo,
              'Total' => $total_dia
            ];
            break;      
        }

        $total_viajes_semana = $total_viajes_semana + $total_viajes;
        $total_propinas_semana = $total_propinas_semana + $ganancias_propina;
        #conductor
        $monto_total_sin_iva = $monto_total_sin_iva + $ganancias_conductor_sin_iva;
        $iva_monto = $iva_monto + $iva_ganancias_conductor;
        #lubo
        $lubo_total_sin_iva = $lubo_total_sin_iva + $comision_lubo_sin_iva;
        $iva_lubo = $iva_lubo + $iva_comision_lubo;
        #totales semanales
        $ganancias_efectivo_semana = Round($ganancias_efectivo_semana + $ganancias_efectivo,2);
        $ganancias_tarjeta_semana = Round($ganancias_tarjeta_semana + $ganancias_tarjeta,2);
        $total_semana = Round($total_semana + ($comision_lubo + $ganancias_conductor),2);
        $ganancias_conductor_semana = Round(($ganancias_conductor_semana + $ganancias_conductor), 2);
        $comision_lubo_semana = Round(($comision_lubo_semana + $comision_lubo), 2);
      }

      if ($ganancias_tarjeta_semana > $ganancias_efectivo_semana) {
        #Se le tiene que dar al conductor
        $balance = 'Lubo te hará una transferencia por el monto de: $'.Round((($ganancias_tarjeta_semana - $comision_lubo_semana)), 2);
      } else {
        #Se le tiene que dar a lubo
        $balance = 'Lubo te hará una transferencia por el monto de: $'.Round((($ganancias_efectivo_semana - $ganancias_conductor_semana)), 2);
      }

             $this->response->result = [
              'Semana' => $semana,
              'Totales' => [
                'TotalViajes' => $total_viajes_semana,
                'Propina' => $total_propinas_semana,
                'Monto' => $monto_total_sin_iva,
                'ivaMonto' => $iva_monto,
                'Lubo' => $lubo_total_sin_iva,
                'ivaLubo' => $iva_lubo,
                'Total' => $total_semana
              ],
              'CobroViajesEfectivo' => $ganancias_efectivo_semana,
              'CobroViajesTarjeta' => $ganancias_tarjeta_semana,
              'ComisionesLubo' => $comision_lubo_semana,
              'Propinas' => $total_propinas_semana,
              'TusGanancias' => $ganancias_conductor_semana,
              'Balance' => $balance
            ];
      return $this->response->SetResponse(true);
    } else {
             $this->response->errors = 'La semana que has consultado aún no está registrada, contacta a Soporte Lubo:lubo.com.mx, para obtener más detalles.';
      return $this->response->SetResponse(false);
    }
  }

  public function pagarSemana($idConductor, $idSemana, $tipoTransaccion,$monto,$metodoPago,$idUsuario,$fechaPago,$factura){
    $semana_activa = $this->db->from('tbsemana')
                              ->select(null)
                              ->select('InicioSemana, FinSemana')
                              ->where('idSemana', $idSemana)
                              ->fetch();

    if ($semana_activa != false) {
        $pago = $this->db->from($this->tbInformacionbalance)
                         ->select(null)
                         ->select('StatusBalance')
                         ->where('idConductor',$idConductor)
                         ->where('idSemana',$idSemana)
                         ->where('StatusBalance = 1')
                         ->fetch()
                         ->StatusBalance;
        if ($pago == false){
            $data = [
              "idConductor" => $idConductor,
              "idSemana" => $idSemana,
              "Factura" => $factura,
              "StatusBalance" => 1 
            ]; 
            $registrarInformacionbalance = $this->db->insertInto($this->tbInformacionbalance, $data)
                                  ->execute();

            // $fechaPago = date("Y-m-d H:i:s");
            
            $dataTransaccion = [
                "MetodoPago"=>$metodoPago,
                "Status"=>2,
                "Monto"=>$monto,
                "MontoTotal"=>$monto,
                "FechaPago"=>$$fechaPago,
                "tipotransaccion"=>$tipoTransaccion,
                "idUsuario"=>$idUsuario
            ];
            
            $altaTransaccion  = $this->db->insertInto($this->tbTransaccion,$dataTransaccion)
                                        ->execute();
            $dataBalanceTransaccion = [
                "idBalance" => $registrarInformacionbalance,
                "idTransaccion" => $altaTransaccion
            ];
            $altaBalanceTransaccion = $this->db->insertInto($this->tbBalancetransaccion,$dataBalanceTransaccion)
                                              ->execute();

                  $this->response->result = [
                                  "Transaccion"=>$altaTransaccion,
                                  "IformacionBalance"=>$registrarInformacionbalance
                  ];
            return $this->response->SetResponse(true);
        }else{
                 $this->response->errors = 'No se puede acceder a esta semana.';
          return $this->response->SetResponse(false);
        }
      }else{
               $this->response->errors = 'La semana que has consultado aún no está registrada, contacta a Soporte Lubo:lubo.com.mx, para obtener más detalles.';
        return $this->response->SetResponse(false);
      }
  }

  public function listaMetosPago(){
    $data = $this->db->from($this->tbMetodoPago)
                     ->fetchAll();
            
           $this->response->result = $data;
    return $this->response->SetResponse(true);
  }

  public function enviarTiket(){
    $attachment = EnviarTicked::enviarPdf();
    //        $this->response->result = $attachment;
    // return $this->response->SetResponse(true);


    // $to = $emailUser;
    $to = "jorge@stardust.com.mx";
    $from = "soporte@stardust.com.mx";
    $subject = "Tu tiket Lubo";

    $message = "<p>Consulte el archivo adjunto.</p>";
    $separator = md5(time());
    $eol = PHP_EOL;
    $filename = "tiket_lubo.pdf";

    $headers = "From: " . $from . $eol;
    $headers .= "MIME-Version: 1.0" . $eol;
    $headers .= "Content-Type: multipart/mixed; boundary=\"" . $separator . "\"" . $eol . $eol;

    $body = '';

    $body .= "Content-Transfer-Encoding: 7bit" . $eol;
    $body .= "This is a MIME encoded message." . $eol; //had one more .$eol


    $body .= "--" . $separator . $eol;
    $body .= "Content-Type: text/html; charset=\"iso-8859-1\"" . $eol;
    $body .= "Content-Transfer-Encoding: 8bit" . $eol . $eol;
    $body .= $message . $eol;


    $body .= "--" . $separator . $eol;
    $body .= "Content-Type: application/octet-stream; name=\"" . $filename . "\"" . $eol;
    $body .= "Content-Transfer-Encoding: base64" . $eol;
    $body .= "Content-Disposition: attachment" . $eol . $eol;
    $body .= $attachment . $eol;
    $body .= "--" . $separator . "--";


    $urlP = "http://stardust.com.mx/Lubo/email.php";
    $data = array(
        "from"=>"Lubo <soporte@stardust.com.mx>",
        "to"=>$to,
        "subject"=>$subject,
        "message"=>$body,
        "headers"=>$headers
    );
    $opts = array('http' =>
    array(
        'method'  => 'POST',
        'header'  => 'Content-type: application/json',
        'content' =>  json_encode($data)));
    $context  = stream_context_create($opts);
    $res = file_get_contents($urlP, false, $context);
    $respuesta = json_decode($res);

    if ($respuesta->success){
        // var_dump($respuesta);
        // echo $to;
        // var_dump($pdfdoc);
        // var_dump($attachment);
        // header("Location:".$_SERVER['HTTP_REFERER']);
            $this->response->result = $respuesta;
      return $this->response->SetResponse(true);
    }else{
             $this->response->errors = "No se envio el mail";
      return $this->response->SetResponse(false);
    }  
  }

  public function dashBoardAdmin($fechaPeticion, $tipoSolicitud){
    switch ($tipoSolicitud) {
      case 1: #dia
        $inicio_peticion = $fechaPeticion.' '.'00:00:00';
        $fin_peticion = $fechaPeticion.' '.'23:59:59';
        break;
      
      case 2: #semana
        $inicio_peticion = date('Y-m-d 00:00:00', strtotime('monday this week', strtotime($fechaPeticion)));
        $fin_peticion = date('Y-m-d 23:59:59', strtotime('sunday this week', strtotime($fechaPeticion)));
        break;

      case 3: #mes
        $fecha_seccionada = explode('-', $fechaPeticion);
        $mes = $fecha_seccionada[1];
        $año = $fecha_seccionada[0];

        $inicio_peticion = $año.'-'.$mes.'-'.'01'.' '.'00:00:00';
        $fin_peticion = date('Y-m-d 23:59:59', (mktime(0, 0, 0, $mes+1, 1, $año) - 1));
        break;

      default:
               $this->response->errors = 'Tipo de solicitud incorrecta.';
        return $this->response->SetResponse(false);
        break;
    }

    #Total de viajes completados
    $total_viajes = $this->db->from($this->tbTransaccion)
      ->select('COUNT(*) AS TotalViajes')
      ->leftJoin('tbviaje ON tbviaje.idViaje = tbtransaccion.idViaje')
      ->where('tbtransaccion.FechaAlta Between :inicio AND :fin',
        array(':inicio' => $inicio_peticion, 'fin' => $fin_peticion))
      ->where('tbtransaccion.Status = 2 AND tbviaje.Pagado = 1')
      ->fetch()
      ->TotalViajes;

    $total_efectivo = $this->db->from($this->tbTransaccion)
      ->select(null)
      ->select('IFNULL(SUM(Round(tbtransaccion.Monto, 2)), 0) AS Total_dia')
      ->leftJoin('tbviaje ON tbviaje.idViaje = tbtransaccion.idViaje')
      ->where('tbtransaccion.FechaAlta BETWEEN :inicio AND :fin', 
        array(':inicio' => $inicio_peticion, ':fin' => $fin_peticion))
      ->where('tbtransaccion.Status = 2 AND tbtransaccion.MetodoPago = 1 AND tbviaje.Pagado = 1')
      ->fetchAll();

    $total_tarjeta = $this->db->from($this->tbTransaccion)
      ->select(null)
      ->select('
        IFNULL(SUM(Round(tbtransaccion.Monto, 2)), 0) AS Total_dia,
        IFNULL(SUM(Round(tbtransaccion.Propina, 2)), 0) AS Total_propina_dia
        ')
      ->leftJoin('tbviaje ON tbviaje.idViaje = tbtransaccion.idViaje')
      ->where('tbtransaccion.FechaAlta BETWEEN :inicio AND :fin', 
        array(':inicio' => $inicio_peticion, ':fin' => $fin_peticion))
      ->where('tbtransaccion.Status = 2 AND tbtransaccion.MetodoPago = 2 AND tbviaje.Pagado = 1')
      ->fetchAll();

    $efectivo_total = $total_efectivo[0]->Total_dia;
    $tarjeta_total = $total_tarjeta[0]->Total_dia;
    $propinas_total = $total_tarjeta[0]->Total_propina_dia;

    #Ganancias de todos los conductores
    $ganancias_conductor = (($efectivo_total + $tarjeta_total) * 0.85);
    #Ganancias de lubo
    $ganancias_lubo = (($efectivo_total + $tarjeta_total) * 0.15);

    #Número de usuarios
    $usuarios_registrados = $this->db->from($this->tbPersona)
      ->select('COUNT(*) AS TotalUsuarios')
      ->where('Tipo_de_usuario = 2 AND Fecha_Registro Between :inicio AND :fin',
        array(':inicio' => $inicio_peticion, 'fin' => $fin_peticion))
      ->fetch()
      ->TotalUsuarios;

    $conductores_registrados = $this->db->from($this->tbPersona)
      ->select('COUNT(*) AS TotalConductores')
      ->where('Tipo_de_usuario = 3 AND Fecha_Registro Between :inicio AND :fin',
        array(':inicio' => $inicio_peticion, 'fin' => $fin_peticion))
      ->fetch()
      ->TotalConductores;

           $this->response->result = [
            'TotalViajesRealizados' => floatval($total_viajes),
            'GananciasConductores' => round($ganancias_conductor, 2),
            'GananciasLubo' => round($ganancias_lubo, 2),
            'TotalPropinas' => floatval($propinas_total),
            'UsuariosRegistrados' => floatval($usuarios_registrados),
            'ConductoresRegistrados' => floatval($conductores_registrado)
          ];
    return $this->response->SetResponse(true);
  }
}
?>