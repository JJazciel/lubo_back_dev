<?php 
namespace App\Model;

use App\Lib\Response;

class SemanasModel {
	private $db;
	private $tbSemanas = 'tbsemana';

	public function __CONSTRUCT($db){
        $this->db = $db;
        $this->response = new Response();
    }

    public function actualizaSemanas() {
    	$semanas = $this->db->from($this->tbSemanas)
    						->select(null)
    						->select('idSemana, InicioSemana, Semana')
    						->orderBy('idSemana DESC')
    						->fetch();

    	if ($semanas != false) {
    		$numero_semana = floatval(substr($semanas->Semana, 5)) + 1;

    		$inicio_semana = date('Y-m-d H:i:s', strtotime($semanas->InicioSemana.'+1 week'));

    		$informacion_semana = [
    			'InicioSemana' => $inicio_semana,
    			'FinSemana' => date('Y-m-d 23:59:59', strtotime($inicio_semana.'+6 days')),
    			'Semana' => date('Y-'.$numero_semana)
    		];

    		$inserta_semana = $this->db->insertInto($this->tbSemanas, $informacion_semana)
    								   ->execute();

    		if ($inserta_semana > 0) {
    				   $this->response->result = 'Semana: '.$numero_semana.', agregada para balance.';
    			return $this->response->SetResponse(true);
    		} else {
    				   $this->response->errors = 'No se pudo generar la información de la semana';
    			return $this->response->SetResponse(false);
    		}
    	} else {
    		$informacion_semana = [
    			'InicioSemana' => date('2020-04-06 00:00:00'),
    			'FinSemana' => date('Y-m-d 23:59:59', strtotime('2020-04-06'.'+6 days')),
    			'Semana' => date('Y-'.'1')
    		];

    		$inserta_semana = $this->db->insertInto($this->tbSemanas, $informacion_semana)
    								   ->execute();

    		if ($inserta_semana > 0) {
    				   $this->response->result = 'Semana agregada para balance.';
    			return $this->response->SetResponse(true);
    		} else {
    				   $this->response->errors = 'No se pudo generar la información de la semana';
    			return $this->response->SetResponse(false);
    		}
    	}
    }
}

?>