<?php
namespace App\Model;

use App\Lib\Response;

class ListarDataModel
{
	private $db;
	private $tbpersona = 'tbpersona';
    private $tbverificarimg = 'tbverificarimg';
    private $tbvalidarinfo = 'tbvalidarinfo';
    private $response;

     public function __CONSTRUCT($db)
    {
        $this->db = $db;
        $this->response = new Response();
    }
    public function listarImg($l, $p){
    $mostrar = $this->db->from($this->tbpersona)
                        ->select('tbpersona.id_tbPersona,tbpersona.Nombre,tbpersona.Apellidos,tbpersona.Telefono')
                        ->leftJoin('tbverificarimg on tbverificarimg.idPersona = tbpersona.id_tbPersona')
                        ->where('tbverificarimg.status = 0')
                        ->limit($l)
                        ->offset($p)
                        ->groupBy('id_tbPersona')
                        ->orderBy('id_tbPersona ASC')
                        ->fetchAll();

   	if ($mostrar != false) {
             $this->response->result=$mostrar;
      return $this->response->SetResponse(true);
   	}else{
             $this->response->errors='No hay imagenes pendientes para revision';
      return $this->response->SetResponse(false);
    }
  }
  public function listarDatosPendientes($l, $p){
    $mostrar = $this->db->from($this->tbpersona)
                        ->select('tbpersona.id_tbPersona,tbpersona.Nombre,tbpersona.Apellidos,tbpersona.Telefono')
                        ->leftJoin('tbvalidarinfo on tbvalidarinfo.idUsuario = tbpersona.id_tbPersona')
                        ->where('tbvalidarinfo.status = 0')
                        ->limit($l)
                        ->offset($p)
                        ->groupBy('id_tbPersona')
                        ->orderBy('id_tbPersona ASC')
                        ->fetchAll();

   	if ($mostrar != false) {
             $this->response->result=$mostrar;
      return $this->response->SetResponse(true);
   	}else{
             $this->response->errors='No hay datos pendientes para revision';
      return $this->response->SetResponse(false);
    }
  }

}
?>