<?php
namespace App\Model;

use App\Lib\Response;

class ReportesModel
{
    private $db;
    private $table = 'tbviaje';
    private $tbtransaccion = 'tbtransaccion';
    private $response;

    public function __CONSTRUCT($db)
    {
        $this->db = $db;
        $this->response = new Response();
    }

    public function listar($l, $p)
    {
        $data = $this->db->from($this->table)
                         ->limit($l)
                         ->offset($p)
                         ->orderBy('id DESC')
                         ->fetchAll();//para mas de un registro

        $total = $this->db->from($this->table)
                          ->select('COUNT(*) Total')
                          ->fetch()
                          ->Total;

        return [
            'data'  => $data,
            'total' => $total
        ];
    }

   public function listarTotalV($fechaInicio,$fechaFin,$idzona){
    $data = $this->db->from($this->table)
                     ->where('(Status = :s) AND (idCiudad = :c) AND (tbviaje.FechaSolicitud BETWEEN :i AND :f)', array(':s' => 7 ,':c' => $idzona, ':i' => $fechaInicio, ':f' => $fechaFin))
                     ->fetchAll(); //para mas de un registro

    $total = $this->db->from($this->table)
                      ->select('COUNT(*) Total')
                      ->where('idCiudad',$idzona)
                      ->fetch()
                      ->Total;

    if ($data != true) {
        $data = $this->db->from($this->table)
                         ->orderBy('idViaje DESC')
                         ->where('Status',7)
                         ->fetchAll();//para mas de un registro

        $total = $this->db->from($this->table)
                          ->select('COUNT(*) Total')
                          ->fetch()
                          ->Total;

               $this->response->result = [
                                          'total' => $total
                                         ];
        return $this->response->SetResponse(true);
    }else{
              $this->response->result = [
                                          'total' => $total
                                         ];
      return $this->response->SetResponse(true);

    }
 
  }
    public function listarV($fechaInicio,$fechaFin,$idzona,$status){
    if($idzona != 0){
        $data = $this->db->from($this->table)
                        ->select("tbviaje.idViaje,FechaSolicitud,FechaInicio,FechaFin,tbviaje.idUsuario,CONCAT(tbpersona_user.Nombre,' ',tbpersona_user.Apellidos )AS NombreUser,idConductor,CONCAT(tbpersona_driver.Nombre,' ',tbpersona_driver.Apellidos )AS NombreDriver,idVehiculo,tbvehiculo.Placa AS Placa,tbvehiculo.Modelo AS Modelo,tbvehiculo.Año AS Año, idOrigen,idDestino,idFirebase,CostoAproximado,tbviaje.Status,tbviaje.idMetodoPago,tbmetodo_de_pago.Descripcion,round((tbtransaccion.MontoTotal *.15),2) AS Empresa,round((tbtransaccion.MontoTotal *.85),2) AS Conductor,round(tbtransaccion.MontoTotal, 2) AS Total,CalificadoUsuario,CalificadoDriver,Pagado")
                        ->leftJoin('tbpersona AS tbpersona_user ON tbpersona_user.id_tbPersona = tbviaje.idUsuario')
                        ->leftJoin('tbpersona AS tbpersona_driver ON tbpersona_driver.id_tbPersona = tbviaje.idConductor')
                        ->leftJoin('tbmetodo_de_pago ON tbmetodo_de_pago.idMetodoPago = tbviaje.idMetodoPago')
                        ->leftJoin('tbvehiculo ON tbvehiculo.id_tbVehiculo = tbviaje.idVehiculo')
                        ->leftJoin('tbtransaccion ON tbtransaccion.idViaje = tbviaje.idViaje')
                        ->where('(tbviaje.idCiudad = :c) AND (tbviaje.FechaSolicitud BETWEEN :i AND :f) AND (tbtransaccion.MetodoPago = 1 OR tbtransaccion.MetodoPago = 2)', array(':c' => $idzona, ':i' => $fechaInicio, ':f' => $fechaFin))
                        ->orderBy('idViaje DESC')
                        ->fetchAll();//para mas de un registro

        $total = $this->db->from($this->table)
                          ->select('COUNT(*) Total')
                          ->where('(tbviaje.Status = :s) AND (tbviaje.idCiudad = :c) AND (tbviaje.FechaSolicitud BETWEEN :i AND :f)', array(':s' => 7 ,':c' => $idzona, ':i' => $fechaInicio, ':f' => $fechaFin))
                          ->fetch()
                          ->Total;

                $this->response->result = [
                    'data' => $data,
                    'total' => $total
                ];
          return $this->response->SetResponse(true);
    }else{
        $data = $this->db->from($this->table)
                         ->select("tbviaje.idViaje,FechaSolicitud,FechaInicio,FechaFin,tbviaje.idUsuario,CONCAT(tbpersona_user.Nombre,' ',tbpersona_user.Apellidos )AS NombreUser,idConductor,CONCAT(tbpersona_driver.Nombre,' ',tbpersona_driver.Apellidos )AS NombreDriver,idVehiculo,tbvehiculo.Placa AS Placa,tbvehiculo.Modelo AS Modelo,tbvehiculo.Año AS Año, idOrigen,idDestino,idFirebase,CostoAproximado,tbviaje.Status,tbviaje.idMetodoPago,tbmetodo_de_pago.Descripcion,round((tbtransaccion.MontoTotal *.15),2) AS Empresa,round((tbtransaccion.MontoTotal *.85),2) AS Conductor,round(tbtransaccion.MontoTotal, 2) AS Total,CalificadoUsuario,CalificadoDriver,Pagado")
                         ->leftJoin('tbpersona AS tbpersona_user ON tbpersona_user.id_tbPersona = tbviaje.idUsuario')
                         ->leftJoin('tbpersona AS tbpersona_driver ON tbpersona_driver.id_tbPersona = tbviaje.idConductor')
                         ->leftJoin('tbvehiculo ON tbvehiculo.id_tbVehiculo = tbviaje.idVehiculo')
                         ->leftJoin('tbmetodo_de_pago ON tbmetodo_de_pago.idMetodoPago = tbviaje.idMetodoPago')
                         ->leftJoin('tbtransaccion ON tbtransaccion.idViaje = tbviaje.idViaje')
                         ->where('(tbviaje.Status = :c) AND (tbviaje.FechaSolicitud BETWEEN :i AND :f) AND (tbtransaccion.MetodoPago = 1 OR tbtransaccion.MetodoPago = 2)', array(':c' => $status, ':i' => $fechaInicio, ':f' => $fechaFin))
                         ->orderBy('idViaje DESC')
                         ->fetchAll();//para mas de un registro

        $total = $this->db->from($this->table)
                          ->select('COUNT(*) Total')
                         ->where('(tbviaje.Status = :s) AND (tbviaje.FechaSolicitud BETWEEN :i AND :f)', array(':s' => $status,':i' => $fechaInicio, ':f' => $fechaFin))
                          ->fetch()
                          ->Total;

               $this->response->result = [
                                          'data' => $data,
                                          'total' => $total
                                         ];
        return $this->response->SetResponse(true);
    }
  }

  public function monto($fechaInicio,$fechaFin,$idzona){
 
    $total = $this->db->from($this->tbtransaccion)
                      ->select('SUM(MontoTotal) AS Total')
                      ->leftJoin('tbviaje ON tbtransaccion.idViaje = tbviaje.idViaje')
                      ->where('(tbtransaccion.Status = :s) AND (tbviaje.idCiudad = :c) AND (tbtransaccion.FechaPago BETWEEN :i AND :f) AND  (tbtransaccion.MetodoPago = 1 OR tbtransaccion.MetodoPago = 2)', array(':s' => 2 ,':c' => $idzona, ':i' => $fechaInicio, ':f' => $fechaFin))
                      ->orderBy('idViaje DESC')
                      ->fetch()
                      ->Total;

   return $this->response->result =[
                                    'total' => $total
                                   ];
   return $this->response->SetResponse(true);
  }
  public function gananciaEmp($fechaInicio,$fechaFin,$idzona){
 
    $total = $this->db->from($this->tbtransaccion)
                      ->select('(SUM(MontoTotal)*.15) AS Total')
                      ->leftJoin('tbviaje ON tbtransaccion.idViaje = tbviaje.idViaje')
                      ->where('(tbtransaccion.Status = :s) AND (tbviaje.idCiudad = :c) AND (tbtransaccion.FechaPago BETWEEN :i AND :f) AND  (tbtransaccion.MetodoPago = 1 OR tbtransaccion.MetodoPago = 2)', array(':s' => 2 ,':c' => $idzona, ':i' => $fechaInicio, ':f' => $fechaFin))
                      ->orderBy('idViaje DESC')
                      ->fetch()
                      ->Total;

    if ($total != true) {
       $total = $this->db->from($this->tbtransaccion)
                      ->select('(SUM(MontoTotal)*.15) AS Total')
                      ->where('(tbtransaccion.Status = :s) AND (tbtransaccion.FechaPago BETWEEN :i AND :f) AND  (tbtransaccion.MetodoPago = 1 OR tbtransaccion.MetodoPago = 2)', array(':s' => 2 , ':i' => $fechaInicio, ':f' => $fechaFin))
                      ->orderBy('idViaje DESC')
                      ->fetch()
                      ->Total;
         return $this->response->result =[
                                          'total' => $total
                                         ];
         return $this->response->SetResponse(true);
    }else{
        return $this->response->result = [
                                          'total' => $total
                                         ];
        return $this->response->SetResponse(true);
    }


  }

  public function gananciaCond($fechaInicio,$fechaFin,$idzona){
 
    $total = $this->db->from($this->tbtransaccion)
                      ->select('(SUM(MontoTotal)*.85) AS Total')
                      ->leftJoin('tbviaje ON tbtransaccion.idViaje = tbviaje.idViaje')
                      ->where('(tbtransaccion.Status = :s) AND (tbviaje.idCiudad = :c) AND (tbtransaccion.FechaPago BETWEEN :i AND :f) AND  (tbtransaccion.MetodoPago = 1 OR tbtransaccion.MetodoPago = 2)', array(':s' => 2 ,':c' => $idzona, ':i' => $fechaInicio, ':f' => $fechaFin))
                      ->orderBy('idViaje DESC')
                      ->fetch()
                      ->Total;
    if ($total != true) {
       $total = $this->db->from($this->tbtransaccion)
                      ->select('(SUM(MontoTotal)*.85) AS Total')
                      ->where('(tbtransaccion.Status = :s) AND (tbtransaccion.FechaPago BETWEEN :i AND :f) AND  (tbtransaccion.MetodoPago = 1 OR tbtransaccion.MetodoPago = 2)', array(':s' => 2 , ':i' => $fechaInicio, ':f' => $fechaFin))
                      ->orderBy('idViaje DESC')
                      ->fetch()
                      ->Total;
         return $this->response->result =[
                                          'total' => $total
                                         ];
         return $this->response->SetResponse(true);
    }else{
        return $this->response->result = [
                                          'total' => $total
                                         ];
        return $this->response->SetResponse(true);
    }
  }
   public function detalleViaje($idViaje){
      $detalle = $this->db->from($this->table)
                          ->select("tbviaje.Status,CONCAT(tbpersona_user.Nombre,' ',tbpersona_user.Apellidos )AS NombreUser,CONCAT(tbpersona_driver.Nombre,' ',tbpersona_driver.Apellidos )AS NombreDriver,idVehiculo,tbvehiculo.Placa AS Placa,tbvehiculo.Modelo AS Modelo,tbvehiculo.Año AS Año,IF(tbfotos_vehiculo.Imagen IS NULL,'este auto no tiene imagen',tbfotos_vehiculo.Imagen) AS Imagen,tbviaje.idCiudad,CostoAproximado AS Tarifa,tbtransaccion_propina.Propina,DATE_FORMAT(FechaSolicitud,'%d-%m-%Y %T') AS FechaSolicitud,DATE_FORMAT(FechaInicio,'%d-%m-%Y %T') AS FechaInicio,DATE_FORMAT(FechaFin,'%d-%m-%Y %T') AS FechaFin,DATE_FORMAT(tbtransaccion.FechaPago,'%d-%m-%Y %T') AS FechaPago,tbstatus_transaccion.Descripcion")
                          ->leftJoin('tbpersona AS tbpersona_user ON tbpersona_user.id_tbPersona = tbviaje.idUsuario')
                          ->leftJoin('tbpersona AS tbpersona_driver ON tbpersona_driver.id_tbPersona = tbviaje.idConductor')
                          ->leftJoin('tbtransaccion AS tbtransaccion ON tbtransaccion.idViaje = tbviaje.idViaje')
                          ->leftJoin('tbtransaccion AS tbtransaccion_propina ON tbtransaccion_propina.idViaje = tbviaje.idViaje')
                          ->leftJoin('tbvehiculo ON tbvehiculo.id_tbVehiculo = tbviaje.idVehiculo')
                          ->leftJoin('tbfotos_vehiculo ON tbfotos_vehiculo.id_tbVehiculo = tbviaje.idVehiculo')
                          ->leftJoin('tbstatus_transaccion AS tbstatus_transaccion ON tbstatus_transaccion.idStatusTransaccion = tbviaje.Pagado')
                          ->where('tbviaje.idViaje',$idViaje)
                          ->fetch();

      if ($detalle != false) {
               $this->response->result=$detalle;
        return $this->response->setResponse(true);
      }else{
               $this->response->errors='No existen detalles de este viaje';
        return $this->response->setResponse(false);
      }
    }

    public function verMapaDetalleViaje($origin, $destination, $size = "410x200"){
        $markers = array();
        $markers[] = "markers=icon:http://lubo.com.mx/luboadmi/html/imagenes/marker-inicio.png" . urlencode("|") . $origin;
        $markers[] = "markers=icon:http://lubo.com.mx/luboadmi/html/imagenes/marker-destino.png" . urlencode("|") . $destination;
        $url = "https://maps.googleapis.com/maps/api/directions/json?origin=$origin&destination=$destination&key=AIzaSyBWf0L2kXje4Ywp8HHRU-94BTL2PTavruk";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, false);
        $result = curl_exec($ch);
        curl_close($ch);
        $googleDirection = json_decode($result, true);
        $polyline = urlencode($googleDirection['routes'][0]['overview_polyline']['points']);
        $markers = implode($markers, '&');
        
        /*return $polyline;*/
        $this->response->result = @"https://maps.googleapis.com/maps/api/staticmap?size=$size&scale=1&maptype=roadmap&path=color:0x870080|weight:3|enc:$polyline&$markers&zoom=14&key=AIzaSyBWf0L2kXje4Ywp8HHRU-94BTL2PTavruk";
        return $this->response->SetResponse(true);
    }

    public function reporteDiario($data){
      // var_dump($data);
      $fechaIni  = $data['fechaInicio'];
      $fechaFin = $data['fechaFin'];
      $s = $data['Status'];
      $idzona = $data['idZona'];
      $lista = $this->db->from($this->table)
                        ->select(null)
                        ->select('tbviaje.idViaje,
                        tbviaje.FechaInicio, tbviaje.FechaFin,
                        tbmodotrabajo.Nombre as Tipo,
                        tbpersona.Nombre,
                        if(tbtransaccion.MetodoPago = 2,"Tarjeta","Efectivo") as Pago,
                        ROUND((tbdetallesviaje.CostoAproximado - tbdetallesviaje.IVA) * .85,2) as MontoD,
                        ROUND(tbdetallesviaje.IVA * .85,2) as ivaDriver,
                        ROUND((tbdetallesviaje.CostoAproximado - tbdetallesviaje.IVA) * .15,2) as Lubo,
                        ROUND(tbdetallesviaje.IVA * 0.15,2) as ivaLubo,
                        tbdetallesviaje.CostoAproximado as Total,
                        tbdetallesviaje.IVA,
                        tbtransaccion.Propina,
                        tbviaje.Status
                        ')
                        ->leftJoin('tbvehiculo on tbvehiculo.id_tbVehiculo = tbviaje.idVehiculo')
                        ->leftJoin('tbmodotrabajo on tbmodotrabajo.idModoTrabajo = tbvehiculo.idModoTrabajo')
                        ->leftJoin('tbpersona on tbpersona.id_tbPersona = tbviaje.idConductor')
                        ->leftJoin('tbtransaccion on tbtransaccion.idViaje = tbviaje.idViaje')
                        ->leftJoin('tbdetallesviaje on tbdetallesviaje.idViaje = tbviaje.idViaje')
                        ->where("tbviaje.Status = :s and tbviaje.FechaInicio BETWEEN :fi AND :ff",
                                array(
                                      ":s"=>$s,
                                      ":fi"=>$fechaIni,
                                      ":ff"=>$fechaFin
                                ))
                        ->orderBy('tbviaje.idViaje desc')
                        ->limit($data['limit'])
                        ->offset($data['offset'])
                        ->fetchAll();
        
        $total = $this->db->from($this->table)
                          ->select(null)
                          ->select('count(tbviaje.idViaje) as Total,
                                    SUM(ROUND((tbdetallesviaje.CostoAproximado - tbdetallesviaje.IVA) * .85,2)) as MontoD,
                                    SUM(ROUND(tbdetallesviaje.IVA * .85,2)) as ivaDriver,
                                    SUM(ROUND((tbdetallesviaje.CostoAproximado - tbdetallesviaje.IVA) * .15,2)) as Lubo,
                                    SUM(ROUND(tbdetallesviaje.IVA * 0.15,2)) as ivaLubo,
                                    ROUND(SUM(tbdetallesviaje.CostoAproximado),2) as CostoTotal,
                                    ROUND(SUM(tbdetallesviaje.IVA),2) as IVA,
                                    ROUND(SUM(tbtransaccion.Propina),2) as Propina'
                          )
                          ->leftJoin('tbvehiculo on tbvehiculo.id_tbVehiculo = tbviaje.idVehiculo')
                          ->leftJoin('tbmodotrabajo on tbmodotrabajo.idModoTrabajo = tbvehiculo.idModoTrabajo')
                          ->leftJoin('tbpersona on tbpersona.id_tbPersona = tbviaje.idConductor')
                          ->leftJoin('tbtransaccion on tbtransaccion.idViaje = tbviaje.idViaje')
                          ->leftJoin('tbdetallesviaje on tbdetallesviaje.idViaje = tbviaje.idViaje')
                          ->where("tbviaje.Status = :s and tbviaje.FechaInicio BETWEEN :fi AND :ff",
                                array(
                                      ":s"=>$s,
                                      ":fi"=>$fechaIni,
                                      ":ff"=>$fechaFin
                                ))
                        ->fetch();

        if ($lista != false) {
          $this->response->result=[
                                      "data"=>$lista,
                                      "totales"=>$total
                                  ];
          return $this->response->setResponse(true);
        }else{
                  $this->response->errors='No existen viajes';
          return $this->response->setResponse(false);
        }
    }


}
?>