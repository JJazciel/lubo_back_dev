<?php
 namespace App\Model;

use App\Lib\Response,
    App\Lib\Cifrado;

class ZonaModel{
    private $db;
    private $tbZona = 'tbzona';
    private $response;

    public function __CONSTRUCT($db){
        $this->db = $db;
        $this->response = new Response();
    }

    public function listar(){
        $zonas_disponibles = $this->db->from($this->tbZona)
            ->select(null)
            ->select('idZona, Descripcion')
            ->where("idZona != 0 AND Activo = 1")
            ->orderBy('idZona DESC')
            ->fetchAll();

        if ($zonas_disponibles != false) {
                   $this->response->result = [
                       'data'  => $zonas_disponibles,
                       'total' => count($zonas_disponibles)
                   ];
            return $this->response->SetResponse(true);
        } else {
                   $this->response->errors = 'No hay zonas disponibles por el momento.';
            return $this->response->SetResponse(false);
        }
    }

    public function obtener($id){
        $obtener = $this->db->from($this->tbZona)
                            ->where('idZona', $id)
                            ->fetch();

        if ($obtener !=false) {
                   $this->response->result=$obtener;
            return $this->response->SetResponse(true);
        }else{
                   $this->response->errors='Esta zona no existe';
            return $this->response->SetResponse(false);
        }
    }

    public function registrar($data){
        $insert = $this->db->insertInto($this->tbZona,$data)
                             ->execute();

               $this->response->result = $insert;
        return $this->response->SetResponse(true);
    }
       
    public function actualizar($data,$id){
        $actualizar = $this->db->update($this->tbZona, $data)
                               ->where('idZona',$id)
                               ->execute();
                
               $this->response->result = $actualizar;
        return $this->response->SetResponse(true);
    }

    public function eliminar($id){
        $eliminar = $this->db->deleteFrom($this->tbZona)
                             ->where('idZona',$id)
                             ->execute();

               $this->response->result = $eliminar;
        return $this->response->SetResponse(true);
    }
}
?>