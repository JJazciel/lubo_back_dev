<?php
namespace App\Model;

use App\Lib\Response,
    App\Lib\Auth,
    App\Lib\Cifrado,
    App\Lib\Codigos,
    App\Lib\Email,
    App\Lib\ValidarEmail,
    App\Lib\SMS,
    App\Lib\Push,
    App\Lib\CURL;

class AuthModel{
    private $db;
    private $tbPersona = 'tbpersona';
    private $tbMensaje = 'tbmensajes';
    private $tbValidaremail ='tbvalidaremail';
    private $tbToken = 'tbtoken';
    private $tbTransaccion = 'tbtransaccion';
    private $response;

    public function __CONSTRUCT($db) {
        $this->db = $db;
        $this->response = new Response();
    }

    public function autenticar($telefono, $password, $tipo_de_usuario, $tokenFB, $plataforma) {
        $password = Cifrado::Sha512($password);

        $persona = $this->db->from($this->tbPersona)
                            ->select(null)
                            ->select("id_tbPersona,Tipo_de_usuario,IFNULL(Nombre,'') AS Nombre,IFNULL(Apellidos,'')AS Apellidos,IFNULL(Email,'') AS Email,IFNULL(Telefono,'') as Telefono,IFNULL(Imagen,'') AS Imagen ,IFNULL(Sexo,'') AS Sexo,IFNULL(Status,'') AS Status,IFNULL(Fecha_Registro,'') AS Fecha_Registro,IFNULL(tbpersona.idZona,0) AS idZona, tbzona.Descripcion AS zona, CodigoPais")
                            ->leftJoin('tbzona ON tbzona.idZona = tbpersona.idZona')
                            ->where('(status <= 1 or status = 3 or status = 4) and (Tipo_de_usuario = :tipo_de_usuario and Password = :password) and (Telefono = :telefono or Email = :correo )',
                                array(':tipo_de_usuario' => $tipo_de_usuario, ':password' => $password, ':telefono' => $telefono , ':correo' => $telefono))
                            ->fetch();
       
        
        if(is_object($persona)){
            // activar user en mysql
            $userActivo = $this->db->update($this->tbPersona, ['Status' => 1, 'RegistroFirebase' => 1])
                                   ->where('id_tbPersona', $persona->id_tbPersona)
                                   ->execute();

            $Calificacion = $this->db->from('tbcalificacion')
                                     ->select(null)
                                     ->select('IFNULL(AVG(Calificacion), 5) as Promedio')
                                     ->where('tbcalificacion.Calificado', $persona->id_tbPersona)
                                     ->fetch('Promedio');
            
            $emailStatus = $this->db->from('tbvalidaremail')
                                    ->select(null)
                                    ->select('IFNULL(tbvalidaremail.status, 0) as status')
                                    ->where('idPersona',$persona->id_tbPersona)
                                    ->orderBy('id DESC')
                                    ->limit(1)
                                    ->fetch('status');
            if ($emailStatus == 1) {
                $emailStatus = true;
            }else{
                $emailStatus = false;
            }
            if($tokenFB != null){
                $data = [
                    'idPersona' => $persona->id_tbPersona,
                    'Token' => $tokenFB,
                    'Plataforma' => $plataforma
                ];

                $idPersona = $data['idPersona'];
                $idToken = $this->db->from($this->tbToken)
                                    ->select('COUNT(*) Total')
                                    ->where('idPersona', $idPersona)
                                    ->fetch()
                                    ->Total;

                if ($idToken > 0) {
                    $actualizatoken = $this->db->update($this->tbToken, $data)
                                               ->where('idPersona', $idPersona)
                                               ->execute();
                }else{
                    $altatoken=$this->db->insertInto($this->tbToken, $data)
                                        ->execute();
                }
            }

            if($persona->Tipo_de_usuario == 3){ #Driver
                $statusReg = $this->db->from('tbregistro')
                                      ->select(null)
                                      ->select('Status')
                                      ->where('idPersona',$persona->id_tbPersona)
                                      ->fetch('Status');
                // var_dump($statusReg);
                $statusPERSONA = 0;
                if($statusReg != 5){
                    $userActivo = $this->db->update($this->tbPersona)
                                                 ->set('Status', 3)
                                                 ->where('id_tbPersona', $persona->id_tbPersona)
                                                 ->execute();
                    #activar driver en firebase
                    $driver_inactivo_firebase = CURL::statusDriver($idPersona, 3 , $persona->idZona);
                    $statusPERSONA = 3;
                #
                }else{
                    #activar driver en firebase
                    $driver_inactivo_firebase = CURL::statusDriver($idPersona, 1 , $persona->idZona);
                    $statusPERSONA = 1;
                #
                }
                
                $auto  = $this->db->from('tbvehiculo')
                                  ->select(null)
                                  ->select("IFNULL(id_tbVehiculo,'') AS id_tbVehiculo,IFNULL(Placa,'') AS Placa,IFNULL(Modelo,'') AS Modelo, IFNULL(Color,'') AS Color,IFNULL(Año,'') AS Año, IFNULL(Num_Pasajeros,'') AS Num_Pasajeros, IFNULL(idModoTrabajo,'') AS idModoTrabajo,IFNULL(idCiudad,'') AS idCiudad")
                                  ->where('idPersona',$persona->id_tbPersona)
                                  ->fetch();

                                  $fechaIni = date('Y-m-d H:i:s',mktime(0,0,0));
                                  $fechaFin = date('Y-m-d H:i:s',mktime(23,59,59));

                $ganancias = $this->db->from($this->tbTransaccion)
                                      ->select(null)
                                      ->select('IFNULL(SUM(tbtransaccion.MontoTotal), 0) AS TotalDia')
                                      ->leftJoin('tbviaje ON tbviaje.idViaje = tbtransaccion.idViaje')
                                      ->where("tbviaje.idConductor = :id AND tbtransaccion.Status = 2 AND tbtransaccion.FechaPago > :fechaini AND tbtransaccion.FechaPago < :fechafin",array(":id"=>$persona->id_tbPersona,":fechaini"=>$fechaIni,":fechafin"=>$fechaFin))
                                      ->fetch()
                                      ->TotalDia;
                                
                $ganancias = ($ganancias * 0.85);
                $gananciasDia = number_format($ganancias, 2, ".", ",");

                $viajes = $this->db->from('tbviaje')
                                   ->select('COUNT(*) totalViajes')
                                   ->where('(idConductor = :driver) AND (CalificadoDriver = 1) AND (Status = 7)', array(':driver' => $persona->id_tbPersona))
                                   ->fetch()
                                   ->totalViajes;

                $token = Auth::SignIn([
                    'id' => $persona->id_tbPersona,
                    'Nombre' => $persona->Nombre,
                    'Apellidos' => $persona->Apellidos,
                    'Tipo_de_usuario' => $persona->Tipo_de_usuario,
                    'Email' => $persona->Email,
                    'CodigoPais' => $persona->CodigoPais,
                    'Telefono' => $persona->Telefono,
                    'Imagen' => $persona->Imagen,
                    'Sexo' => $persona->Sexo,
                    'Calificación' => round($Calificacion, 2),
                    'Status' => $statusPERSONA,
                    'Fecha_Registro' => $persona->Fecha_Registro,
                    'id_vehiculo' => $auto->id_tbVehiculo,
                    'Placa'=> $auto->Placa,
                    'Modelo' =>$auto->Modelo,
                    'Año' => $auto->Año,
                    'Color' => $auto->Color,
                    'Num_Pasajeros' => $auto->Num_Pasajeros,
                    'idModoTrabajo' => $auto->idModoTrabajo,
                    'idCiudad'=>$auto->idCiudad,
                    'EmailStatus'=>$emailStatus,
                    'idZona'=>$persona->idZona,
                    'zona' => $persona->zona,
                    'Ganancia_Diaria' => $gananciasDia,
                    'totalTravels' => $viajes
                ]);
            }else{ #Usuario normal
                $token = Auth::SignIn([
                    'id' => $persona->id_tbPersona,
                    'Nombre' => $persona->Nombre,
                    'Apellidos' => $persona->Apellidos,
                    'Tipo_de_usuario' => $persona->Tipo_de_usuario,
                    'Email' => $persona->Email,
                     'CodigoPais' => $persona->CodigoPais,   
                    'Telefono' => $persona->Telefono,
                    'Imagen' => $persona->Imagen,
                    'Sexo' => $persona->Sexo,
                    'Calificación' => round($Calificacion, 2),
                    'Status' => $persona->Status,
                    'EmailStatus'=>$emailStatus,
                    'idZona'=>$persona->idZona
                ]);
            }
            

            $this->response->result = [
                                        'token' => $token,
                                        'persona' => $persona->id_tbPersona];

            return $this->response->SetResponse(true);
        }else{
            return $this->response->SetResponse(false, "Credenciales no válidas");
        }
    }

  #Flujo para cambio de número de telefono.
  public function validarNumero($codigoPais, $telefono, $tipoUsuario){
    $busca_numero_registrado =  $this->db->from($this->tbPersona)
      ->select('COUNT(*) as Num')
      ->where('
        Tipo_de_usuario = :tipo AND 
        CodigoPais = :codigoPais AND 
        Telefono = :tel AND 
        (status <= 1 or status = 3 or status = 4)', 
        array(':tipo' => $tipoUsuario, ':tel' => $telefono, ':codigoPais' => $codigoPais))
      ->fetch()
      ->Num;

    if ($busca_numero_registrado == 0) {
      $codigo_verificacion = Codigos::Codigo(4);

      $cuerpo = "Tu código Lubo es : $codigo_verificacion";
      $send = SMS::SendMensaje($codigoPais, $telefono, $cuerpo);
      $send = '1';

      if ($send == '1') {
        $fecha_envio = date('Y-m-d H:i:s');
        $fecha_expiracion = date('Y-m-d H:i:s', (strtotime ("+1 Hours")));

        $data_mensaje = [
          "FechaEnvio" => $fecha_envio,
          "Mensaje" => "Su codigo de verificacion es: ",
          "FechaExpiracion" => $fecha_expiracion,
          "Codigo" => $codigo_verificacion,
          "CodigoPais" => $codigoPais,
          "TempTelefono" => $telefono
        ];

        $altaCodigo = $this->db->insertInto($this->tbMensaje, $data_mensaje)
          ->execute();

               $this->response->result = $altaCodigo;
        return $this->response->SetResponse(true, 'Código de verificación enviado al teléfono: '.$telefono);
      } else {
               $this->response->errors='Error al enviar el código de verificación al télefono: '.$telefono.' inténtelo de nuevo.';
        return $this->response->SetResponse(false);
      }
    } else {
             $this->response->errors='El número de télefono ya está registrado en otra cuenta.';
      return $this->response->SetResponse(false);
    }
  }

  public function validarCodigo($codigoPais, $telefono, $codigo){
    $data_mensaje = $this->db->from($this->tbMensaje)
      ->select(null)
      ->select('Id AS idMensaje, Codigo')
      ->where('Id = (SELECT MAX(Id) FROM tbmensajes 
        WHERE TempTelefono = :telefono AND 
        CodigoPais = :codigoPais AND 
        status = 1)',
        array(':telefono' => $telefono,':codigoPais'=> $codigoPais))
      ->fetch();

    if ($data_mensaje != false) {
      if ($data_mensaje->Codigo === $codigo ){
        $updateStaus = $this->db->update($this->tbMensaje)
          ->set('status', 0)
          ->where('Id', $data_mensaje->idMensaje)
          ->execute();

                 $this->response->result = $codigo;
          return $this->response->SetResponse(true, 'Código de verificación correcto.');
      }else{
               $this->response->errors='El código que ingresó es incorrecto.';
        return $this->response->SetResponse(false); 
      }
    }else{
             $this->response->errors='No hay código para este número teléfono: '.$telefono;
      return $this->response->SetResponse(false);
    }
  }

  public function actualizarTelefono($idPersona, $telefono, $tipo_de_usuario){
    $busca_persona_registrada = $this->db->from($this->tbPersona)
      ->select('COUNT(*) AS Total')
      ->where('id_tbPersona', $idPersona)
      ->fetch()
      ->Total;

    if ($busca_persona_registrada > 0) {
      $busca_numero_registrado = $this->db->from($this->tbPersona)
        ->select('COUNT(*) AS Total')
        ->where('Telefono', $telefono)
        ->where('Status != 2')
        ->where('Tipo_de_usuario', $tipo_de_usuario)
        ->fetch()
        ->Total;

      if ($busca_numero_registrado == 0) {
        $token_persona = $this->db->from($this->tbToken)
          ->select('Token')
          ->where('idPersona', $idPersona)
          ->fetch()
          ->Token;

        $dataPush = array(
          'Lubo' => 'Push'
        );

        $envia_push = Push::FMC('Lubo', 'Tu número de teléfono se ha actualizado con éxito, para ver los cambios en tu perfil cierra e inicia sesión nuevamente.', $token, $dataPush);

        $actualiza_telefono = $this->db->update($this->tbPersona)
          ->set('Telefono', $telefono)
          ->where('id_tbPersona', $idPersona)
          ->execute();

               $this->response->result = $actualiza_telefono;
        return $this->response->SetResponse(true, 'Tu número de teléfono se ha actualizado con éxito, para ver los cambios en tu perfil, cierra e inicia sesión nuevamente.');
      } else {
               $this->response->errors = 'Este número ya está registrado en otra cuenta.';
        return $this->response->SetResponse(false);
      }
    }else {
              $this->response->errors = 'El usuario al que hace referencia no existe, verifique sus datos.';
      return  $this->response->SetResponse(false);
    }
  }
  #Fin del flujo para actualizar el número de teléfono

  #Flujo para recuperar la contraseña
  public function recuperarPassword($email, $tipoUsuario){
    $busca_persona_registrada = $this->db->from($this->tbPersona)
      ->select(NULL)
      ->select('id_tbPersona, CONCAT(Nombre," ",Apellidos) as NombreCompleto')
      ->where('Email', $email)
      ->where('Status != 2')
      ->where('Tipo_de_usuario', $tipoUsuario)
      ->limit(1)
      ->orderBy('id_tbPersona DESC')
      ->fetch();

    if ($busca_persona_registrada > 0) {
      $codigo_verificacion = Codigos::Codigo(6);

      $send = Email::Send($email, $codigo_verificacion, $busca_persona_registrada->id_tbPersona, $busca_persona_registrada->NombreCompleto);
    
      if ($send == '1') {
        $fecha_envio = date('Y-m-d H:i:s');
        $fecha_expiracion = date('Y-m-d H:i:s', (strtotime ("+24 Hours")));

        $data_mensaje = [
          'idPersona' => $busca_persona_registrada->id_tbPersona,
          'FechaEnvio' => $fecha_envio,
          'Mensaje' => 'Su código de verificación es: ',
          'FechaExpiracion' => $fecha_expiracion,
          'Codigo' => $codigo_verificacion
        ];

        $altaCodigo = $this->db->insertInto($this->tbMensaje, $data_mensaje)
          ->execute();

               $this->response->result = $busca_persona_registrada->NombreCompleto;
        return $this->response->SetResponse(true, 'Se ha enviado un código de verifiación al correo: '.$email);
      }else{
               $this->response->errors = 'Error al enviar el código de verificación, por favor inténtelo de nuevo.';
        return $this->response->SetResponse(false);
      }
    }else{
             $this->response->errors = 'El correo ingresado no está asociado a ninguna cuenta.';
      return $this->response->SetResponse(false);
    }
  }

  #Valida el código de verificación enviado al correo
  public function validaCodigoWeb($id, $codigo){
        $busca = $this->db->from($this->tbMensaje)
                          ->where('idPersona', $id)
                          ->where('Codigo', $codigo)
                          ->fetch();
        
        if ($busca > 0) {
            $actual = date('Y-m-d H:i:s');
            if ($actual <= $busca->FechaExpiracion) {
                if ($busca->status == 1) {
                    $this->db->update($this->tbMensaje, ["status"=>"0"])
                             ->where('idPersona', $id)
                             ->where('Codigo', $codigo)
                             ->execute();

                    return $this->response->setResponse(true, 'Código verificado');
                }else{
                           $this->response->errors='El código ya se usó';
                    return $this->response->SetResponse(false);
                }
            }else{
                       $this->response->errors='El código ha caducado';
                return $this->response->SetResponse(false);
            }
        }else{
                   $this->response->errors='El código es incorrecto';
            return $this->response->SetResponse(false);
        }
  }

  public function actualizaPass($id, $data){
        $data['Password'] = Cifrado::Sha512($data['Password']);

        $update = $this->db->update($this->tbPersona, $data)
                           ->where('id_tbPersona', $id)
                           ->execute();

        if ($update != false) {
                   $this->response->result=$update;
            return $this->response->SetResponse(true);
        }else{
                   $this->response->errors='No se ha podido actualizar';
            return $this->response->setResponse(false);
        }
  }
  #Fin del flujo para recuperación de contraseña

  #Flujo para verifiación y actualización de correo
  public function verificarEmail($email, $tipoUsuario, $idPersona){
    $buscar = $this->db->from($this->tbPersona)
      ->select(NULL)
      ->select('id_tbPersona, CONCAT(Nombre," ",Apellidos) as NombreCompleto')
      ->where('Email', $email)
      ->where('Status != 2')
      ->where('Tipo_de_usuario', $tipoUsuario)
      ->fetch();

    if ($buscar > 0) {
      $send = ValidarEmail::Send($email, $buscar->id_tbPersona, $buscar->NombreCompleto);
  
      if ($send == '1') {
        $data_verificar = [
          'idPersona' => $buscar->id_tbPersona,
          'status' => 0
        ];

        $altaCodigo = $this->db->insertInto($this->tbValidaremail, $data_verificar)
          ->execute();

               $this->response->result=$buscar->NombreCompleto;
        return $this->response->SetResponse(true, 'Se ha enviado un mensaje a tu correo: '.$email.', para su validación.');
      } else {
               $this->response->errors = 'No se ha podido enviar el correo de validación, reintenta.';
        return $this->response->SetResponse(false);
      }
    }else{
             $this->response->errors = 'El email ingresado no está asociado a ninguna cuenta.';
      return $this->response->SetResponse(false);
    }
  }

  public function validarEmail($id){
    $buscar = $this->db->from($this->tbValidaremail)
      ->where('idPersona', $id)
      ->limit(1)
      ->orderBy('id DESC')
      ->fetch();

    if ($buscar != false) {
      if ($buscar->status == 0) {
        $correoValidado = $this->db->update($this->tbValidaremail)
          ->set('status', 1)
          ->where('idPersona', $id)
          ->where('id', $buscar->id)
          ->execute();

        $token = $this->db->from($this->tbToken)
          ->select('Token')
          ->where('idPersona', $id)
          ->fetch()
          ->Token;

        $data = array('Lubo' => 'Push');
        $push = Push::FMC('Lubo', 'Tu correo electrónico se ha validado con éxito, para ver los cambios en tu perfil cierra e inicia sesión nuevamente.', $token, $data);

               $this->response->result = 'Tu correo electrónico se ha validado con éxito, para ver los cambios en tu perfil cierra e inicia sesión nuevamente.';
        return $this->response->setResponse(true);
      } else {
        return $this->response->SetResponse(false, 'Tu correo electrónico ya ha sido validado anteriormente, si tienes algún problema con esto, contacta a Soporte Lubo: lubo.com.mx, para obtener más información.');
      }
    } else {
             $this->response->errors='No existe un correo electrónico para validar.';
      return $this->response->SetResponse(false);
    }
  }

  public function actualizarEmail($id, $email, $tipo_de_usuario){
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
             $this->response->errors = 'El correo ingresado no tiene el formato correcto.';
      return $this->response->SetResponse(false);
    } else {
      $busca_persona_registrada = $this->db->from($this->tbPersona)
        ->select('COUNT(*) AS Total')
        ->where('id_tbPersona', $id)
        ->fetch()
        ->Total;

      if ($busca_persona_registrada == 1) {
        $busca_correo_registrado = $this->db->from($this->tbPersona)
          ->select('COUNT(*) AS Total')
          ->where('Email', $email)
          ->where('Status != 2')
          ->where('Tipo_de_usuario', $tipo_de_usuario)
          ->fetch()
          ->Total;

        if ($busca_correo_registrado == 0) {
          $busca_token = $this->db->from($this->tbToken)
            ->select('Token')
            ->where('idPersona', $id)
            ->fetch()
            ->Token;

          $dataPush = array('Lubo' => 'Push');
          $push = Push::FMC('Lubo', 'El correo se a actualizado con éxito, para ver los cambios en tu perfil cierra e inicia sesión nuevamente.', $busca_token, $dataPush);

          $actualizaEmail = $this->db->update($this->tbPersona)
            ->set('Email', $email)
            ->where('id_tbPersona', $id)
            ->execute();

                 $this->response->result=$busca_persona_registrada->NombreCompleto;
          return $this->response->SetResponse(true, 'El correo se a actualizado con éxito, para ver los cambios en tu perfil cierra e inicia sesión nuevamente.');
        } else {
                 $this->response->errors = 'El correo que ha ingresado ya está registrado en otra cuenta.';
          return $this->response->SetResponse(false);
        }
      } else {
               $this->response->errors = 'El usuario al que hace referencia no existe.';
        return $this->response->SetResponse(false);
      }
    }
  }
  #Fin del flujo para verificación y actualización de correo

  public function getData($token){
    $data = Auth::GetData($token);
    if ($data === "Signature verification failed" || $data == 'Expired token' ) {
             $this->response->errors='Token incorrecto';
      return $this->response->SetResponse(false);
    }else{
             $this->response->result=$data;
      return $this->response->SetResponse(true);
      }
  }

  #SERVICIOS PARA LA WEB
  #valida si el id que manda desde la url es válido
  public function validaId($id){
    $busca = $this->db->from($this->tbMensaje)
      ->select('tbmensajes.idPersona, tbpersona.Email')
      ->leftJoin('tbpersona On tbpersona.id_tbPersona = tbmensajes.idPersona')
      ->where('idPersona', $id)
      ->fetch();

    if ($busca > 0) {
             $this->response->result=[
               'id' => $busca->idPersona,
               'email' => $busca->Email
             ];
      return $this->response->setResponse(true);
    } else {
             $this->response->errors='No hay código para esta cuenta';
      return $this->response->SetResponse(false);
    }
  }

  public function desactivarUsuario($data){
    $usuario = $this->db->from($this->tbPersona)
      ->select(null)
      ->select('Tipo_de_usuario, idZona')
      ->where('id_tbPersona', $data['idUsuario'])
      ->fetch();

    if ($usuario != false) {
      switch ($usuario->Tipo_de_usuario) {
        case 2: #Mysql
          $driverInactivo = $this->db->update($this->tbPersona)
            ->set('Status', $data['status'])
            ->where('id_tbPersona', $data['idUsuario'])
            ->execute();

          break;

        case 3: #Mysql Firebase
          $driverInactivo = $this->db->update($this->tbPersona)
            ->set('Status', $data['status'])
            ->where('id_tbPersona', $data['idUsuario'])
            ->execute();

          $driver_inactivo_firebase = CURL::statusDriver($data['idUsuario'], floatval($data['status']), $usuario->idZona);
          break;
                
        default:
          return $this->response->SetResponse(false, 'El Tipo de usuario ingresado es incorrecto.');
          break;
            }

             $this->response->result=$driverInactivo;
      return $this->response->SetResponse(true, 'Estus actualizado.');
    } else {
      return $this->response->SetResponse(false, 'El usuario al que hace referencia no existe.');
    }
  }

  public function logout($data){
    $id_Persona = $data['id'];

    $usuario_registrado = $this->db->from($this->tbPersona)
      ->select(null)
      ->select('Tipo_de_usuario, idZona')
      ->where('id_tbPersona', $id_Persona)
      ->fetch();
    
    if ($usuario_registrado != false) {
      $tipo_usuario = $usuario_registrado->Tipo_de_usuario;

      if($tipo_usuario == 3){
        $status = 0;
        $id_zona = $usuario_registrado->idZona;

        $desactivar_condcutor = CURL::statusDriver($id_Persona, $status , $id_zona);
        $borrar_token = CURL::deleteToken($id_Persona, $id_zona);
      } elseif ($tipo_usuario == 2) {
        $borrar_token = CURL::deleteToken($id_Persona, 0);
      }
    
      $data_token = [
        'Token' => '',
        'Plataforma' => ''
      ];
    
      $eliminar_token = $this->db->update($this->tbToken, $data_token)
        ->where('idPersona', $id_Persona)
        ->execute();
    
      $desactivar_usuario = $this->db->update($this->tbPersona)
        ->set('Status', 0)
        ->where('id_tbPersona', $id_Persona)
        ->execute();

             $this->response->result = $desactivar_usuario;
      return $this->response->SetResponse(true, 'Estus actualizado.');
    } else {
      return $this->response->SetResponse(false, 'El usuario al que hace referencia no existe.');
    }        
  }
}
?>