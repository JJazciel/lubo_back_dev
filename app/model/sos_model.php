<?php
namespace App\Model;

use App\Lib\Response,
    App\Lib\SMS;

class SosModel{
  private $db;
  private $tbSos = 'tbsos';
  private $tbContactos ='tbcontactos';
  private $response;

  public function __CONSTRUCT($db){
    $this->db = $db;
    $this->response = new Response();
  }

  public function listar($l, $p){
    $data = $this->db->from($this->tbSos)
                     ->limit($l)
                     ->offset($p)
                     ->orderBy('id DESC')
                     ->fetchAll();

    if ($data != false) {
      $totalData = count($data);

      return [
        'data'  => $data,
        'total' => $totalData
      ];
    } else {
      return $this->response->SetResponse(false, 'No se encontró información');
    }
  }

  public function obtener($id){
    $obtener=$this->db->from($this->tbSos)
                      ->where('id', $id)
                      ->fetch();

    if ($obtener != false) {
             $this->response->result=$obtener;
      return $this->response->SetResponse(true);
    }else{
             $this->response->errors='Este id no existe';
      return $this->response->SetResponse(false);
    }
  }

  public function registrar($data){
    $buscarContactos = $this->db->from($this->tbContactos)
                       ->select(null)
                       ->select('tbcontactos.codigoPais, tbcontactos.telefono, tbpersona.Nombre')
                       ->leftJoin('tbpersona ON tbpersona.id_tbPersona = tbcontactos.idPersona')
                       ->where('idPersona', $data['idPersona'])
                       ->fetchAll();

    if ($buscarContactos != false) {
      foreach ($buscarContactos as $contacto) {
        $telefono = $contacto->telefono;
        $codigoPais = $contacto->codigoPais;
        $url = $data['URL'];
        $mensaje = $contacto->Nombre." te mandó un mensaje SOS Lubo, para seguir el viaje, clic en: $url";
        $enviarSms = SMS::SendMensaje($codigoPais, $telefono, $mensaje);
      }

      $registrarSos = $this->db->insertInto($this->tbSos, $data)
                               ->execute();

             $this->response->result=$registrarSos;
      return $this->response->SetResponse(true, 'Se han enviado los mensajes SOS a tus contactos.');
    } else {
             $this->response->errors = 'Aún no tienes registrados tus contactos de emergencia.';
      return $this->response->SetResponse(false);
    }
  }

  public function actualizar($data, $id){
    $actualizar = $this->db->update($this->tbSos, $data)
                           ->where('id', $id)
                           ->execute();

    if ($actualizar != false) {
             $this->response->result=$actualizar;
      return $this->response->SetResponse(true);
    }else{
             $this->response->errors='No se pudo actualizar la información, reintente.';
      return $this->response->SetResponse(false);
    }
  }

  public function eliminar($id){
    $eliminar = $this->db->delete($this->tbSos)
                         ->where('id', $id)
                         ->execute();

           $this->response->result = $eliminar;
    return $this->response->SetResponse(true);
  }
}
?>