<?php
namespace App\Model;
use App\Lib\Response,
    App\Lib\Push;

Class FacturaModel
{
	private $db;
    private $tbPersona = 'tbpersona';
    private $tbrfc = 'tbrfc';
    private $tbfacturas = 'tbfacturas';
    private $tbToken = 'tbtoken';
    private $tbViaje = 'tbviaje';
    private $tbTransaccion = 'tbtransaccion';
    // private $url = 'https://apisandbox.facturama.mx';
    private $url = 'https://api.facturama.mx';
    // private $userFacturama = 'HumbertStardust007';
    private $userFacturama = 'HumbertoStardust007';
    private $passwordFacturama = 'Stardust007';
    private $authFacturama = '';


	public function __CONSTRUCT($db){
        $this->db = $db;
        $this->response = new Response();
    
        $this->authFacturama = base64_encode("$this->userFacturama:$this->passwordFacturama");
    

    }

  public function agregarCSD($data){
    $buscar = $this->db->from($this->tbPersona)
                        ->where('id_tbPersona',$data['idPersona'])
                        ->fetch();
                        
    if ($buscar != false) {
      $dataAlta = [
        "rfc"=>$data['Rfc'],
        "idPersona"=>$data['idPersona'],
        "razon"=>$data['razon'],
        "regimenFiscal"=>$data['regimenFiscal'],
        "descripcion"=>$data['descripcion']
      ];
        $rfc = $this->db->from('tbrfc')
                        ->where('idPersona',$data['idPersona'])
                        ->fetch();
        if ($rfc == false) {
            $data =  json_encode($data);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "$this->url/api-lite/csds");
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Authorization'. ':' .'Basic '. $this->authFacturama,
                'Content-Type:application/json'
            ]);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_POST,1);
            curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
            $res = curl_exec($ch);
            curl_close($ch);
            $d = json_decode($res);
            if(!isset($d->Message)){
              $altaRfc = $this->db->insertInto($this->tbrfc,$dataAlta)
                                ->execute();
                     $this->response->result = $d;
              return $this->response->SetResponse(true);
            }else{
                     $this->response->errors = $d->Message;
              return $this->response->SetResponse(false);
            }
        }else{
               $this->response->errors = 'Este usuario ya dio de alta sus CSD.';
        return $this->response->SetResponse(false);
        }
    }else{
               $this->response->errors = 'Este usuario no existe.';
        return $this->response->SetResponse(false);
    }
  }
  public function perfilEx($id){
    $ex = $this->db->from($this->tbrfc)
                   ->where('idPersona',$id)
                   ->fetch();
    if($ex!=false){
             $this->response->result = $ex;
      return $this->response->SetResponse(true);
    }else{
      $this->response->errors = "No hay perfil de facturacion";
      return $this->response->SetResponse(false);
    }
  }
  public function listarCSDs(){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "$this->url/api-lite/csds");
    curl_setopt($ch, CURLOPT_HTTPHEADER, [
        'Authorization'. ':' .'Basic '. $this->authFacturama
    ]);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $res = curl_exec($ch);
    curl_close($ch);
    $d = json_decode($res);

             $this->response->result = $d;
      return $this->response->SetResponse(true);
  }

  public function obtebnerCSD($rfc){
    //   api-lite/csds/ALO110913N98
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "$this->url/api-lite/csds/$rfc");
    curl_setopt($ch, CURLOPT_HTTPHEADER, [
        'Authorization'. ':' .'Basic '. $this->authFacturama
    ]);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $res = curl_exec($ch);
    curl_close($ch);
    $d = json_decode($res);

             $this->response->result = $d;
      return $this->response->SetResponse(true);
  }

  public function actualizarCSD($rfc,$data){
    //  /api-lite/csds/ALO110913N98
    // curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
    $data = $data  = json_encode($data);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "$this->url/api-lite/csds/$rfc");
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Authorization'. ':' .'Basic '. $this->authFacturama,
                'Content-Type:application/json'
            ]);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_POST,1);
            curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
            $res = curl_exec($ch);
            curl_close($ch);
            $d = json_decode($res);

                   $this->response->result = $rfc;
            return $this->response->SetResponse(true);
  }

  public function eliminarCSD($rfc){
    // api-lite/csds/AAA010101AAA
    $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "$this->url/api-lite/csds/$rfc");
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Authorization'. ':' .'Basic '. $this->authFacturama,
                'Content-Type:application/json'
            ]);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_POST,1);
            // curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
            $res = curl_exec($ch);
            curl_close($ch);
            $d = json_decode($res);
            $eliminarRFC =$this->db->deleteFrom($this->tbrfc)
                                    ->where('rfc',$rfc)
                                    ->execute();

                   $this->response->result = $rfc;
            return $this->response->SetResponse(true);
  }

  public function crearFactura($idViaje){

    $factura = $this->db->from($this->tbfacturas)
                        ->where('idViaje',$idViaje)
                        ->fetch('idViaje');
    if ($factura != false) {
             $this->response->errors = "ex";
      return $this->response->SetResponse(false);
    }else{
      $modoPago = $this->db->from($this->tbTransaccion)
                           ->select(null)
                           ->select('MetodoPago')
                           ->where('idViaje',$idViaje)
                           ->fetch('MetodoPago');
      $datosFactura = null;
      if ($modoPago == 1) {
        $datosFactura = $this->db->from($this->tbViaje)
                               ->select(null)
                               ->select('tbviaje.idViaje,
                               conductor.id_tbPersona as idConductor,
                               conductor.Nombre as NombreConductor , 
                               tbrfc.rfc as rfcConductor, 
                               tbrfc.razon,
                               tbrfc.regimenFiscal,
                               idMetodoPago,
                               usuario.id_tbPersona as idUser,
                               CONCAT(usuario.Nombre," ",usuario.Apellidos) as NombreUsuario,
                               tbtransaccion.MetodoPago,
                               tbdetallesviaje.IVA,
                               tbtransaccion.Monto - tbdetallesviaje.IVA as SubTotal,
                               tbtransaccion.Monto as MontoTotal,
                               perfil.Rfc as rfcUser,
                               perfil.NombreLegal as NombreLegalUser')
                               ->leftJoin('tbpersona as conductor on conductor.id_tbPersona = tbviaje.idConductor')
                               ->leftJoin('tbrfc on conductor.id_tbPersona = tbrfc.idPersona')
                               ->leftJoin('tbpersona as usuario on usuario.id_tbPersona = tbviaje.idUsuario')
                               ->leftJoin('tbtransaccion on tbviaje.idViaje = tbtransaccion.idViaje')
                               ->leftJoin('tbdetallesviaje on tbviaje.idViaje = tbdetallesviaje.idViaje')
                               ->leftJoin('tbperfilImpuestos as perfil on perfil.idPersona = tbviaje.idUsuario 
                                                                       and perfil.idmetodo_de_pago = tbtransaccion.MetodoPago')
                               ->where('tbviaje.idViaje',$idViaje)
                               ->fetch();
      } else if ($modoPago == 2) {
        $datosFactura = $this->db->from($this->tbViaje)
                               ->select(null)
                               ->select('tbviaje.idViaje,
                               conductor.id_tbPersona as idConductor,
                               conductor.Nombre as NombreConductor , 
                               tbrfc.rfc as rfcConductor, 
                               tbrfc.razon,
                               tbrfc.regimenFiscal,
                               idMetodoPago,
                               usuario.id_tbPersona as idUser,
                               CONCAT(usuario.Nombre," ",usuario.Apellidos) as NombreUsuario,
                               tbtransaccion.MetodoPago,
                               tbdetallesviaje.IVA,
                               tbtransaccion.Monto - tbdetallesviaje.IVA as SubTotal,
                               tbtransaccion.Monto as MontoTotal,
                               perfil.Rfc as rfcUser,
                               perfil.NombreLegal as NombreLegalUser')
                               ->leftJoin('tbpersona as conductor on conductor.id_tbPersona = tbviaje.idConductor')
                               ->leftJoin('tbrfc on conductor.id_tbPersona = tbrfc.idPersona')
                               ->leftJoin('tbpersona as usuario on usuario.id_tbPersona = tbviaje.idUsuario')
                               ->leftJoin('tbtransaccion on tbviaje.idViaje = tbtransaccion.idViaje')
                               ->leftJoin('tbdetallesviaje on tbviaje.idViaje = tbdetallesviaje.idViaje')
                               ->leftJoin('tbperfilImpuestos as perfil on perfil.idPersona = tbviaje.idUsuario 
                                                                       and perfil.idmetodo_de_pago = tbtransaccion.MetodoPago 
                                                                       and perfil.idTarjeta = tbtransaccion.idClienteConekta')
                               ->where('tbviaje.idViaje',$idViaje)
                               ->fetch();
      }
      
      if ($datosFactura->rfcUser == null) {
               $this->response->errors = "no hay rfc del Usuario";
        return $this->response->SetResponse(false);
      }
      if ($datosFactura->rfcConductor == null) {
               $this->response->errors = "no hay rfc del Driver";
        return $this->response->SetResponse(false);
      }
      $formaPago = 0;
      switch ($datosFactura->MetodoPago) {
        case 1:
          $formaPago = "01";
          break;
        case 2:
          $formaPago = "04";
          break;
      }
      $data='{
        "Folio": "'.$idViaje.'",
        "Serie": "R",
        "Currency": "MXN",
        "ExpeditionPlace": "73170",
        "CfdiType": "I",
        "PaymentForm": "'.$formaPago.'",
        "PaymentMethod": "PUE",
        "Issuer": {
          "FiscalRegime": "'.$datosFactura->regimenFiscal.'",
          "Rfc": "'.$datosFactura->rfcConductor.'",
          "Name": "'.$datosFactura->razon.'"
        },
        "Receiver": {
          "Rfc": "'.$datosFactura->rfcUser.'",
          "Name": "'.$datosFactura->NombreLegalUser.'",
          "CfdiUse": "P01"
        },
        "Items": [
          {
            "ProductCode": "78111808",
            "IdentificationNumber": "001",
            "Description": "Alquiler de vehículos",
            "Unit": "NO APLICA",
            "UnitCode": "E48",
            "UnitPrice": '.$datosFactura->SubTotal.',
            "Quantity": 1,
            "Subtotal": '.$datosFactura->SubTotal.',
            "Taxes": [
              {
                "Total": '.$datosFactura->IVA.',
                "Name": "IVA",
                "Base": '.$datosFactura->SubTotal.',
                "Rate": 0.16,
                "IsRetention": false
              }
            ],
            "Total": '.$datosFactura->MontoTotal.'
          },
          
        ]
      }';
      
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "$this->url/api-lite/2/cfdis");
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Authorization'. ':' .'Basic '. $this->authFacturama,
            'Content-Type:application/json'
        ]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST,1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
        $res = curl_exec($ch);
        curl_close($ch);
        $d = json_decode($res);

        if (!isset($d->Message)) {
          $idfactura = $d->Id;
          $datatable = [
            "idFactura"=>$idfactura,
            "idViaje"=>$idViaje
          ];
          $altaFactura = $this->db->insertInto($this->tbfacturas,$datatable)
                                  ->execute();

                 $this->response->result = $d;
          return $this->response->SetResponse(true);
        }else{
                  $this->response->errors = [$d,$data];
          return $this->response->SetResponse(false);
        }
      }
  }

  public function descargarFactura($idViaje){

    $idFactura = $this->db->from($this->tbfacturas)
                          ->select(null)
                          ->select('idFactura')
                          ->where('idViaje',$idViaje)
                          ->fetch()
                          ->idFactura;

    if ($idFactura != false) {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, "$this->url/api/Cfdi/pdf/issuedLite/$idFactura");
      curl_setopt($ch, CURLOPT_HTTPHEADER, [
          'Authorization'. ':' .'Basic '. $this->authFacturama
      ]);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      $res = curl_exec($ch);
      curl_close($ch);
      $d = json_decode($res);
      // // Content
      // $data = base64_decode($d->Content);
      // return $data;
               $this->response->result = $d;
        return $this->response->SetResponse(true);
    }else{
              $this->response->erros = "Viaje sin Factura";
        return $this->response->SetResponse(false,$idFactura);
    }
  }

  public function detallesFactura(){
      
  }

  public function cancelarFactura(){
      
  }

  public function obtenerAcuseCancelacion(){
      
  }

  public function filtrarFacturas($filtro){
      // api-lite/cfdis?keyword=radial
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, "$this->url/api-lite/cfdis?keyword=$filtro");
      curl_setopt($ch, CURLOPT_HTTPHEADER, [
          'Authorization'. ':' .'Basic '. $this->authFacturama
      ]);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      $res = curl_exec($ch);
      curl_close($ch);
      $d = json_decode($res);

             $this->response->result = $d;
      return $this->response->SetResponse(true);
  }

  public function enviarfacturaEmail($idViaje,$email){
    $data = array("status"=>1);
    $data = json_encode($data);
    $idFactura = $this->db->from($this->tbfacturas)
                          ->select(null)
                          ->select('idFactura')
                          ->where('idViaje',$idViaje)
                          ->fetch()
                          ->idFactura;

    if ($idFactura != false) {
      $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "$this->url/Cfdi?cfdiType=issuedLite&cfdiId=$idFactura&email=$email");
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Authorization'. ':' .'Basic '. $this->authFacturama
        ]);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST,1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
        $res = curl_exec($ch);
        curl_close($ch);
        $d = json_decode($res);

               $this->response->result = $res;
        return $this->response->SetResponse(true);
    }else{
              $this->response->errors = "Viaje sin Factura";
      return $this->response->SetResponse(false,$idFactura);
    }

  }
}