<?php
namespace App\Model;

use App\Lib\Response,
    App\Lib\Push,
    App\Lib\CURL;
use FluentLiteral;
// use FluentPDO;

class ImgModel
{
	private $db;
  private $table;
  private $response;
  private $tbToken = 'tbtoken';
  private $tbverificarimg = 'tbverificarimg';
  private $tbPersona = 'tbpersona';
  // private $dirSubida = '/var/www/html/lubo_back_dev/img/';#local && dev
  private $dirSubida = '/var/www/html/lubo/img/';#prod
  
  // private $dirServer = 'http://dev.lubo.com.mx/lubo_back_dev/img/';#dev
  private $dirServer = 'http://api.lubo.com.mx/lubo/img/';#prod
  // private $dirServer = 'http://localhost/lubo_back_dev/img/';#local
  
  public function __CONSTRUCT($db){
    $this->db = $db;
    $this->response = new Response();
  }
  
  #lsitar imagenes
  public function listar($id,$tipo){
    $data = $this->db->from($this->table)
                     ->where('IdSolicitud',$id)
                     // ->limit($l)
                     // ->offset($p)
                     // ->orderBy('Id DESC')
                     ->fetchAll();

    $total = $this->db->from($this->table)
                      ->select('COUNT(*) Total')
                      ->fetch()
                      ->Total;

           $this->response->result = [
            'data'  => $data,
            'total' => $total
        ];
    return $this->response->SetResponse(true);
  }

  public function listarDoc($id){

    $output = [];

    $listaDocs = $this->db->from('tbtipo_documento')
                          ->select(null)
                          ->select('id_tipo_documento,Descripcion,Url,Tipo')
                          ->where('(tbtipo_documento.id_tipo_documento != 3 AND tbtipo_documento.id_tipo_documento != 4) and Tipo = "Persona"')
                          ->fetchAll();

    $data = $this->db->from('tbdocumentos_persona')
                     ->select(null)
                     ->select("`id_documentos_persona`,tbtipo_documento.id_tipo_documento,`id_tbPersona`,`Documento`,`Status`,`Descripcion`, tbdocumentos_persona.fechaAlta,DATE_FORMAT(fechaExpiracion, '%d-%m-%Y') AS fechaExpiracion,tbtipo_documento.Tipo,tbtipo_documento.Url")
                     ->where('id_tbPersona',$id)
                     ->leftJoin('tbtipo_documento on tbdocumentos_persona.id_tipo_documento = tbtipo_documento.id_tipo_documento')
                     ->fetchAll();
    
    foreach ($listaDocs as $l) {
      if ($data == false) {
        $fila = [
          "id_documentos_persona" => null,
          "id_tipo_documento" => $l->id_tipo_documento,
          "id_tbPersona" => $id,
          "Documento" => "",
          "Status" => "2",
          "Descripcion" => $l->Descripcion,
          "fechaAlta" => null,
          "fechaExpiracion" => null,
          "Tipo" => $l->Tipo,
          "Url" => $l->Url
        ];
        $output[] = $fila; 
      }else{

        $temporal = null;
        foreach ($data as $d) {
          $temporal = null;
          if ($d->id_tipo_documento == $l->id_tipo_documento) {
            $temporal = $d;
            $encon[] = $d;
            break;
          }
        }

        if($temporal != null){
          $fila = [
            "id_documentos_persona" => $temporal->id_documentos_persona,
            "id_tipo_documento" => $l->id_tipo_documento,
            "id_tbPersona" => $id,
            "Documento" => $temporal->Documento,
            "Status" => $temporal->Status,
            "Descripcion" => $l->Descripcion,
            "fechaAlta" => $temporal->fechaAlta,
            "fechaExpiracion" => $temporal->fechaExpiracion,
            "Tipo" => $l->Tipo,
            "Url" => $l->Url
          ];
        }else{
          $fila = [
            "id_documentos_persona" => null,
            "id_tipo_documento" => $l->id_tipo_documento,
            "id_tbPersona" => $id,
            "Documento" => "",
            "Status" => "2",
            "Descripcion" => $l->Descripcion,
            "fechaAlta" => null,
            "fechaExpiracion" => null,
            "Tipo" => $l->Tipo,
            "Url" => $l->Url
          ];
        }
        $output[] = $fila;
      }
    }

    if ($data != false) {
             $this->response->result = $output;
      return $this->response->SetResponse(true);
    }else{
            //  $this->response->errors = 'No hay elementos';
            $this->response->result = $output;
      return $this->response->SetResponse(true);
    }

  }

  public function listarDA($id){
    $output = [];

    $listaDocs = $this->db->from('tbtipo_documento')
                          ->select(null)
                          ->select('id_tipo_documento,Descripcion,Url,Tipo')
                          ->where('(tbtipo_documento.id_tipo_documento != 3 AND tbtipo_documento.id_tipo_documento != 4) and Tipo = "Vehiculo"')
                          ->fetchAll();

    $data = $this->db->from('tbdocumentos_vehiculo')
                     ->select(null)
                     ->select("`id_documentos_vehiculo`,tbdocumentos_vehiculo.id_tipo_documento,`id_tbVehiculo`,`Documento`,`Status`,`fechaAlta`,DATE_FORMAT(fechaExpiracion, '%d-%m-%Y') AS fechaExpiracion ,tbtipo_documento.Descripcion,tbtipo_documento.Tipo,tbtipo_documento.Url")
                     ->where('id_tbVehiculo',$id)
                     ->leftJoin("tbtipo_documento ON tbtipo_documento.id_tipo_documento = tbdocumentos_vehiculo.id_tipo_documento")
                     ->fetchAll();

    foreach ($listaDocs as $l) {
      if ($data == false) {
        $fila = [
          "id_documentos_vehiculo" => null,
          "id_tipo_documento" => $l->id_tipo_documento,
          "id_tbVehiculo" => $id,
          "Documento" => "",
          "Status" => "2",
          "Descripcion" => $l->Descripcion,
          "fechaAlta" => null,
          "fechaExpiracion" => null,
          "Tipo" => $l->Tipo,
          "Url" => $l->Url
        ];
        $output[] = $fila; 
      }else{

        $temporal = null;
        foreach ($data as $d) {
          $temporal = null;
          if ($d->id_tipo_documento == $l->id_tipo_documento) {
            $temporal = $d;
            $encon[] = $d;
            break;
          }
        }

        if($temporal != null){
          $fila = [
            "id_documentos_vehiculo" => $temporal->id_documentos_persona,
            "id_tipo_documento" => $l->id_tipo_documento,
            "id_tbVehiculo" => $id,
            "Documento" => $temporal->Documento,
            "Status" => $temporal->Status,
            "Descripcion" => $l->Descripcion,
            "fechaAlta" => $temporal->fechaAlta,
            "fechaExpiracion" => $temporal->fechaExpiracion,
            "Tipo" => $l->Tipo,
            "Url" => $l->Url
          ];
        }else{
          $fila = [
            "id_documentos_vehiculo" => null,
            "id_tipo_documento" => $l->id_tipo_documento,
            "id_tbVehiculo" => $id,
            "Documento" => "",
            "Status" => "2",
            "Descripcion" => $l->Descripcion,
            "fechaAlta" => null,
            "fechaExpiracion" => null,
            "Tipo" => $l->Tipo,
            "Url" => $l->Url
          ];
        }
        $output[] = $fila;
      }
    }                
    
    if ($data != false) {
             $this->response->result = $output;
      return $this->response->SetResponse(true);
    }else{
             $this->response->result = $output;
      return $this->response->SetResponse(true);
    }
    
  }

  public function verImg($idPersona){
    $obtener = $this->db->from($this->tbverificarimg)
                        ->where('idPersona', $idPersona)
                        ->where('status = 0')
                        ->orderBy('id DESC')
                        ->limit(1)
                        ->fetch();

    if ($obtener != false) {
             $this->response->result=$obtener;
      return $this->response->SetResponse(true);
    }else{
             $this->response->errors='este usuario no ha subido ninguna imagen';
      return $this->response->SetResponse(false);
    }
  }

  public function actualizarstatus($id,$verificar){   
    $actualizar = $this->db->update($this->tbverificarimg,$data)
                           ->where('id',$id)
                           ->execute();

    switch ($verificar) {
      case 1:
        $pago = array('status' => $verificar);
        $metodopago = $this->db->update($this->tbverificarimg, $verificar)
                               ->where('status',$verificar)
                               ->execute();
        break;

      case 2:
        $pago = array('status' => $verificar);
        $metodopago = $this->db->update($this->tbverificarimg, $verificar)
                               ->where('status',$verificar)
                               ->execute();
        break;
      }

           $this->response->result=$actualizar;
    return $this->response->SetResponse(true);
  }

  #cargar
  public function cargar($file,$documento,$id){
    $max_upload = false;
    // "img": {
    //   "name": "Reflection_of_the_Kanas_Lake_by_Wang_Jinyu.jpg",
    //   "type": "image/jpeg",
    //   "tmp_name": "/tmp/phpaflGkd",
    //   "error": 0,
    //   "size": 1780533
    // }
	  if ($file['img']['error'] > 0 or $file['img']['tmp_name'] == "") {

             $this->response->errors = 'Tenemos problemas al cargar el archivo';
      return $this->response->SetResponse(false);
      
    } else {
      $dir_subida = $this->dirSubida; #ruta
      $dir_server = $this->dirServer; #linux

      if ($documento <= 4) {
        $table = 'tbdocumentos_vehiculo';
      }

      if ($documento >= 5 && $documento <= 11) {
        $table = 'tbdocumentos_persona';
      }

      if($documento == 'perfil'){
        $table = 'tbpersona';
      }

      if ($documento == 'vehiculo') {
        $table = 'tbfotos_vehiculo';
      }

      #ruta del archivo
      if ($documento == 'perfil'){
        $fichero_subida = $dir_subida.'verificar/' . $table . "-" .$id."-". basename($file['img']['name']);
        $NombreArchivo = $dir_server.'verificar/' . $table . "-"  .$id."-". basename($file['img']['name']);
      }else{
        $fichero_subida = $dir_subida . $table . "-" .$id."-". basename($file['img']['name']);
        $NombreArchivo = $dir_server . $table . "-"  .$id."-". basename($file['img']['name']);
      }

      // buscar $id para saber si es update o insert
      switch ($table) {
        case 'tbdocumentos_vehiculo':
          $identificador = 'id_tbVehiculo';
          $identificador2 = 'id_tipo_documento';
          $metodo = $this->db->from($table)
                             ->select(null)
                             ->select('COUNT(*) as num')
                             ->where('id_tbVehiculo',$id)
                             ->where('id_tipo_documento',$documento)
                             ->limit(1)
                             ->fetch('num');

          $data = array('id_tipo_documento ' => $documento,
                        'id_tbVehiculo'      => $id,
                        'Documento'          => $NombreArchivo ,
                        'Status'             => 0
                      );
          break;

        case 'tbdocumentos_persona':
          $identificador = 'id_tbPersona';
          $identificador2 = 'id_tipo_documento';
          $metodo = $this->db->from($table)
                              ->select(null)
                              ->select('COUNT(*) as num')
                              ->where('id_tbPersona',$id)
                              ->where('id_tipo_documento',$documento)
                              ->limit(1)
                              ->fetch('num');

          $data = array('id_tipo_documento ' => $documento,
                        'id_tbPersona'       => $id,
                        'Documento'          => $NombreArchivo ,
                        'Status'             => 0
                      );
          break;

        case 'tbpersona':
          $identificador = 'id_tbPersona';
          $metodo = $this->db->from($table)
                              ->select(null)
                              ->select('COUNT(*) as num')
                              ->where('id_tbPersona',$id)
                              ->fetch('num');

          $data = array('idPersona' => $id,
                        'Imagen' => $NombreArchivo
                      );
          break;

        case 'tbfotos_vehiculo':
          $identificador = 'id_tbVehiculo';
          $metodo = $this->db->from($table)
                             ->select(null)
                             ->select('COUNT(*) as num')
                             ->where('id_tbVehiculo',$id)
                             ->limit(1)
                             ->fetch('num');

          if($metodo<3)$metodo = 0;
          if($metodo>=3)$metodo = 1;
          $data = array('Imagen' => $NombreArchivo,
                        'id_tbVehiculo'=>$id);
          break;
      }

      $ex = true;
      #metodo y busqueda de elementos
      switch ($metodo) {
        case 0:
          // insert
          if($table == 'tbpersona'){
            $ex = false;
            $error = 'El elemento al que hace referencia no existe  - metodo:'.$metodo;
          }else{
            $alta = $this->db->insertInto($table,$data)
                             ->execute();        
          }
          break;
       
        case 1:
          //update
          if($table == 'tbpersona'){
            $alta = $this->db->insertInto($this->tbverificarimg,$data)
                             ->execute();
          // $alta = $tbverificarimg;
          }elseif ($table == 'tbfotos_vehiculo') {
            $ex = false;
            $error = 'Ya no puede subir mas imagenes';
          }else{
            $alta = $this->db->update($table, $data)
                              ->where($identificador,$id)
                              ->where($identificador2,$documento)
                              ->execute();
          }
          break;
      }

      #alta de archivos 
      try{
        move_uploaded_file($file['img']['tmp_name'], $fichero_subida);
        if ($ex) {
                 $this->response->result = $alta;
          return $this->response->SetResponse(true);
        } else {
                 $this->response->errors = $error;
          return $this->response->SetResponse(false);
        }
      }catch(\Exception $e){
               $this->response->errors='Error encontrado: '.  $e->getMessage(). "\n";
        return $this->response->SetResponse(false);
      }
    }
  }

  #Validar y mover la imagen
  public function validarImg($idUsuario){
    $infoUser = $this->db->from($this->tbPersona)
                         ->select(null)
                         ->select('Tipo_de_usuario, Imagen, idZona, tbtoken.Token')
                         ->leftJoin('tbtoken ON tbtoken.idPersona = tbpersona.id_tbPersona')
                         ->where('id_tbPersona', $idUsuario)
                         ->fetch();

    if ($infoUser != false) {
      $urlImg = $infoUser->Imagen;
      $buscaImagenAValidar = $this->db->from($this->tbverificarimg)
                                      ->limit(1)
                                      ->where('idPersona', $idUsuario)
                                      ->orderBy('id DESC')
                                      ->fetch();

      if ($buscaImagenAValidar != false) {
        $urlImagenAValidar = $buscaImagenAValidar->Imagen;
        $imagenExplode = explode('/', $urlImagenAValidar);
        $nombreImagen = $imagenExplode[count($imagenExplode) - 1];

        $dir_subida = $this->dirSubida.'verificar/';
        $fichero_subida = $dir_subida.$nombreImagen; #Donde está alojado el archivo
        $fichero_destino = $this->dirSubida.$nombreImagen; #A donde se alojará el archivo

        if (rename($fichero_subida, $fichero_destino)) {
          if ($urlImg != null) { #Eliminar la imagen anterior que tenía.
            $urlImgExplode = explode('/', $urlImg);
            $name = $urlImgExplode[count($urlImgExplode) - 1];
            $eliminado = $this->dirSubida.$name;
            unlink($eliminado);
          }

          $dir_server = $this->dirServer.$nombreImagen;

          $updateStatus = $this->db->update($this->tbverificarimg)
                                    ->set('status', 1) #Aceptado
                                    ->where('idPersona', $idUsuario)
                                    ->execute();

          $updateUrl = $this->db->update($this->tbPersona)
                                ->set('Imagen', $dir_server)
                                ->where('id_tbPersona', $idUsuario)
                                ->execute();

          $token = $infoUser->Token;

            $dataPush = array('lubo' => 'push');
            $push = Push::FMC('Lubo','La foto de perfil se ha actualizado, para ver los cambios en tu perfil tienes que cerrar e iniciar sesion de nuevo', $token, $dataPush);

          if ($infoUser->Tipo_de_usuario == 3) {
            $idZona = $infoUser->idZona;
            $urlFirebase = CURL::updateUrlImage($idUsuario, $dir_server, $idZona);
          }

                  $this->response->result = 'Se ha actualizado tu foto de perfil';
          return $this->response->SetResponse(true); 
        }else{
               $this->response->errors = 'Tuvimos problemas al mover el archivo, reintenta.';
        return $this->response->SetResponse(false);
        }
      } else {
               $this->response->errors = 'No se encontraron registros para actualizar.';
        return $this->response->SetResponse(false);
      }
    } else {
             $this->response->errors = 'El usuario al que hace referencia no existe.';
      return $this->response->SetResponse(false);
    }
  }


  public function updateStatusDoc($id,$data){
      $idDoc = $data['idDoc'];
      $status = $data['status'];
      if ($idDoc >= 5) {
         $updateStatus = $this->db->update('tbdocumentos_persona')
                                 ->set('Status',$status)
                                 ->where("id_tbPersona",$id)
                                 ->where("id_tipo_documento",$idDoc)
                                 ->execute();
      }else if($idDoc <= 3){
        $updateStatus = $this->db->update('tbdocumentos_vehiculo')
                                 ->set('Status',$status)
                                 ->where("id_tbVehiculo",$id)
                                 ->where("id_tipo_documento",$idDoc)
                                 ->execute();
      }
               $this->response->result='Se actualizo el status';
        return $this->response->SetResponse(true);      
  }

  public function updataFechaEx($id,$data){
    $idDoc = $data['idDoc'];
    $fechaEX = $data['fechaEX'];
    if ($idDoc >= 5) {
      $updateStatus = $this->db->update('tbdocumentos_persona')
                              ->set('fechaExpiracion',$fechaEX)
                              ->where("id_tbPersona",$id)
                              ->where("id_tipo_documento",$idDoc)
                              ->execute();
   }else if($idDoc <= 3){
     $updateStatus = $this->db->update('tbdocumentos_vehiculo')
                              ->set('fechaExpiracion',$fechaEX)
                              ->where("id_tbVehiculo",$id)
                              ->where("id_tipo_documento",$idDoc)
                              ->execute();
   }
            $this->response->result='Se actualizo la fecha de Expiracion';
     return $this->response->SetResponse(true);      

  }

  public function updateFotoAuto($file,$id){
    if ($file['img']['error'] > 0 or $file['img']['tmp_name']=="") {

             $this->response->errors = 'Tenemos problemas al cargar el archivo';
      return $this->response->SetResponse(false);

    } else {
      $dir_subida = $this->dirSubida; #ruta
      $dir_server = $this->dirServer; #linux
      $table = "tbfotos_vehiculo";
      
      $obtener = $this->db->from($table)
                          ->where('id_tbFotosVehiculo',$id)
                          ->fetch();
      $idAuto = $obtener->id_tbVehiculo;
      $fichero_subida = $dir_subida . $table . "-" .$idAuto."-". basename($file['img']['name']);
      $NombreArchivo = $dir_server . $table . "-"  .$idAuto."-". basename($file['img']['name']);
      $nueva = new FluentLiteral('NOW()');
      $alta = $this->db->update($table,[
                                  "Imagen"=>$NombreArchivo,
                                  "fechaAlta"=> $nueva
                                ])
                                ->where('id_tbFotosVehiculo',$id)
                                ->execute();
      $ex = true;

      try{
        move_uploaded_file($file['img']['tmp_name'], $fichero_subida);
        if ($ex) {
                  $this->response->result = $alta;
          return $this->response->SetResponse(true);
        } else {
                  $this->response->errors = "Error";
          return $this->response->SetResponse(false);
        }
      }catch(\Exception $e){
                $this->response->errors='Error encontrado: '.  $e->getMessage(). "\n";
        return $this->response->SetResponse(false);
      } 
    }
  }

  public function rechazarImagenPerfil($idPersona){
    $registroImagen = $this->db->from($this->tbverificarimg)
                               ->select(null)
                               ->select('Imagen,id')
                               ->where('idPersona', $idPersona)
                               ->where('status = 0')
                               ->orderBy('id DESC')
                               ->limit(1)
                               ->fetch();
    if ($registroImagen!=false) {
      $imagen = $registroImagen->Imagen;
      $id = $registroImagen->id;
      $token = $this->db->from($this->tbToken)
                        ->where('idPersona',$idPersona)
                        ->fetch()
                        ->Token;

      $dataPush = array('lubo' => 'push');
      $push = Push::FMC('Lubo','La foto de perfil fue rachazada', $token, $dataPush);
            
      $urlImgExplode = explode('/', $imagen);
      $name = $urlImgExplode[count($urlImgExplode) - 1];
      $eliminado = $this->dirSubida.$name;
      unlink($eliminado);

      $eliminar = $this->db->delete($this->tbverificarimg)
                              ->where('id',$id)
                              ->execute();
      
             $this->response->errors="Imagen rechazada";
      return $this->response->SetResponse(true);
    }else{
               $this->response->errors="La imagen no existe";
        return $this->response->SetResponse(false);
    }
  }

  public function actuurl(){

    $tabla = 'tbdocumentos_persona';
    // $tabla = 'tbdocumentos_vehiculo';
    // $tabla = 'tbfotos_vehiculo';
    // $tabla = 'tbverificarimg';
    // $tabla = 'tbpersona';

    $result  = [];

    $td = $this->db->from($tabla)
                    ->select(null)
                    ->select('Documento,id_documentos_persona')
                    ->where('Documento != ""')
                    ->fetchAll();
    
    foreach ($td as $value) {
      $id = $value->id_documentos_persona;
      $imagenExplode = explode('/', $value->Documento);
      $nombreImagen = $imagenExplode[count($imagenExplode) - 1];
      $result[] = [
        "id_documentos_persona"=>"$id",
        "Documento"=>"http://api.lubo.com.mx/lubo/img/$nombreImagen"
      ];

      $data = [
        "Documento"=>"http://api.lubo.com.mx/lubo/img/$nombreImagen"
      ];

      $actualizar = $this->db->update($tabla,$data)
                             ->where('id_documentos_persona',$id)
                             ->execute();
    
    }
    
             $this->response->result = $result;
      return $this->response->SetResponse(true);

  }


}	

