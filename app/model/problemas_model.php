<?php
namespace App\Model;
use App\Lib\Response,
    App\Lib\CURL;

class ProblemasModel{
	private $db;
	private $tbPersona = 'tbpersona';
	private $tbProblema = 'tbproblemas';
	private $tbViaje = 'tbviaje';
  private $tbDetalleProblema = 'tbdetalleproblema';
  private $response;

  public function __CONSTRUCT($db){
    $this->db = $db;
    $this->response = new Response();
  }

  public function listarDetallesProblemas($tipouser, $vista, $metodoPago){
    $tipoProblema = $this->db->from($this->tbDetalleProblema)
                             ->select(null)
                             ->select('id, descripcion')
                             ->orderBy('id ASC')
                             ->where('Tipo Like ?', '%'.$tipouser.'%')
                             ->where('Vista Like ?', '%'.$vista.'%')
                             ->where('TipoPago Like ?', '%'.$metodoPago.'%')
                             ->fetchAll();

    if ($tipoProblema != false) {
      $totalTiposProblemas = count($tipoProblema);

             $this->response->result = [
               'data'  => $tipoProblema,
               'total' => $totalTiposProblemas
             ];
      return $this->response->SetResponse(true);
    } else {
             $this->response->errors='No existen detalles.';
      return $this->response->SetResponse(false);
    }
  }

  public function listarproblemas($opcional){
    switch ($opcional) {
    	case 0:
   		$data = $this->db->from($this->tbProblema)
                       ->orderBy('id DESC')
                       ->where('status',$opcional)
                       ->fetchAll();//para mas de un registro

   		$total = $this->db->from($this->tbProblema)
                        ->select('COUNT(*) Total')
                        ->fetch()
                        ->Total;

          			 $this->response->result = [
            					     'data'  => $data,
            					     'total' => $total
            					 ];
     	return $this->response->SetResponse(true);
     	  break;

     	case 1:
     		$data = $this->db->from($this->tbProblema)
                     		 ->orderBy('id DESC')
                     		 ->where('status',$opcional)
                     		 ->fetchAll();//para mas de un registro

     		$total = $this->db->from($this->tbProblema)
                      	  ->select('COUNT(*) Total')
                      	  ->fetch()
                      	  ->Total;

         		 $this->response->result = [
            					     'data'  => $data,
            					     'total' => $total
            					 ];
     	 return $this->response->SetResponse(true);
     	  break;

     	case 2:
     		$data = $this->db->from($this->tbProblema)
                      	 ->orderBy('id DESC')
                      	 ->fetchAll();//para mas de un registro

     		$total = $this->db->from($this->tbProblema)
                      	  ->select('COUNT(*) Total')
                      	  ->fetch()
                      	  ->Total;

          			 $this->response->result = [
            						 'data'  => $data,
            						 'total' => $total
            						];
     	return $this->response->SetResponse(true);
     	  break;
    }
  }

  public function registrarproblemas($data){
   	$idReporte = $data['idReporte'];
    $buscaReporte = $this->db->from($this->tbDetalleProblema)
                             ->where('id', $idReporte)
                             ->fetch();

    if ($buscaReporte != false) {
      $idUsuario = $data['idUsuario'];
      $buscaUsuario = $this->db->from($this->tbPersona)
                               ->select(null)
                               ->select('tbpersona.Tipo_de_usuario')
                               ->where('id_tbPersona', $idUsuario)
                               ->fetch();

      if ($buscaUsuario != false) {
        $idViaje = $data['idViaje'];

        $buscaViaje = $this->db->from($this->tbViaje)
                               ->select(null)
                               ->select('
                                tbviaje.idViaje,
                                tbviaje.idFirebase,
                                Status
                                ')
                               ->where('idViaje', $idViaje)
                               ->fetch();

        if ($buscaViaje != false) {
          if ($buscaViaje->Status >= 6) {
            $reporte_registrado = $this->db->from($this->tbProblema)
                                           ->select('COUNT(*) AS Total')
                                           ->where('idViaje', $idViaje)
                                           ->fetch()
                                           ->Total;

            if ($reporte_registrado == 0) {
              $name = $buscaViaje->idFirebase;

              switch ($buscaUsuario->Tipo_de_usuario) {
                case 2: #Usuario
                  $registrarProblema = $this->db->insertInto($this->tbProblema, $data)
                                                  ->execute();

                  if ($registrarProblema > 0) {
                    $reporte = $this->db->update($this->tbViaje)
                                        ->set('Reportado', 8)
                                        ->where('idViaje', $idViaje)
                                        ->execute();

                    #$updateViajeFirebase = CURL::updateStatus($name, 8);

                           $this->response->result=$registrarProblema;
                    return $this->response->SetResponse(true, 'Se ha enviado un reporte de este viaje a Soporte Lubo: lubo.com.mx');
                  } else {
                           $this->response->errors = 'Tenemos problemas al registrar el reporte, reintenta.';
                    return $this->response->SetResponse(false);
                  }
                  break;

                case 3: #Conductor
                  $registrarProblema = $this->db->insertInto($this->tbProblema, $data)
                                                ->execute();

                  if ($registrarProblema > 0) {
                    $reporte = $this->db->update($this->tbViaje)
                                        ->set('Reportado', 11)
                                        ->where('idViaje', $idViaje)
                                        ->execute();

                    #$updateViajeFirebase = CURL::updateStatus($name, 11);

                           $this->response->result=$registrarProblema;
                    return $this->response->SetResponse(true, 'Se ha enviado un reporte de este viaje a Soporte Lubo: lubo.com.mx');
                  } else {
                           $this->response->errors = 'Tenemos problemas al registrar el reporte, reintenta.';
                    return $this->response->SetResponse(false);
                  }
                  break;
                
                default:
                         $this->response->errors = 'Tipo de usuario no válido.';
                  return $this->response->SetResponse(false);
                  break;
              }
            } else {
                     $this->response->errors = 'Este viaje ya ha sido marcado como reportado, si tienes algún problema con el mismo comunícate a Soporte Lubo: lubo.com.mx con el ID de viaje: '.$idViaje;
              return $this->response->SetResponse(false);
            }
          } else {
                   $this->response->errors = 'Este viaje no puede reportarse porque aún no ha finalizado.';
            return $this->response->SetResponse(false);
          }
        } else {
                 $this->response->errors = 'El viaje al que hace referencia no existe';
          return $this->response->SetResponse(false);
        }
      } else {
               $this->response->errors = 'El usuario al que hace referencia no existe.';
        return $this->response->SetResponse(false);
      }
    } else {
             $this->response->errors = 'Este detalle de reporte no está registrado.';
      return $this->response->SetResponse(false);
    }
	}
}
?>