<?php

namespace App\Model;
use App\Lib\Response,
    App\Lib\Cifrado,
    App\Lib\CURL,
    App\Lib\Pago;

class TransaccionModel{
	private $db;
	private $tbTransaccion = 'tbtransaccion';
	private $tbViaje = 'tbviaje';
	private $tbConekta = 'tbconekta';
	private $porcentajeDriver = 0.85;
	private $response;

	public function __CONSTRUCT($db){
	    $this->db = $db;
	    $this->response = new Response();
	}

    public function listar($l,$p){
        $data = $this->db->from($this->tbTransaccion)
                         ->limit($l)
                         ->offset($p)
                         ->orderBy('idTransaccion DESC')
                         ->fetchAll();//para mas de un registro

        return [
                'data'  => $data,
                'total' => count($data)
               ];
    }

  	public function obtener($id){
	    $obtener = $this->db->from($this->tbTransaccion)
	                        ->where('idTransaccion', $id)
	                        ->fetch();

	    if ($obtener != false) {
	             $this->response->result=$obtener;
	      return $this->response->SetResponse(true);
	    }else{
	             $this->response->errors='este id no existe';
	      return $this->response->SetResponse(false);
	    }
  	}

  	public function registrarTransaccion($data){
	    $idViaje = $data['idViaje'];
	    $viaje = $this->db->from($this->tbViaje)
	                      ->select(null)
	                      ->select('tbviaje.idConductor, tbviaje.idUsuario, tbviaje.idFirebase, tbpersona.idZona')
	                      ->leftJoin('tbpersona ON tbpersona.id_tbPersona = tbviaje.idConductor')
	                      ->where('idViaje', $idViaje)
	                      ->fetch();

    	if ($viaje != false) {
    		$transaccion_registrada = $this->db->from($this->tbTransaccion)
    										   ->select(null)
    										   ->select('idTransaccion')
    										   ->where('tbtransaccion.idViaje', $idViaje)
    										   ->fetch();

    		if ($transaccion_registrada == false) {
    			$metodo = $data['metodoPago'];
	    		$propina = $data['Propina'];
	    		$monto = Round($data['Monto'], 2);
	    		$tipoTransaccion = $data['tipotransaccion'];

	    		$idDriver = $viaje->idConductor;
	    		$name = $viaje->idFirebase;
	    		$zonaDriver = $viaje->idZona;
	    		$idUser = $viaje->idUsuario;

	      		$montoMaspropina = ($monto + $propina);

		      	switch ($metodo) {
			        case 1: #Efectivo
			            $data_transaccion = array(
			              'idViaje' => $idViaje,
			              'idUsuario' => $idUser,
			              'tipotransaccion'=> $tipoTransaccion,
			              'Monto'      => $monto,
			              'Propina'=> $propina,
			              'MontoTotal'=> $montoMaspropina,
			              'FechaAlta'=> date('Y-m-d H:i:s'),
			              'FechaPago' => date('Y-m-d H:i:s'),
			              'Status' => 2,
			              'MetodoPago' => 1
			            );

			            #Actualizar las ganancias del dia
			            $ganancias = $this->actualizarGanancias($idDriver, $monto, $name, $idViaje, $data_transaccion, $zonaDriver);

			                   $this->response->result=$ganancias;
			            return $this->response->SetResponse(true, 'Transaccion registrada');

			            break;

		        	case 2: #Tarjeta
		          		$buscarUser = $this->db->from($this->tbConekta)
			                                   ->select(null)
			                                   ->select('idClienteConekta')
			                                   ->where('idPersona', $idUser)
			                                   ->fetch();

			          	if ($buscarUser != false) {
			            	$idTarjeta = $data['idTarjetaCK'];

			            	$idCustomer = $buscarUser->idClienteConekta;

			            	$montoConekta = Round(($montoMaspropina * 100), 0);

			            	$cobro = Pago::pagar($idCustomer, $montoConekta, $montoMaspropina, $idTarjeta);

			            	$payment_status_cobro = $cobro['payment_status'];
			            	
				            switch ($payment_status_cobro) {
				              	case 'L':
				                       	   $this->response->result=$cobro;
				                	return $this->response->SetResponse(false);
				                	break;

				                case 'u':
				                		   $this->response->result=$cobro;
				                	return $this->response->SetResponse(false);

				              	case 'declined':
				                	$status = 0;
				                    	   $this->response->result=$status;
				                	return $this->response->SetResponse(false, 'Transacción declinada, reintenta con otra tarjeta. Código de error: 000');
				                	break;

		              			case 'pending_payment':
		                			$status = 1;

		                       			   $this->response->result=$status;
		                			return $this->response->SetResponse(false,'La transacción no se pudo completar. Código de error: 001');
		                			break;

				              	case 'paid':
					                $data_transaccion = array(
					                  'idClienteConekta' => $idTarjeta,
					                  'idViaje' => $idViaje,
					                  'FechaAlta' =>date('Y-m-d H:i:s'),
					                  'FechaPago' =>date('Y-m-d H:i:s'),
					                  'Monto' => $monto,
					                  'Propina' => $propina,
					                  'idTransaccionConekta' => $cobro->id,
					                  'Status' => 2,
					                  'tipotransaccion'=> $data['tipotransaccion'],
					                  'MetodoPago' => 2,
					                  'idUsuario' => $idUser,
					                  'MontoTotal' => $montoMaspropina
					                );

		                			#Actualizar las ganancias del dia
		                			$ganancias = $this->actualizarGanancias($idDriver, $monto, $name, $idViaje, $data_transaccion, $zonaDriver);
		                    
					                       $this->response->result=2;
					                return $this->response->SetResponse(true, 'Transaccion registrada con éxito.');
					                break;

				              	case 'expired':
					                $status = 4;
					                       $this->response->result=$status;
					                return $this->response->SetResponse(false,'La transacción no se pudo completar. Código de error: 004');
					                break;

				              	case 'refunded':
					                $status = 5;
					                       $this->response->result=$status;
					                return $this->response->SetResponse(false,'La transacción no se pudo completar. Código de error: 005');
					                break;

				              	case 'partially_refunded':
					                $status = 6;
					                       $this->response->result=$status;
					                return $this->response->SetResponse(false,'La transacción no se pudo completar. Código de error: 006');
					                break;

				              	case 'charged_back':
					                $status = 7;
					                       $this->response->result=$status;
					                return $this->response->SetResponse(false,'La transacción no se pudo completar. Código de error: 007');
					                break;

				              	default:
					                $status = 0;
					                       $this->response->result=$status;
					                return $this->response->SetResponse(false,'La transacción no se pudo completar. Status inválido.');
					                break;
		            		}
			          	}else{
			            	return $this->response->SetResponse(false, 'No tienes tarjetas registradas.');
			          	}
			          	break; #Aquí termina metodoPago -> 2

			        default:
			          	return $this->response->SetResponse(false, 'Ingresa un método de pago válido.');
			          	break;
		      	}
    		} else {
    				   $this->response->errors = 'Ya existe una transacción para este viaje, si tienes algún problema con la misma, comunícate a lubo.com.mx con el ID '.$transaccion_registrada->idTransaccion.' para más detalles.';
    			return $this->response->SetResponse(false);
    		}
	    }else{
	    		   $this->response->errors = 'El viaje al que hace referencia no existe.';
	    	return $this->response->SetResponse(false);
	    }
  	}

  	public function actualizarGanancias($idDriver, $total_viaje, $name, $idViaje, $data_transaccion, $zonaDriver){
	    $insertarTran = $this->db->insertInto($this->tbTransaccion, $data_transaccion)
	                             ->execute();

	    #Obtener las ganancias del día
	    $informacion_driver = CURL::ObtenerViajesDriver($idDriver, $zonaDriver);

	    $parcial_double = floatval(str_replace(",", "", $informacion_driver->ganancia_Diaria));

	    $total_dia = ($total_viaje * 0.85) + $parcial_double; #Solo lo que le corresponde al conductor
	    
	    $total_dia_format = number_format($total_dia, 2,".",",");

	    #Actualizar ganancia diaria en Firebase
	    $ganancia = CURL::GananciaDiaria($idDriver, $total_dia_format, $zonaDriver);

	    #Actualizar el campo pagado en firebase y MySQL a 1
	    $pagado = CURL::updatePagado($name);

	    $pagadoSql = $this->db->update($this->tbViaje)
	                          ->set('Pagado', 1)
	                          ->where('idViaje', $idViaje)
	                          ->execute();

	    #Finalizar el viaje en Firebase
	    $updateFirebase = CURL::updateStatus($name, 7);

	    #Finalizar el viaje en MySQL
	    $viajeFinalizado = $this->db->update($this->tbViaje)
	                                ->set('Status', 7)
	                                ->where('idViaje', $idViaje)
	                                ->execute();

	    return $insertarTran;
  	}

  	#Ganancias de toda la semana
  	public function listarTransaccion($idDriver, $fecha_peticion){
  		$semana_actual = $this->db->from('tbsemana')
  			->select(null)
  			->select('idSemana')
  			->where(':peticion BETWEEN InicioSemana AND FinSemana',
  				array(':peticion' => $fecha_peticion))
  			->fetch();

  		if ($semana_actual != false) {
  			$id_semana_activa = $semana_actual->idSemana;

  			$semana_pagada = $this->db->from('tbinformacionbalance')
	            ->select(null)
	            ->select('idBalance, StatusBalance')
	            ->where('idSemana = :semana AND idConductor = :conductor',
	              array(':semana' => $id_semana_activa, ':conductor' => $idDriver))
	            ->fetch();

	        if ($semana_pagada != false) {
	            $status_balance = $semana_pagada->StatusBalance;

	            if ($status_balance != 1) {
	        	    $pagado = 'No pagado';
	            } else {
	            	$pagado = 'Pagado';
	            }
	        } else {
	        	$pagado = 'No pagado';
	        }

	    	$peticion = strtotime($fecha_peticion); #fecha de peticion para calcular dias de la semana
		    $numeroMes = substr($fecha_peticion, -5, 2); #Obtener el número del mes
		    switch ($numeroMes){
		        case 1: $mes = "Enero"; break;
		        case 2: $mes = "Febrero"; break;
		        case 3: $mes = "Marzo"; break;
		        case 4: $mes = "Abril"; break;
		        case 5: $mes = "Mayo"; break;
		        case 6: $mes = "Junio"; break;
		        case 7: $mes = "Julio"; break;
		        case 8: $mes = "Agosto"; break;
		        case 9: $mes = "Septiembre"; break;
		        case 10: $mes = "Octubre"; break;
		        case 11: $mes = "Noviembre"; break;
		        case 12: $mes = "Diciembre"; break;
		    }

		    #return $formatoPeticion;
		    $dias_semana = array(); #dias de la semana empezando con domingo
		    if (date('w', $peticion) == '1') { #Lunes
		      for ($i=0; $i < 7; $i++) {
		        $dias_semana[] = date("Y-m-d", strtotime("$fecha_peticion, + $i days"));
		      }
		    }else{
		      $inicio = strtotime('sunday this week -1 week', $peticion);
		      $inicioSemana = date('Y-m-d', $inicio); #fecha de inicio de esa semana
		      for ($i=1; $i < 8; $i++) {
		        $dias_semana[] = date("Y-m-d", strtotime("$inicioSemana + $i day"));
		      }
		    }

	    	for ($i=0; $i < 7; $i++) {
	      		$total_efectivo_tarjeta = $this->db->from($this->tbTransaccion)
	                ->select(null)
	                ->select('
	                	tbviaje.idViaje, 
	                	IFNULL(SUM(Round(tbtransaccion.Monto, 2)), 0) AS Total_dia, 
	                	IFNULL(SUM(Round(tbtransaccion.Propina, 2)), 0) AS Total_propina_dia
	                ')
	                ->leftJoin('tbviaje ON tbviaje.idViaje = tbtransaccion.idViaje')
	                ->where('tbviaje.idConductor', $idDriver)
	                ->where('tbtransaccion.FechaAlta LIKE ?','%'.$dias_semana[$i].'%')
	                ->where('tbtransaccion.Status = 2 AND tbviaje.Pagado = 1')
	                ->fetchAll();

	      		$viajes_por_dia = $this->db->from($this->tbTransaccion)
	                ->select('COUNT(*) AS Total')
	                ->leftJoin('tbviaje ON tbviaje.idViaje = tbtransaccion.idViaje')
	                ->where('tbviaje.idConductor', $idDriver)
	                ->where('tbtransaccion.FechaAlta LIKE ?','%'.$dias_semana[$i].'%')
	                ->where('tbtransaccion.Status = 2 AND tbviaje.Pagado = 1')
	                ->fetch()
	                ->Total;

	            $ganancia_diaria = Round(($total_efectivo_tarjeta[0]->Total_dia * 0.85), 2);
	            $ganancia_diaria_propina = floatval($total_efectivo_tarjeta[0]->Total_propina_dia);
	            $total_viajes_dia = floatval($viajes_por_dia);

	      		switch ($i) {
			        case 0:
			          	$semana[] = [
			          		'Valor' => $ganancia_diaria,
			                'Propina' => $ganancia_diaria_propina,
			                'Dia' => 'Lunes',
			                'Fecha' => $dias_semana[$i],
			                'TotalViajes' => $total_viajes_dia];
			          	break;

			        case 1:
			          	$semana[] = [
			          		'Valor' => $ganancia_diaria,
			          		'Propina' => $ganancia_diaria_propina,
			            	'Dia' => 'Martes',
			            	'Fecha' => $dias_semana[$i],
			            	'TotalViajes' => $total_viajes_dia];
			          	break;

	        		case 2:
	          			$semana[] = [
	          				'Valor' => $ganancia_diaria,
	                       	'Propina' => $ganancia_diaria_propina,
	                       	'Dia' => 'Miercoles',
	                       	'Fecha' => $dias_semana[$i],
	                       	'TotalViajes' => $total_viajes_dia];
	          			break;

	        		case 3:
	          			$semana[] = [
	          				'Valor' => $ganancia_diaria,
	                       	'Propina' => $ganancia_diaria_propina,
	                       	'Dia' => 'Jueves',
	                       	'Fecha' => $dias_semana[$i],
	                       	'TotalViajes' => $total_viajes_dia];
	          			break;

	        		case 4:
	          			$semana[] = [
	          				'Valor' => $ganancia_diaria,
	                        'Propina' => $ganancia_diaria_propina,
	                        'Dia' => 'Viernes',
	                        'Fecha' => $dias_semana[$i],
	                        'TotalViajes' => $total_viajes_dia];
	          			break;

	        		case 5:
	          			$semana[] = [
	          				'Valor' => $ganancia_diaria,
	                        'Propina' => $ganancia_diaria_propina,
	                        'Dia' => 'Sabado',
	                        'Fecha' => $dias_semana[$i],
	                        'TotalViajes' => $total_viajes_dia];
	          			break;

	        		case 6:
	          			$semana[] = [
	          				'Valor' => $ganancia_diaria,
	                        'Propina' => $ganancia_diaria_propina,
	                        'Dia' => 'Domingo',
	                        'Fecha' => $dias_semana[$i],
	                        'TotalViajes' => $total_viajes_dia];
	          			break;
	      		}

	      		$total_por_semana = Round($total_por_semana + ($ganancia_diaria + $ganancia_diaria_propina), 2);
	    	}

	    	$fecha_dia_inicio = substr($semana[0]['Fecha'], -2);
	    	$fecha_dia_fin = substr($semana[6]['Fecha'], -2);
	    	$fecha_mes_fin = substr($semana[6]['Fecha'], -5, 2);

	    	switch ($fecha_mes_fin){
		        case 1: $mes_fin = "Enero"; break;
		        case 2: $mes_fin = "Febrero"; break;
		        case 3: $mes_fin = "Marzo"; break;
		        case 4: $mes_fin = "Abril"; break;
		        case 5: $mes_fin = "Mayo"; break;
		        case 6: $mes_fin = "Junio"; break;
		        case 7: $mes_fin = "Julio"; break;
		        case 8: $mes_fin = "Agosto"; break;
		        case 9: $mes_fin = "Septiembre"; break;
		        case 10: $mes_fin = "Octubre"; break;
		        case 11: $mes_fin = "Noviembre"; break;
		        case 12: $mes_fin = "Diciembre"; break;
		    }

	    	$fecha = $mes.' '.$fecha_dia_inicio.' - '.$mes_fin.' '.$fecha_dia_fin;

	           	   $this->response->result=[
	           	   	'Mes' => $mes,
	           	   	'Fecha' => $fecha,
	           	   	'Semana' => $semana,
	                'Total' => $total_por_semana,
	                'Pagado' => $pagado
	               ];
	    	return $this->response->SetResponse(true);
  		} else {
  				   $this->response->errors = 'La semana que has consultado aún no está registrada, contacta a Soporte Lubo:lubo.com.mx, para obtener más detalles.';
  			return $this->response->SetResponse(false);
  		}
  	}

  	public function detalleGananciasDiarios($idDriver, $fechaPeticion){
  		$semana_actual = $this->db->from('tbsemana')
  			->select(null)
  			->select('idSemana')
  			->where(':peticion BETWEEN InicioSemana AND FinSemana',
  				array(':peticion' => $fechaPeticion))
  			->fetch();

  		if ($semana_actual != false) {
  			$id_semana_activa = $semana_actual->idSemana;

  			$semana_pagada = $this->db->from('tbinformacionbalance')
	            ->select(null)
	            ->select('idBalance, StatusBalance')
	            ->where('idSemana = :semana AND idConductor = :conductor',
	              array(':semana' => $id_semana_activa, ':conductor' => $idDriver))
	            ->fetch();

	        if ($semana_pagada != false) {
	            $status_balance = $semana_pagada->StatusBalance;

	            if ($status_balance != 1) {
	        	    $pagado = 'No pagado';
	            } else {
	            	$pagado = 'Pagado';
	            }
	        } else {
	        	$pagado = 'No pagado';
	        }

	        $numeroMes = substr($fechaPeticion, -5, 2);
		    $numeroDia = substr($fechaPeticion, -2);

		    switch ($numeroMes){
		        case 1: $mes = "Enero"; break;
		        case 2: $mes = "Febrero"; break;
		        case 3: $mes = "Marzo"; break;
		        case 4: $mes = "Abril"; break;
		        case 5: $mes = "Mayo"; break;
		        case 6: $mes = "Junio"; break;
		        case 7: $mes = "Julio"; break;
		        case 8: $mes = "Agosto"; break;
		        case 9: $mes = "Septiembre"; break;
		        case 10: $mes = "Octubre"; break;
		        case 11: $mes = "Noviembre"; break;
		        case 12: $mes = "Diciembre"; break;
		    }

	    	$listaViajes = $this->db->from($this->tbTransaccion)
	            ->select(null)
	            ->select('
	            	IFNULL(tbviaje.idViaje, "Sin detalle") AS idViaje, 
	            	IFNULL(tbviaje.FechaInicio, "Sin detalle") AS FechaInicio, 
	            	IFNULL(tbmodotrabajo.Nombre, "Sin detalle") AS Nombre, 
	            	IFNULL(Round(tbtransaccion.MontoTotal * 0.85, 2), 0) AS Monto
	            ')
	            ->leftJoin('tbviaje ON tbviaje.idViaje = tbtransaccion.idViaje')
	            ->leftJoin('tbvehiculo ON tbvehiculo.id_tbVehiculo = tbviaje.idVehiculo')
	            ->leftJoin('tbmodotrabajo ON tbmodotrabajo.idModoTrabajo = tbvehiculo.idModoTrabajo')
	            ->where('tbviaje.idConductor', $idDriver)
	            ->where('tbtransaccion.FechaAlta LIKE ?','%'.$fechaPeticion.'%')
	            ->where('tbtransaccion.Status = 2 AND tbviaje.Pagado = 1')
	            ->orderBy('idTransaccion DESC')
	            ->fetchAll();

	    	if ($listaViajes != false) {
	      		for ($i=0; $i < count($listaViajes) ; $i++) {
	      			if ($listaViajes[$i]->FechaInicio != 'Sin detalle') {
	      				$listaViajes[$i]->FechaInicio = date('H:i:s', strtotime($listaViajes[$i]->FechaInicio));
	      			}
	      		}

	      		$totalPropinas = $this->db->from($this->tbTransaccion)
	                ->select(null)
	                ->select('
	                	IFNULL(Round(SUM(tbtransaccion.Propina * 0.85), 2), 0) AS TotalPropina, 
	                	IFNULL(Round(SUM(tbtransaccion.Monto * 0.85), 2), 0) AS Total
	                ')
	                ->leftJoin('tbviaje ON tbviaje.idViaje = tbtransaccion.idViaje')
	                ->where('tbviaje.idConductor', $idDriver)
	                ->where('tbtransaccion.FechaAlta LIKE ?','%'.$fechaPeticion.'%')
	                ->where('tbtransaccion.Status = 2 AND tbviaje.Pagado = 1')
	                ->orderBy('idTransaccion DESC')
	                ->fetchAll();

	      		$totalViajes = count($listaViajes);

		               $this->response->result=[
		                	'DiaSemana' => $mes." ". $numeroDia,
		                	'detallesViaje' => $listaViajes,
		                	'TotalViajes' => $totalViajes,
		                	'TotalAproximado' => floatval($totalPropinas[0]->Total),
		                	'TotalPropinas' => floatval($totalPropinas[0]->TotalPropina),
		                	'Pagado' => $pagado
		               ];
	      		return $this->response->SetResponse(true);
		    } else {
		      return $this->response->SetResponse(false, 'No tienes registro de viajes hoy.');
		    }
  		} else {
  				   $this->response->errors = 'El día que ha consultado no se encuentra dentro de una semana registrada, contacta a Soporte Lubo:lubo.com.mx, para obtener más detalles.';
  			return $this->response->SetResponse(false);
  		}
  	}

  	public function actualizarstatus($data,$id){
	    $actualizar = $this->db->update($this->tbTransaccion,$data)
	                           ->where('idTransaccion',$id)
	                           ->execute();

	           $this->response->result=$actualizar;
	    return $this->response->SetResponse(true);
  	}

  	public function actualizar($data,$id){
	    $actualizar = $this->db->update($this->tbTransaccion, $data)
	                           ->where('idTransaccion',$id)
	                           ->execute();

	           $this->response->result=$actualizar;
	    return $this->response->SetResponse(true);
  	}

  	public function eliminar($id){
	    $eliminar = $this->db->deleteFrom($this->tbTransaccion)
	                         ->where('id',$id)
	                         ->execute();

	           $this->response->result=$eliminar;
	    return $this->response->SetResponse(true);
  	}

  	#Ganancias del día
  	public function gananciasDia($idUser, $fechaPeticion){
	    $consulta = $this->db->from($this->tbTransaccion)
	        ->select('IFNULL(Round(SUM(tbtransaccion.Monto * 0.85), 2), 0) AS TotalDia')
	        ->leftJoin('tbviaje ON tbviaje.idViaje = tbtransaccion.idViaje')
	        ->where('tbviaje.idConductor', $idUser)
	        ->where('(tbtransaccion.FechaAlta LIKE ?)','%'.$fechaPeticion.'%')
	        ->where('tbtransaccion.Status = 2 AND tbviaje.Pagado = 1')
	        ->fetchAll();

	    $total = floatval($consulta[0]->TotalDia);

	           $this->response->result=$total;
	    return $this->response->SetResponse(true);
  	}
}
?>