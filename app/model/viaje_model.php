<?php
namespace App\Model;

use App\Lib\Response,
    App\Lib\Cifrado,
    App\Lib\Pago,
    App\Lib\CURL,
    App\Lib\Push;

require_once __DIR__ . '/../lib/conekta-php-master/lib/Conekta.php';
\Conekta\Conekta::setApiKey("key_bFgzHopChrHtt4u4rf7uqg");#prod
// \Conekta\Conekta::setApiKey("key_yawNuPVcAsFs2bfJsCpVCQ");#dev
\Conekta\Conekta::setApiVersion("2.0.0");

class ViajeModel{
  private $db;
  private $tbViaje = 'tbviaje';
  private $tbPersona = 'tbpersona';
  private $tbVehiculo = 'tbvehiculo';
  private $tbConekta = 'tbconekta';
  private $tbTransaccion = 'tbtransaccion';
  private $tbDireccion = 'tbdireccion';
  private $tbDetalleCancelacion = 'tbdetallecancelacion';
  private $tbViajesCancelados = 'tbviajescancelados';
  private $response;

  public function __CONSTRUCT($db){
    $this->db = $db;
    $this->response = new Response();
  }
  
  #móvil
  public function listarViajes($limite, $paginacion, $idUsuario, $tipoViaje){
    switch ($tipoViaje) {
      case 7:
        $viajes = $this->db->from($this->tbViaje)
          ->select(null)
          ->select('
            tbviaje.idViaje, 
            tbviaje.FechaSolicitud, IFNULL(tbviaje.FechaInicio, "Sin detalle") AS FechaInicio, IFNULL(tbviaje.FechaFin, "Sin detalle") AS FechaFin, tbviaje.Status, tbviaje.Pagado,IFNULL(tbviaje.idMetodoPago, "Sin detalle") AS idMetodoPago, IFNULL(tbtransaccion.Status, "Sin detalle") AS EstadoTransaccion, IFNULL(orig.Latitud, "Sin detalle") AS LatitudOrigen, IFNULL(orig.Longitud, "Sin detalle") AS LongitudOrigen, IFNULL(orig.Calle, "Sin detalle") AS CalleOrigen, IFNULL(orig.Colonia, "Sin detalle") AS ColoniaOrigen, IFNULL(orig.Ciudad, "Sin detalle") AS CiudadOrigen, IFNULL(orig.Numero_Exterior, "Sin detalle") AS NumExtOrigen, IFNULL(dest.Latitud, "Sin detalle") AS LatitudDestino, IFNULL(dest.Longitud, "Sin detalle") AS LongitudDestino, IFNULL(dest.Calle, "Sin detalle") AS CalleDestino, IFNULL(dest.Colonia, "Sin detalle") AS ColoniaDestino, IFNULL(dest.Ciudad, "Sin detalle") AS CiudadDestino, IFNULL(dest.Numero_Exterior, "Sin detalle") AS NumExtDestino, IFNULL(tbvehiculo.Modelo, "Sin detalle") AS Modelo, IFNULL(tbvehiculo.Placa, "Sin detalle") AS Placa,IFNULL(tbviaje.idConductor, "Sin detalle") AS idConductor, IFNULL(tbConductor.Nombre, "Sin detalle") AS Conductor, IFNULL(tbCalifica.Calificacion, "Sin detalle") AS CalificacionDriver, IFNULL(tbviaje.idUsuario, "Sin detalle") AS idUsuario, IFNULL(tbUsuario.Nombre, "Sin detalle") AS Usuario, IFNULL(tbCalificado.Calificacion, "Sin detalle") AS CalificacionUsuario, IFNULL(tbtransaccion.idClienteConekta, "Sin detalle") AS idTarjeta, IFNULL(tbconekta.idClienteConekta, "Sin detalle") AS ClienteConekta,IFNULL(Round(tbtransaccion.MontoTotal * 0.85, 2), 0) AS GananciaConductor, IFNULL(tbtransaccion.MontoTotal, "0") AS TotalViaje')

            ->leftJoin('tbdireccion AS orig ON orig.id_tbDireccion = tbviaje.idOrigen')
            ->leftJoin('tbdireccion AS dest ON dest.id_tbDireccion = tbviaje.idDestino')
            ->leftJoin('tbvehiculo ON tbvehiculo.id_tbVehiculo = tbviaje.idVehiculo')
            ->leftJoin('tbtransaccion ON tbtransaccion.idViaje = tbviaje.idViaje')
            ->leftJoin('tbconekta ON tbconekta.idPersona = tbviaje.idUsuario')
            ->leftJoin('tbcalificacion As tbCalificado ON (tbCalificado.Califica = tbviaje.idUsuario AND tbCalificado.idViaje = tbviaje.idViaje)')
            ->leftJoin('tbcalificacion AS tbCalifica ON (tbCalifica.Califica = tbviaje.idConductor AND tbCalifica.idViaje = tbviaje.idViaje)')
            ->leftJoin('tbpersona AS tbConductor ON tbConductor.id_tbPersona = tbviaje.idConductor')
            ->leftJoin('tbpersona AS tbUsuario ON tbUsuario.id_tbPersona = tbviaje.idUsuario')

            ->where('(
              (tbviaje.idUsuario = :iduser OR tbviaje.idConductor = :idconductor) 
              AND 
              (tbviaje.Status = :status AND tbviaje.Reportado = 0) AND tbviaje.Pagado = 1
            )', 
            array(':iduser' => $idUsuario, ':idconductor' => $idUsuario, ':status' => $tipoViaje))
            ->limit($limite)
            ->offset($paginacion)
            ->orderBy('idViaje DESC')
            ->fetchAll();

        if ($viajes != false) {
          for ($i=0; $i < count($viajes) ; $i++) {
            $viajes[$i]->FechaSolicitud = date('d/m/Y H:i:s', strtotime($viajes[$i]->FechaSolicitud));
            $viajes[$i]->FechaInicio = date('d-m-Y H:i:s', strtotime($viajes[$i]->FechaInicio));
            $viajes[$i]->Tipo = '7';
          }

          $totalViajes = $this->db->from($this->tbViaje)
            ->select('COUNT(*) as  Total')
            ->where('(
              (tbviaje.idUsuario = :iduser OR tbviaje.idConductor = :idconductor) 
              AND 
              (tbviaje.Status = :status AND tbviaje.Reportado = 0) AND tbviaje.Pagado = 1
            )', 
            array(':iduser' => $idUsuario, ':idconductor' => $idUsuario, ':status' => $tipoViaje))
            ->fetch()
            ->Total;

                 $this->response->result=[
                  'Viajes' => $viajes,
                  'Total' => $totalViajes
                ];
          return $this->response->SetResponse(true);
        }else{
                 $this->response->errors='No tienes viajes en tu historial.';
          return $this->response->SetResponse(false);
        }
        break;

      case 8:
        $status = "11";
        $viajes = $this->db->from($this->tbViaje)
          ->select(null)
          ->select('
            tbviaje.idViaje, 
            tbviaje.FechaSolicitud, IFNULL(tbviaje.FechaInicio, "Sin detalle") AS FechaInicio, IFNULL(tbviaje.FechaFin, "Sin detalle") AS FechaFin, tbviaje.Status, tbviaje.Pagado,IFNULL(tbviaje.idMetodoPago, "Sin detalle") AS idMetodoPago, IFNULL(tbtransaccion.Status, "Sin detalle") AS EstadoTransaccion, IFNULL(orig.Latitud, "Sin detalle") AS LatitudOrigen, IFNULL(orig.Longitud, "Sin detalle") AS LongitudOrigen, IFNULL(orig.Calle, "Sin detalle") AS CalleOrigen, IFNULL(orig.Colonia, "Sin detalle") AS ColoniaOrigen, IFNULL(orig.Ciudad, "Sin detalle") AS CiudadOrigen, IFNULL(orig.Numero_Exterior, "Sin detalle") AS NumExtOrigen, IFNULL(dest.Latitud, "Sin detalle") AS LatitudDestino, IFNULL(dest.Longitud, "Sin detalle") AS LongitudDestino, IFNULL(dest.Calle, "Sin detalle") AS CalleDestino, IFNULL(dest.Colonia, "Sin detalle") AS ColoniaDestino, IFNULL(dest.Ciudad, "Sin detalle") AS CiudadDestino, IFNULL(dest.Numero_Exterior, "Sin detalle") AS NumExtDestino, IFNULL(tbvehiculo.Modelo, "Sin detalle") AS Modelo, IFNULL(tbvehiculo.Placa, "Sin detalle") AS Placa,IFNULL(tbviaje.idConductor, "Sin detalle") AS idConductor, IFNULL(tbConductor.Nombre, "Sin detalle") AS Conductor, IFNULL(tbCalifica.Calificacion, "Sin detalle") AS CalificacionDriver, IFNULL(tbviaje.idUsuario, "Sin detalle") AS idUsuario, IFNULL(tbUsuario.Nombre, "Sin detalle") AS Usuario, IFNULL(tbCalificado.Calificacion, "Sin detalle") AS CalificacionUsuario, IFNULL(tbtransaccion.idClienteConekta, "Sin detalle") AS idTarjeta, IFNULL(tbconekta.idClienteConekta, "Sin detalle") AS ClienteConekta,IFNULL(Round(tbtransaccion.MontoTotal * 0.85, 2), 0) AS GananciaConductor, IFNULL(tbtransaccion.MontoTotal, "0") AS TotalViaje')

            ->leftJoin('tbdireccion AS orig ON orig.id_tbDireccion = tbviaje.idOrigen')
            ->leftJoin('tbdireccion AS dest ON dest.id_tbDireccion = tbviaje.idDestino')
            ->leftJoin('tbvehiculo ON tbvehiculo.id_tbVehiculo = tbviaje.idVehiculo')
            ->leftJoin('tbtransaccion ON tbtransaccion.idViaje = tbviaje.idViaje')
            ->leftJoin('tbconekta ON tbconekta.idPersona = tbviaje.idUsuario')
            ->leftJoin('tbcalificacion As tbCalificado ON (tbCalificado.Califica = tbviaje.idUsuario AND tbCalificado.idViaje = tbviaje.idViaje)')
            ->leftJoin('tbcalificacion AS tbCalifica ON (tbCalifica.Califica = tbviaje.idConductor AND tbCalifica.idViaje = tbviaje.idViaje)')
            ->leftJoin('tbpersona AS tbConductor ON tbConductor.id_tbPersona = tbviaje.idConductor')
            ->leftJoin('tbpersona AS tbUsuario ON tbUsuario.id_tbPersona = tbviaje.idUsuario')

            ->where('(
              (tbviaje.idUsuario = :iduser OR tbviaje.idConductor = :idconductor) 
              AND 
              ((tbviaje.Reportado = :tipo OR tbviaje.Reportado = :status) AND tbviaje.Status > 0)
            )', 
            array(':iduser' => $idUsuario, ':idconductor' => $idUsuario, ':status' => $status, ':tipo' => $tipoViaje))
            ->limit($limite)
            ->offset($paginacion)
            ->orderBy('idViaje DESC')
            ->fetchAll();

        if ($viajes != false) {
          for ($i=0; $i < count($viajes) ; $i++) {
            $viajes[$i]->FechaSolicitud = date('d/m/Y H:i:s', strtotime($viajes[$i]->FechaSolicitud));
            $viajes[$i]->FechaInicio = date('d-m-Y H:i:s', strtotime($viajes[$i]->FechaInicio));
            $viajes[$i]->Tipo = '8';
          }

          $totalViajes = $this->db->from($this->tbViaje)
            ->select('COUNT(*) as  Total')
            ->where('(
              (tbviaje.idUsuario = :iduser OR tbviaje.idConductor = :idconductor) 
              AND 
              ((tbviaje.Reportado = :tipo OR tbviaje.Reportado = :status) AND tbviaje.Status > 0)
            )',
            array(':iduser' => $idUsuario, ':idconductor' => $idUsuario, ':status' => $status, ':tipo' => $tipoViaje))
            ->fetch()
            ->Total;

                 $this->response->result=[
                  'Viajes' => $viajes, 
                  'Total' => $totalViajes
                ];
          return $this->response->SetResponse(true);
        }else{
                 $this->response->errors='No tienes viajes reportados en tu historial.';
          return $this->response->SetResponse(false);
        }
        break;

      case 9:
        $status = "10";
        $viajes = $this->db->from($this->tbViaje)
          ->select(null)
          ->select('
            tbviaje.idViaje, 
            tbviaje.FechaSolicitud, IFNULL(tbviaje.FechaInicio, "Sin detalle") AS FechaInicio, IFNULL(tbviaje.FechaFin, "Sin detalle") AS FechaFin, tbviaje.Status, tbviaje.Pagado,IFNULL(tbviaje.idMetodoPago, "Sin detalle") AS idMetodoPago, IFNULL(tbtransaccion.Status, "Sin detalle") AS EstadoTransaccion, IFNULL(orig.Latitud, "Sin detalle") AS LatitudOrigen, IFNULL(orig.Longitud, "Sin detalle") AS LongitudOrigen, IFNULL(orig.Calle, "Sin detalle") AS CalleOrigen, IFNULL(orig.Colonia, "Sin detalle") AS ColoniaOrigen, IFNULL(orig.Ciudad, "Sin detalle") AS CiudadOrigen, IFNULL(orig.Numero_Exterior, "Sin detalle") AS NumExtOrigen, IFNULL(dest.Latitud, "Sin detalle") AS LatitudDestino, IFNULL(dest.Longitud, "Sin detalle") AS LongitudDestino, IFNULL(dest.Calle, "Sin detalle") AS CalleDestino, IFNULL(dest.Colonia, "Sin detalle") AS ColoniaDestino, IFNULL(dest.Ciudad, "Sin detalle") AS CiudadDestino, IFNULL(dest.Numero_Exterior, "Sin detalle") AS NumExtDestino, IFNULL(tbvehiculo.Modelo, "Sin detalle") AS Modelo, IFNULL(tbvehiculo.Placa, "Sin detalle") AS Placa,IFNULL(tbviaje.idConductor, "Sin detalle") AS idConductor, IFNULL(tbConductor.Nombre, "Sin detalle") AS Conductor, IFNULL(tbCalifica.Calificacion, "Sin detalle") AS CalificacionDriver, IFNULL(tbviaje.idUsuario, "Sin detalle") AS idUsuario, IFNULL(tbUsuario.Nombre, "Sin detalle") AS Usuario, IFNULL(tbCalificado.Calificacion, "Sin detalle") AS CalificacionUsuario, IFNULL(tbtransaccion.idClienteConekta, "Sin detalle") AS idTarjeta, IFNULL(tbconekta.idClienteConekta, "Sin detalle") AS ClienteConekta,IFNULL(Round(tbtransaccion.MontoTotal * 0.85, 2), 0) AS GananciaConductor, IFNULL(Round(tbtransaccion.MontoTotal, 2), "0") AS TotalViaje, tbviaje.Excedido')

            ->leftJoin('tbdireccion AS orig ON orig.id_tbDireccion = tbviaje.idOrigen')
            ->leftJoin('tbdireccion AS dest ON dest.id_tbDireccion = tbviaje.idDestino')
            ->leftJoin('tbvehiculo ON tbvehiculo.id_tbVehiculo = tbviaje.idVehiculo')
            ->leftJoin('tbtransaccion ON tbtransaccion.idViaje = tbviaje.idViaje')
            ->leftJoin('tbconekta ON tbconekta.idPersona = tbviaje.idUsuario')
            ->leftJoin('tbcalificacion As tbCalificado ON (tbCalificado.Califica = tbviaje.idUsuario AND tbCalificado.idViaje = tbviaje.idViaje)')
            ->leftJoin('tbcalificacion AS tbCalifica ON (tbCalifica.Califica = tbviaje.idConductor AND tbCalifica.idViaje = tbviaje.idViaje)')
            ->leftJoin('tbpersona AS tbConductor ON tbConductor.id_tbPersona = tbviaje.idConductor')
            ->leftJoin('tbpersona AS tbUsuario ON tbUsuario.id_tbPersona = tbviaje.idUsuario')

            ->where('(
              (tbviaje.idUsuario = :iduser OR tbviaje.idConductor = :idconductor) 
              AND 
              ((tbviaje.Cancelado = :tipo OR tbviaje.Cancelado = :status) AND tbviaje.Status > 0 AND tbviaje.Excedido = 1)
            )', 
              array(':iduser' => $idUsuario, ':idconductor' => $idUsuario, ':status' => $status, ':tipo' => $tipoViaje))
            ->limit($limite)
            ->offset($paginacion)
            ->orderBy('idViaje DESC')
            ->fetchAll();

        if ($viajes != false) {
          for ($i=0; $i < count($viajes) ; $i++) {
            $viajes[$i]->FechaSolicitud = date('d/m/Y H:i:s', strtotime($viajes[$i]->FechaSolicitud));
            if ($viajes[$i]->FechaInicio != 'Sin detalle') {
              $viajes[$i]->FechaInicio = date('d-m-Y H:i:s', strtotime($viajes[$i]->FechaInicio));
            }
            $viajes[$i]->Tipo = '9';
          }

          $totalViajes = $this->db->from($this->tbViaje)
            ->select('COUNT(*) as  Total')
            ->where('((tbviaje.idUsuario = :iduser OR tbviaje.idConductor = :idconductor) AND ((tbviaje.Cancelado = :tipo OR tbviaje.Cancelado = :status) AND tbviaje.Status > 0 AND tbviaje.Excedido = 1))', array(':iduser' => $idUsuario, ':idconductor' => $idUsuario, ':status' => $status, ':tipo' => $tipoViaje))
            ->fetch()
            ->Total;

                 $this->response->result=[
                  'Viajes' => $viajes, 
                  'Total' => $totalViajes
                ];
          return $this->response->SetResponse(true);
        }else{
                 $this->response->errors='No tienes viajes cancelados en tu historial.';
          return $this->response->SetResponse(false);
        }
        break;
      
      default:
               $this->response->errors='Verifica los parámetros de solicitud y reintenta.';
        return $this->response->SetResponse(false);
        break;
    }
  }

  #web
  public function historialViajes($limite, $paginacion, $idUsuario, $tipoViaje) {
    switch ($tipoViaje) {
      case 7:
        $viajes = $this->db->from($this->tbViaje)
          ->select(null)
          ->select('
            tbviaje.idViaje, 
            tbviaje.FechaSolicitud, IFNULL(tbviaje.FechaInicio, "Sin detalle") AS FechaInicio, IFNULL(tbviaje.FechaFin, "Sin detalle") AS FechaFin, tbviaje.Status, tbviaje.Pagado, IFNULL(tbviaje.idMetodoPago, "Sin detalle") AS idMetodoPago, IFNULL(tbtransaccion.Status, "Sin detalle") AS EstadoTransaccion, IFNULL(orig.Latitud, "Sin detalle") AS LatitudOrigen, IFNULL(orig.Longitud, "Sin detalle") AS LongitudOrigen, IFNULL(orig.Calle, "Sin detalle") AS CalleOrigen, IFNULL(orig.Colonia, "Sin detalle") AS ColoniaOrigen, IFNULL(orig.Ciudad, "Sin detalle") AS CiudadOrigen, IFNULL(orig.Numero_Exterior, "Sin detalle") AS NumExtOrigen, IFNULL(dest.Latitud, "Sin detalle") AS LatitudDestino, IFNULL(dest.Longitud, "Sin detalle") AS LongitudDestino, IFNULL(dest.Calle, "Sin detalle") AS CalleDestino, IFNULL(dest.Colonia, "Sin detalle") AS ColoniaDestino, IFNULL(dest.Ciudad, "Sin detalle") AS CiudadDestino, IFNULL(dest.Numero_Exterior, "Sin detalle") AS NumExtDestino, IFNULL(tbvehiculo.Modelo, "Sin detalle") AS Modelo, IFNULL(tbvehiculo.Placa, "Sin detalle") AS Placa, 
            IFNULL(tbviaje.idConductor, "Sin detalle") AS idConductor, 
            IFNULL(tbConductor.Nombre, "Sin detalle") AS Conductor,
            IFNULL(tbmodotrabajo.Nombre, "Sin detalle") AS TipoServicio, 
            IFNULL(tbCalifica.Calificacion, "Sin detalle") AS CalificacionDriver, IFNULL(tbviaje.idUsuario, "Sin detalle") AS idUsuario, IFNULL(tbUsuario.Nombre, "Sin detalle") AS Usuario, IFNULL(tbCalificado.Calificacion, "Sin detalle") AS CalificacionUsuario, IFNULL(tbtransaccion.idClienteConekta, "Sin detalle") AS idTarjeta, IFNULL(tbconekta.idClienteConekta, "Sin detalle") AS ClienteConekta, IFNULL(Round(tbtransaccion.MontoTotal * 0.85, 2), 0) AS GananciaConductor, IFNULL(tbtransaccion.MontoTotal, "0") AS TotalViaje, IFNULL(tbtarjetas.last4, "Sin detalle") AS Ultimos_4_digitos, IFNULL(tbtarjetas.brand, "Sin detalle") AS Brand'
          )

            ->leftJoin('tbdireccion AS orig ON orig.id_tbDireccion = tbviaje.idOrigen')
            ->leftJoin('tbdireccion AS dest ON dest.id_tbDireccion = tbviaje.idDestino')
            ->leftJoin('tbvehiculo ON tbvehiculo.id_tbVehiculo = tbviaje.idVehiculo')
            ->leftJoin('tbmodotrabajo ON tbmodotrabajo.idModoTrabajo = tbvehiculo.idModoTrabajo')
            ->leftJoin('tbtransaccion ON tbtransaccion.idViaje = tbviaje.idViaje')
            ->leftJoin('tbconekta ON tbconekta.idPersona = tbviaje.idUsuario')
            ->leftJoin('tbcalificacion As tbCalificado ON (tbCalificado.Califica = tbviaje.idUsuario AND tbCalificado.idViaje = tbviaje.idViaje)')
            ->leftJoin('tbcalificacion AS tbCalifica ON (tbCalifica.Califica = tbviaje.idConductor AND tbCalifica.idViaje = tbviaje.idViaje)')
            ->leftJoin('tbpersona AS tbConductor ON tbConductor.id_tbPersona = tbviaje.idConductor')
            ->leftJoin('tbpersona AS tbUsuario ON tbUsuario.id_tbPersona = tbviaje.idUsuario')
            ->leftJoin('tbtarjetas ON tbtarjetas.idTarjetaConekta = tbtransaccion.idClienteConekta')

            ->where('(
              (tbviaje.idUsuario = :iduser OR tbviaje.idConductor = :idconductor) 
              AND 
              (
                (tbviaje.Status = :status) AND tbviaje.Reportado = 0) AND tbviaje.Pagado = 1
              )', 
              array(':iduser' => $idUsuario, ':idconductor' => $idUsuario, ':status' => $tipoViaje))
            ->limit($limite)
            ->offset($paginacion)
            ->orderBy('idViaje DESC')
            ->fetchAll();

        if ($viajes != false) {
          for ($i=0; $i < count($viajes) ; $i++) {
            $viajes[$i]->Tipo = '7';
          }

          $totalViajes = $this->db->from($this->tbViaje)
            ->select('COUNT(*) as  Total')
            ->where('(
              (tbviaje.idUsuario = :iduser OR tbviaje.idConductor = :idconductor) 
              AND 
              (tbviaje.Status = :status AND tbviaje.Reportado = 0) AND tbviaje.Pagado = 1
            )', 
            array(':iduser' => $idUsuario, ':idconductor' => $idUsuario, ':status' => $tipoViaje))
            ->fetch()
            ->Total;

                 $this->response->result=[
                  'Viajes' => $viajes, 
                  'Total' => $totalViajes
                ];
          return $this->response->SetResponse(true);
        }else{
                 $this->response->errors='No tienes viajes en tu historial.';
          return $this->response->SetResponse(false);
        }
        break;

      case 8:
        $viajes = $this->db->from($this->tbViaje)
          ->select(null)
          ->select('
            tbviaje.idViaje, 
            tbviaje.FechaSolicitud, IFNULL(tbviaje.FechaInicio, "Sin detalle") AS FechaInicio, IFNULL(tbviaje.FechaFin, "Sin detalle") AS FechaFin, tbviaje.Status, tbviaje.Pagado,IFNULL(tbviaje.idMetodoPago, "Sin detalle") AS idMetodoPago, IFNULL(tbtransaccion.Status, "Sin detalle") AS EstadoTransaccion, IFNULL(orig.Latitud, "Sin detalle") AS LatitudOrigen, IFNULL(orig.Longitud, "Sin detalle") AS LongitudOrigen, IFNULL(orig.Calle, "Sin detalle") AS CalleOrigen, IFNULL(orig.Colonia, "Sin detalle") AS ColoniaOrigen, IFNULL(orig.Ciudad, "Sin detalle") AS CiudadOrigen, IFNULL(orig.Numero_Exterior, "Sin detalle") AS NumExtOrigen, IFNULL(dest.Latitud, "Sin detalle") AS LatitudDestino, IFNULL(dest.Longitud, "Sin detalle") AS LongitudDestino, IFNULL(dest.Calle, "Sin detalle") AS CalleDestino, IFNULL(dest.Colonia, "Sin detalle") AS ColoniaDestino, IFNULL(dest.Ciudad, "Sin detalle") AS CiudadDestino, IFNULL(dest.Numero_Exterior, "Sin detalle") AS NumExtDestino, IFNULL(tbvehiculo.Modelo, "Sin detalle") AS Modelo, IFNULL(tbvehiculo.Placa, "Sin detalle") AS Placa, 
            IFNULL(tbviaje.idConductor, "Sin detalle") AS idConductor, 
            IFNULL(tbConductor.Nombre, "Sin detalle") AS Conductor, 
            IFNULL(tbmodotrabajo.Nombre, "Sin detalle") AS TipoServicio, 
            IFNULL(tbCalifica.Calificacion, "Sin detalle") AS CalificacionDriver, 
            IFNULL(tbviaje.idUsuario, "Sin detalle") AS idUsuario, IFNULL(tbUsuario.Nombre, "Sin detalle") AS Usuario, IFNULL(tbCalificado.Calificacion, "Sin detalle") AS CalificacionUsuario, IFNULL(tbtransaccion.idClienteConekta, "Sin detalle") AS idTarjeta, IFNULL(tbconekta.idClienteConekta, "Sin detalle") AS ClienteConekta,IFNULL(Round(tbtransaccion.MontoTotal * 0.85, 2), 0) AS GananciaConductor,IFNULL(tbtransaccion.MontoTotal, "0") AS TotalViaje, IFNULL(tbtarjetas.last4, "Sin detalle") AS Ultimos_4_digitos, IFNULL(tbtarjetas.brand, "Sin detalle") AS Brand, tbdetalleproblema.descripcion AS MotivoReporte')

            ->leftJoin('tbdireccion AS orig ON orig.id_tbDireccion = tbviaje.idOrigen')
            ->leftJoin('tbdireccion AS dest ON dest.id_tbDireccion = tbviaje.idDestino')
            ->leftJoin('tbvehiculo ON tbvehiculo.id_tbVehiculo = tbviaje.idVehiculo')
            ->leftJoin('tbmodotrabajo ON tbmodotrabajo.idModoTrabajo = tbvehiculo.idModoTrabajo')
            ->leftJoin('tbtransaccion ON tbtransaccion.idViaje = tbviaje.idViaje')
            ->leftJoin('tbconekta ON tbconekta.idPersona = tbviaje.idUsuario')
            ->leftJoin('tbcalificacion As tbCalificado ON (tbCalificado.Califica = tbviaje.idUsuario AND tbCalificado.idViaje = tbviaje.idViaje)')
            ->leftJoin('tbcalificacion AS tbCalifica ON (tbCalifica.Califica = tbviaje.idConductor AND tbCalifica.idViaje = tbviaje.idViaje)')
            ->leftJoin('tbpersona AS tbConductor ON tbConductor.id_tbPersona = tbviaje.idConductor')
            ->leftJoin('tbpersona AS tbUsuario ON tbUsuario.id_tbPersona = tbviaje.idUsuario')
            ->leftJoin('tbtarjetas ON tbtarjetas.idTarjetaConekta = tbtransaccion.idClienteConekta')
            ->leftJoin('tbproblemas On tbproblemas.idViaje = tbviaje.idViaje')
            ->leftJoin('tbdetalleproblema ON tbdetalleproblema.id = tbproblemas.idReporte')

            ->where('(
              (tbviaje.idUsuario = :iduser OR tbviaje.idConductor = :idconductor) 
              AND 
              ((tbviaje.Reportado = :tipo OR tbviaje.Reportado = :status) AND tbviaje.Status > 0)
            )', 
            array(':iduser' => $idUsuario, ':idconductor' => $idUsuario, ':status' => $tipoViaje, ':tipo' => $tipoViaje))
            ->limit($limite)
            ->offset($paginacion)
            ->orderBy('idViaje DESC')
            ->fetchAll();

        if ($viajes != false) {
          for ($i=0; $i < count($viajes) ; $i++) {
            $viajes[$i]->Tipo = '8';
          }

          $totalViajes = $this->db->from($this->tbViaje)
            ->select('COUNT(*) as  Total')
            ->where('(
              (tbviaje.idUsuario = :iduser OR tbviaje.idConductor = :idconductor) 
              AND 
              ((tbviaje.Reportado = :tipo OR tbviaje.Reportado = :status) AND tbviaje.Status > 0)
            )',
            array(':iduser' => $idUsuario, ':idconductor' => $idUsuario, ':status' => $tipoViaje, ':tipo' => $tipoViaje))
            ->fetch()
            ->Total;

                 $this->response->result=[
                  'Viajes' => $viajes, 
                  'Total' => $totalViajes
                ];
          return $this->response->SetResponse(true);
        }else{
                 $this->response->errors='No tienes viajes reportados en tu historial.';
          return $this->response->SetResponse(false);
        }
        break;

      case 9:
        $viajes = $this->db->from($this->tbViaje)
          ->select(null)
          ->select('
            tbviaje.idViaje, 
            tbviaje.FechaSolicitud, IFNULL(tbviaje.FechaInicio, "Sin detalle") AS FechaInicio, IFNULL(tbviaje.FechaFin, "Sin detalle") AS FechaFin, tbviaje.Status, tbviaje.Pagado,IFNULL(tbviaje.idMetodoPago, "Sin detalle") AS idMetodoPago, IFNULL(tbtransaccion.Status, "Sin detalle") AS EstadoTransaccion, IFNULL(orig.Latitud, "Sin detalle") AS LatitudOrigen, IFNULL(orig.Longitud, "Sin detalle") AS LongitudOrigen, IFNULL(orig.Calle, "Sin detalle") AS CalleOrigen, IFNULL(orig.Colonia, "Sin detalle") AS ColoniaOrigen, IFNULL(orig.Ciudad, "Sin detalle") AS CiudadOrigen, IFNULL(orig.Numero_Exterior, "Sin detalle") AS NumExtOrigen, IFNULL(dest.Latitud, "Sin detalle") AS LatitudDestino, IFNULL(dest.Longitud, "Sin detalle") AS LongitudDestino, IFNULL(dest.Calle, "Sin detalle") AS CalleDestino, IFNULL(dest.Colonia, "Sin detalle") AS ColoniaDestino, IFNULL(dest.Ciudad, "Sin detalle") AS CiudadDestino, IFNULL(dest.Numero_Exterior, "Sin detalle") AS NumExtDestino, IFNULL(tbvehiculo.Modelo, "Sin detalle") AS Modelo, IFNULL(tbvehiculo.Placa, "Sin detalle") AS Placa, 
            IFNULL(tbviaje.idConductor, "Sin detalle") AS idConductor, 
            IFNULL(tbConductor.Nombre, "Sin detalle") AS Conductor, 
            IFNULL(tbmodotrabajo.Nombre, "Sin detalle") AS TipoServicio, 
            IFNULL(tbCalifica.Calificacion, "Sin detalle") AS CalificacionDriver, IFNULL(tbviaje.idUsuario, "Sin detalle") AS idUsuario, IFNULL(tbUsuario.Nombre, "Sin detalle") AS Usuario, IFNULL(tbCalificado.Calificacion, "Sin detalle") AS CalificacionUsuario, IFNULL(tbtransaccion.idClienteConekta, "Sin detalle") AS idTarjeta, IFNULL(tbconekta.idClienteConekta, "Sin detalle") AS ClienteConekta,IFNULL(Round(tbtransaccion.MontoTotal * 0.85, 2), 0) AS GananciaConductor,IFNULL(Round(tbtransaccion.MontoTotal, 2), "0") AS TotalViaje, IFNULL(tbtarjetas.last4, "Sin detalle") AS Ultimos_4_digitos, IFNULL(tbtarjetas.brand, "Sin detalle") AS Brand, tbviaje.Excedido, IFNULL(tbdetallecancelacion.Descripcion, "Sin detalle") AS MotivoCancelacion')

            ->leftJoin('tbdireccion AS orig ON orig.id_tbDireccion = tbviaje.idOrigen')
            ->leftJoin('tbdireccion AS dest ON dest.id_tbDireccion = tbviaje.idDestino')
            ->leftJoin('tbvehiculo ON tbvehiculo.id_tbVehiculo = tbviaje.idVehiculo')
            ->leftJoin('tbmodotrabajo ON tbmodotrabajo.idModoTrabajo = tbvehiculo.idModoTrabajo')
            ->leftJoin('tbtransaccion ON tbtransaccion.idViaje = tbviaje.idViaje')
            ->leftJoin('tbconekta ON tbconekta.idPersona = tbviaje.idUsuario')
            ->leftJoin('tbcalificacion As tbCalificado ON (tbCalificado.Califica = tbviaje.idUsuario AND tbCalificado.idViaje = tbviaje.idViaje)')
            ->leftJoin('tbcalificacion AS tbCalifica ON (tbCalifica.Califica = tbviaje.idConductor AND tbCalifica.idViaje = tbviaje.idViaje)')
            ->leftJoin('tbpersona AS tbConductor ON tbConductor.id_tbPersona = tbviaje.idConductor')
            ->leftJoin('tbpersona AS tbUsuario ON tbUsuario.id_tbPersona = tbviaje.idUsuario')
            ->leftJoin('tbtarjetas ON tbtarjetas.idTarjetaConekta = tbtransaccion.idClienteConekta')
            ->leftJoin('tbviajescancelados On tbviajescancelados.idViaje = tbviaje.idViaje')
            ->leftJoin('tbdetallecancelacion ON tbdetallecancelacion.idDetalle = tbviajescancelados.idDetalleCancelacion')

            ->where('(
              (tbviaje.idUsuario = :iduser OR tbviaje.idConductor = :idconductor) 
              AND 
              ((tbviaje.Cancelado = :tipo OR tbviaje.Cancelado = :status) AND tbviaje.Status > 0 AND tbviaje.Excedido = 1)
            )', 
            array(':iduser' => $idUsuario, ':idconductor' => $idUsuario, ':status' => $tipoViaje, ':tipo' => $tipoViaje))
            ->limit($limite)
            ->offset($pagina)
            ->orderBy('idViaje DESC')
            ->fetchAll();

        if ($viajes != false) {
          for ($i=0; $i < count($viajes) ; $i++) {
            $viajes[$i]->Tipo = '9';
          }

          $totalViajes = $this->db->from($this->tbViaje)
            ->select('COUNT(*) as  Total')
            ->where('(
              (tbviaje.idUsuario = :iduser OR tbviaje.idConductor = :idconductor) 
              AND 
              ((tbviaje.Cancelado = :tipo OR tbviaje.Cancelado = :status) AND tbviaje.Status > 0 AND tbviaje.Excedido = 1)
            )', 
            array(':iduser' => $idUsuario, ':idconductor' => $idUsuario, ':status' => $tipoViaje, ':tipo' => $tipoViaje))
            ->fetch()
            ->Total;

                 $this->response->result=[
                  'Viajes' => $viajes, 
                  'Total' => $totalViajes
                ];
          return $this->response->SetResponse(true);
        }else{
                 $this->response->errors='No tienes viajes cancelados en tu historial.';
          return $this->response->SetResponse(false);
        }
        break;
      
      default:
               $this->response->errors='Verifica los parámetros de solicitud y reintenta.';
        return $this->response->SetResponse(false);
        break;
    }
  }

  public function obtener($id){
    $obtener = $this->db->from($this->tbViaje)
                        ->select('orig.Latitud AS LatitudOrigen, orig.Longitud AS LongitudOrigen, orig.Calle AS CalleOrigen, orig.Colonia AS ColoniaOrigen, orig.Ciudad AS CiudadOrigen, orig.Numero_Exterior AS NumExtOrigen, dest.Latitud AS LatitudDestino, dest.Longitud AS LongitudDestino, dest.Calle AS CalleDestino, dest.Colonia AS ColoniaDestino, dest.Ciudad AS CiudadDestino, dest.Numero_Exterior AS NumExtDestino, tbvehiculo.Modelo, tbvehiculo.Placa, tbtransaccion.Monto, tbtransaccion.Propina')
                         ->leftJoin('tbdireccion AS orig ON orig.id_tbDireccion = tbviaje.idOrigen')
                         ->leftJoin('tbdireccion AS dest ON dest.id_tbDireccion = tbviaje.idDestino')
                         ->leftJoin('tbvehiculo ON tbvehiculo.id_tbVehiculo = tbviaje.idVehiculo')
                         ->leftJoin('tbtransaccion ON tbtransaccion.idViaje = tbviaje.idViaje')
                         ->where('tbviaje.idViaje', $id)
                         ->fetch();

    if ($obtener !=false) {
             $this->response->result=$obtener;
      return $this->response->SetResponse(true);
    }else{
             $this->response->errors='Este viaje no existe';
      return $this->response->SetResponse(false);
    }
  }

  public function detallesViaje($idUser, $idViaje, $tipo){
    $buscaUser = $this->db->from($this->tbPersona)
                          ->select(null)
                          ->select('Tipo_de_usuario')
                          ->where('id_tbPersona', $idUser)
                          ->fetch();

    if ($buscaUser != false) {
      if ($tipo == 7 || $tipo == 8) {
        $detalle = $this->db->from($this->tbViaje)
          ->select(null)
          ->select('
            tbviaje.idViaje,
            IFNULL(tbviaje.FechaInicio, "Sin detalle") AS FechaInicio,
            IFNULL(tbviaje.FechaFin, "Sin detalle") AS FechaFin,
            IFNULL(tbviaje.Status, "Sin detalle") AS Status,
            IFNULL(tbviaje.Reportado, "Sin detalle") AS Reportado,
            IFNULL(tbviaje.idMetodoPago, "Sin detalle") AS idMetodoPago,
            IFNULL(tbviaje.Pagado, "Sin detalle") AS Pagado,

            IFNULL(tbpersona.Nombre, "Sin detalle") AS NombreConductor, 
            IFNULL(tbpersona.Apellidos, "Sin detalle") AS ApellidosConductor, 
            IFNULL(tbpersona.Imagen, "Sin detalle") AS ImagenConductor, 

            IFNULL(orig.Latitud, "Sin detalle") AS LatitudOrigen, 
            IFNULL(orig.Longitud, "Sin detalle") AS LongitudOrigen, 
            IFNULL(orig.Calle, "Sin detalle") AS CalleOrigen, 
            IFNULL(orig.Colonia, "Sin detalle") AS ColoniaOrigen, 
            IFNULL(orig.Ciudad, "Sin detalle") AS CiudadOrigen, 
            IFNULL(dest.Latitud, "Sin detalle") AS LatitudDestino, 
            IFNULL(dest.Longitud, "Sin detalle") AS LongitudDestino, 
            IFNULL(dest.Calle, "Sin detalle") AS CalleDestino, 
            IFNULL(dest.Colonia, "Sin detalle") AS ColoniaDestino, 
            IFNULL(dest.Ciudad, "Sin detalle") AS CiudadDestino, 

            IFNULL(tbvehiculo.Placa, "Sin detalle") AS Placa, 
            IFNULL(tbvehiculo.Modelo, "Sin detalle") AS Modelo, 
            IFNULL(tbmodotrabajo.Nombre, "Sin detalle") AS TipoServicio,

            IFNULL(tbdetallesviaje.DistanciaKilometros, "Sin detalle") AS DistanciaKilometros,
            IFNULL(tbdetallesviaje.TiempoMinutos, "Sin detalle") AS TiempoMinutos,
            IFNULL(tbdetallesviaje.TarifaBase, "Sin detalle") AS TarifaBase, 
            IFNULL(tbdetallesviaje.TotalKm, "Sin detalle") AS Costo_por_Km, 
            IFNULL(tbdetallesviaje.TotalTiempo, "Sin detalle") AS Costo_por_minuto, 
            IFNULL(tbdetallesviaje.LargaDistancia, "Sin detalle") AS LargaDistancia, 
            IFNULL(tbdetallesviaje.CuotaPeticion, "Sin detalle") AS CuotaPeticion,
            IFNULL(tbdetallesviaje.TarifaNocturna, "Sin detalle") AS TarifaNocturna,
            IFNULL(tbdetallesviaje.IVA, "Sin detalle") AS IVA,
            IFNULL(tbdetallesviaje.Impuesto, "Sin detalle") AS Impuesto,

            IFNULL(tbtransaccion.Monto, "Sin detalle") AS Subtotal_User,

            IFNULL(Round(tbdetallesviaje.CostoAproximado * 0.85, 2), 0) AS Subtotal_Driver,

            IFNULL(Round((
              tbdetallesviaje.TarifaBase + 
              tbdetallesviaje.TotalKm + 
              tbdetallesviaje.TotalTiempo + 
              tbdetallesviaje.LargaDistancia + 
              tbdetallesviaje.CuotaPeticion + 
              tbdetallesviaje.TarifaNocturna
            ), 2), 0) AS Subtotal,

            IFNULL(tbtransaccion.Propina, "Sin detalle") AS Propina,
            IFNULL(tbtransaccion.MontoTotal, "Sin detalle") AS CostoTotal,

            IFNULL(tbcalificacion.Calificacion, "Sin detalle") AS CalificacionADriver')

            ->leftJoin('tbpersona ON tbpersona.id_tbPersona = tbviaje.idConductor')
            ->leftJoin('tbdireccion as orig ON orig.id_tbDireccion = tbviaje.idOrigen')
            ->leftJoin('tbdireccion as dest ON dest.id_tbDireccion = tbviaje.idDestino')
            ->leftJoin('tbvehiculo ON tbvehiculo.id_tbVehiculo = tbviaje.idVehiculo')
            ->leftJoin('tbtransaccion ON tbtransaccion.idViaje = tbviaje.idViaje')
            ->leftJoin('tbvehiculo ON tbvehiculo.id_tbVehiculo = tbviaje.idVehiculo')
            ->leftJoin('tbmodotrabajo ON tbmodotrabajo.idModoTrabajo = tbvehiculo.idModoTrabajo')
            ->leftJoin('tbdetallesviaje ON tbdetallesviaje.idViaje = tbviaje.idViaje')
            ->leftJoin('tbcalificacion ON tbcalificacion.idviaje = tbviaje.idViaje')
            ->leftJoin('tbcalificacion ON tbcalificacion.Califica = tbviaje.idUsuario')

            ->where('(tbviaje.idViaje = :idviaje) AND (tbviaje.idUsuario = :iduser OR tbviaje.idConductor = :idconductor)', array(':idviaje' => $idViaje, ':iduser' => $idUser, ':idconductor' => $idUser))
            ->fetch();

        $detalle->CuotaPago = '100%';
      } elseif ($tipo == 9) {
        $detalle = $this->db->from($this->tbViaje)
          ->select(null)
          ->select('
            tbviaje.idViaje,
            IFNULL(tbviaje.FechaInicio, "Sin detalle") AS FechaInicio,
            IFNULL(tbviaje.FechaFin, "Sin detalle") AS FechaFin,
            IFNULL(tbviaje.Status, "Sin detalle") AS Status,
            IFNULL(tbviaje.Cancelado, "Sin detalle") AS Cancelado,
            IFNULL(tbviaje.idMetodoPago, "Sin detalle") AS idMetodoPago,
            IFNULL(tbviaje.Pagado, "Sin detalle") AS Pagado,

            IFNULL(tbpersona.Nombre, "Sin detalle") AS NombreConductor, 
            IFNULL(tbpersona.Apellidos, "Sin detalle") AS ApellidosConductor, 
            IFNULL(tbpersona.Imagen, "Sin detalle") AS ImagenConductor, 

            IFNULL(orig.Latitud, "Sin detalle") AS LatitudOrigen, 
            IFNULL(orig.Longitud, "Sin detalle") AS LongitudOrigen, 
            IFNULL(orig.Calle, "Sin detalle") AS CalleOrigen, 
            IFNULL(orig.Colonia, "Sin detalle") AS ColoniaOrigen, 
            IFNULL(orig.Ciudad, "Sin detalle") AS CiudadOrigen, 
            IFNULL(dest.Latitud, "Sin detalle") AS LatitudDestino, 
            IFNULL(dest.Longitud, "Sin detalle") AS LongitudDestino, 
            IFNULL(dest.Calle, "Sin detalle") AS CalleDestino, 
            IFNULL(dest.Colonia, "Sin detalle") AS ColoniaDestino, 
            IFNULL(dest.Ciudad, "Sin detalle") AS CiudadDestino, 

            IFNULL(tbvehiculo.Placa, "Sin detalle") AS Placa, 
            IFNULL(tbvehiculo.Modelo, "Sin detalle") AS Modelo, 
            IFNULL(tbmodotrabajo.Nombre, "Sin detalle") AS TipoServicio, 
            IFNULL(tbmodotrabajo.Descripcion, "Sin detalle") AS DescripcionServicio,

            IFNULL(tbdetallesviaje.DistanciaKilometros, "Sin detalle") AS DistanciaKilometros,
            IFNULL(tbdetallesviaje.TiempoMinutos, "Sin detalle") AS TiempoMinutos,
            IFNULL(tbdetallesviaje.TarifaBase, "Sin detalle") AS TarifaBase, 
            IFNULL(tbdetallesviaje.TotalKm, "Sin detalle") AS Costo_por_Km, 
            IFNULL(tbdetallesviaje.TotalTiempo, "Sin detalle") AS Costo_por_minuto, 
            IFNULL(tbdetallesviaje.LargaDistancia, "Sin detalle") AS LargaDistancia, 
            IFNULL(tbdetallesviaje.CuotaPeticion, "Sin detalle") AS CuotaPeticion,
            IFNULL(tbdetallesviaje.TarifaNocturna, "Sin detalle") AS TarifaNocturna,

            IFNULL(Round((
              tbdetallesviaje.TarifaBase + 
              tbdetallesviaje.TotalKm + 
              tbdetallesviaje.TotalTiempo + 
              tbdetallesviaje.LargaDistancia + 
              tbdetallesviaje.CuotaPeticion + 
              tbdetallesviaje.TarifaNocturna
            ), 2), 0) AS Subtotal,

            IFNULL(tbdetallesviaje.IVA, "Sin detalle") AS IVA,
            IFNULL(tbdetallesviaje.Impuesto, "Sin detalle") AS Impuesto,
            IFNULL(tbdetallesviaje.CostoAproximado, "Sin detalle") AS Total,
            IFNULL(Round(tbtransaccion.MontoTotal, 2), "Sin detalle") AS PagoPorCancelacion,

            IFNULL(tbcalificacion.Calificacion, "Sin detalle") AS CalificacionADriver,
            IFNULL(tbdetallecancelacion.Descripcion, "Sin detalle") AS MotivoCancelacion')

            ->leftJoin('tbpersona ON tbpersona.id_tbPersona = tbviaje.idConductor')
            ->leftJoin('tbdireccion as orig ON orig.id_tbDireccion = tbviaje.idOrigen')
            ->leftJoin('tbdireccion as dest ON dest.id_tbDireccion = tbviaje.idDestino')
            ->leftJoin('tbvehiculo ON tbvehiculo.id_tbVehiculo = tbviaje.idVehiculo')
            ->leftJoin('tbtransaccion ON tbtransaccion.idViaje = tbviaje.idViaje')
            ->leftJoin('tbvehiculo ON tbvehiculo.id_tbVehiculo = tbviaje.idVehiculo')
            ->leftJoin('tbmodotrabajo ON tbmodotrabajo.idModoTrabajo = tbvehiculo.idModoTrabajo')
            ->leftJoin('tbdetallesviaje ON tbdetallesviaje.idViaje = tbviaje.idViaje')
            ->leftJoin('tbcalificacion ON tbcalificacion.idviaje = tbviaje.idViaje')
            ->leftJoin('tbcalificacion ON tbcalificacion.Califica = tbviaje.idUsuario')
            ->leftJoin('tbviajescancelados ON tbviajescancelados.idViaje = tbviaje.idViaje')
            ->leftJoin('tbdetallecancelacion ON tbdetallecancelacion.idDetalle = tbviajescancelados.idDetalleCancelacion')
            ->where('(tbviaje.idViaje = :idviaje) AND (tbviaje.idUsuario = :iduser OR tbviaje.idConductor = :idconductor)', array(':idviaje' => $idViaje, ':iduser' => $idUser, ':idconductor' => $idUser))
            ->fetch();

        if ($detalle->Status > 3) {
          $detalle->CuotaPago = '100%';
        } else {
          $detalle->CuotaPago = '35%';
        }
      }

      if ($detalle > 0) {
        if ($buscaUser->Tipo_de_usuario == 2) { #Usuario normal
                 $this->response->result=$detalle;
          return $this->response->SetResponse(true);
        } elseif ($buscaUser->Tipo_de_usuario == 3) { #Conductor
          if ($detalle->FechaFin != 'Sin detalle') {
            $fechaFin = date('d/m/Y', strtotime($detalle->FechaFin));
            $horaFin = date('H:i:s', strtotime($detalle->FechaFin));
          } else {
            $fechaFin = 'Sin detalle';
            $horaFin = 'Sin detalle';
          }
          if ($detalle->FechaInicio != 'Sin detalle') {
            $horaInicio = date('H:i:s', strtotime($detalle->FechaInicio));
          } else {
            $horaInicio = 'Sin detalle';
          }

          $horario = [
            'FechaInicio' => $detalle->FechaInicio,
            'FechaFin' => $fechaFin,
            'HoraInicio' => $horaInicio,
            'HoraFin' => $horaFin
          ];

          if ($tipo == 7 || $tipo == 8) {
            $recibe_driver = array(
              'Tarifa_driver' => Round((($detalle->Subtotal) * 0.85), 2),
              'IVA_driver' => Round((($detalle->IVA) * 0.85), 2),
              'Total' => Round(($detalle->Subtotal_Driver), 2)
            );

            $lubo_recibe = array(
              'Pago_del_usuario' => Round((($detalle->Subtotal) * 0.15), 2),
              'IVA_user' => Round((($detalle->IVA) * 0.15), 2),
              'Total' => Round(($detalle->Subtotal_User * 0.15), 2)
            );
          } elseif ($tipo == 9) {
            $totalDriver = Round(($detalle->PagoPorCancelacion * 0.85), 2);
            $tarifaDriver = Round((($detalle->PagoPorCancelacion * 0.85) / 1.16), 2);

            $recibe_driver = array(
              'Tarifa_driver' => $tarifaDriver,
              'IVA_driver' => Round($totalDriver - $tarifaDriver, 2),
              'Total' => $totalDriver
            );

            $totalLubo = Round(($detalle->PagoPorCancelacion * 0.15), 2);
            $pagoUsuario =  Round(((($detalle->PagoPorCancelacion) * 0.15) / 1.16), 2);

            $lubo_recibe = array(
              'Pago_del_usuario' => $pagoUsuario,
              'IVA_user' => Round($totalLubo - $pagoUsuario, 2),
              'Total' => $totalLubo
            );
          }

                 $this->response->result=[
                  'Horario' => $horario,
                  'Desglose_tarifa' => $detalle, 
                  'Recibes' => $recibe_driver, 
                  'Lubo_recibe' => $lubo_recibe
                ];
          return $this->response->setResponse(true);
        }
      }else{
               $this->response->errors = 'No existen detalles de este viaje';
        return $this->response->setResponse(false);
      }
    }else{
             $this->response->errors='El usuario al que hace referencia no existe';
      return $this->response->setResponse(false);
    }
  }

  public function tarifaViaje($idViaje){
    $tarifa = $this->db->from('tbdetallesviaje')
                       ->select('
                        IFNULL(tbtransaccion.Propina, "Sin detalle") AS Propina, 
                        IFNULL(tbtransaccion.MontoTotal, "Sin detalle") AS MontoTotal')
                       ->leftJoin('tbtransaccion ON tbtransaccion.idViaje = tbdetallesviaje.idViaje')
                       ->where('tbdetallesviaje.idViaje', $idViaje)
                       ->fetch();

    if ($tarifa != false) {
              $this->response->result=[
                'TarifaBase' => floatval($tarifa->TarifaBase),
                'Km' => floatval($tarifa->DistanciaKilometros),
                'Min' => floatval($tarifa->TiempoMinutos),
                'Total_por_Km' => floatval($tarifa->TotalKm),
                'Total_por_min' => floatval($tarifa->TotalTiempo),
                'LargaDistancia' => floatval($tarifa->LargaDistancia),
                'CuotaPeticion' => floatval($tarifa->CuotaPeticion),
                'TarifaNocturna' => floatval($tarifa->TarifaNocturna),
                'IVA' => floatval($tarifa->IVA),
                'Impuesto' => floatval($tarifa->Impuesto),
                'CostoAproximado' => floatval($tarifa->CostoAproximado),
                'Propina' => floatval($tarifa->Propina),
                'TotalPropina' => floatval($tarifa->MontoTotal)
              ];
      return $this->response->SetResponse(true);
    }else{
             $this->response->errors='No existen detalles de este viaje';
      return $this->response->SetResponse(false);
    }
  }

  public function cancelarViajeUsuario($data){
    $idViaje = $data['idViaje'];
    $viaje = $this->db->from($this->tbViaje)
      ->select(null)
      ->select('
        IFNULL(tbpersona.idZona, 0) AS idZona, 
        IFNULL(tbviaje.idConductor, 0) AS idConductor, 
        IFNULL(tbviaje.Status, 0) AS Status, 
        IFNULL(tbviaje.idFirebase, "") AS idFirebase, 
        IFNULL(tbviaje.FechaSolicitud, 0) AS FechaSolicitud, 
        IFNULL(tbviaje.idMetodoPago, 0) AS idMetodoPago, 
        IFNULL(tbtoken.Token, 0) AS TokenDriver, 
        IFNULL(tbdetallesviaje.CostoAproximado, 0) AS CostoAproximado,
        IFNULL(tbviaje.Cancelado, 0) AS Cancelado')

      ->leftJoin('tbpersona ON tbpersona.id_tbPersona = tbviaje.idConductor')
      ->leftJoin('tbtoken ON tbtoken.idPersona = tbviaje.idConductor')
      ->leftJoin('tbdetallesviaje ON tbdetallesviaje.idViaje = tbviaje.idViaje')
      ->where('tbviaje.idViaje', $idViaje)
      ->fetch();

    if ($viaje != false) {
      $viaje_cancelado = $viaje->Cancelado;
      switch ($viaje_cancelado) {
        case 9:
                 $this->response->errors = 'Ya has cancelado previamente este viaje, si tienes algún problema con esta acción, contacta a Soporte Lubo: lubo.com.mx con el ID de viaje: '.$idViaje. ', para más detalles.';
          return $this->response->SetResponse(false);
          break;

        case 10:
                 $this->response->errors = 'Este viaje ya ha sido cancelado previamente por el conductor, si tienes algún problema con esta acción, contacta a Soporte Lubo: lubo.com.mx con el ID de viaje: '.$idViaje. ', para más detalles.';
          return $this->response->SetResponse(false);
          break;

        case 0:
          $idDriver = $viaje->idConductor;
          $statusViaje = $viaje->Status;
          $name = $viaje->idFirebase;
          $zonaDriver = $viaje->idZona;
          $tokenDriver = $viaje->TokenDriver;
          $metodoPago = $viaje->idMetodoPago;

          $new_date = date('Y-m-d H:i:s', strtotime("+3 Minute", strtotime($viaje->FechaSolicitud)));

          if ($statusViaje == 0) {
            if ($idDriver != 0 && $name != '' && $zonaDriver != 0 && $metodoPago != 0) {
             $viajeCancelado = self::viajeCancelado($idViaje, $name, $idDriver, 2, $zonaDriver, $statusViaje, $tokenDriver);
            
              if ($viajeCancelado == 1) {
                return $this->response->SetResponse(true, 'Tu solicitud de viaje ha sido cancelada.');
              } else {
                return $this->response->SetResponse(false, 'Este viaje ya no puede cancelarse.');
              } 
            } else {
                     $this->response->errors = 'Estamos teniendo problemas para cancelar tu viaje, contacta a Soporte Lubo: lubo.com.mx con el ID de viaje: '.$idViaje. ', para obtener ayuda.';
              return $this->response->SetResponse(false);
            }
          } elseif (($statusViaje == 1 || $statusViaje == 3) && (date('Y-m-d H:i:s') <= $new_date)) {
            if ($idDriver != 0 && $statusViaje != 0 && $name != '' && $zonaDriver != 0 && $metodoPago != 0) {
              $viajeCancelado = self::viajeCancelado($idViaje, $name, $idDriver, 2, $zonaDriver, 1, $tokenDriver);

              if ($viajeCancelado == 1) {
                CURL::updateStatusCancelacion($name, 1);

                return $this->response->SetResponse(true, 'Tu solicitud de viaje ha sido cancelada.');
              } else {
                return $this->response->SetResponse(false, 'Este viaje ya no puede cancelarse.');
              }
            } else {
                     $this->response->errors = 'Estamos teniendo problemas para cancelar tu viaje, contacta a Soporte Lubo: lubo.com.mx con el ID de viaje: '.$idViaje. ', para obtener ayuda.';
              return $this->response->SetResponse(false);
            }
          } elseif (($statusViaje == 1 || $statusViaje == 3) && (date('Y-m-d H:i:s') >= $new_date)) {
            if ($idDriver != 0 && $statusViaje != 0 && $name != '' && $zonaDriver != 0 && $metodoPago != 0) {
              if ($metodoPago == 2) { #Cobrar el 35% del total del viaje
                $tarjeta = $this->db->from($this->tbConekta)
                                    ->where('idPersona', $data['idUsuario'])
                                    ->fetch();

                if ($tarjeta != false) {
                  #Buscar todas sus tarjetas
                  $idCustomer = $tarjeta->idClienteConekta;
                  $customer = \Conekta\Customer::find($idCustomer);
                  $listaTarjetas  = $customer->payment_sources;
                    
                  foreach ($listaTarjetas as $key => $value) {
                    $tarjetasDisponibles[] = $value;
                  }

                  if (count($tarjetasDisponibles > 0)) {
                    $costoAproximado = floatval($viaje->CostoAproximado);
                    $penalizacion = round($costoAproximado * 0.35, 2); #La penalización que se cobrara

                    foreach ($tarjetasDisponibles as $key => $value) {
                      $idClienteConekta = $value->id;

                      $cobrarViaje = self::cobrar($idClienteConekta, $idCustomer, $data, $penalizacion);

                      if (($cobrarViaje['Registro'] > 0) && ($cobrarViaje['Pagado'] == 1)) {
                        break;
                      }
                    }

                    $viajeCancelado = self::viajeCancelado($idViaje, $name, $idDriver, 2, $zonaDriver, $statusViaje, $tokenDriver);

                    if (($cobrarViaje['Registro'] > 0) && ($cobrarViaje['Pagado'] == 1)) {
                      CURL::updateStatusCancelacion($name, 2);

                      #Actualiza las ganancias diarias del conductor
                      $informacion_driver = CURL::ObtenerViajesDriver($idDriver, $zonaDriver);

                      $parcial_dia = floatval(str_replace(",", "", $informacion_driver->ganancia_Diaria));

                      $total_dia = $parcial_dia + ($penalizacion * 0.85);
                      
                      $format_total_dia = number_format($total_dia, 2,".",",");

                      #Actualizar ganancia diaria en Firebase
                      $ganancia = CURL::GananciaDiaria($idDriver, $format_total_dia, $zonaDriver);

                             $this->response->result=$cobrarViaje;
                      return $this->response->SetResponse(true, 'Tu solicitud de viaje ha sido cancelada. Se ha generado un pago con tarjeta por: '.$penalizacion.' pesos');
                    } elseif (($cobrarViaje['Registro'] > 0) && ($cobrarViaje['Pagado'] == 0)) {
                             $this->response->result=$cobrarViaje;
                      return $this->response->SetResponse(false, 'Tu solicitud de viaje ha sido cancelada. El viaje permanecerá en tu historial hasta que sea pagado.');
                    }
                  } else {
                           $this->response->errors = 'No tienes tarjetas disponibles.';
                    return $this->response->SetResponse(false);
                  }
                } else {
                         $this->response->errors = 'Para continuar, registra una tarjeta válida.';
                  return $this->response->setResponse(false);
                }
              } else {
                       $this->response->errors = 'Para poder cancelar tu viaje es necesario que cambies tu método de pago a tarjeta.';
                return $this->response->SetResponse(false);
              }
            } else {
                     $this->response->errors = 'Estamos teniendo problemas para cancelar tu viaje, contacta a Soporte Lubo: lubo.com.mx con el ID de viaje: '.$idViaje. ', para obtener ayuda.';
              return $this->response->SetResponse(false);
            }
          } elseif ($statusViaje == 4) {
            if ($idDriver != 0 && $statusViaje != 0 && $name != '' && $zonaDriver != 0 && $metodoPago != 0) {
              if ($metodoPago == 2) {
                $tarjeta = $this->db->from($this->tbConekta)
                                    ->where('idPersona', $data['idUsuario'])
                                    ->fetch();

                if ($tarjeta != false) {
                  #Buscar todas sus tarjetas
                  $idCustomer = $tarjeta->idClienteConekta;
                  $customer = \Conekta\Customer::find($idCustomer);
                  $listaTarjetas  = $customer->payment_sources;
                    
                  foreach ($listaTarjetas as $key => $value) {
                    $tarjetasDisponibles[] = $value;
                  }

                  if (count($tarjetasDisponibles > 0)) {
                    $penalizacion = floatval($viaje->CostoAproximado); #La penalización que se cobrara

                    foreach ($tarjetasDisponibles as $key => $value) {
                      $idClienteConekta = $value->id;

                      $cobrarViaje = self::cobrar($idClienteConekta, $idCustomer, $data, $penalizacion);
                      
                      if (($cobrarViaje['Registro'] > 0) && ($cobrarViaje['Pagado'] == 1)) {
                        break;
                      }
                    }

                    $viajeCancelado = self::viajeCancelado($idViaje, $name, $idDriver, 2, $zonaDriver, $statusViaje, $tokenDriver);

                    if (($cobrarViaje['Registro'] > 0) && ($cobrarViaje['Pagado'] == 1)) {
                      CURL::updateStatusCancelacion($name, 3);

                      #Actualiza las ganancias diarias del conductor
                      $informacion_driver = CURL::ObtenerViajesDriver($idDriver, $zonaDriver);

                      $parcial_dia = floatval(str_replace(",", "", $informacion_driver->ganancia_Diaria));

                      $total_dia = $parcial_dia + ($penalizacion * 0.85);
                      
                      $format_total_dia = number_format($total_dia, 2,".",",");

                      #Actualizar ganancia diaria en Firebase
                      $ganancia = CURL::GananciaDiaria($idDriver, $format_total_dia, $zonaDriver);

                             $this->response->result=$cobrarViaje;
                      return $this->response->SetResponse(true, 'Tu solicitud de viaje ha sido cancelada. Se ha generado un pago con tarjeta por: '.$penalizacion.' pesos');
                    } elseif (($cobrarViaje['Registro'] > 0) && ($cobrarViaje['Pagado'] == 0)) {
                             $this->response->result=$cobrarViaje;
                      return $this->response->SetResponse(false, 'Tu solicitud de viaje ha sido cancelada. El viaje permanecerá en tu historial hasta que sea pagado.');
                    }
                  } else {
                           $this->response->errors = 'No tienes tarjetas disponibles.';
                    return $this->response->SetResponse(false);
                  }
                } else {
                         $this->response->errors = 'Para continuar, registra una tarjeta válida.';
                  return $this->response->setResponse(false);
                }
              } else {
                       $this->response->errors = 'Para poder cancelar tu viaje es necesario que cambies tu método de pago a tarjeta.';
                return $this->response->SetResponse(false);
              }
            } else {
                     $this->response->errors = 'Estamos teniendo problemas para cancelar tu viaje, contacta a Soporte Lubo: lubo.com.mx con el ID de viaje: '.$idViaje. ', para obtener ayuda.';
              return $this->response->SetResponse(false);
            }
          } elseif (($statusViaje != 1) && ($statusViaje != 3) && ($statusViaje != 4)) {
                   $this->response->errors = 'No es posible cancelar este viaje una vez que haya inciado.';
            return $this->response->SetResponse(false);
          }
          break;
        
        default:
                 $this->response->errors = 'Error cancelando este viaje. Contacta a Soporte Lubo: lubo.com.mx con el ID de viaje: '.$idViaje.', para ayudarte a resolver este problema.';
          return $this->response->SetResponse(false);
          break;
      }
    } else {
             $this->response->errors = 'El viaje al que hace referencia no existe.';
      return $this->response->SetResponse(false);
    }
  }

  public function cobrar($idClienteConekta, $idCustomer, $data, $penalizacion){
    $montoConekta = $penalizacion * 100;
    $montoMsjConekta = $penalizacion / 100;

    $cobro = Pago::pagar($idCustomer, $montoConekta, $montoMsjConekta, $idClienteConekta);
    $estadoPago = $cobro->payment_status;

    $status = '';
    switch ($estadoPago) {
      case 'declined': $status = 0; break;          #Declinado
      case 'pending_payment': $status = 1; break;   #Pago pendiente
      case 'paid': $status = 2; break;              #Pagado
      case 'expired': $status = 4; break;           #Expirado
      case 'refunded': $status = 5; break;          #Reintegrado
      case 'partially_refunded': $status = 6; break;#Parcialmente retornado
      case 'charged_back': $status = 7; break;      #Pago retornado
      case null: $status = 8;                       #El tiempo de espera desde la captura de datos de la tarjeta expiró
    }

    $pagado = 0;
    #Agregar el registro a la DB
    $data_transaction = array(
      'idClienteConekta' => $idClienteConekta,
      'idViaje' => $data['idViaje'],
      'idUsuario'=> $data['idUsuario'],
      'tipotransaccion'=> 4, #Viaje cancelado
      'Monto' => $penalizacion,
      'Propina'=> 0,
      'MontoTotal' => $penalizacion,
      'FechaAlta'=>date('Y-m-d H:i:s'),
      'FechaPago'=>date('Y-m-d H:i:s'),
      'idTransaccionConekta' => $cobro->id,
      'Status' => $status,
      'MetodoPago'=> 2
    );

    if ($status == 2) { 
      $viaje_pagado = $this->db->update($this->tbViaje, array('Pagado' => 1, 'Excedido' => 1))
                               ->where('idViaje', $data['idViaje'])
                               ->execute();

      $pagado = 1;
    }

    $insertarTransaccion = $this->db->insertInto($this->tbTransaccion, $data_transaction)
                                    ->execute();

    return ['Registro' => $insertarTransaccion, 'Pagado' => $pagado];
  }

  public function viajeCancelado($viaje, $name, $idConductor, $tipoUser, $zonaDriver, $statusViaje, $token){
    if ($tipoUser == 2) { #user
      #Marca el viaje como cancelado por usuario en MySQL
      $cancelarViaje = $this->db->update($this->tbViaje)
                                ->set('Cancelado', 9)
                                ->where('idViaje', $viaje)
                                ->execute();

      if ($cancelarViaje == 1) {
        #Actualizar el estado del viaje
        $viajeCancelado = CURL::updateStatus($name, 9);
        #Libera al conductor del viaje en Firebase
        $delete = CURL::deleteIds($idConductor, $zonaDriver);
        #liberar del viaje al conductor en MySQL
        $liberarDriver = $this->db->update($this->tbPersona)
                                  ->set('Status', 1)
                                  ->where('tbpersona.id_tbPersona', $idConductor)
                                  ->execute();

        #Eliminar el viaje de firebase
        #$deleteViaje = CURL::deleteTravel($name);

        $title = 'LUBO';
        $text = 'El usuario ha cancelado el viaje.';
        $data = array('Prueba' => 'prueba');
        $push = Push::FMC($title, $text, $token, $data);
        $pushj = json_decode($push);
        
        return 1;
      } else {
        return 0;
      }
    } elseif ($tipoUser == 3) { #driver
      if ($statusViaje == 0) {
        $delete = CURL::deleteIds($idConductor, $zonaDriver); #Libera al conductor del viaje en Firebase

        $liberarDriver = $this->db->update($this->tbPersona)
                                  ->set('Status', 1)
                                  ->where('tbpersona.id_tbPersona', $idConductor)
                                  ->execute();

        return 1;
      } elseif ($statusViaje == 1 || $statusViaje == 3) {
        $cancelado = $this->db->update($this->tbViaje)
                              ->set('Cancelado', 10)
                              ->where('idViaje', $viaje)
                              ->execute();

        if ($cancelado == 1) {
          $viajeCancelado = CURL::updateStatus($name, 10); #Actualizar el estado del viaje

          $delete = CURL::deleteIds($idConductor, $zonaDriver); #Libera al conductor del viaje en Firebase

          $liberarDriver = $this->db->update($this->tbPersona)
                                    ->set('Status', 1)
                                    ->where('tbpersona.id_tbPersona', $idConductor)
                                    ->execute();

          return 1;
        } else {
          return 0;
        }
      }
    }
  }

  public function detallesCancelacion($tipoUsuario, $tipo) {
    $detalles = $this->db->from($this->tbDetalleCancelacion)
                             ->select(null)
                             ->select('idDetalle, Descripcion')
                             ->orderBy('idDetalle ASC')
                             ->where('TipoUsuario Like ?', '%'.$tipoUsuario.'%')
                             ->where('Tipo Like ?', '%'.$tipo.'%')
                             ->fetchAll();

    if ($detalles != false) {
      $totalDetalles = count($detalles);

             $this->response->result = [
               'Detalles'  => $detalles,
               'Total' => $totalDetalles
             ];
      return $this->response->SetResponse(true);
    } else {
             $this->response->errors='No existen detalles.';
      return $this->response->SetResponse(false);
    } 
  }

  public function cancelarViajeDriver($data){
    $idViaje = $data['idViaje'];
    $viaje = $this->db->from($this->tbViaje)
                      ->select(null)
                      ->select('
                        IFNULL(tbviaje.idFirebase, "") AS idFirebase, 
                        IFNULL(tbviaje.Status, 0) AS Status, 
                        IFNULL(tbpersona.idZona, 0) AS idZona, 
                        IFNULL(tbtoken.Token, "") AS TokenUsuario,
                        IFNULL(tbviaje.Cancelado, 0) AS Cancelado
                        ')
                      ->leftJoin('tbpersona ON tbpersona.id_tbPersona = tbviaje.idConductor')
                      ->leftJoin('tbtoken ON tbtoken.idPersona = tbviaje.idUsuario')
                      ->where('tbviaje.idviaje', $idViaje)
                      ->fetch();

    if ($viaje != false) {
      $viaje_cancelado = $viaje->Cancelado;
      switch ($viaje_cancelado) {
        case 0:
          $status_viaje = $viaje->Status;

          if ($status_viaje < 4) {
            $name = $viaje->idFirebase;
            $idConductor = $data['idDriver'];
            $zonaDriver = $viaje->idZona;
            $tokenUsuario = $viaje->TokenUsuario;
            $detalleCancelacion = $data['detalleCancelacion'];

            if ($name != "" && $idConductor != 0 && $zonaDriver != 0) {
              $cancelar_viaje = self::viajeCancelado($idViaje, $name, $idConductor, 3, $zonaDriver, $status_viaje, $tokenUsuario);

              if ($cancelar_viaje == 1) {
                $detalles_cancelacion = [
                  'idViaje' => $idViaje, 
                  'idUsuario' => $idConductor, 
                  'idDetalleCancelacion' => $detalleCancelacion
                ];

                $agregarDetalles = $this->db->insertInto($this->tbViajesCancelados, $detalles_cancelacion)
                                            ->execute();

                #Agregar MotivoCancelacionDriver con la descripción del motivo de cancelación a firebase.
                if ($detalleCancelacion != 0) {
                  CURL::addDetailCancel($name, $data['descripcion']);
                }

                       $this->response->result = 'Has cancelado este viaje con éxito.';
                return $this->response->setResponse(true);
              } else {
                       $this->response->errors = 'No fue posible cancelar el viaje.';
                return $this->response->SetResponse(false);
              }
            } else {
                     $this->response->errors = 'Tenemos problemas para recuperar los datos de control para poder cancelar el viaje, comunícate a Soporte Lubo: lubo.com.mx si sigues teniendo estos problemas.';
              return $this->response->SetResponse(false);
            }
          } else {
                   $this->response->errors = 'Una vez iniciado el viaje no es posible cancelarlo.';
            return $this->response->SetResponse(false);
          }
          break;

        case 9:
                 $this->response->errors = 'Este viaje ya ha sido cancelado previamente por el usuario, si tienes algún problema con esta acción, contacta a Soporte Lubo: lubo.com.mx con el ID de viaje: '.$idViaje. ', para más detalles.';
          return $this->response->SetResponse(false);
          break;

        case 10:
                 $this->response->errors = 'Ya has cancelado previamente este viaje, si tienes algún problema con esta acción, contacta a Soporte Lubo: lubo.com.mx con el ID de viaje: '.$idViaje. ', para más detalles.';
          return $this->response->SetResponse(false);
          break;
        
        default:
                 $this->response->errors = 'Error cancelando este viaje. Contacta a Soporte Lubo: lubo.com.mx con el ID de viaje: '.$idViaje.', para ayudarte a resolver este problema.';
          return $this->response->SetResponse(false);
          break;
      }
    } else {
             $this->response->errors = 'El viaje al que hace referencia no existe.';
      return $this->response->setResponse(false);
    }
  }

  public function viajesSinTransaccion($l, $o, $idDriver){
    $viajes = $this->db->from($this->tbViaje)
      ->select(null)
      ->select('
        tbviaje.idViaje, 
        IFNULL(tbviaje.FechaInicio, "Sin detalle") AS FechaInicio, 
        tbviaje.Status,
        tbviaje.idMetodoPago AS MetodoPago, 
        tbviaje.Pagado,
        origen.Latitud AS LatitudOrigen, 
        origen.Longitud AS LongitudOrigen, 
        destino.Latitud AS LatitudDestino, 
        destino.Longitud AS LongitudDestino, 
        IFNULL(tbvehiculo.Modelo, "Sin detalle") AS ModeloAuto, 
        IFNULL(tbdetallesviaje.CostoAproximado * 0.85, "Sin detalle") AS TotalViaje, 
        IFNULL(tbtransaccion.Status, "Sin detalle") AS EstadoTransaccion')

      ->leftJoin('tbdireccion AS origen ON origen.id_tbDireccion = tbviaje.idOrigen')
      ->leftJoin('tbdireccion AS destino ON destino.id_tbDireccion = tbviaje.idDestino')
      ->leftJoin('tbvehiculo ON tbvehiculo.id_tbVehiculo = tbviaje.idVehiculo')
      ->leftJoin('tbdetallesviaje ON tbdetallesviaje.idViaje = tbviaje.idViaje')
      ->leftJoin('tbtransaccion ON tbtransaccion.idViaje = tbviaje.idViaje')

      ->where('tbviaje.idConductor', $idDriver)
      ->where('tbviaje.Status > 0 AND tbviaje.Pagado = 0')
      ->limit($l)
      ->offset($o)  
      ->orderBy('idViaje DESC')
      ->fetchAll();

    if ($viajes != false) {
      $totalViajes = $this->db->from($this->tbTransaccion)
                              ->select('COUNT(*) as  Total')
                              ->leftJoin('tbviaje ON tbviaje.idViaje = tbtransaccion.idViaje')
                              ->where('tbviaje.idConductor', $idDriver)
                              ->where('tbviaje.Status > 0')
                              ->where('tbviaje.Pagado = 0')
                              ->fetch()
                              ->Total;

      for ($i=0; $i < count($viajes) ; $i++) {
        if ($viajes[$i]->FechaInicio != 'Sin detalle') {
          $viajes[$i]->FechaInicio = date('d/m/Y', strtotime($viajes[$i]->FechaInicio));
        }
      }
             $this->response->result=[
              'Viajes' => $viajes, 
              'TotalViajes' => $totalViajes
            ];
      return $this->response->SetResponse(true);
    } else {
      return $this->response->SetResponse(false, 'No hay registros de viajes sin pagar.');
    }
  }
}
?>