<?php
namespace App\Model;

use App\Lib\Response,
    App\Lib\Cifrado;

class VehiculoModel{
  private $db;
  private $tbVehiculo = 'tbvehiculo';
  private $tbPersona = 'tbpersona';
  private $response;

  public function __CONSTRUCT($db){
    $this->db = $db;
    $this->response = new Response();
  }

  public function listar($l,$p){
    $data = $this->db->from($this->tbVehiculo)
                     ->limit($l)
                     ->offset($p)
                     ->orderBy('id_tbVehiculo DESC')
                     ->fetchAll();

    return [
      'data'  => $data,
      'total' => count($data)
    ];
  }

  public function obtener($id){ 
    $obtener = $this->db->from($this->tbVehiculo)
                        ->where('idPersona', $id)
                        ->fetch();

    if ($obtener !=false) {
      $fotos = $this->db->from('tbfotos_vehiculo')
                        ->select('COUNT(*) AS num') 
                        ->where('id_tbVehiculo',$obtener->id_tbVehiculo)
                        ->fetch('num');

      $img = $this->db->from('tbfotos_vehiculo')
                      ->where('id_tbVehiculo',$obtener->id_tbVehiculo)
                      ->fetchAll();

      $obtener = get_object_vars($obtener);  
      array_push($obtener, $fotos, $img);
      
             $this->response->result=$obtener;
      return $this->response->SetResponse(true);
    }else{
             $this->response->errors='este id no existe';
      return $this->response->SetResponse(false);
    }
  }

  public function registrar($data){
  	$placa = $data['Placa'];
  	$no_de_serie_del_vehiculo = $data['No_de_serie_del_vehiculo'];

   	$buscar = $this->db->from($this->tbVehiculo)
                       ->select(array('Placa','No_de_serie_del_vehiculo' ))
                       ->where('(No_de_serie_del_vehiculo = :serie_del_vehiculo OR Placa = :PlacaVehiculo)',
                         	array(':serie_del_vehiculo' => $no_de_serie_del_vehiculo,':PlacaVehiculo' => $placa))
                       ->execute()
                       ->fetchAll();

   	if (count($buscar)>0) {
    		     $this->response->errors='La placa  o el numero de serie ya esta registrado';
   		return $this->response->SetResponse(false);
    }else{
      $driver = $this->db->from($this->tbPersona)
                         ->where('id_tbPersona', $data['idPersona'])
                         ->fetch();
                           
      if ($driver != false) {
        $ciudad = array('idCiudad' => 1);
        $data = array_merge($data, $ciudad);
        
        $insertVehiculo = $this->db->insertInto($this->tbVehiculo, $data)
                                   ->execute();

               $this->response->result=$insertVehiculo;
        return $this->response->SetResponse(true);
      }else{
               $this->response->result=$data;
        return $this->response->SetResponse(false);
      }
    }
  }

  public function actualizar($data,$id){
    $actualizar = $this->db->update($this->tbVehiculo, $data)
                           ->where('id_tbVehiculo',$id)
                           ->execute();

           $this->response->result = $actualizar;
    return $this->response->SetResponse(true);
  }

  public function eliminar($id){
    $eliminar = $this->db->deleteFrom($this->tbVehiculo)
                         ->where('id_tbVehiculo',$id)
                         ->execute();

             $this->response->result = $eliminar;
      return $this->response->SetResponse(true);
  }

  public function actualizarCiudad($idUser, $ciudad){
    $update = $this->db->update($this->tbVehiculo)
                       ->update('idCiudad', $ciudad)
                       ->where('idPersona', $idUser)
                       ->execute();

           $this->response->result=$update;
    return $this->response->SetResponse(true);
  }

  public function actualizarZona($idUser, $zona){
    $update = $this->db->update($this->tbPersona)
                       ->update('idZona', $zona)
                       ->where('id_tbPersona', $idUser)
                       ->execute();

           $this->response->result=$update;
    return $this->response->SetResponse(true);
  }
}
?>