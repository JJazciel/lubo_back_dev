<?php
namespace App\Model;

use App\Lib\Response;

class PerfilImpuestosModel{
    private $db;
    private $table = "tbperfilImpuestos";
    private $response;

    public function __CONSTRUCT($db){
        $this->db = $db;
        $this->response = new Response();
    }

    public function listar($idPersona){
        $data = $this->db->from($this->table)
                         ->select('last4,brand,tbtarjetas.status as tarStatus')
                         ->leftJoin('tbtarjetas on tbperfilImpuestos.idTarjeta = tbtarjetas.idTarjetaConekta')
                         ->where('tbperfilImpuestos.idPersona',$idPersona)
                         ->where('tbperfilImpuestos.status',1)
                         ->fetchAll();
        if($data != false){
                    $this->response->result = $data;
            return  $this->response->SetResponse(true);
        }else{
                    $this->response->errors = 'No hay datos para este usuario';
            return  $this->response->SetResponse(false);
        }
    }

    public function obtener($idPerfilImpuestos){
        $data = $this->db->from($this->table)
                         ->where('idtbperfilImpuestos',$idPerfilImpuestos)
                         ->select('last4,brand,tbtarjetas.status as tarStatus')
                         ->leftJoin('tbtarjetas on tbperfilImpuestos.idTarjeta = tbtarjetas.idTarjetaConekta')
                         ->fetch();
        
        if($data != false){
                    $this->response->result = $data;
            return  $this->response->SetResponse(true);
        }else{
                    $this->response->errors = 'No hay datos';
            return  $this->response->SetResponse(false);
        }     
    }

    public function agregar($data){
        $alta = $this->db->insertInto($this->table,$data)
                         ->execute();
        if ($alta != false) {
                    $this->response->result = $data;
            return  $this->response->SetResponse(true);
        } else {
                    $this->response->errors = 'Error al dar de alta la el perfil';
            return  $this->response->SetResponse(false);
        }
        
    }

    public function actualizar($data){
        $idPerfilImpuestos = $data['idtbperfilImpuestos'];
        $alta = $this->db->update($this->table,$data)
                         ->where('idtbperfilImpuestos',$idPerfilImpuestos)
                         ->execute();
        if ($alta != false) {
                    $this->response->result = $data;
            return  $this->response->SetResponse(true);
        } else {
                    $this->response->errors = 'Error al actualizar de alta la el perfil';
            return  $this->response->SetResponse(false);
        }
    }

    public function eliminar($idPerfilImpuestos){
       
        $alta = $this->db->update($this->table)
                         ->set('status',0)
                         ->where('idtbperfilImpuestos',$idPerfilImpuestos)
                         ->execute();
        if ($alta != false) {
                    $this->response->result = $alta;
            return  $this->response->SetResponse(true);
        } else {
                    $this->response->errors = 'Error al actualizar de alta la el perfil';
            return  $this->response->SetResponse(false);
        }
    }

    public function cp($cp){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api-sepomex.hckdrk.mx/query/info_cp/$cp");
        // curl_setopt($ch, CURLOPT_HTTPHEADER, [
        //     'Authorization'. ':' .'Basic '. $this->authFacturama
        // ]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $res = curl_exec($ch);
        curl_close($ch);
        $d = json_decode($res);
        $arrayColonias = [];
        if (isset($d->error)) {
                   $this->response->errors = $d->error_message;
            return $this->response->SetResponse(false);
        }else{
            $ciudad = $d[0]->response->ciudad;
            $municipio = $d[0]->response->municipio;
            $estado = $d[0]->response->estado;
            
            foreach ($d as $elemento) {
                $arrayColonias[] = $elemento->response->asentamiento;
            }
            $output = [
                "ciudad" => $ciudad,
                "municipio" => $municipio,
                "estado" =>$estado,
                "colonias" => $arrayColonias
            ];
            
                    $this->response->result = $output;
            return $this->response->SetResponse(true);
        }
    }

    public function metodosSinPerfil($idPersona){
        
        $arraymetodos = [];

        $dataEfectivo = $this->db->from($this->table)
                                 ->select('idmetodo_de_pago')
                                 ->where('idPersona',$idPersona)
                                 ->where('idmetodo_de_pago',1)
                                 ->where('status',1)
                                 ->fetch();
        if ($dataEfectivo!=false) {
            $arraymetodos[]= [
                "idMetodoPago"=>1,
                "status"=>false,
                "info"=>null
            ];
        } else {
            $arraymetodos[]= [
                "idMetodoPago"=>1,
                "status"=>true,
                "info"=>null
            ];
        }
        
        $dataTarjetas = $this->db->from('tbtarjetas')
                                 ->select(null)
                                 ->select('idTarjetaConekta,last4,brand')
                                 ->where('idPersona',$idPersona)
                                 ->where('status',1)
                                 ->fetchAll();

        $dataPerfilesTarjetas = $this->db->from($this->table)
                                 ->select('idmetodo_de_pago')
                                 ->where('idPersona',$idPersona)
                                 ->where('idmetodo_de_pago',2)
                                 ->where('status',1)
                                 ->fetchAll();

        if ($dataPerfilesTarjetas != false) {
            // dataTarjetas tarjetas
            foreach ($dataTarjetas as $tarjetas) {
                $ex = false;
                foreach ($dataPerfilesTarjetas as $perfil) {

                    if ($perfil->idTarjeta == $tarjetas->idTarjetaConekta) {
                        $ex = true;
                    }

                }
                if ($ex) {
                    $arraymetodos[]= [
                        "idMetodoPago"=>2,
                        "status"=>false,
                        "info"=>[
                            "idTarjetaConekta" => $tarjetas->idTarjetaConekta,
                            "last4"=>$tarjetas->last4,
                            "brand"=>$tarjetas->brand
                        ]
                    ];
                } else {
                    $arraymetodos[]= [
                        "idMetodoPago"=>2,
                        "status"=>true,
                        "info"=>[
                            "idTarjetaConekta" => $tarjetas->idTarjetaConekta,
                            "last4"=>$tarjetas->last4,
                            "brand"=>$tarjetas->brand
                        ]
                    ];
                }
                
            }
        } else {
            foreach ($dataTarjetas as $d) {
                $arraymetodos[]= [
                    "idMetodoPago"=>2,
                    "status"=>true,
                    "info"=>[
                        "idTarjetaConekta" => $d->idTarjetaConekta,
                        "last4"=>$d->last4,
                        "brand"=>$d->brand
                    ]
                ];
            }
        }
        

               $this->response->result = $arraymetodos;
        return $this->response->SetResponse(true);
    }

}