<?php
#Modified by J_Jazc

namespace App\Model;
require_once __DIR__ . '/../lib/conekta-php-master/lib/Conekta.php';
\Conekta\Conekta::setApiKey("key_hiyJP38jUQyqLAC15xobvw");
\Conekta\Conekta::setApiVersion("2.0.0");

use App\Lib\Response,
    App\Lib\Cifrado,
    App\Lib\Push,
    App\Lib\CURL,
    App\Lib\EmailWelcome,
    App\Lib\EmailWelcomeDriver;

class PersonaModel {
  private $db;
  private $tbPersona = 'tbpersona';
  private $tableViaje = 'tbviaje';
  private $tabledireccion = 'tbdireccion';
  private $tmodo = 'tbdetallepersona';
  private $tToken = 'tbtoken';
  private $tRechazado = 'tbrechazado';
  private $tVehiculo = 'tbvehiculo';
  private $tbPush = 'tbpush';
  private $tbCalificacion = 'tbcalificacion';
  private $tbtarjeta = 'tbconekta';
  private $tbDetallesViaje = 'tbdetallesviaje';
  private $response;

  public function __CONSTRUCT($db){
    $this->db = $db;
    $this->response = new Response();
  }

  public function listar($l,$p){
    $data = $this->db->from($this->tbPersona)
                     ->limit($l)
                     ->offset($p)
                     ->orderBy('id_tbPersona DESC')
                     ->fetchAll();

    if ($data != false) {
      $total = count($data);

             $this->response->result = [
              'data'  => $data,
              'total' => $total
          ];
      return $this->response->SetResponse(true);
    } else {
      return $this->response->SetResponse(false, 'No tienes viajes');
    }
  }

  public function obtener($id){
    $obtener = $this->db->from($this->tbPersona)
                        ->where('id_tbPersona', $id)
                        ->fetch();

    if ($obtener !=false) {
             $this->response->result=$obtener;
      return $this->response->SetResponse(true);
    }else{
             $this->response->errors='este id no existe';
      return $this->response->SetResponse(false);
    }
  }

  public function getCars($id){
    $vehiculos = $this->db->from($this->tVehiculo)
                          ->where('idPersona',$id)
                          ->fetchAll();

    if ($vehiculos != false) {
            $this->response->result=$vehiculos;
      return $this->response->SetResponse(true);
    }else{
              $this->response->errors='No tienes automóviles disponibles';
      return $this->response->SetResponse(false);
    }
  }

  public function registrar($data,$TipoUser){
    $tel = $data['Telefono'];
    $mail = $data['Email'];
    $codigoPais = $data['CodigoPais'];
    $tipo = $TipoUser;

    if ($codigoPais != null) {
      $buscar = $this->db->from($this->tbPersona)
                        //->where('tbpersona.Tipo_de_usuario',$tipo)
                        ->where('Tipo_de_usuario = :tipoU AND ((Telefono = :telefono AND CodigoPais = :codigoPais ) OR Email = :email) AND Status != 2',
                                  array(':tipoU' => $tipo, ':telefono' => $tel, ':email' => $mail,':codigoPais'=>$codigoPais))
                        ->execute()
                        ->fetchAll();

      if ($buscar != false) {
              $this->response->errors='Ya hay un usuario con este Email';
        return $this->response->setResponse(false);
      }else{
        $data['Password'] = Cifrado::Sha512($data['Password']);

        if ($TipoUser == 1 || $TipoUser == 2) {
          $new_data = array('Tipo_de_usuario' => $TipoUser,'Status' => 1);
        }else{
          $new_data = array('Tipo_de_usuario' => $TipoUser,'Status' => 3);
        }

        $data = array_merge($data, $new_data);
        $insertar_user = $this->db->insertInto($this->tbPersona, $data)
                                  ->execute();
        $send = '1';
        switch ($TipoUser) {
          case 2:
            $send = EmailWelcome::Send($mail,$insertar_user,$data['Nombre']);
            break;
          case 3:
            #buscar id en tabla de registro
            $send = EmailWelcomeDriver::Send($mail,$insertar_user,$data['Nombre']);
            $datR = array('Status'=>0,'IdPersona'=>$insertar_user);
            $insertSR = $this->db->insertInto('tbregistro',$datR)
                                ->execute();
            break;
        }

        
        
        if ($send != '1') {
                $this->response->result=$insertar_user;
          return $this->response->SetResponse(false,'No Se pudo enviar el mensaje a tu correo');
        }

              $this->response->result=$insertar_user;
        return $this->response->SetResponse(true,'Se ha enviado un mensaje a tu correo');
      }
    }else{
          return $this->response->SetResponse(false,'No se encontro el codigo de pais');
    }
  }

  public function actualizar($data, $id){
    $emailUpdate = $data['Email'];
    $telUpdate = $data['Telefono'];

    $buscar = $this->db->from($this->tbPersona)
                       ->where('(Telefono = :telefono or email = :email) AND id_tbPersona != :id',
                                   array (':telefono' => $telUpdate,':email' => $emailUpdate,':id' => $id))
                       ->where('Status != 2')
                       ->execute()
                       ->fetchAll();
          
    if (count($buscar) > 0){
             $this->response->errors='Este correo o número de teléfono ya están registrados en una cuenta';
      return $this->response->SetResponse(false);
    }else{
      if ($telUpdate == "") {#No se actualiza
        $var = false;
      }else{
        $actualizar = $this->db->update($this->tbPersona)
                               ->set('Telefono', $telUpdate)
                               ->where('id_tbPersona',$id)
                               ->execute();

               $this->response->result=$actualizar;
        return $this->response->SetResponse(true);
      }
      if ($emailUpdate == "") {#No se actualiza
        $var = false;
      }else{
        if (!filter_var($emailUpdate, FILTER_VALIDATE_EMAIL)) {
                 $this->response->errors='El correo no tiene el formato correcto';
          return $this->response->SetResponse(false);
        }else{
          $actualizar = $this->db->update($this->tbPersona)
                                 ->set('Email', $emailUpdate)
                                 ->where('id_tbPersona',$id)
                                 ->execute();

                 $this->response->result=$actualizar;
          return $this->response->SetResponse(true);
        }
      }
    }
  }
   
  public function eliminar($id){
    $estado = array('Status' => 2);
    $eliminar = $this->db->update($this->tbPersona, $estado)
                         ->where('id_tbPersona',$id)
                         ->execute();

           $this->response->result=$eliminar;
    return $this->response->SetResponse(true);
  }

  #Revisar el servicio  verificarviaje y crono ---------------------------------------------------------------
  public function pedirAhora($data){
    #Verificar las zonas disponibles
    $idZona = $data['idZona'];
    $zonasDisponibles = $this->db->from('tbzona')
      ->select('COUNT(*) AS Total')
      ->where('idZona', $idZona)
      ->fetch()
      ->Total;

    if ($zonasDisponibles > 0) {
      #Agregar el viaje a Firebase y MySQL
      $idUsuario = $data['idUsuario'];
      $idOrigen = $data['idOrigen'];
      $idDestino = $data['idDestino'];
      $tipoServicio = $data['idTipoServicio'];

      $base_travel = array(
        'FechaSolicitud' => date('Y-m-d H:i:s'),
        'idUsuario' => $idUsuario,
        'idOrigen' => $idOrigen,
        'idDestino' => $idDestino
      );

      $insertarViaje = $this->db->insertInto($this->tableViaje, $base_travel)
        ->execute();

      if ($insertarViaje > 0) {
        #Insertar los detales del viaje a MySQL
        $detalles_viaje = array(
          'TarifaBase' => $data['TarifaBase'],
          'TotalKm' => $data['TotalKm'],
          'TotalTiempo' => $data['TotalTiempo'],
          'LargaDistancia' => $data['LargaDistancia'],
          'CuotaPeticion' => $data['CuotaPeticion'],
          'TarifaNocturna' => $data['TarifaNocturna'],
          'IVA' => $data['IVA'],
          'Impuesto' => $data['Impuesto'],
          'CostoAproximado' => $data['CostoAproximado'],
          'idViaje' => $insertarViaje,
          'DistanciaKilometros' => $data['distanciaKm']
        );

        $insertarDetalles = $this->db->insertInto($this->tbDetallesViaje, $detalles_viaje)
          ->execute();
        
        if ($insertarDetalles > 0) {
          #Buscar el conductor más cercano
          $latitudLongitudOrigen = $this->db->from('tbdireccion')
            ->select(null)
            ->select('tbdireccion.Latitud, tbdireccion.Longitud')
            ->where('tbdireccion.id_tbDireccion', $idOrigen)
            ->fetch();

          if ($latitudLongitudOrigen->Latitud != null && $latitudLongitudOrigen->Longitud != null) {
            $info_drivers = CURL::GetInfoDriver($idZona);

            foreach ($info_drivers as $key => $value) {
              if ($value->latitude != 0 && $value->longitude != 0 && $value->status == 1) {
                $conductoresDisponibles[$key] = $value;
              }
            }
            
            if (count($conductoresDisponibles) > 0) {
              $updateInfo = $this->updateInfoDriver($conductoresDisponibles);

              if ($updateInfo > 0) {
                $latitudOrigen = $latitudLongitudOrigen->Latitud;
                $longitudOrigen = $latitudLongitudOrigen->Longitud;

                $masCercano = $this->Driver($latitudOrigen, $longitudOrigen, $tipoServicio, 1, 6, $idZona);

                if ($masCercano != false) {
                  $direccion = $this->db->from($this->tableViaje)
                  ->select(null)
                  ->select('
                    IFNULL(Origen.Calle, "Sin detalle") AS CalleOrigen, 
                    IFNULL(Origen.Colonia, "Sin detalle") AS ColoniaOrigen, 
                    IFNULL(Origen.Municipio, "Sin detalle") AS MunicipioOrigen, 
                    IFNULL(Origen.Ciudad, "Sin detalle") AS CiudadOrigen, 
                    IFNULL(Origen.Estado, "Sin detalle") AS EstadoOrigen, 
                    IFNULL(Origen.C_P, "Sin detalle") AS C_P_Origen, 
                    IFNULL(Destino.Calle, "Sin detalle") AS CalleDestino, 
                    IFNULL(Destino.Colonia, "Sin detalle") AS ColoniaDestino, 
                    IFNULL(Destino.Municipio, "Sin detalle") AS MunicipioDestino, 
                    IFNULL(Destino.Ciudad, "Sin detalle") AS CiudadDestino, 
                    IFNULL(Destino.Estado, "Sin detalle") AS EstadoDestino, 
                    IFNULL(Destino.C_P, "Sin detalle") AS C_P_Destino, 
                    IFNULL(Destino.Latitud, "Sin detalle") AS LatitudDestino, 
                    IFNULL(Destino.Longitud, "Sin detalle") AS LongitudDestino, 
                    IFNULL(tbpersona.Nombre, "Sin detalle") AS Nombre, 
                    IFNULL(tbpersona.Apellidos, "Sin detalle") AS Apellidos, 
                    IFNULL(tbpersona.Imagen, "Sin detalle") AS Imagen, 
                    IFNULL(tbpersona.MetodoPago, 1) AS MetodoPago,
                    IFNULL(AVG(tbcalificacion.Calificacion), 5) AS Promedio
                    ')

                  ->leftJoin('tbdireccion AS Origen ON Origen.id_tbDireccion = tbviaje.idOrigen')
                  ->leftJoin('tbdireccion AS Destino ON Destino.id_tbDireccion = tbviaje.idDestino')
                  ->leftJoin('tbpersona ON tbpersona.id_tbPersona = tbviaje.idUsuario')
                  ->leftJoin('tbcalificacion On tbcalificacion.Calificado = tbviaje.idUsuario')
                  ->where('tbviaje.idViaje', $insertarViaje)
                  ->fetch();

                  #return['idViaje' => $insertarViaje, 'idDetallesViaje' => $insertarDetalles, $masCercano, $direccion];

                  $direccionOrigen = $direccion->CalleOrigen.' '.$direccion->ColoniaOrigen.' '.$direccion->MunicipioOrigen.' '.$direccion->CiudadOrigen.' '.$direccion->EstadoOrigen.' '.$direccion->C_P_Origen;

                  $direccionDestino = $direccion->CalleDestino.' '.$direccion->ColoniaDestino.' '.$direccion->MunicipioDestino.' '.$direccion->CiudadDestino.' '.$direccion->EstadoDestino.' '.$direccion->C_P_Destino;

                  $nombreUsuario = $direccion->Nombre;
                  $calificacionUsuario = $direccion->Promedio;
                  $imagenUsuario = $direccion->Imagen;

                  $travel_data_firebase = '{
                    "latitudOrigen":'.floatval($latitudOrigen).',
                    "longitudOrigen":'.floatval($longitudOrigen).',
                    "latitudDestino":'.floatval($direccion->LatitudDestino).',
                    "longitudDestino":'.floatval($direccion->LongitudDestino).',
                    "direccionOrigen":"'.$direccionOrigen.'",
                    "direccionDestino":"'.$direccionDestino.'",
                    "montoAproximado":'.floatval($data['CostoAproximado']).',
                    "idViaje":'.floatval($insertarViaje).',
                    "fechaSolicitud":"'.date('Y-m-d H:i:s').'",
                    "idConductor":'.floatval(0).',
                    "idUsuario":'.floatval($idUsuario).',
                    "nombreUsuario":"'.$nombreUsuario.'",
                    "calificacionUsuario":'.round($calificacionUsuario).',
                    "imagenUsuario":"'.$imagenUsuario.'",
                    "status":'.floatval(0).',
                    "metodoPago":'.floatval($direccion->MetodoPago).',
                    "calificadoUsuario":'.floatval(0).',
                    "calificadoDriver":'.floatval(0).',
                    "pagado":'.floatval(0).',
                    "zonaDriver":'.floatval(0).',
                    "recalcularRuta":'."false".',
                    "statusCancelacion":'.floatval(0).',
                    "motivoCancelacionDriver":""
                  }';

                  $add_travel_to_firebase = CURL::AddTravel($travel_data_firebase);
                  $name = $add_travel_to_firebase->name;

                  $travel_complement = array('idFirebase' => $name, 'idMetodoPago' => $direccion->MetodoPago);

                  $add_name = $this->db->update($this->tableViaje, $travel_complement)
                    ->where('idViaje', $insertarViaje)
                    ->execute();

                  $idMasCercano = $masCercano[0]->id_tbPersona;
                  $tokenConductor = $masCercano[0]->Token;
                  $modeloAuto = $masCercano[0]->Modelo;
                  $placaAuto = $masCercano[0]->Placa;
                  $tipoAuto = $masCercano[0]->TipoAuto;

                  $update_info = $this->actualizaciones($idMasCercano, $name, $insertarViaje, $modeloAuto, $placaAuto, $tipoAuto, $idZona);

                  if ($update_info == 1) {
                    #Mandar la push al conductor más cercano
                    $title = 'LUBO';
                    $text = 'Solicitud de viaje';
                    $data = array('Prueba' => 'prueba');
                    $push = Push::FMC($title, $text, $tokenConductor, $data); 
                    $pushJ = json_decode($push);

                           $this->response->result=[
                            $pushJ, 
                            'idviaje' => $insertarViaje, 
                            'name' => $name, 
                            'idConductor' => $idMasCercano
                           ];
                    return $this->response->SetResponse(true,1);
                  } else {
                    $viaje_cancelado = array('idConductor' => null, 'Status' => 2);

                    $viajeSinConductor = $this->db->update($this->tableViaje, $viaje_cancelado)
                                                  ->where('tbviaje.idViaje', $insertarViaje)
                                                  ->execute();

                    $deleteViaje = CURL::deleteTravel($name);

                    #Liberar al conductor de MySQL y Firebase
                    $updateStatusDriver = $this->db->update($this->tbPersona)
                                                   ->set('Status', 5)
                                                   ->where('tbpersona.id_tbPersona', $idMasCercano)
                                                   ->execute();

                    $liberarConductor = deleteIds($idMasCercano, $idZona);

                           $this->response->errors = 'No se pudo agregar el conductor al viaje.';
                    return $this->response->SetResponse(false);
                  }

                } else {
                  $viajeSinConductor = $this->db->update($this->tableViaje)
                                                ->set('Status', 2)
                                                ->where('tbviaje.idViaje', $insertarViaje)
                                                ->execute();
                                            
                         $this->response->errors = 'No hemos podido encontrar un conductor para ti, reintenta.';
                  return $this->response->SetResponse(false);
                }
              } else {
                       $this->response->errors = 'No se pudo actualizar la información de los conductores en tu zona.';
                return $this->response->SetResponse(false);
              }

            } else {
                     $this->response->errors = 'No se pudo descargar la información de los conductores en tu zona';
              return $this->response->SetResponse(false);
            }

          } else {
                   $this->response->errors = 'No se pudo obtener la informacion de tu dirección de origen, reintenta.';
            return $this->response->SetResponse(false);
          }

        } else {                  
                 $this->response->errors = 'No se pudo registrar los detalles de tu viaje, reintenta.';
          return $this->response->SetResponse(false);  
        }

      } else {
               $this->response->errors = 'No se pudo registrar tu viaje, reintenta.';
        return $this->response->SetResponse(false);
      }

    } else {
             $this->response->errors = 'Lubo aún no está disponible en esta zona.';
      return $this->response->SetResponse(false);
    }
  }

  #Conductor más cercano
  public function Driver($latitudOrigen, $longitudOrigen, $tipoServicio, $limite, $diametro, $zona){
    $cercano = $this->db->from($this->tbPersona)
                        ->select(null)
                        ->select('(6371 * ACOS(COS(RADIANS('.$latitudOrigen.')) * COS(RADIANS(Latitud)) * COS(RADIANS(Longitud) - RADIANS('.$longitudOrigen.')) + SIN(RADIANS('.$latitudOrigen.')) *SIN(RADIANS(Latitud)))) AS distance, 
                          id_tbPersona,
                          tbtoken.Token, 
                          tbvehiculo.idModoTrabajo, 
                          tbvehiculo.Placa, 
                          tbvehiculo.Modelo, 
                          tbvehiculo.Color, 
                          tbvehiculo.Año, 
                          tbmodotrabajo.Nombre AS TipoAuto')
                        ->leftJoin('tbtoken ON tbtoken.idPersona = tbpersona.id_tbPersona')
                        ->leftJoin('tbvehiculo ON tbvehiculo.idPersona = tbpersona.id_tbPersona')
                        ->leftJoin('tbmodotrabajo ON tbmodotrabajo.idModoTrabajo = tbvehiculo.idModoTrabajo')
                        ->where('Tipo_de_usuario = 3')
                        ->where('tbpersona.Status = 1')
                        ->where('tbpersona.idZona', $zona)
                        ->where('tbvehiculo.idModoTrabajo', $tipoServicio)
                        ->where('tbvehiculo.Activo = 1')
                        ->having('distance > 0')
                        ->having('distance < ?', $diametro)
                        ->orderBy('distance ASC')
                        ->limit($limite)
                        ->fetchAll();
    return $cercano;
  }

  public function actualizaciones($idDriver, $child, $idViaje, $modeloAuto, $placaAuto, $tipoAuto, $idZona){
    $updateIdTravel = CURL::updateidTravel($idDriver, $child, $idViaje, $idZona, 5);

    $addDriver = $this->db->update($this->tableViaje)
      ->set('idConductor', $idDriver)
      ->where('idViaje', $idViaje)
      ->execute();

    $updateStatusDriver = $this->db->update($this->tbPersona)
      ->set('Status', 5)
      ->where('tbpersona.id_tbPersona', $idDriver)
      ->execute();

    $expiracion = date('Y-m-d H:i:s', (strtotime("+15 second")));
    
    $datosPush = array(
      'FechaExpiracion' => $expiracion,
      'idConductor' => $idDriver,
      'idViaje' => $idViaje
    );

    $pushConductor = $this->db->insertInto($this->tbPush,$datosPush)
      ->execute();

    $auto = array(
      'model' => $modeloAuto,
      'plate' => $placaAuto,
      'typeCar' => $tipoAuto
    );

    $car = CURL::addCar($child, $auto); #Actualizar el auto en la tabla viajes en Firebase

    return 1;
  }

  public function verificarViaje($data){
    $child = $data['name'];
    $idDriver = $data['idConductor'];
    $tipoServicio = $data['idTipoServicio'];
    $zona = $data['idZona'];
    $idViaje = $data['idViaje'];

    $cronometro = $this->crono($child, $idDriver, $idViaje, $tipoServicio, $zona);

    return $cronometro;
  }

  function crono($child, $conductor, $viaje, $tipo, $zona) {
    $viajeRechazado = array(
      'idConductor' => null,
      'Status' => 2
    );

    sleep(15); #Segundos restantes en la primera ronda
    $viajeFirebase = CURL::getInfoTravel($child); #Verificar por primera vez el viaje

    if ($viajeFirebase->idConductor == 0) {
      if ($viajeFirebase->status != 9) { #Estatus 9 es cancelado por usuario
        $latitud_origen = $viajeFirebase->latitudOrigen;
        $longitud_origen = $viajeFirebase->longitudOrigen;

        #Driver al que se le asignará el viaje     #Conductor es el primero que ha rechazado
        $segundoDriver = $this->pedirOtroConductor($conductor, $viaje, $latitud_origen, $longitud_origen, $tipo, $child, 7, $zona);

        if ($segundoDriver != false) { #Sí encontró al segundo driver
          $segundo = $segundoDriver['Conductor'][0]->id_tbPersona;
          sleep(15);

          $viajeFirebase = CURL::getInfoTravel($child); #Verificar por segunda vez

          if ($viajeFirebase->idConductor == 0) {
            
            if ($viajeFirebase->status != 9) { #Aún no ha sido cancelado por el usuario
              #Driver al que se le asignará el viaje    #segundo driver que ha rechazado
              $tercerDriver = $this->pedirOtroConductor($segundo, $viaje, $latitud_origen, $longitud_origen, $tipo, $child, 8, $zona);

              if ($tercerDriver != false) { #Sí encontró al driver
                $tercero = $tercerDriver['Conductor'][0]->id_tbPersona;
                sleep(15);

                $viajeFirebase = CURL::getInfoTravel($child); #Verificar por tercera vez

                if ($viajeFirebase->idConductor == 0) { #if 3
                  
                  if ($viajeFirebase->status != 9) {
                    $dataRechazado = array(
                      'idConductor' => $tercero,
                      'idViaje' => $viaje,
                      'Fecha' => date('Y-m-d H:i:s')
                    );

                    $rechazado = $this->db->insertInto($this->tRechazado, $dataRechazado)
                                          ->execute();

                    $limpiarViaje = $this->db->update($this->tableViaje, $viajeRechazado)
                                             ->where('idViaje', $viaje)
                                             ->execute();

                    $liberarDriver = $this->db->update($this->tbPersona) #liberar del viaje al conductor en MySQL
                                              ->set('status', 1)
                                              ->where('tbpersona.id_tbPersona', $tercero)
                                              ->execute();

                    $deleteViaje = CURL::deleteTravel($child);
                    $deleteId = CURL::deleteIds($tercero, $zona);

                           $this->response->errors='No hemos podido encontrar un conductor para ti. 3';
                    return $this->response->SetResponse(false);
                  } else {
                           $this->response->errors='Tu solicitud de viaje ha sido cancelada. 3';
                    return $this->response->SetResponse(false);
                  }
                } else {
                  return $this->response->SetResponse(true, 'Ya tienes un conductor asigando. 3');
                }
              } else {
                $limpiarViaje = $this->db->update($this->tableViaje, $viajeRechazado)
                                         ->where('idViaje', $viaje)
                                         ->execute(); 

                $deleteViaje = CURL::deleteTravel($child);

                       $this->response->errors='No hemos podido encontrar un conductor para ti. 2';
                return $this->response->SetResponse(false);
              }
            } else {
                     $this->response->errors='Tu solicitud de viaje ha sido cancelada. 2';
              return $this->response->SetResponse(false);
            }
          } else {
            return $this->response->SetResponse(true, 'Ya tienes un conductor asigando. 2');
          }
        } else {
          $limpiarViaje = $this->db->update($this->tableViaje, $viajeRechazado)
                                   ->where('idViaje', $viaje)
                                   ->execute();

          $deleteViaje = CURL::deleteTravel($child);

                 $this->response->errors='No hemos podido encontrar un conductor para ti. 1';
          return $this->response->SetResponse(false);
        }
      } else {
               $this->response->errors='Tu solicitud de viaje ha sido cancelada. 1';
        return $this->response->SetResponse(false);
      }
    } else {
             $this->response->message='Ya tienes un conductor asignado. 1';
      return $this->response->SetResponse(true);
    }
  }

  public function pedirOtroConductor($idDriver, $idViaje, $latitudOrigen, $longitudOrigen, $tipoServicio, $child, $diametro, $zona){
    $conductor_rechazado = array(
      'idConductor' => $idDriver,
      'idViaje' => $idViaje,
      'Fecha' => date('Y-m-d H:i:s')
    );

    $rechazado = $this->db->insertInto($this->tRechazado, $conductor_rechazado)
                          ->execute();

    $deleteId = CURL::deleteIds($idDriver, $zona); #liberar del viaje al conductor de firebase

    $liberarDriver = $this->db->update($this->tbPersona) #liberar del viaje al conductor en MySQL
                              ->set('status', 1)
                              ->where('tbpersona.id_tbPersona', $idDriver)
                              ->execute();

    $conductoresRechazados = $this->db->from($this->tRechazado)
                                      ->where('idViaje', $idViaje)
                                      ->fetchAll();

    $Totalrechazados = count($conductoresRechazados);

    $totalR = floatval($Totalrechazados + 1);

    $res = CURL::GetInfoDriver($zona);

    foreach ($res as $key => $value) {
      if ($value->latitude != 0 && $value->longitude != 0 && $value->status == 1) {
        $conductoresdisponible[$key] = $value;
      }
    }

    if(!$conductoresdisponible){
             $this->response->errors = 'No se pudo descargar la informacion de los conductores en tu zona';
      return $this->response->SetResponse(false);
    }else{
      $updateInfo = $this->updateInfoDriver($conductoresdisponible); #Actualiza info de los Drivers a MySQl

      $masCercano = $this->Driver($latitudOrigen, $longitudOrigen, $tipoServicio, $totalR, $diametro, $zona); #Obtiene al más cercano + 1

      $sinRechazar = array(); #Contiene al conductor que aún no ha rechazado el viaje
      foreach ($masCercano as $cercano) {
        $exist = 0;
        foreach ($conductoresRechazados as $rechazados) {
          if ($cercano->id_tbPersona == $rechazados->idConductor) {
            $exist++;
          }
        }
        if ($exist == 0) {
          $sinRechazar[] = $cercano;
        }
      }

      if (count($sinRechazar) > 0) {
        $modeloAuto = $sinRechazar[0]->Modelo;
        $placaAuto = $sinRechazar[0]->Placa;
        $tipoAuto = $sinRechazar[0]->TipoAuto;

        $consultas = $this->actualizaciones($sinRechazar[0]->id_tbPersona, $child, $idViaje, $modeloAuto, $placaAuto, $tipoAuto, $zona);

        $title = 'LUBO';
        $text = 'Solicitud de viaje';
        $to = $sinRechazar[0]->Token;
        $data = array('Prueba' => 'prueba');
        $push = Push::FMC($title,$text,$to,$data);
        $pushJ = json_decode($push);
        
        return [$pushJ, 'Conductor' => $sinRechazar];
      }else{
        return 0;
      }
    }
  }

  #Actualiza la información de los conductores de Firebase a MySQL
  public function updateInfoDriver($conductoresDisponibles){
    foreach ($conductoresDisponibles as $idDriver => $data) {
      $driver_data = array(
        'Latitud' => $data->latitude,
        'Longitud' => $data->longitude,
        'Status' => $data->status,
        'idZona' => $data->idZona
      );

      $actualizaInfoDriver = $this->db->update($this->tbPersona, $driver_data)
                                  ->where('id_tbPersona', $idDriver)
                                  ->execute();

      $busca_token = $this->db->from($this->tToken)
                              ->where('idPersona', $idDriver)
                              ->fetch();

      if ($busca_token != false) { #Update
        $driver_token = array(
          'Token' => $data->deviceToken
        );

        $updateToken = $this->db->update($this->tToken, $driver_token)
                                ->where('idPersona', $idDriver)
                                ->execute();
      } else { #Add
        $driver_token = array(
          'idPersona' => $idDriver,
          'Token' => $data->deviceToken,
          'Plataforma' => $data->os
        );

        $addToken = $this->db->insertInto($this->tToken, $driver_token)
                             ->execute();
      }
    }

    return 1;
  }
    
    public function aceptarViaje($data){
      $idViaje = $data['idViaje'];
      $idConductor = $data['idConductor'];

      $expiracion = $this->db->from($this->tbPush)
                             ->where('idViaje', $idViaje)
                             ->where('idConductor', $idConductor)
                             ->fetch();

      $horaExpiracion = $expiracion->FechaExpiracion;
      $now = date('Y-m-d H:i:s');

      if ($now > $horaExpiracion) {
               $this->response->errors='Ya no puedes aceptar este viaje';
        return $this->response->SetResponse(false);
      }else{
        $zona = $data['idZona'];

        $usuarioToken = $this->db->from($this->tableViaje)
                                 ->select(null)
                                 ->select('tbtoken.Token, tbviaje.idFirebase')
                                 ->leftJoin('tbtoken ON tbtoken.idPersona = tbviaje.idUsuario')
                                 ->where('tbviaje.idViaje', $idViaje)
                                 ->fetch();

        $vehiculo = $this->db->from($this->tVehiculo)
                             ->where('idPersona = :driver AND Activo = 1', array(':driver' => $idConductor))
                             ->fetch();

        $name = $usuarioToken->idFirebase;

        $updateFirebase = CURL::addDriverToTravel($name, $idConductor, $zona);

        //Actualiza la tabla de viaje con el idConductor
        $dataC = array('idConductor' => $idConductor, 'idVehiculo' => $vehiculo->id_tbVehiculo, 'Status' => 1);
        $updateViaje = $this->db->update($this->tableViaje, $dataC)
                                ->where('idViaje',$data['idViaje'])
                                ->execute();

        #Actualizar el Status del conductor a 4 a MySQL
        $actualizado = $this->db->update($this->tbPersona)
                                ->set('Status', 4)
                                ->where('id_tbPersona', $idConductor)
                                ->execute();

        #actualizar el Status del conductor a 4 en firebase
        $status4 = CURL::statusDriver($idConductor, 4, $zona);

        //Manda la push al token del usuario
        $title = 'Viaje aceptado';
        $text = 'Un conductor ha aceptado tu viaje, espéralo';
        $tokenU = $usuarioToken->Token;
        $data = array('Prueba' => 'prueba');
        $push = Push::FMC($title,$text,$tokenU,$data);
        $pushJ = json_decode($push);

               $this->response->result=[$pushJ,'idViaje' => $idViaje];
        return $this->response->SetResponse(true);
      }
    }

    public function enOrigen($idViaje, $data){
      $estado = $data['Status'];
      if ($estado == 3) {
        $origen = $this->db->update($this->tableViaje, $data)
                           ->where('idViaje', $idViaje)
                           ->execute();
       
        #Obtener el name de Firebase de acuerdo al idViaje
        $info = $this->db->from($this->tableViaje)
                         ->select(null)
                         ->select('tbviaje.idFirebase, tbtoken.Token AS TokenUsuario, tbpersona.Nombre, tbpersona.Apellidos, tbvehiculo.Modelo, tbvehiculo.Placa')
                         ->leftJoin('tbtoken ON tbtoken.idPersona = tbviaje.idUsuario')
                         ->leftJoin('tbpersona ON tbpersona.id_tbPersona = tbviaje.idConductor')
                         ->leftJoin('tbvehiculo ON tbvehiculo.id_tbVehiculo = tbviaje.idVehiculo')
                         ->where('idViaje', $idViaje)
                         ->fetch();

        $name = $info->idFirebase;
        $tokenUsuario = $info->TokenUsuario;
        $nameDriver = $info->Nombre;
        $surnameDriver = $info->Apellidos;
        $nombreCompleto = $nameDriver.' '.$surnameDriver;
        $modelo = $info->Modelo;
        $placa = $info->Placa;

        $updateFirebase = CURL::updateStatus($name, 3);

        #Enviar la push al usuario
        $title = 'Tu conductor ha llegado';
        $to = $tokenUsuario;
        $text = 'Encuentra a '.$nombreCompleto.' en el punto de partida ('.$modelo.', '.$placa.')';
        $data = array('Prueba' => 'Prueba');
        $push = Push::FMC($title, $text, $to, $data);
        $pushJ = json_decode($push);

               $this->response->result=[$pushJ, $updateFirebase];
        return $this->response->SetResponse(true);
      }else{
               $this->response->errors='Status incorrecto';
        return $this->response->SetResponse(false);
      }
    }

    public function enCurso($idViaje, $data){
      $estado = $data['Status'];
      if ($estado == 4) {
        $dataCurso = array('FechaInicio' => date('Y-m-d H:i:s'),'Status' => $estado);
        
        $curso = $this->db->update($this->tableViaje, $dataCurso)
                           ->where('idViaje', $idViaje)
                           ->execute();

        $nameF = $this->db->from($this->tableViaje)
                          ->select(null)
                          ->select('tbviaje.idFirebase, tbtoken.Token AS TokenUsuario')
                          ->leftJoin('tbtoken ON tbtoken.idPersona = tbviaje.idUsuario')
                          ->where('idViaje', $idViaje)
                          ->fetch();

        $name = $nameF->idFirebase;
        $tokenUsuario = $nameF->TokenUsuario;

        #Actualizar en firebase el Status a 4
        $updateFirebase = CURL::updateStatus($name, 4);

        #Enviar la push al usuario
        $title = 'En ruta';
        $to = $tokenUsuario;
        $text = 'Tu viaje ha iniciado, recuerda que puedes compartir tu ubicación para seguir tu viaje';
        $data = array('Prueba' => 'Prueba');
        $push = Push::FMC($title, $text, $to, $data);
        $pushJ = json_decode($push);

               $this->response->result=[$pushJ, $updateFirebase];
        return $this->response->SetResponse(true);
      }else{
               $this->response->errors='Status incorrecto';
        return $this->response->SetResponse(false);
      }
    }

    public function enDestino($idViaje,$data){
      $estado = $data['Status'];
      $horaFin = array('FechaFin' => date('Y-m-d H:i:s'));
      $data = array_merge($data,$horaFin);

      if ($estado == 5) {
        $destino = $this->db->update($this->tableViaje, $data)
                            ->where('idViaje', $idViaje)
                            ->execute();

        //Obtener el name de Firebase de acuerdo al idViaje
        $nameF = $this->db->from($this->tableViaje)
                          ->select(null)
                          ->select('tbviaje.idFirebase, tbtoken.Token AS TokenUsuario')
                          ->leftJoin('tbtoken ON tbtoken.idPersona = tbviaje.idUsuario')
                          ->where('idViaje',$idViaje)
                          ->fetch();

        $name = $nameF->idFirebase;
        $tokenUsuario = $nameF->TokenUsuario;

        #Actualizar a firebase el Status a 5
        $updateFirebase = CURL::updateStatus($name, 5);
        
        #Enviar la push al usuario
        $title = 'Has llegado';
        $to = $tokenUsuario;
        $text = 'Has llegado a tu destino. por favor realiza el pago y califica tu viaje';
        $data = array('Prueba' => 'Prueba');
        $push = Push::FMC($title, $text, $to, $data);
        $pushJ = json_decode($push);
        
               $this->response->result=[$pushJ, $updateFirebase];
        return $this->response->SetResponse(true);
      }else{
               $this->response->errors='Status incorrecto';
        return $this->response->SetResponse(false);
      }
    }

  public function pagando($idViaje,$data){
    $estado = $data['Status'];
    if ($estado == 6) {
      $data_travel = $this->db->from($this->tableViaje)
        ->select(null)
        ->select('FechaInicio, idFirebase, tbtoken.Token AS TokenUsuario')
        ->leftJoin('tbtoken ON tbtoken.idPersona = tbviaje.idUsuario')
        ->where('idViaje', $idViaje)
        ->fetch();

      if ($data_travel != false) {
        $inicio_viaje = $data_travel->FechaInicio;
        $fin_viaje = date('Y-m-d H:i:s');

        $update_travel = $this->db->update($this->tableViaje, ['FechaFin' => $fin_viaje, 'Status' => 6])
          ->where('idViaje', $idViaje)
          ->execute();
        
        $minutos = Round(((strtotime($fin_viaje) - strtotime($inicio_viaje)) / 60), 0);

        $update_tiempo_viaje = $this->db->update($this->tbDetallesViaje)
          ->set('TiempoMinutos', $minutos)
          ->where('idViaje', $idViaje)
          ->execute();

        $update_status_viaje = CURL::updateStatus($data_travel->idFirebase, 6);
        
        #Mandar push al usuario
        $text = 'Has llegado a tu punto de destino. Ahora solo realiza el pago y califica a tu conductor.';
        $data = array('Prueba' => 'Prueba');
        $push = Push::FMC('En destino.' , $text, $data_travel->TokenUsuario, $data);

        
               $this->response->result = $update_travel;
        return $this->response->SetResponse(true);
      } else{
               $this->response->errors='El viaje al que hace referencia no existe.';
        return $this->response->SetResponse(false);
      }
    } else {
             $this->response->errors = 'Status incorrecto';
      return $this->response->SetResponse(false);
    }
    }

    #Finalizado
    public function finalizado($idViaje,$data){ #7
      $estado = $data['Status'];
      if ($estado == 7) {
        $finalizado = $this->db->update($this->tableViaje,$data)
                               ->where('idViaje', $idViaje)
                               ->execute();

        //Obtener el name de Firebase de acuerdo al idViaje
        $nameF = $this->db->from($this->tableViaje)
                          ->select(null)
                          ->select('tbviaje.idFirebase, tbviaje.idUsuario, tbviaje.idConductor')
                          ->where('idViaje',$idViaje)
                          ->fetch();

        $name = $nameF->idFirebase;
        $idUsuario = $nameF->idUsuario;
        $idDriver = $nameF->idConductor;

        $updateFirebase = CURL::updateStatus($name, 7);

               $this->response->result=$delete;
        return $this->response->SetResponse(true);
      }else{
               $this->response->errors='Status incorrecto';
        return $this->response->SetResponse(false);
      }
    }
    
    public function LiberarConductor($id){
      $driver = $this->db->update($this->tbPersona)
                         ->set('Status',1)
                         ->where('id_tbPersona',$id)
                         ->execute();

      $update = CURL::statusDriver($id, 1);

             $this->response->result=$driver;
      return $this->response->SetResponse(true);
    }

  public function TipoPago($data){
    $metodo = $data['metodoPago'];
    if ($metodo == 2) {
      $informacionPersona = $this->db->from($this->tbtarjeta)
                                     ->select(null)
                                     ->select('tbconekta.idPersona')
                                     ->where('tbconekta.idPersona', $data['idUsuario'])
                                     ->fetch();

      if ($informacionPersona == false) {
               $this->response->errors='No tienes tarjetas registradas, para continuar, agrega una.';
        return $this->response->SetResponse(false);
      } else {
        $metodo_pago_usuario = $this->db->update($this->tbPersona)
                                        ->set('MetodoPago', $data['metodoPago'])
                                        ->where('id_tbPersona', $data['idUsuario'])
                                        ->execute();

        if ($data['idViaje'] != "") {
          $metodo_pago_viaje = $this->db->update('tbviaje')
                                        ->set('idMetodoPago', $data['metodoPago'])
                                        ->where('tbviaje.idViaje', $data['idViaje'])
                                        ->execute();

          if ($data['name'] != "") {
            $metodo_pago_viaje_firebase = CURL::updateMetodoPago($data['name'], $data['metodoPago']);
          }
        }
               $this->response->result=$metodo_pago_usuario;
        return $this->response->SetResponse(true, 'Método de pago actualizado.');
      }
    } else {
      if ($data['idViaje'] != "") { #Está en viaje
        $statusViaje = $this->db->from($this->tableViaje)
                                  ->select(null)
                                  ->select('tbviaje.Status')
                                  ->where('tbviaje.idViaje', $data['idViaje'])
                                  ->fetch();
          
        if ($statusViaje->Status == 7) { #Quiere cambiar el método de pago a efectivo y no se puede --> 7
                 $this->response->errors = 'No puedes cambiar el método de pago de este viaje.';
          return $this->response->SetResponse(false);
        } else {
          $metodo_pago_usuario = $this->db->update($this->tbPersona)
                                          ->set('MetodoPago', $data['metodoPago'])
                                          ->where('id_tbPersona', $data['idUsuario'])
                                          ->execute();

          $metodo_pago_viaje = $this->db->update('tbviaje')
                                        ->set('idMetodoPago', $data['metodoPago'])
                                        ->where('tbviaje.idViaje', $data['idViaje'])
                                        ->execute();

          if ($data['name'] != "") {
            $metodo_pago_viaje_firebase = CURL::updateMetodoPago($data['name'], $data['metodoPago']);
          }
                 $this->response->result=$metodo_pago_usuario;
          return $this->response->SetResponse(true, 'Método de pago actualizado.');
        }
      } else { #No está en viaje
        $metodo_pago_usuario = $this->db->update($this->tbPersona)
                                        ->set('MetodoPago', $data['metodoPago'])
                                        ->where('id_tbPersona', $data['idUsuario'])
                                        ->execute();

               $this->response->result=$metodo_pago_usuario;
        return $this->response->SetResponse(true, 'Método de pago actualizado.');
      }
    }
  }

  public function obtenerMetodoPago($id){
    $obtener = $this->db->from($this->tbPersona)
                        ->select('tbpersona.MetodoPago')
                        ->where('id_tbPersona', $id)
                        ->fetch('MetodoPago');

    if ($obtener != false) {
      $clienteConekta = $this->db->from($this->tbtarjeta)
                             ->where('idPersona', $id)
                             ->fetch();

      $tarjetaDefecto = [];
      $tarjetasDisponibles = [];

      if ($clienteConekta != false) {
        $idCustomer = $clienteConekta->idClienteConekta;
        $customer = \Conekta\Customer::find($idCustomer);
        $listaTarjetas  = $customer->payment_sources;
          
        foreach ($listaTarjetas as $key => $tarjeta) {
          if($tarjeta->default == true){
            $tarjetaDefecto[] = $tarjeta;
          } else {
            $tarjetasDisponibles[] = $tarjeta;
          }
        }
      }

             $this->response->result=[
                'MetodoPagoDefecto' => $obtener,
                'TarjetaDefecto' => $tarjetaDefecto,
                'TarjetasDisponibles' => $tarjetasDisponibles];
      return $this->response->SetResponse(true);
    } else {
             $this->response->errors='Agrega un método de pago por defecto.';
      return $this->response->SetResponse(false);
    }
  }
}
?>