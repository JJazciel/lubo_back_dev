<?php
use App\Lib\Auth,
    App\Lib\Response,
    App\Validation\PersonaValidation,
    App\Middleware\AuthMiddleware;

$app->group('/persona/', function () {
    $this->get('listar/{l}/{p}', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->persona->listar($args['l'],$args['p']))
                   );
    });

    $this->get('obtener/{id}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->persona->obtener($args['id']))
                 );
    });

    $this->get('getCars/{id}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->persona->getCars($args['id']))
                 );
    });

    $this->post('obtenerConsulta', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->persona->obtenerConsulta($req->getParsedBody()))
                 );
    });

    $this->post('pedirAhora', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(json_encode($this->model->persona->pedirAhora($req->getParsedBody()))
                 );
    });

    $this->post('verificarViaje', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(json_encode($this->model->persona->verificarViaje($req->getParsedBody()))
                 );
    });

    $this->put('aceptarViaje', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(json_encode($this->model->persona->aceptarViaje($req->getParsedBody()))
                 );
    });

    $this->post('pedirOtroConductor', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(json_encode($this->model->persona->pedirOtroConductor($req->getParsedBody()))
                 );
    });

    #user
    $this->post('registrarUsuario', function ($req, $res, $args) {
       return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->persona->registrar($req->getParsedBody(),2))
                 );
    });
    #driver
    $this->post('registrarDriver', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->persona->registrar($req->getParsedBody(),3))
                 );
    });

    #admin
    $this->post('registrarAdmin', function ($req, $res, $args) {
      $r = PersonaValidation::validate($req->getParsedBody());
      if(!$r->response){
           return $res->withHeader('Content-type', 'application/json')
                      ->withStatus(422)
                      ->write(json_encode($r->errors));
      }

      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->persona->registrar($req->getParsedBody(),1))
                 );
    });

    $this->put('actualizar/{id}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                  json_encode($this->model->persona->actualizar($req->getParsedBody(), $args['id']))
                );
      });

    $this->delete('eliminar/{id}', function($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(json_encode($this->model->persona->eliminar($args['id']))
                 );
    });

    $this->put('enOrigen/{idViaje}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(json_encode($this->model->persona->enOrigen($args['idViaje'],$req->getParsedBody()))
                 );
    });

    $this->put('enCurso/{idViaje}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(json_encode($this->model->persona->enCurso($args['idViaje'],$req->getParsedBody()))
                 );
    });

    $this->put('enDestino/{idViaje}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(json_encode($this->model->persona->enDestino($args['idViaje'],$req->getParsedBody()))
                 );
    });

    $this->put('pagando/{idViaje}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(json_encode($this->model->persona->pagando($args['idViaje'],$req->getParsedBody()))
                 );
    });

    $this->put('finalizado/{idViaje}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(json_encode($this->model->persona->finalizado($args['idViaje'],$req->getParsedBody()))
                 );
    });
    
    $this->put('LiberarConductor/{id}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                  json_encode($this->model->persona->LiberarConductor($args['id']))
                );
      });
    
    $this->put('TipoPago', function($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(json_encode($this->model->persona->TipoPago($req->getParsedBody()))
                 );
    });
    
    $this->get('obtenerMetodoPago/{id}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->persona->obtenerMetodoPago($args['id']))
                 );
    });

    $this->put('actualizaMetodoPago/{viaje}/{idMetodo}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(json_encode($this->model->persona->actualizaMetodoPago($args['viaje'], $args['idMetodo']))
                 );
    });

});
?>