<?php
use App\Lib\Auth,
    App\Lib\Response,
    App\Validation\TarjetaValidation,
    App\Middleware\AuthMiddleware;

    $app->group('/tarjeta/',function(){
    	$this->post('registrar', function($req, $res, $args){
        $data = $req->getParsedBody();
        $id = $data['id'];
        $tokenId = $data['token_id'];
    		return $res->withHeader('Content-type','application/json')
    		           ->write(
    		           	 json_encode($this->model->tarjeta->registrar($tokenId, $id))
    		       );
    	});

      $this->get('listar/{idCliente}', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->tarjeta->listar($args['idCliente']))
                   );
      });
      
      $this->get('obtener/{id}/{idTarjeta}', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json')
                   ->write(
                     json_encode($this->model->tarjeta->obtener($args['id'],$args['idTarjeta']))
                    );
      });
      
      $this->put('actualizar', function ($req, $res, $args) {
        // $r = TarjetaValidation::actualizar($req->getParsedBody(),true);
        $data = $req->getParsedBody();
        $id = $data['id'];
        $idTarjeta = $data['idTarjeta'];
        // if(!$r->response){
        //   return $res->withHeader('Content-type', 'application/json')
        //              ->withStatus(422)
        //              ->write(json_encode($r->errors));
        // }

        return $res->withHeader('Content-type','application/json')
                   ->write(
                     json_encode($this->model->tarjeta->actualizar($req->getParsedBody(), $id, $idTarjeta))
                   );
      });
        
      $this->delete('eliminar', function($req, $res, $args) {
        $data = $req->getParsedBody();
        $id = $data['id'];
        $idTarjeta = $data['idTarjeta'];
        return $res->withHeader('Content-type','application/json')
                   ->write(json_encode($this->model->tarjeta->eliminar($id,$idTarjeta)));
      });
          
      $this->put('defaultPaymentSource', function ($req, $res, $args) {

        $data = $req->getParsedBody();
        $id = $data['id'];
        $idTarjeta = $data['idTarjeta'];

        return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->tarjeta->defaultPaymentSource($id,$idTarjeta))
                    );
      });
    });
?>