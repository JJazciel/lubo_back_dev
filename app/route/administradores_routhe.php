<?php 

use App\Lib\Response,
    App\Middleware\AuthMiddleware;

    $app->group('/administradores/', function(){
    	$this->get('listar', function($req, $res, $args){
    		return $res->withHeader('Content-type','application/json')
    		           ->write(
    		           	 json_encode($this->model->administradores->listar())
    		       );
    	});
    	$this->get('infoAdmin/{idAdmin}', function($req, $res, $args){
    		return $res->withHeader('Content-type','application/json')
    		           ->write(
    		           	 json_encode($this->model->administradores->infoAdmin($args['idAdmin'])));
		});
		$this->put('actualizar/{id}',function($req,$res,$args){
			return $res->withHeader('Content-type','application/json')
    		           ->write(
    		           	 json_encode($this->model->administradores->actualizar($args['id'],$req->getParsedBody())));
		});
    })
 ?>