<?php
use App\Lib\Auth,
    App\Lib\Response,
    App\Validation\TokenServices;

$app->group('/tokenservices/', function () {
    $this->post('sendPush/{id}', function ($req, $res, $args) {
      $parametros = $req->getParsedBody();
        return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->tokenservices->sendPush($args['id'],$parametros['mensaje']))
                   );
    });
});
    ?>