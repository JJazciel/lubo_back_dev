<?php
use App\Lib\Auth,
    App\Lib\Response,
    //App\Validation\TarifaValidation,
    App\Middleware\AuthMiddleware;

$app->group('/tarifa/', function () {
	 $this->get('obtener/{id}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->tarifa->obtener($args['id']))
                 );
    });

	 $this->post('GetDrivingDistance', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->tarifa->GetDrivingDistance($req->getParsedBody()))
                 );
    });
   
    $this->post('NormasTarifa', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->tarifa->NormasTarifa($req->getParsedBody()))
                 );
    });

    $this->get('poligonoCiudad', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->tarifa->poligonoCiudad())
                 );
    });

});
?>