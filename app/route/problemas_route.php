<?php
use App\Lib\Auth,
    App\Lib\Response,
    App\Middleware\AuthMiddleware;

$app->group('/problemas/', function () {
	 $this->get('listarproblemas/{opcional}', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->problemas->listarproblemas($req->getParsedBody()))
                   );
    });
    
    $this->get('listar/{tipoUser}/{vista}/{metodoPago}', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->problemas->listarDetallesProblemas($args['tipoUser'], $args['vista'], $args['metodoPago']))
                   );
    });

    $this->post('registrarproblemas', function($req, $res, $args){
    		return $res->withHeader('Content-type','application/json')
    		           ->write(
    		           	 json_encode($this->model->problemas->registrarproblemas($req->getParsedBody()))
    		       );
    	});
    });


?>