<?php
use App\Lib\Response,
    App\Middleware\AuthMiddleware;

$app->group('/cron/', function(){
	$this->get('restart', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->cron->restartEarnigs())
                 );
    });
});

 ?>