<?php
use App\Lib\Auth,
    App\Lib\Response,
    //App\Validation\ActualizacionValidation,
    App\Middleware\AuthMiddleware;

$app->group('/actualizacion/', function () {
     $this->get('verinfo/{idPersona}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->actualizacioninfo->verinfo($args['idPersona']))
                 );
    });

    $this->post('actualizar', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                  json_encode($this->model->actualizacioninfo->actualizar($req->getParsedBody()))
                );
    });

    $this->put('datosCorrectos/{id}/{validacion}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                  json_encode($this->model->actualizacioninfo->datosCorrectos($args['id'],$args['validacion']))
                );
    });

});
?>