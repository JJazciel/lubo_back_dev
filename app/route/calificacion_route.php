<?php
use App\Lib\Auth,
    App\Lib\Response,
    App\Validation\CalificacionValidation,
    App\Middleware\AuthMiddleware;

  $app->group('/calificacion/',function(){
   	$this->post('registrar', function($req, $res, $args){
   		return $res->withHeader('Content-type','application/json')
        ->write(
        	json_encode($this->model->calificacion->registrarCalificacion($req->getParsedBody()))
        );
    });

    $this->get('listarcalificacion/{id}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
        ->write(
          json_encode($this->model->calificacion->listarcalificacion($args['id']))
        );
    });
  });
?>