<?php
use App\Lib\Auth,
    App\Lib\Response,
    App\Middleware\AuthMiddleware;

$app->group('/infobalance/', function () {
    $this->get('listar/{idBalance}', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->infobalance->listar($args['idBalance']))
                   );
    });

    $this->get('obtener/{id}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->infobalance->obtener($args['id']))
                 );
    });

    $this->get('registrar/{fechaInicio}/{fechaFin}/{id}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->infobalance->registrar($args['fechaInicio'],$args['fechaFin'],$args['id']))
                 );
    });

    $this->put('actualizar/{id}', function ($req, $res, $args) {

      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->infobalance->actualizar($req->getParsedBody(), $args['id']))
                 );
    });

    $this->delete('eliminar/{id}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->infobalance->eliminar($args['id']))
                 );
    });
});
?>