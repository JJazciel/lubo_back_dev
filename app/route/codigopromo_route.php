<?php
use App\Lib\Auth,
    App\Lib\Response,
    App\Validation\CodigoPromoValidation,
    App\Middleware\AuthMiddleware;

    $app->group('/codigopromo/',function(){
    	$this->post('registrar', function($req, $res, $args){
    		return $res->withHeader('Content-type','application/json')
    		           ->write(
    		           	 json_encode($this->model->codigopromo->registrarCod($req->getParsedBody()))
    		       );
    	});
         $this->get('listar/{l}/{p}', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->codigopromo->listar($args['l'],$args['p']))
                   );
      });
      $this->get('obtener/{id}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->codigopromo->obtener($args['id']))
                 );
       });
         $this->put('actualizar/{id}', function ($req, $res, $args) {
            $r = CodigoPromoValidation::actualizar($req->getParsedBody(),true);
               if(!$r->response){
               return $res->withHeader('Content-type', 'application/json')
                          ->withStatus(422)
                          ->write(json_encode($r->errors));
            }

             return $res->withHeader('Content-type','application/json')
                        ->write(json_encode($this->model->codigopromo->actualizar($req->getParsedBody(), $args['id']))
                  );
            });
        $this->delete('eliminar/{id}', function($req, $res, $args) {
            return $res->withHeader('Content-type','application/json')
                 ->write(json_encode($this->model->codigopromo->eliminar($args['id']))
                 );
        });

    });
?>