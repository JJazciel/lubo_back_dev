<?php 

use App\Lib\Response,
    App\Middleware\AuthMiddleware;

    $app->group('/conductores/', function(){
    	$this->get('listar', function($req, $res, $args){
    		return $res->withHeader('Content-type','application/json')
    		           ->write(
    		           	 json_encode($this->model->conductores->listar())
    		       );
    	});
		 // listarxzona
		$this->get('listarxzona/{idZona}',function($req,$res,$args){

			return $res->withHeader('Content-type','application/json')
					   ->write(
						 json_encode($this->model->conductores->listarxzona($args['idZona']))
	   				);
		});
    	$this->get('infoDriver/{idDriver}', function($req, $res, $args){
    		return $res->withHeader('Content-type','application/json')
    		           ->write(
    		           	 json_encode($this->model->conductores->infoDriver($args['idDriver'])));
    	});
    })
 ?>