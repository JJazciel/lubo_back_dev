<?php 

use App\Lib\Response,
    App\Middleware\AuthMiddleware;

    $app->group('/chat/', function(){
    	$this->post('altaMensaje', function($req, $res, $args){
    		return $res->withHeader('Content-type','application/json')
    		           ->write(
    		           	 json_encode($this->model->chat->altaMensaje($req->getParsedBody()))
    		       );
    	});

    	$this->post('actualizarToken/{idPersona}', function($req, $res, $args){
    		return $res->withHeader('Content-type','application/json')
    		           ->write(
    		           	 json_encode($this->model->chat->actualizarToken($args['idPersona'],$req->getParsedBody()))
    		       );
    	});

        $this->post('historialMensajes', function($req, $res, $args){
            return $res->withHeader('Content-type','application/json')
                       ->write(
                         json_encode($this->model->chat->historialMensajes($req->getParsedBody()))
                   );
        });

    })
 ?>