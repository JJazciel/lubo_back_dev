<?php
use App\Lib\Auth,
    App\Lib\Response,
    //App\Validation\CodigoPromoValidation,
    App\Middleware\AuthMiddleware;

    $app->group('/factura/',function(){
        $this->post('agregarCSD', function ($req, $res, $args) {
            $data = $req->getParsedBody();
            return $res->withHeader('Content-type','application/json')
                        ->write(
                            json_encode($this->model->factura->agregarCSD($data))
                        );
        });

        $this->get('listarCSDs', function ($req, $res, $args) {
            return $res->withHeader('Content-type','application/json')
                        ->write(
                            json_encode($this->model->factura->listarCSDs())
                        );
        });
        $this->get('perfilEx/{id}', function ($req, $res, $args) {
            return $res->withHeader('Content-type','application/json')
                        ->write(
                            json_encode($this->model->factura->perfilEx($args['id']))
                        );
        });
        $this->get('obtenerCSD/{rfc}', function ($req, $res, $args) {
            return $res->withHeader('Content-type','application/json')
                        ->write(
                            json_encode($this->model->factura->obtebnerCSD($args['rfc']))
                        );
        });

        $this->put('actualizarCSD', function ($req, $res, $args) {
            $data = $req->getParsedBody();
            return $res->withHeader('Content-type','application/json')
                        ->write(
                            json_encode($this->model->factura->actualizarCSD($data['Rfc'],$data))
                        );
        });

        $this->delete('eliminarCSD', function ($req, $res, $args) {
            $data = $req->getParsedBody();
            return $res->withHeader('Content-type','application/json')
                        ->write(
                            json_encode($this->model->factura->eliminarCSD($data['Rfc']))
                        );
        });

        $this->post('crearFactura', function ($req, $res, $args) {
            $data = $req->getParsedBody();
            return $res->withHeader('Content-type','application/json')
                        ->write(
                            json_encode($this->model->factura->crearFactura($data['idViaje']))
                        );
        });

        $this->get('descargarFactura/{idViaje}', function ($req, $res, $args) {
            return $res->withHeader('Content-type','application/json')
                        ->write(
                            json_encode($this->model->factura->descargarFactura($args['idViaje']))
                            // $this->model->factura->descargarFactura($args['idViaje'])
                        );
        });

        $this->get('filtrarFacturas/{keys}', function ($req, $res, $args) {
            return $res->withHeader('Content-type','application/json')
                        ->write(
                            json_encode($this->model->factura->filtrarFacturas($args['keys']))
                        );
        });

        $this->get('enviarEmail/{idViaje}/{email}', function ($req, $res, $args) {
            return $res->withHeader('Content-type','application/json')
                        ->write(
                            json_encode($this->model->factura->enviarfacturaEmail($args['idViaje'],$args['email']))
                        );
        });

    });
  ?>   