<?php
use App\Lib\Auth,
    App\Lib\Response,
    App\Validation\DireccionValidation,
    App\Middleware\AuthMiddleware;

    $app->group('/direccion/',function(){
      $this->put('defecto/{idDireccion}/{idPersona}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->direccion->defecto($args['idDireccion'], $args['idPersona']))
                 );
      });

      $this->put('dirTrabajo/{idDireccion}/{idPersona}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->direccion->dirTrabajo($args['idDireccion'], $args['idPersona']))
                 );
      });

    	$this->post('registrarD', function($req, $res, $args){
    		return $res->withHeader('Content-type','application/json')
    		           ->write(
    		           	 json_encode($this->model->direccion->registrarD($req->getParsedBody()))
    		       );
    	});

      $this->get('listar/{id}', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->direccion->listar($args['id']))
                   );
      });

      $this->get('obtener/{id}', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->direccion->obtener($args['id']))
                 );
      });
      
      $this->put('actualizar/{idDireccion}/{idPersona}', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json')
                   ->write(json_encode($this->model->direccion->actualizar($args['idDireccion'],$args['idPersona'],$req->getParsedBody()))
              );
      });
      
      $this->delete('eliminar/{id}', function($req, $res, $args) {

            return $res->withHeader('Content-type','application/json')
                       ->write(json_encode($this->model->direccion->eliminar($args['id']))
                 );
      });
    });
    ?>