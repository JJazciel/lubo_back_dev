<?php
use App\Lib\Auth,
    App\Lib\Response,
    //App\Validation\CodigoPromoValidation,
    App\Middleware\AuthMiddleware;

    $app->group('/detalletarifa/',function(){
         $this->get('listar/{l}/{p}/{id}', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json')
                   ->write(json_encode($this->model->detalletarifa->listarTarifa($args['l'],$args['p'],$args['id'])));
      });
         });
  ?>      