<?php
use App\Lib\Auth,
    App\Lib\Response,
    App\Middleware\AuthMiddleware;

$app->group('/balance/', function () {
    $this->get('listar/{l}/{p}', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->balance->listar($args['l'],$args['p']))
                   );
    });

    $this->get('obtener/{id}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->balance->obtener($args['id']))
                 );
    });

    $this->post('registrar', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->balance->registrar($req->getParsedBody()))
                 );
    });

    $this->put('actualizar/{id}', function ($req, $res, $args) {

      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->balance->actualizar($req->getParsedBody(), $args['id']))
                 );
    });

    $this->delete('eliminar/{id}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->balance->eliminar($args['id']))
                 );
    });

    $this->get('listaSemanas', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->balance->listaSemanas())
                 );
    });

    $this->get('InformacionBalance/{idSemana}/{limite}/{paginacion}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->balance->InformacionBalance($args['idSemana'], $args['limite'], $args['paginacion']))
                 );
    });

    $this->get('corteSemanaConductor/{idConductor}/{idSemana}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->balance->corteSemanaConductor($args['idConductor'], $args['idSemana']))
                 );
    });

    $this->post('pagarSemana',function ($req,$res,$args){
      $data = $req->getParsedBody();
      $idConductor = $data["idConductor"];
      $idSemana = $data["idSemana"];
      $tipoTransaccion = $data["tipoTransaccion"];
      $monto = $data["monto"];
      $metodoPago = $data["metodoPago"];
      $idUsuario = $data["idUsuario"];
      $fechaPago = $data["FechaPago"];
      $factura = $data["Factura"];
      return $res->withHeader('Content-type','application/json')
                 ->write(
                  json_encode($this->model->balance->pagarSemana($idConductor, $idSemana, $tipoTransaccion,$monto,$metodoPago,$idUsuario,$fechaPago,$factura))
                 );
    });

    $this->get('listaMetosPago',function($req,$res,$args){
      return $res->withHeader('Content-type','application/json')
                 ->write(
                    json_encode($this->model->balance->listaMetosPago())
                 );
    });

    $this->post('enviarTiket',function($req,$res,$args){
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->balance->enviarTiket())
                 );
    });

    $this->get('dashBoardAdmin/{fechaPeticion}/{tipoSolicitud}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
        ->write(
          json_encode($this->model->balance->dashBoardAdmin($args['fechaPeticion'], $args['tipoSolicitud']))
        );
    });

});
?>