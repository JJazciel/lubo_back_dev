<?php 
use App\Lib\Response,
    App\Middleware\AuthMiddleware;

$app->group('/semana/', function(){
	$this->put('actualizaSemanas', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->semana->actualizaSemanas())
                 );
    });
});

?>