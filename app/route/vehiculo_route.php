<?php
use App\Lib\Auth,
    App\Lib\Response,
    App\Validation\VehiculoValidation,
    App\Middleware\AuthMiddleware;

$app->group('/vehiculo/',function(){
 	$this->post('registrar', function($req, $res, $args){
	return $res->withHeader('Content-type','application/json')
	           ->write(
            	json_encode($this->model->vehiculo->registrar($req->getParsedBody()))
    		     );
  });

  $this->get('listar/{l}/{p}', function ($req, $res, $args) {
    return $res->withHeader('Content-type','application/json')
               ->write(
                json_encode($this->model->vehiculo->listar($args['l'],$args['p']))
               );
  });

  $this->get('obtener/{id}', function ($req, $res, $args) {
    return $res->withHeader('Content-type','application/json')
               ->write(
                json_encode($this->model->vehiculo->obtener($args['id']))
              );
  });
      
  $this->put('actualizar/{id}', function ($req, $res, $args) {
    return $res->withHeader('Content-type','application/json')
               ->write(json_encode($this->model->vehiculo->actualizar($req->getParsedBody(), $args['id']))
               );
  });
  
  $this->delete('eliminar/{id}', function($req, $res, $args) { 
    return $res->withHeader('Content-type','application/json')
               ->write(json_encode($this->model->transaccion->eliminar($args['id']))
               );
  });

  $this->put('actualizarCiudad/{idUser}/{idCiudad}', function ($req, $res, $args) {
    return $res->withHeader('Content-type','application/json')
               ->write(json_encode($this->model->vehiculo->actualizarCiudad($args['idUser'], $args['idCiudad']))
               );
  });

  $this->put('actualizarZona/{idUser}/{idZona}', function ($req, $res, $args) {
    return $res->withHeader('Content-type','application/json')
               ->write(json_encode($this->model->vehiculo->actualizarZona($args['idUser'], $args['idZona']))
               );
  });

});
    ?>
