<?php
use App\Lib\Auth,
    App\Lib\Response,
    App\Middleware\AuthMiddleware;

$app->group('/reportes/', function () {
      $this->get('listarTotalV/{fechaInicio}/{fechaFin}/{idzona}', function ($req, $res, $args) {
            return $res->withHeader('Content-type','application/json')
                       ->write(
                              json_encode($this->model->reportes->listarTotalV($args['fechaInicio'],$args['fechaFin'],$args['idzona']))
                        );
      });
      $this->get('listarV/{fechaInicio}/{fechaFin}/{idzona}/{status}', function ($req, $res, $args) {
            return $res->withHeader('Content-type','application/json')
                       ->write(
                              json_encode($this->model->reportes->listarV($args['fechaInicio'],$args['fechaFin'],$args['idzona'],$args['status']))
                        );
      });
      $this->get('monto/{fechaInicio}/{fechaFin}/{idzona}', function ($req, $res, $args) {
            return $res->withHeader('Content-type','application/json')
                       ->write(
                              json_encode($this->model->reportes->monto($args['fechaInicio'],$args['fechaFin'],$args['idzona']))
                        );
      });
      $this->get('empresa/{fechaInicio}/{fechaFin}/{idzona}', function ($req, $res, $args) {
            return $res->withHeader('Content-type','application/json')
                       ->write(
                              json_encode($this->model->reportes->gananciaEmp($args['fechaInicio'],$args['fechaFin'],$args['idzona']))
                        );
      });
      $this->get('conductor/{fechaInicio}/{fechaFin}/{idzona}', function ($req, $res, $args) {
            return $res->withHeader('Content-type','application/json')
                       ->write(
                              json_encode($this->model->reportes->gananciaCond($args['fechaInicio'],$args['fechaFin'],$args['idzona']))
                        );
      });
      $this->get('detalleViaje/{idViaje}', function ($req, $res, $args) {
            return $res->withHeader('Content-type','application/json')
                       ->write(
                              json_encode($this->model->reportes->detalleViaje($args['idViaje']))
                        );
      });

      $this->get('verMapaDetalleViaje/{origen}/{destino}',function ($req , $res, $args){
            return $res->withHeader('Content-type','application/json')
                       ->write(
                              json_encode($this->model->reportes->verMapaDetalleViaje($args['origen'],$args['destino'],"410x200"))
                        );
      });
      // reporteDiario

      $this->get('reporteDiario/{s}/{fi}/{ff}/{l}/{of}',function ($req , $res, $args){
            $data = array(
                  
                        "Status"=>$args['s'],
                        "fechaInicio"=>$args['fi'],
                        "fechaFin"=> $args['ff'],
                        "limit"=>$args['l'],
                        "offset"=>$args['of']
            );
            return $res->withHeader('Content-type','application/json')
                       ->write(
                              json_encode($this->model->reportes->reporteDiario($data))
                        );
      });
});