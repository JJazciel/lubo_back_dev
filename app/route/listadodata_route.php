<?php
use App\Lib\Auth,
    App\Lib\Response,
    App\Middleware\AuthMiddleware;

$app->group('/listadodata/', function () {
    $this->get('listarImg/{l}/{p}', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->listadodata->listarImg($args['l'],$args['p']))
                   );
    });
     $this->get('listarDatosPendientes/{l}/{p}', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->listadodata->listarDatosPendientes($args['l'],$args['p']))
                   );
    });
});
?>