<?php
use App\Lib\Auth,
    App\Lib\Response,
    App\Validation\EmpleadoValidation,
    App\Middleware\AuthMiddleware;

$app->group('/auth/', function () {
    $this->post('autenticar', function ($req, $res, $args) {
      $parametros = $req->getParsedBody();
      $token = '';
      $plataforma = '';
      if (isset($parametros['Token'])) {
        $token = $parametros['Token'];
        $plataforma = $parametros['Plataforma'];
      }
      if($token == null || $token == ''){
        $token = null;
      }
      if($plataforma == null || $plataforma == ''){
        $plataforma = null;
      }
      return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->auth->autenticar($parametros['Telefono'],$parametros['Password'],$parametros['Tipo_usuario'],$token,$plataforma))
                   );
    });

    #validar numero
    $this->post('validarNumero', function ($req, $res, $args) {
      $parametros = $req->getParsedBody();
      return $res->withHeader('Content-type','application/json')
                   ->write(
                          json_encode($this->model->auth->validarNumero($parametros['CodigoPais'],$parametros['Telefono'],$parametros['TipoUsuario']))
                          );
    });

    #validar codigo
    $this->post('validarCodigo', function ($req, $res, $args){
      $parametros = $req->getParsedBody();
      return $res->withHeader('Content-type','application/json')
              ->write(
                json_encode($this->model->auth->validarCodigo($parametros['CodigoPais'],$parametros['Telefono'],$parametros['Codigo'])
              ));
    });

    #recuperar password
    $this->post('recuperarPassword', function ($req, $res, $args) {
      $parametros = $req->getParsedBody();
      return $res->withHeader('Content-type','application/json')
                 ->write(
                          json_encode($this->model->auth->recuperarPassword($parametros['email'], $parametros['TipoUsuario']))
                        );
    });

    #validar correo
    $this->post('validarcorreo', function ($req, $res, $args) {
      $parametros = $req->getParsedBody();
      return $res->withHeader('Content-type','application/json')
                 ->write(
                          json_encode($this->model->auth->verificarEmail($parametros['email'], $parametros['TipoUsuario'], $parametros['idPersona']))
                        );
    });

    #Token
    $this->get('getData/{token}', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json')
                     ->write(
                     json_encode($this->model->auth->getData($args['token']))
                     );
    });

    //Servicio para actualizar el número de teléfono
    $this->put('actualizarTelefono', function ($req, $res, $args) {
      $data = $req->getParsedBody(); 
      $idPersona = $data['idPersona'];
      $telefono = $data['Telefono'];
      $tipo_de_usuario = $data['Tipo_de_usuario'];
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->auth->actualizarTelefono( $idPersona,$telefono,$tipo_de_usuario))
                 );
    });
    //Actualizar email
    $this->put('actualizarEmail', function ($req, $res, $args) {
      $data = $req->getParsedBody(); 
      $idPersona = $data['idPersona'];
      $email = $data['Email'];
      $tipo_de_usuario = $data['Tipo_de_usuario'];
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->auth->actualizarEmail( $idPersona,$email,$tipo_de_usuario))
                 );
    });
    //SERVICIOS PARA WEB---------------------------------------------------------------------------------------------------------------------
    $this->get('validaId/{id}', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->auth->validaId($args['id']))
                   );
    });

    $this->post('validaCodigoWeb/{id}', function ($req, $res, $args) {
      $parametros = $req->getParsedBody();
      return $res->withHeader('Content-type','application/json')
                 ->write(
                  json_encode($this->model->auth->validaCodigoWeb($args['id'],$parametros['codigo']))
                );
    });

    $this->put('actualizaPass/{id}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->auth->actualizaPass($args['id'],$req->getParsedBody()))
                 );
    });

    $this->get('verificarurl/{id}', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->auth->validarEmail($args['id']))
                   );
    });

    $this->put('desactivarUsuario', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->auth->desactivarUsuario($req->getParsedBody()))
                 );
    });

    $this->put('logout',function ($req,$res,$args){
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->auth->logout($req->getParsedBody()))
                );
    });

});