<?php
use App\Lib\login,
    App\Lib\Response,
    App\Middleware\AuthMiddleware;

$app->group('/img/', function () {
  
  $this->get('listar', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->img->listar($args['l'],$args['p'],$args['u']))
                   );
  });

  $this->get('listarDoc/{id}', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->img->listarDoc($args['id']))
                   );
  });

  $this->get('listarDA/{id}', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->img->listarDA($args['id']))
                   );
  });

  $this->get('obtener/{id}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->img->obtener($args['id']))
                 );
  });

  $this->post('cargar/{documento}/{id}', function ($req, $res, $args) {
      $file = $_FILES;
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->img->cargar($file,$args['documento'],$args['id']))
                 );
  });

  $this->put('actualizar/{id}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->img->actualizar($req->getParsedBody(), $args['id']))
                 );
  });

  $this->delete('eliminar/{id}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->img->eliminar($args['id']))
                 );
  });

  $this->get('verImgenPerfil/{idPersona}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->img->verImg($args['idPersona']))
                 );
  });
  
  $this->put('actualizarstatus/{id}/{verificar}', function ($req, $res, $args) {

      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->img->actualizarstatus($req->getParsedBody(), $args['id'],$args['verificar']))
                 );
  });

  $this->put('validarImg/{idUsuario}', function ($req, $res, $args) {
    return $res->withHeader('Content-type','application/json')
               ->write(
                   json_encode($this->model->img->validarImg($args['idUsuario']))
               );
  });

  $this->post('updateFotoAuto/{id}', function ($req, $res, $args) {
    $file = $_FILES;
    return $res->withHeader('Content-type','application/json')
               ->write(
                 json_encode($this->model->img->updateFotoAuto($file,$args['id']))
               );
  });

  $this->put('updateStatusDoc/{id}',function($req,$res,$args){
      return $res->withHeader('Content-type','application/json')
                ->write(
                    json_encode($this->model->img->updateStatusDoc($args['id'],$req->getParsedBody()))
                );
  });

  $this->put('updataFechaEx/{id}',function($req,$res,$args){
    return $res->withHeader('Content-type','application/json')
              ->write(
                  json_encode($this->model->img->updataFechaEx($args['id'],$req->getParsedBody()))
              );
  });
  $this->get('actuurl',function($req,$res,$args){
    return $res->withHeader('Content-type','application/json')
              ->write(
                  json_encode($this->model->img->actuurl())
              );
  });

  $this->delete('rechazarImagenPerfil',function($req,$res,$args){
    $data = $req->getParsedBody();
    $id = $data["idPersona"];
    return $res->withHeader('Content-type','application/json')
              ->write(
                  json_encode($this->model->img->rechazarImagenPerfil($id))
              );
  });

});