<?php
use App\Lib\Auth,
    App\Lib\Response,
    App\Validation\ViajeValidation,
    App\Middleware\AuthMiddleware;

  $app->group('/viaje/',function(){
    $this->get('listarViajes/{limite}/{paginacion}/{idUsuario}/{tipoViaje}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                  json_encode($this->model->viaje->listarViajes($args['limite'], $args['paginacion'], $args['idUsuario'], $args['tipoViaje'])));
    });

    #web
    $this->get('historialViajes/{limite}/{paginacion}/{idUsuario}/{tipoViaje}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                  json_encode($this->model->viaje->historialViajes($args['limite'], $args['paginacion'], $args['idUsuario'], $args['tipoViaje'])));
    });

    $this->get('obtener/{id}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                  json_encode($this->model->viaje->obtener($args['id'])));
    });

    $this->get('detallesViaje/{idUser}/{idViaje}/{tipo}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                  json_encode($this->model->viaje->detallesViaje($args['idUser'], $args['idViaje'], $args['tipo'])));
    });

    $this->get('tarifaViaje/{idViaje}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                  json_encode($this->model->viaje->tarifaViaje($args['idViaje'])));
    });

    $this->put('cancelarViajeUsuario', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                  json_encode($this->model->viaje->cancelarViajeUsuario($req->getParsedBody()))
                 );
    });

    $this->get('detallesCancelacion/{tipoUsuario}/{tipo}', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->viaje->detallesCancelacion($args['tipoUsuario'], $args['tipo']))
                   );
    });

    $this->put('cancelarViajeDriver', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                  json_encode($this->model->viaje->cancelarViajeDriver($req->getParsedBody()))
                 );
    });

    $this->get('viajesSinTransaccion/{l}/{o}/{idDriver}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                  json_encode($this->model->viaje->viajesSinTransaccion($args['l'], $args['o'], $args['idDriver'])));
    });

  });
?>