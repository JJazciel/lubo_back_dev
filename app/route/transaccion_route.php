<?php
use App\Lib\Auth,
    App\Lib\Response,
    App\Validation\TransaccionValidation,
    App\Middleware\AuthMiddleware;

  $app->group('/transaccion/',function(){
  	$this->post('registrar', function($req, $res, $args){
  		return $res->withHeader('Content-type','application/json')
  		           ->write(
  		           	 json_encode($this->model->transaccion->registrarTransaccion($req->getParsedBody()))
  		       );
  	});

    $this->get('listar/{l}/{p}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                  json_encode($this->model->transaccion->listar($args['l'],$args['p']))
                 );
    });

    $this->get('listarTransaccion/{id}/{fechaPeticion}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->transaccion->listarTransaccion($args['id'],$args['fechaPeticion']))
                 );
    });

    $this->get('detalleGanancias/{idDriver}/{fechaPeticion}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->transaccion->detalleGananciasDiarios($args['idDriver'],$args['fechaPeticion']))
                 );
    });

    $this->get('obtener/{id}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->transaccion->obtener($args['id']))
                 );
    });

    $this->put('actualizarstatus/{id}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(json_encode($this->model->transaccion->actualizarstatus($req->getParsedBody(), $args['id']))
                );
    });

    $this->put('actualizar/{id}', function ($req, $res, $args) {
      $r = TransaccionValidation::actualizar($req->getParsedBody(),true);
      if(!$r->response){
        return $res->withHeader('Content-type', 'application/json')
                   ->withStatus(422)
                   ->write(json_encode($r->errors));
      }

      return $res->withHeader('Content-type','application/json')
                 ->write(json_encode($this->model->transaccion->actualizar($req->getParsedBody(), $args['id'])));
    });

    $this->delete('eliminar/{id}', function($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(json_encode($this->model->transaccion->eliminar($args['id'])));
    });

    $this->get('gananciasDia/{idUser}/{fechaPeticion}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->transaccion->gananciasDia($args['idUser'],$args['fechaPeticion'])));
    });

  });
?>