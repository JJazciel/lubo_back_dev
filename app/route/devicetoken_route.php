<?php
use App\Lib\Auth,
    App\Lib\Response,
    App\Validation\DevicetokenValidation,
    App\Middleware\AuthMiddleware;

    $app->group('/devicetoken/',function(){
    	$this->post('altaToken', function($req, $res, $args){
    		return $res->withHeader('Content-type','application/json')
    		           ->write(
    		           	 json_encode($this->model->devicetoken->altaToken($req->getParsedBody()))
    		       );
    	});

      $this->get('listar/{l}/{p}', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->devicetoken->listar($args['l'],$args['p']))
                   );
      });

      $this->get('obtener/{id}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->devicetoken->obtener($args['id']))
                 );
      });
      
      $this->delete('eliminar/{id}', function($req, $res, $args) {
            return $res->withHeader('Content-type','application/json')
                 ->write(json_encode($this->model->devicetoken->eliminar($args['id']))
                 );
        });
    });
    ?>