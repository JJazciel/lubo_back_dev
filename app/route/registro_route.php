<?php
use App\Lib\Auth,
    App\Lib\Response,
    // App\Validation\VehiculoValidation,
    App\Middleware\AuthMiddleware;

    $app->group('/registro/',function(){
    	$this->post('registrar', function($req, $res, $args){
    		return $res->withHeader('Content-type','application/json')
    		           ->write(
    		           	 json_encode($this->model->registro->registrar($req->getParsedBody()))
    		       );
            });
      $this->get('listar', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->registro->listar())
                   );
      });

      $this->get('obtener/{id}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->registro->obtener($args['id']))
                 );
       });
      
        $this->put('actualizar/{id}', function ($req, $res, $args) {
            //  $r = VehiculoValidation::actualizar($req->getParsedBody(),true);
            //    if(!$r->response){
            //    return $res->withHeader('Content-type', 'application/json')
            //           ->withStatus(422)
            //           ->write(json_encode($r->errors));
            // }
             return $res->withHeader('Content-type','application/json')
                        ->write(json_encode($this->model->registro->actualizar($req->getParsedBody(), $args['id']))
                  );
            });
    	


         $this->delete('eliminar/{id}', function($req, $res, $args) {
   
            return $res->withHeader('Content-type','application/json')
                       ->write(json_encode($this->model->transaccion->eliminar($args['id']))
                 );
        });
    });
    ?>
