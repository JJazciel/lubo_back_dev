<?php
use App\Lib\Auth,
    App\Lib\Response,
    App\Validation\DocumentosValidation,
    App\Middleware\AuthMiddleware;

    $app->group('/documentos/',function(){
    	$this->post('registrar', function($req, $res, $args){
    		return $res->withHeader('Content-type','application/json')
    		           ->write(
    		           	 json_encode($this->model->documentos->registrarD($req->getParsedBody()))
    		       );
    	});
      $this->get('listar/{l}/{p}', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->documentos->listar($args['l'],$args['p']))
                   );
      });
      $this->get('listartipo/{l}/{p}', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->documentos->listartipo($args['l'],$args['p']))
                   );
      });
      $this->get('obtener/{id}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->documentos->obtener($args['id']))
                 );
       });
        $this->put('actualizar/{id}', function ($req, $res, $args) {
            $r = DocumentosValidation::actualizar($req->getParsedBody(),true);
               if(!$r->response){
               return $res->withHeader('Content-type', 'application/json')
                          ->withStatus(422)
                          ->write(json_encode($r->errors));
            }

             return $res->withHeader('Content-type','application/json')
                        ->write(json_encode($this->model->documentos->actualizar($req->getParsedBody(), $args['id']))
                  );
            });
        $this->delete('eliminar/{id}', function($req, $res, $args) {

            return $res->withHeader('Content-type','application/json')
                       ->write(json_encode($this->model->documentos->eliminar($args['id']))
                 );
        });
    });
    ?>