<?php
use App\Lib\Auth,
    App\Lib\Response,
    App\Middleware\AuthMiddleware;
    $app->group('/efectivo/',function(){
    	$this->post('registrar/{id}', function($req, $res, $args){
          return $res->withHeader('Content-type','application/json')
                   ->write(
                     json_encode($this->model->efectivo->registrar($req->getParsedBody(),$args['id']))
               );
      });

    });
?>