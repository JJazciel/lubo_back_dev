<?php
use App\Lib\Auth,
    App\Lib\Response,
    App\Middleware\AuthMiddleware;

$app->group('/perfilimpuestos/', function () {
    $this->get('listar/{idPersona}', function ($req, $res, $args){
        return $res->withHeader('Content-type','application/json')
                   ->write(
                        json_encode($this->model->perfilImpuestos->listar($args['idPersona']))
                   );
    });

    $this->get('obtener/{idPerfil}', function ($req, $res, $args){
        return $res->withHeader('Content-type','application/json')
                   ->write(
                        json_encode($this->model->perfilImpuestos->obtener($args['idPerfil']))
                   );
    });

    $this->post('agregar',function($req,$res,$args){
        $data = $req->getParsedBody();
        return $res->withHeader('Content-type','application/json')
                   ->write(
                       json_encode($this->model->perfilImpuestos->agregar($data))
                   );
    });

    $this->put('actualizar',function($req,$res,$args){
        $data = $req->getParsedBody();
        return $res->withHeader('Content-type','application/json')
                   ->write(
                       json_encode($this->model->perfilImpuestos->actualizar($data))
                   );
    });

    $this->delete('eliminar',function($req,$res,$args){
       $data = $req->getParsedBody();
       return $res->withHeader('Content-type','application/json')
                  ->write(
                       json_encode($this->model->perfilImpuestos->eliminar($data['idPerfilImpuestos']))
                  );
    });

    $this->get('cp/{cp}',function($req,$res,$args){
        return $res->withHeader('Content-type','application/json')
                  ->write(
                       json_encode($this->model->perfilImpuestos->cp($args['cp']))
                  );
    });

    $this->get('metodosSinPerfil/{idPersona}',function($req,$res,$args){
        return $res->withHeader('Content-type','application/json')
                  ->write(
                       json_encode($this->model->perfilImpuestos->metodosSinPerfil($args['idPersona']))
                  );
    });

});