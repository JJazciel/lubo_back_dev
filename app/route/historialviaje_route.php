<?php
use App\Lib\Auth,
    App\Lib\Response,
    //App\Validation\CodigoPromoValidation,
    App\Middleware\AuthMiddleware;

    $app->group('/historialviaje/',function(){
         $this->get('listarViaje/{l}/{p}/{id}', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->historialviaje->listarViaje($args['l'],$args['p'],$args['id']))
                   );
      });
         });
  ?>      