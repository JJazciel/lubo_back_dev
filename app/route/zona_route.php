<?php
use App\Lib\Auth,
    App\Lib\Response,
    App\Middleware\AuthMiddleware;

  $app->group('/zona/',function(){
   	$this->post('registrar', function($req, $res, $args){
   		return $res->withHeader('Content-type','application/json')
   		           ->write(
                  json_encode($this->model->zona->registrarCod($req->getParsedBody()))
    		         );
    });
    
    $this->get('listar', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                  json_encode($this->model->zona->listar($args['l'],$args['p']))
                 );
    });

    $this->get('obtener/{id}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->zona->obtener($args['id']))
                 );
    });
    
    $this->put('actualizar/{id}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                  json_encode($this->model->zona->actualizar($req->getParsedBody(), $args['id']))
                 );
    });

    $this->delete('eliminar/{id}', function($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                  json_encode($this->model->zona->eliminar($args['id']))
                 );
    });

  });
?>