<?php
use App\Lib\Auth,
    App\Lib\Response,
    App\Validation\DataPersonaModel;

 $app->group('/datapersona/',function(){
 	 $this->get('obtener/{id}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->datapersona->obtener($args['id']))
                 );
       });
     });
?>