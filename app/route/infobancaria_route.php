<?php
use App\Lib\Response;

$app->group('/infobancaria/',function(){
    $this->get('listar/{id}',function($req, $res, $args){
        return $res->withHeader('Content-type','application/json')
                   ->write(
                       json_encode($this->model->infobancaria->listar($args['id']))
                   );
    });

    $this->post('registrar',function($req,$res,$args){
        return $res->withHeader('Content-type','application/json')
                   ->write(
                       json_encode($this->model->infobancaria->registrar($req->getParsedBody()))
                   );
    });

    $this->put('actualizar/{id}',function($req,$res,$args){
        return $res->withHeader('Content-type','application/json')
                    ->write(
                        json_encode($this->model->infobancaria->actualizar($args['id'],$req->getParsedBody()))
                    );
    });
});
