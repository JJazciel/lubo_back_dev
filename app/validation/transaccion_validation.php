<?php
namespace App\Validation;

use App\Lib\Response;
class TransaccionValidation {
	  public static function actualizar($data, $update = false) {
        $response = new Response();

          $key = 'id_tbTarifa';
        if(empty($data[$key])) {
            $response->errors[$key] = 'Este campo es obligatorio';
        } else {
            $value = $data[$key];

            if(!is_numeric($value)) {
                $response->errors[$key] = 'el id debe ser un numero';
            }
        }

      $key = 'id_tbViaje';
        if(empty($data[$key])){
            $response->errors[$key][] = 'Este campo es obligatorio';
        } else {
            $value = $data[$key];

            if(!is_numeric($value)) {
                $response->errors[$key][] = 'el id viaje debe ser un numero';
            }
        }
        $key = 'Monto';
        if(empty($data[$key])){
            $response->errors[$key][] = 'Este campo es obligatorio';
        } else {
            $value = $data[$key];

            if(strlen($value) < 2) {
                $response->errors[$key][] = 'Debe contener como mínimo 2 caracteres';
            }
        }
        $key = 'Tipo';
        if(empty($data[$key])){
            $response->errors[$key][] = 'Este campo es obligatorio';
        } else {
            $value = $data[$key];

            if(strlen($value) < 4) {
                $response->errors[$key][] = 'Debe contener como minimo 4 caracteres';
            }
        }
    
        $response->setResponse(count($response->errors) === 0);

        return $response;
    }
}
?>