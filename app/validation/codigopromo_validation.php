<?php
namespace App\Validation;
use App\Lib\Response;

class CodigoPromoValidation {
	  public static function actualizar($data, $update = false) {
        $response = new Response();

          $key = 'id_tbPersona';
        if(empty($data[$key])) {
            $response->errors[$key] = 'Este campo es obligatorio';
        } else {
            $value = $data[$key];

            if(!is_numeric($value)) {
                $response->errors[$key] = 'El id persona debe ser un numero';
            }
        }

        $response->setResponse(count($response->errors) === 0);

        return $response;
    }
}
?>