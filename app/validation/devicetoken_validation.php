<?php
namespace App\Validation;
use App\Lib\Response;

class DevicetokenValidation {
	  public static function actualizar($data, $update = false) {
        $response = new Response();

          $key = 'id_tbPersonas';
        if(empty($data[$key])) {
            $response->errors[$key] = 'Este campo es obligatorio';
        } else {
            $value = $data[$key];

            if(!is_numeric($value)) {
                $response->errors[$key] = 'El id persona debe ser un numero';
            }
        }

        $key = 'Token';
        if(empty($data[$key])) {
            $response->errors[$key] = 'Este campo es obligatorio';
        } else {
            $value = $data[$key];

            if(strlen($value) > 46) {
                $response->errors[$key] = 'El id viaje tiene que ser un numero';
            }
        }
        $key = 'Plataforma';
        if(empty($data[$key])) {
            $response->errors[$key] = 'Este campo es obligatorio';
        } else {
       
            $value = $data[$key];

            if(strlen($value) > 46) {
                $response->errors[$key][] = 'Debe contener caracteres';
            }
        }
    
        $response->setResponse(count($response->errors) === 0);

        return $response;
    }
}
?>