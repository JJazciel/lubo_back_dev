<?php
namespace App\Validation;

use App\Lib\Response;
class ViajeValidation {
	  public static function actualizar($data, $update = false) {
        $response = new Response();

          $key = 'Fecha';
        if(empty($data[$key])) {
            $response->errors[$key] = 'Este campo es obligatorio';
        } else {
            $value = $data[$key];

            if(strlen($value) < 4) {
                $response->errors[$key] = 'Fecha debe contener datos';
            }
        }

      $key = 'Hora';
        if(empty($data[$key])){
            $response->errors[$key][] = 'Este campo es obligatorio';
        } else {
            $value = $data[$key];

            if(strlen($value) < 4) {
                $response->errors[$key][] = 'Hora debe contener datos';
            }
        }
        $key = 'idUsuarios';
        if(empty($data[$key])){
            $response->errors[$key][] = 'Este campo es obligatorio';
        } else {
            $value = $data[$key];

            if(!is_numeric($value)) {
                $response->errors[$key][] = 'el campo id debe contener solo numeros';
            }
        }
        $key = 'idConductores';
        if(empty($data[$key])){
            $response->errors[$key][] = 'Este campo es obligatorio';
        } else {
            $value = $data[$key];

            if(!is_numeric($value)) {
                $response->errors[$key][] = 'el campo id debe contener solo numeros';
            }
        }
        $key = 'id_tbVehiculo';
        if(empty($data[$key])){
            $response->errors[$key][] = 'Este campo es obligatorio';
        } else {
            $value = $data[$key];

            if(!is_numeric($value)) {
                $response->errors[$key][] = 'el id vehiculo debe ser un numero';
            }
        }
        $key = 'idOrigen';
        if(empty($data[$key])) {
            $response->errors[$key] = 'Este campo es obligatorio';
        } else {
            $value = $data[$key];

            if(strlen($value) < 4) {
                $response->errors[$key] = 'idOrigen debe contener datos';
            }
        }
        $key = 'idDestino';
        if(empty($data[$key])) {
            $response->errors[$key] = 'Este campo es obligatorio';
        } else {
            $value = $data[$key];

            if(strlen($value) < 4) {
                $response->errors[$key] = 'idDestino debe contener datos';
            }
        }
        $key = 'Status';
        if(empty($data[$key])) {
            $response->errors[$key] = 'Este campo es obligatorio';
        } else {
            $value = $data[$key];

            if(strlen($value) < 4) {
                $response->errors[$key] = 'status debe contener mas de 4 caracteres';
            }
        }
    
        $response->setResponse(count($response->errors) === 0);

        return $response;
    }
}
?>