<?php
namespace App\Validation;
use App\Lib\Response;

class TarjetaValidation {
	  public static function actualizar($data, $update = false) {
        $response = new Response();

          $key = 'Nombre';
        if(empty($data[$key])) {
            $response->errors[$key] = 'Este campo es obligatorio';
        } else {
            $value = $data[$key];

            if(strlen($value) < 4) {
                $response->errors[$key] = 'Debe contener como mínimo 4 caracteres';
            }
        }
         $key = 'mes';
        if(empty($data[$key])) {
            $response->errors[$key] = 'Este campo es obligatorio';
        } else {
            $value = $data[$key];

            if(!is_numeric($value)) {
                $response->errors[$key] = 'este campo debe ser numerico';
            }
        }
         $key = 'year';
        if(empty($data[$key])) {
            $response->errors[$key] = 'Este campo es obligatorio';
        } else {
            $value = $data[$key];

            if(!is_numeric($value)) {
                $response->errors[$key] = 'este campo debe ser numerico';
            }
        }

        $response->setResponse(count($response->errors) === 0);

        return $response;
    }
}
?>