<?php
namespace App\Validation;
use App\Lib\Response;

class CalificacionValidation {
	  public static function actualizar($data, $update = false) {
        $response = new Response();

          $key = 'idPersona';
        if(empty($data[$key])) {
            $response->errors[$key] = 'Este campo es obligatorio';
        } else {
            $value = $data[$key];

            if(!is_numeric($value)) {
                $response->errors[$key] = 'El id persona debe ser un numero';
            }
        }

        $key = 'idViaje';
        if(empty($data[$key])) {
            $response->errors[$key] = 'Este campo es obligatorio';
        } else {
            $value = $data[$key];

            if(!is_numeric($value)) {
                $response->errors[$key] = 'El id viaje tiene que ser un numero';
            }
        }

      $key = 'Calificacion';
        if(empty($data[$key])){
            $response->errors[$key][] = 'Este campo es obligatorio';
        } else {
            $value = $data[$key];

            if(!is_numeric($value)) {
                $response->errors[$key][] = 'EL campo calificacion debe contener un numero';
            }
        }
        $key = 'Comentario';
       
            $value = $data[$key];

            if(strlen($value) > 101) {
                $response->errors[$key][] = 'Debe contener 100 caracteres o menos';
            }
    
        $response->setResponse(count($response->errors) === 0);

        return $response;
    }
}
?>