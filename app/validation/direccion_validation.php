<?php
namespace App\Validation;
use App\Lib\Response;

class DireccionValidation {
	  public static function actualizar($data, $update = false) {
        $response = new Response();

          $key = 'id_tbViajes';
        if(empty($data[$key])) {
            $response->errors[$key] = 'Este campo es obligatorio';
        } else {
            $value = $data[$key];

            if(!is_numeric($value)) {
                $response->errors[$key] = 'El id viaje debe ser un numero';
            }
        }

        $key = 'Descripcion';
            $value = $data[$key];

            if(strlen($value) > 46) {
                $response->errors[$key] = 'El id viaje tiene que ser un numero';
            }
        $key = 'Guardado';
        if(empty($data[$key])) {
            $response->errors[$key] = 'Este campo es obligatorio';
        } else {
       
            $value = $data[$key];

            if(strlen($value) > 46) {
                $response->errors[$key][] = 'Debe contener caracteres';
            }
        }

        $key = 'Calle';
        if(empty($data[$key])) {
            $response->errors[$key] = 'Este campo es obligatorio';
        } else {
       
            $value = $data[$key];

            if(strlen($value) > 46) {
                $response->errors[$key][] = 'Debe contener caracteres';
            }
        }$key = 'Avenida';
        if(empty($data[$key])) {
            $response->errors[$key] = 'Este campo es obligatorio';
        } else {
       
            $value = $data[$key];

            if(strlen($value) > 46) {
                $response->errors[$key][] = 'Debe contener caracteres';
            }
        }$key = 'Numero_Interior';
        if(empty($data[$key])) {
            $response->errors[$key] = 'Este campo es obligatorio';
        } else {
       
            $value = $data[$key];

            if(strlen($value) > 6) {
                $response->errors[$key][] = 'Debe contener caracteres';
            }
        }$key = 'Numero_Exterior';
        if(empty($data[$key])) {
            $response->errors[$key] = 'Este campo es obligatorio';
        } else {
       
            $value = $data[$key];

            if(strlen($value) > 46) {
                $response->errors[$key][] = 'Debe contener caracteres';
            }
        }$key = 'Municipio';
        if(empty($data[$key])) {
            $response->errors[$key] = 'Este campo es obligatorio';
        } else {
       
            $value = $data[$key];

            if(strlen($value) > 21) {
                $response->errors[$key][] = 'Debe contener caracteres';
            }
        }$key = 'C_P';
        if(empty($data[$key])) {
            $response->errors[$key] = 'Este campo es obligatorio';
        } else {
       
            $value = $data[$key];

            if(strlen($value) > 6) {
                $response->errors[$key][] = 'Debe contener caracteres';
            }
        }$key = 'Estado';
        if(empty($data[$key])) {
            $response->errors[$key] = 'Este campo es obligatorio';
        } else {
       
            $value = $data[$key];

            if(strlen($value) > 21) {
                $response->errors[$key][] = 'Debe contener caracteres';
            }
        }$key = 'Latitud';
        if(empty($data[$key])) {
            $response->errors[$key] = 'Este campo es obligatorio';
        } else {
       
            $value = $data[$key];

            if(strlen($value) > 46) {
                $response->errors[$key][] = 'Debe contener caracteres';
            }
        }$key = 'Longitud';
        if(empty($data[$key])) {
            $response->errors[$key] = 'Este campo es obligatorio';
        } else {
       
            $value = $data[$key];

            if(strlen($value) > 46) {
                $response->errors[$key][] = 'Debe contener caracteres';
            }
        }
        $key = 'id_tbPersona';
        if(empty($data[$key])) {
            $response->errors[$key] = 'Este campo es obligatorio';
        } else {
            $value = $data[$key];

            if(!is_numeric($value)) {
                $response->errors[$key] = 'El id Persona debe ser un numero';
            }
        }
    
        $response->setResponse(count($response->errors) === 0);

        return $response;
    }
}
?>