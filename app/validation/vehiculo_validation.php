<?php
namespace App\Validation;

use App\Lib\Response;
class VehiculoValidation {
	  public static function actualizar($data, $update = false) {
        $response = new Response();

          $key = 'id_tbPersona';
        if(empty($data[$key])) {
            $response->errors[$key] = 'Este campo es obligatorio';
        } else {
            $value = $data[$key];

            if(!is_numeric($value)) {
                $response->errors[$key] = 'el id debe ser un numero';
            }
        }

        $key = 'Placa';
        if(empty($data[$key])) {
            $response->errors[$key] = 'Este campo es obligatorio';
        } else {
            $value = $data[$key];

            if(strlen($value) > 10) {
                $response->errors[$key] = 'Debe contener como mínimo 10 caracteres';
            }
        }

      $key = 'Modelo';
        if(empty($data[$key])){
            $response->errors[$key][] = 'Este campo es obligatorio';
        } else {
            $value = $data[$key];

            if(strlen($value) < 4) {
                $response->errors[$key][] = 'Debe contener como mínimo 4 caracteres';
            }
        }
        $key = 'Color';
        if(empty($data[$key])){
            $response->errors[$key][] = 'Este campo es obligatorio';
        } else {
            $value = $data[$key];

            if(strlen($value) < 4) {
                $response->errors[$key][] = 'Debe contener como mínimo 4 caracteres';
            }
        }
        $key = 'Tipo';
        if(empty($data[$key])){
            $response->errors[$key][] = 'Este campo es obligatorio';
        } else {
            $value = $data[$key];

            if(strlen($value) < 4) {
                $response->errors[$key][] = 'Debe contener como mínimo 4 caracteres';
            }
        }
        $key = 'No_de_serie_del_vehiculo';
        if(empty($data[$key])){
            $response->errors[$key][] = 'Este campo es obligatorio';
        } else {
            $value = $data[$key];

            if(strlen($value) < 20) {
                $response->errors[$key][] = 'Debe contener como mínimo 20 caracteres';
            }
        }
    
        $response->setResponse(count($response->errors) === 0);

        return $response;
    }
}
?>