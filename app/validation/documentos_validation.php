<?php
namespace App\Validation;

use App\Lib\Response;
class DocumentosValidation {
	  public static function actualizar($data, $update = false) {
        $response = new Response();

          $key = 'id_tbPersona';
        if(empty($data[$key])) {
            $response->errors[$key] = 'Este campo es obligatorio';
        } else {
            $value = $data[$key];

            if(!is_numeric($value)) {
                $response->errors[$key] = 'el id debe ser un numero';
            }
        }

      $key = 'Comprobante_de_domicilio';
        if(empty($data[$key])){
            $response->errors[$key][] = 'Este campo es obligatorio';
        } else {
            $value = $data[$key];

            if(strlen($value) < 4) {
                $response->errors[$key][] = 'Debe contener como mínimo 4 caracteres';
            }
        }
        $key = 'Identificacion_Visible';
        if(empty($data[$key])){
            $response->errors[$key][] = 'Este campo es obligatorio';
        } else {
            $value = $data[$key];

            if(strlen($value) < 4) {
                $response->errors[$key][] = 'Debe contener como mínimo 4 caracteres';
            }
        }
        $key = 'Licencia_Vigente';
        if(empty($data[$key])){
            $response->errors[$key][] = 'Este campo es obligatorio';
        } else {
            $value = $data[$key];

            if(strlen($value) < 4) {
                $response->errors[$key][] = 'Debe contener como minimo 4 caracteres';
            }
        }
        $key = 'Antecedentes_no_penales';
        if(empty($data[$key])){
            $response->errors[$key][] = 'Este campo es obligatorio';
        } else {
            $value = $data[$key];

            if(strlen($value) < 4) {
                $response->errors[$key][] = 'Debe contener como minimo 20 caracteres';
            }
        }
    
        $response->setResponse(count($response->errors) === 0);

        return $response;
    }
}
?>