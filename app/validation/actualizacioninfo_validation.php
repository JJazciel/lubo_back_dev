<?php
namespace App\Validation;

use App\Lib\Response;

class ActualizacionValidation {
    public static function validate($data, $update = false) {
        $response = new Response();

        $key = 'Nombre';
        if (empty($data[$key])) {
          $response->errors[$key] = 'Este campo es obligatorio';
        } else {
          $value = $data[$key];
          if (is_numeric($value)) {
            $response->errors[$key] = 'Este campo no admite números';
          }
        }

        $key = 'Apellidos';
        if (empty($data[$key])) {
          $response->errors[$key] = 'Este campo es obligatorio';
        } else {
          $value = $data[$key];
          if (is_numeric($value)) {
            $response->errors[$key] = 'Este campo no admite números';
          }
        }

               $response->setResponse(count($response->errors) === 0);
        return $response;
    }

    // public static function validateUpdate($data){
    //   $response = new Response();

    //   // $key = 'Telefono';
    //   // if (strlen($data[$key]) < 10) {
    //   //   $response->errors='Debe contener como mínimo 10 caracteres';
    //   // }

    //   // $key = 'Email';
    //   // if (!filter_var($data[$key], FILTER_VALIDATE_EMAIL)) {
    //   //   $response->errors='El correo ingresado no tiene el formato correcto';
    //   // }

    //          $response->SetResponse(count($response->errors) === 0);
    //   return $response;
    // }
}