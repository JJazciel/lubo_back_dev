<?php 
namespace App\Lib;
require_once __DIR__ . '/conekta-php-master/lib/Conekta.php';
// \Conekta\Conekta::setApiKey("key_yawNuPVcAsFs2bfJsCpVCQ");#dev
\Conekta\Conekta::setApiKey("key_bFgzHopChrHtt4u4rf7uqg");#prod
\Conekta\Conekta::setApiVersion("2.0.0");

class Pago{
	public static function pagar($idCustomer, $montoConekta, $montoMsjConekta, $idTarjeta){
        try{
            $order = \Conekta\Order::create(
                array(
                    "line_items" => array(
                        array(
                            "name" => "Lubo $".$montoMsjConekta,
                            "unit_price" => $montoConekta,
                            "quantity" => 1
                        )//first line_item
                    ), //line_items
                    "shipping_lines" => array(
                        array(
                            "amount" => 0,
                            "carrier" => "null"
                        )
                    ), //shipping_lines - physical goods only
                    "currency" => "MXN",
                    "customer_info" => array(
                        "customer_id" => $idCustomer
                    ),//customer_info
                    "shipping_contact" => array(
                        "address" => array(
                            "street1" => "Calle null",
                            "postal_code" => "00000",
                            "country" => "MX"
                        )//address
                    ), //shipping_contact - required only for physical goods

                    //"metadata" => array("reference" => "12987324097", "more_info" => "lalalalala"),
                    "charges" => array(
                        array(
                           "payment_method" => array(
                                "type" => "card",
                                "payment_source_id" => $idTarjeta
                            ) //payment_method - use customer's <code>default</code> - a card
                        ) //first charge
                    ) //charges
                )//order
            );

            return $order;
            
        } catch (\Conekta\ProccessingError $error){
            return $error->getMessage();
        } catch (\Conekta\ParameterValidationError $error){
            return $error->getMessage();
        } catch (\Conekta\Handler $error){
            return $error->getMessage();
        }
	}
}

 ?>