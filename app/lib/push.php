<?php
namespace App\Lib;

class Push{
  public function FMC($titulo, $texto, $to, $data){
    try{
      $fields = array(
        'to' => $to,
        'notification' => array(
          "title" => $titulo,
          "text" => $texto,
          "sound" => 'notificacionlubo.caf',
          "badge" => 1
        ),
        'data' => $data
      );
  
    $firebase = new Firebase();
    $respuesta = $firebase->sendPushNotification($fields);
    }catch (Exception $ex){
      $respuesta = $ex;
    }
    return $respuesta;
  }
}
#clase firebase para enio de push
class Firebase {
   /*
   * This function will make the actuall curl request to firebase server
   * and then the message is sent
   */
   public function sendPushNotification($fields) {

    // $server_key = 'AIzaSyD_ty1_r6vjZcWJ_-guXtoShdqG2Brs_o4'; #Dev
    #"AIzaSyBlB_CXYwON3Dbb3u8AJvZSAVDyYSv2aXE";  ->  CloudMessagin anterior
    $server_key = 'AIzaSyBlB_CXYwON3Dbb3u8AJvZSAVDyYSv2aXE'; #prod
    //firebase server url to send the curl request
    $url = 'https://fcm.googleapis.com/fcm/send';
    //building headers for the request
    $headers = array(
           'Authorization: key='.$server_key,
           'Content-Type: application/json'
       );

       //Initializing curl to open a connection
       $ch = curl_init();

       //Setting the curl url
       curl_setopt($ch, CURLOPT_URL, $url);

       //setting the method as post
       curl_setopt($ch, CURLOPT_POST, true);

       //adding headers
       curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
       curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

       //disabling ssl support
       curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

       //adding the fields in json format
       curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

       //finally executing the curl request
       $result = curl_exec($ch);
       if ($result === FALSE) {
           die('Curl failed: ' . curl_error($ch));
       }

       //Now close the connection
       curl_close($ch);

       //and return the result
       return $result;
   }
}
?>
