<?php
namespace App\Lib;

class EmailWelcome
{
	function Send($email,$id,$nombreCompleto ){

		$titulo = 'Bienvenido a lubo!!!';
		$header = 'From: '. $email;
		$mensaje = "
		<html lang='es'>
	    <head>
	    <meta charset='utf-8'>
	    <title>Lubo - Bienvenida Usuario</title>
	    <STYLE TYPE='text/css'>
			@font-face{
				font-family: 'Proxima Nova Light';
				src: url('http://lubo.com.mx/fonts/sffont/sanfranciscodisplay-regular-webfont.woff')  format('woff');
			}
			@font-face{
				font-family: 'Vibes';
				src: url('https://stardust.com.mx/Lubo/elmentEmail/Saturda-Vibes_0.ttf') format('truetype');
			}
			
			p{
				font-family: 'Proxima Nova Light', 'Helvetica Neue', Arial, sans-serif;
				font-size: 17px;
				color: #4a4a4a;
			}
			pV{
				font-family: 'Vibes';
				font-size: 29px;
				color: black;
			}
			.logo{
				margin-top: -500px;
				margin-left: 8%;
				padding-bottom: 100px;
			}
			@media (min-width: 857px) and (max-width: 1036px){
				.logo{
					padding-bottom: 80px;
				}
			}
			@media (min-width: 732px) and (max-width: 856px){
				.logo{
					padding-bottom: 65px;
				}
			}
			@media (min-width: 600px) and (max-width: 731px){
				.logo{
					padding-bottom: 50px;
				}
			}
			@media (min-width: 400px) and (max-width: 599px){
				.logo{
					padding-bottom: 40px;
				}
			}
			.pass a{
					color: white;
					text-decoration: none;
					font-family: 'Helvetica';
					} 
			.pass{
					text-align: center;
					padding :10px ;
					background:#ED3093 ;
					border: 50px;
					width: 150px;
					border-radius: 7px;
					}
		</STYLE>
		<link rel ='Stylesheet' type='text/css' href='http://back.lubo.com.mx/lubo/app/recursos/css/estilo.css'>
		<link href='https://fonts.googleapis.com/css?family=Montserrat:300' rel='stylesheet'>
	    </head>
	    <body style='width:550px; margin:0 auto;'>
	      	<br>
	      	<br>
	      	<div class='' style='width: 100%; height:190px; background-image: linear-gradient(-90deg, #870080, #ED3093);'>
		      	<center>
		      		<img src='http://www.lubo.com.mx/email/correodeinvitacion.png' width='70%' style='margin-top: 10px;'>
		      	</center>
		      	<center>
		      	<h1 style='font-family: Vibes; font-size: 66px; margin-top: 10px;'>Hola <span style='color: #FFFFFF;'>".$nombreCompleto."</span></h1>
		      	</center>
		      	<center>
		      	<p style='font-family: Proxima Nova Light, Montserrat, sans-serif; line-height:0px; font-size: 20px; color: #FFFFFF; margin-top: -30px;'>¡Bienvenido!</p>
		      	</center>
	      	</div>
	      	<br>
	      	<div class=''>
	      	<h2 style='font-family: Proxima Nova Light, Montserrat, sans-serif; margin-left: 20px;'>
		      	Gracias por registrarte
	      	</h2>
	      	<div class='content' style='margin-top:0px; margin-left: 20px;'>
				<p style='font-family: Proxima Nova Light, Montserrat, sans-serif; line-height:23px;'>Hiciste una gran elección, te aseguramos que no vas a arrepentirte pues haremos de cada uno de tus viajes una nueva experiencia llena de seguridad, comodidad y eficiencia.
				</p>

				<p style='font-family: Proxima Nova Light, Montserrat, sans-serif; line-height:23px;'>
					Bien, ya es momento de que comiences a disfrutar de los grandes beneficios que te ofrecemos.
				</p>
				<p style='font-family: Proxima Nova Light, Montserrat, sans-serif; line-height:23px;'>
					Así que, ¿Porqué no pides tu primer viaje? estamos seguros será el mejor de todos.
				</p>
				</p>
				<font size='3' color= '#424242' face='helvetica'>
					<center>
					<div class='pass'>
						<a href='https://stardust.com.mx/Lubo/redirUrlProd/index.html?a=verificar&u=".$id."'>Verificar Correo</a>
					</div>
					</center>
					</font>
				<br>
				<h3 style='font-family: Proxima Nova Light, Montserrat, sans-serif;'>
				    Viajemos juntos
			    </h3>
				<div style='text-align: right'><img src='http://www.lubo.com.mx/email/automail.png' width='100' style='margin-right: 100px;'/></div>
				<br>
				<p style='font-family: Proxima Nova Light, Montserrat, sans-serif;'>Ten un grandioso día! :)</p>
				<p style='font-family: Proxima Nova Light, Montserrat, sans-serif;'>El equipo de Lubo</p>
				<p></p>
				<br>
				<br>
				<br>
		  	</div>
			<hr>
				<p><span style='color:979797; font-size:12px;'>Para mayor información sobre este correo electrónico, puede hacerlo por medio electrónico enviando un mensaje a soporte@lubo.com.mx</span></p>
				<p><span style='color:979797; font-size:12px;'>
					© 2020 Todos los Derechos Reservados, Startdust Inc. S.A. de C.V.</p> 
				</span></p>
				<br>
			  	<br>
	        </body>
        </html>";
		// Cabecera que especifica que es un HMTL
		$cabeceras  = 'MIME-Version: 1.0' . "\r\n";
		$cabeceras .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
		// Cabeceras adicionales
		$cabeceras .= 'From: Lubo <soporte@stardust.com.mx>' . "\r\n";
		$urlP = "http://stardust.com.mx/Lubo/email.php";
		$data = array(
			"from"=>"Lubo <soporte@stardust.com.mx>",
			"to"=>$email,
			"subject"=>$titulo,
			"message"=>$mensaje,
			"headers"=>$cabeceras
		);
		$opts = array('http' =>
		array(
			'method'  => 'POST',
			'header'  => 'Content-type: application/json',
			'content' =>  json_encode($data)));
		$context  = stream_context_create($opts);
		$res = file_get_contents($urlP, false, $context);
		$respuesta = json_decode($res);
		if ($respuesta->success){
			return '1';
		}else{
			return '0';
		}
	}
}
?>