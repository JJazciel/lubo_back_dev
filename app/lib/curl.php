<?php 
namespace App\Lib;

class CURL{
	private static $urlbase = 'https://lubo-b3831.firebaseio.com/'; #Firebase DB producción
      // private static $urlbase = 'https://lubo-dev.firebaseio.com/'; #Firebase DB dev

	public static function GetInfoDriver($zona){
            $urlG = self::$urlbase."Drivers/".$zona.".json";
		$ch = curl_init();
      	curl_setopt($ch, CURLOPT_URL, $urlG);
      	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      	curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
      	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
      	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      	$res = curl_exec($ch);
      	curl_close($ch);
            $data = json_decode($res);
      	return $data;
	}

      #Recupera información de un viaje en específico
      public static function getInfoTravel($name){
            $urlG = self::$urlbase."Viajes/".$name.".json";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $urlG);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            $res = curl_exec($ch);
            curl_close($ch);
            $res = json_decode($res);
            return $res;
      }

	public static function AddTravel($data){
            $urlP = self::$urlbase."Viajes.json";
		$ch = curl_init();
      	curl_setopt($ch, CURLOPT_URL, $urlP);
      	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      	curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
      	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
      	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      	curl_setopt($ch, CURLOPT_POST,1);
      	curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
      	$name = curl_exec($ch);
      	curl_close($ch);
      	$resp = json_decode($name);
      	return $resp;
	}

      public static function GetRuta($idUsuario){
            $urlRuta = self::$urlbase."Rutas/User/".$idUsuario.".json";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $urlRuta);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            $res = curl_exec($ch);
            curl_close($ch);
            $resultado = json_decode($res);
            return $res;
      }

      //Agrega el id del conductor al viaje además de actualizar el Status a 1
      public static function addDriverToTravel($name, $idConductor, $zonaDriver){
            $urlUpdate= self::$urlbase."Viajes/".$name.".json";
            $postdata = array(
                              'idConductor' => floatval($idConductor),
                              'status' => 1,
                              'zonaDriver' => floatval($zonaDriver)
                             );
            $opts = array('http' =>
                                array(
                                    'method'  => 'PATCH',
                                    'header'  => 'Content-type: application/json',
                                    'content' =>  json_encode($postdata)));
            $context  = stream_context_create($opts);
            $res = file_get_contents($urlUpdate, false, $context);
            $respuesta = json_decode($res);
            return $respuesta;
      }

      //Actualiza el Status del viaje en Firebase
      public static function updateStatus($name, $status){
            $urlUpdate = self::$urlbase."Viajes/".$name.".json";
            $postdata = array('status' => floatval($status));
            $opts = array('http' =>
                                array(
                                    'method'  => 'PATCH',
                                    'header'  => 'Content-type: application/json',
                                    'content' =>  json_encode($postdata)
            ));
            $context  = stream_context_create($opts);
            $res = file_get_contents($urlUpdate, false, $context);
            $respuesta = json_decode($res);
            return $respuesta;
      }

      //Borrar la ruta del usuario
      public static function deleteRoute($idUser,$name){
            $deleteViaje = self::$urlbase."Viajes/".$name.".json";
            $deleteRuta = self::$urlbase."Rutas/User/".$idUser.".json";
            $opts = array('http' =>
                                array(
                                    'method'  => 'DELETE',
                                    'header'  => 'Content-type: application/json'
                         ));
            $context  = stream_context_create($opts);
            $ruta = file_get_contents($deleteRuta, false, $context);
            $viaje = file_get_contents($deleteViaje, false, $context);
      }

      #Actualiza el idTravel del Driver
      public static function updateidTravel($idUser, $name, $idViaje, $zona, $status){
            $urlUpdate = self::$urlbase."Drivers/".$zona."/".$idUser.".json";
            $postdata = array('idTravelFb' => "$name",
                              'idTravel' => "$idViaje",
                              'status' => floatval($status));

            $opts = array('http' =>
                                array(
                                    'method'  => 'PATCH',
                                    'header'  => 'Content-type: application/json',
                                    'content' =>  json_encode($postdata)
            ));
            $context  = stream_context_create($opts);
            $res = file_get_contents($urlUpdate, false, $context);
            $respuesta = json_decode($res);
            return $respuesta;
      }

      #Elimina el viaje asignado a un conductor
      public static function deleteIds($idConductor, $zona){
            if ($zona > 0) {
                  $urlUpdate = self::$urlbase."Drivers/".$zona."/".$idConductor.".json";
                  $postdata = array('idTravelFb' => "",
                                    'idTravel' => "",
                                    'status' => 1);

                  $opts = array('http' =>
                                      array(
                                          'method'  => 'PATCH',
                                          'header'  => 'Content-type: application/json',
                                          'content' =>  json_encode($postdata)
                  ));
                  $context  = stream_context_create($opts);
                  $res = file_get_contents($urlUpdate, false, $context);
                  $respuesta = json_decode($res);
                  return $respuesta;
            }
      }

      #Cambiar el Status del conductor
      public static function statusDriver($idConductor, $status, $zona){
            $urlUpdate = self::$urlbase."Drivers/".$zona."/".$idConductor.".json";
            $postdata = array('status' => $status);
            $opts = array('http' =>
                                array(
                                    'method'  => 'PATCH',
                                    'header'  => 'Content-type: application/json',
                                    'content' =>  json_encode($postdata)
            ));
            $context  = stream_context_create($opts);
            $res = file_get_contents($urlUpdate, false, $context);
            $respuesta = json_decode($res);
            return $respuesta;
      }

      #Cambiar el Metodo de pago del viaje
      public static function updateMetodoPago($name, $tipo){
            $urlUpdate = self::$urlbase."Viajes/".$name.".json";
            $postdata = array('metodoPago' => floatval($tipo));
            $opts = array('http' =>
                                array(
                                    'method'  => 'PATCH',
                                    'header'  => 'Content-type: application/json',
                                    'content' =>  json_encode($postdata)
            ));
            $context  = stream_context_create($opts);
            $res = file_get_contents($urlUpdate, false, $context);
            $respuesta = json_decode($res);
            return $respuesta;
      }

      #Actualizar el campo Calificado en la tabla de Viajes
      public static function Calificado($name, $tipo){
            $urlUpdate= self::$urlbase."Viajes/".$name.".json";
            switch ($tipo) {
                  case 2: #Usuario claifica a Driver
                        $postdata = array(
                              'calificadoUsuario' => floatval(1)
                        );
                        break;
                  case 3: #Driver califica a Usuario
                        $postdata = array(
                              'calificadoDriver' => floatval(1)
                        );
            }
            $opts = array('http' =>
                                array(
                                    'method'  => 'PATCH',
                                    'header'  => 'Content-type: application/json',
                                    'content' =>  json_encode($postdata)));
            $context  = stream_context_create($opts);
            $res = file_get_contents($urlUpdate, false, $context);
            $respuesta = json_decode($res);
            return $respuesta;  
      }

      #Actualiza la URL de la imagen
      public static function updateUrlImage($idUser, $url, $idZona){
            $urlUpdate = self::$urlbase."Drivers/".$idZona."/".$idUser.".json";
            $postdata = array('imageUrl' => "$url");

            $opts = array('http' =>
                                array(
                                    'method'  => 'PATCH',
                                    'header'  => 'Content-type: application/json',
                                    'content' =>  json_encode($postdata)
            ));
            $context  = stream_context_create($opts);
            $res = file_get_contents($urlUpdate, false, $context);
            $respuesta = json_decode($res);
            return $respuesta;
      }

      #Agrega el auto al viaje en Firebase
      public static function addCar($name, $car){
            $urlUpdate = self::$urlbase."Viajes/".$name.".json";
            $postdata = array('car' => $car);
            $opts = array('http' =>
                                array(
                                    'method'  => 'PATCH',
                                    'header'  => 'Content-type: application/json',
                                    'content' =>  json_encode($postdata)
            ));
            $context  = stream_context_create($opts);
            $res = file_get_contents($urlUpdate, false, $context);
            $respuesta = json_decode($res);
            return $respuesta;
      }

      #Actualiza el campo qualification en Firebase y su ganancia por día
      public static function updateCalification($id, $prom, $zona){
            $urlUpdate= self::$urlbase."Drivers/".$zona."/".$id.".json";
            $postdata = array(
                              'qualification' => floatval($prom)
                             );
            $opts = array('http' =>
                                array(
                                    'method'  => 'PATCH',
                                    'header'  => 'Content-type: application/json',
                                    'content' =>  json_encode($postdata)));
            $context  = stream_context_create($opts);
            $res = file_get_contents($urlUpdate, false, $context);
            $respuesta = json_decode($res);
            return $respuesta;  
      }

      #Obtener información del contador de viajes del driver
      public static function ObtenerViajesDriver($idDriver, $zona){
            $urlG = self::$urlbase."Drivers/".$zona."/".$idDriver.".json";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $urlG);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            $res = curl_exec($ch);
            curl_close($ch);
            $res = json_decode($res);
            return $res;
      }

      #Actualizar el contador de los viajes del driver
      public static function updateTotalTravels($idDriver, $total, $zona){
            $urlUpdate= self::$urlbase."Drivers/".$zona."/".$idDriver.".json";
            $postdata = array(
                              'totalTravels' => floatval($total)
                             );
            $opts = array('http' =>
                                array(
                                    'method'  => 'PATCH',
                                    'header'  => 'Content-type: application/json',
                                    'content' =>  json_encode($postdata)));
            $context  = stream_context_create($opts);
            $res = file_get_contents($urlUpdate, false, $context);
            $respuesta = json_decode($res);
            return $respuesta;
      }

      #Actualizar ganancia diaria el Firebase
      public static function GananciaDiaria($idDriver, $monto, $zona){
            $urlUpdate= self::$urlbase."Drivers/".$zona."/".$idDriver.".json";
            $postdata = array(
                              'ganancia_Diaria' => $monto
                             );
            $opts = array('http' =>
                                array(
                                    'method'  => 'PATCH',
                                    'header'  => 'Content-type: application/json',
                                    'content' =>  json_encode($postdata)));
            $context  = stream_context_create($opts);
            $res = file_get_contents($urlUpdate, false, $context);
            $respuesta = json_decode($res);
            return $respuesta;
      }

      #Deja en 0 los contadores de GananciasDiarias
      public static function reiniciarGanancias($id, $zona){
            $urlUpdate= self::$urlbase."Drivers/".$zona."/".$id.".json";
            $postdata = array(
                  "ganancia_Diaria" => "0"
            );
            $opts = array('http' =>
                                array(
                                    'method'  => 'PATCH',
                                    'header'  => 'Content-type: application/json',
                                    'content' =>  json_encode($postdata)));
            $context  = stream_context_create($opts);
            $res = file_get_contents($urlUpdate, false, $context);
            $respuesta = json_decode($res);
            return $respuesta;
      }

      #Actualiza el campo pagado en la tabla viajes en Firebase
      public static function updatePagado($name){
            $urlUpdate= self::$urlbase."Viajes/".$name.".json";
            $postdata = array(
                              'pagado' => floatval(1)
                             );
            $opts = array('http' =>
                                array(
                                    'method'  => 'PATCH',
                                    'header'  => 'Content-type: application/json',
                                    'content' =>  json_encode($postdata)));
            $context  = stream_context_create($opts);
            $res = file_get_contents($urlUpdate, false, $context);
            $respuesta = json_decode($res);
            return $respuesta;
      }

      public static function deleteTravel($name){
            $deleteRuta = self::$urlbase."Viajes/".$name.".json";
            $opts = array('http' =>
                                array(
                                    'method'  => 'DELETE',
                                    'header'  => 'Content-type: application/json'
                         ));
            $context  = stream_context_create($opts);
            $ruta = file_get_contents($deleteRuta, false, $context);

            return $deleteRuta;
      }

      public static function updateStatusCancelacion($name, $statusCancelacion){
            $urlUpdate= self::$urlbase."Viajes/".$name.".json";
            switch ($statusCancelacion) {
                  case 1:
                        $postdata = array(
                              'statusCancelacion' => floatval($statusCancelacion)
                             );
                        break;

                  case 2:
                        $postdata = array(
                              'statusCancelacion' => floatval($statusCancelacion)
                             );
                        break;

                  case 3:
                        $postdata = array(
                              'statusCancelacion' => floatval($statusCancelacion)
                             );
                        break;
            }
            
            $opts = array('http' =>
                  array(
                        'method'  => 'PATCH',
                        'header'  => 'Content-type: application/json',
                        'content' =>  json_encode($postdata)
                  )
            );
            $context  = stream_context_create($opts);
            $res = file_get_contents($urlUpdate, false, $context);
            $respuesta = json_decode($res);
            return $respuesta;
      }

      public static function addDetailCancel($name, $description){
            $urlUpdate= self::$urlbase."Viajes/".$name.".json";
            $postdata = array(
                  'motivoCancelacionDriver' => $description
            );

            $opts = array('http' =>
                  array(
                        'method'  => 'PATCH',
                        'header'  => 'Content-type: application/json',
                        'content' =>  json_encode($postdata)
                  )
            );
            $context  = stream_context_create($opts);
            $res = file_get_contents($urlUpdate, false, $context);
            $respuesta = json_decode($res);
            return $respuesta;
      }

      public static function deleteToken($idConductor, $zona){
            if ($zona >= 1) {
                  $urlUpdate = self::$urlbase."Drivers/".$zona."/".$idConductor.".json";      
            } else {
                  $urlUpdate = self::$urlbase."User/".$idConductor.".json";
            }

            $postdata = array('deviceToken' => "");
            $opts = array('http' =>
                  array(
                        'method'  => 'PATCH',
                        'header'  => 'Content-type: application/json',
                        'content' =>  json_encode($postdata)
            ));
            $context  = stream_context_create($opts);
            $res = file_get_contents($urlUpdate, false, $context);
      }
}

 ?>