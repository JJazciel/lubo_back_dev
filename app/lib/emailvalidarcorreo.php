<?php
 namespace App\Lib;

 class ValidarEmail
{
	
	function Send($email,$id,$nombreCompleto)
	{
		#enviar el correo
		$titulo = 'Solicitud de validación de correo';
		$header = 'From: ' . $email;
		//$fechaActual = getdate();
		$mensaje = '
		<html lang="es">
	    <head>
	      <meta charset="utf-8">
	      <title>Confirmar e-mail</title>
	      <STYLE TYPE="text/css">
			@font-face{
				font-family: "SFRegular";
				src: url("http://lubo.com.mx/fonts/sffont/sanfranciscodisplay-regular-webfont.woff")  format("woff");
			}
			p{
				font-family:"Helvetica Neue", Arial, sans-serif;
				font-size: 17px;
				color: #4a4a4a;
				font-weight: 200;
			}
			.logo{
				margin-top: -500px;
				margin-left: 8%;
				padding-bottom: 100px;
			}
			.pass a{
				color: white;
				text-decoration: none;
				font-family: "Helvetica";
			} 
			.pass{
				text-align: center;
				padding :10px ;
				background:#ED3093;
				border: 50px;
				width: 150px;
				border-radius: 7px;
			}
			@media (min-width: 857px) and (max-width: 1036px){
				.logo{
					padding-bottom: 80px;
				}
			}
			@media (min-width: 732px) and (max-width: 856px){
				.logo{
					padding-bottom: 65px;
				}
			}
			@media (min-width: 600px) and (max-width: 731px){
				.logo{
					padding-bottom: 50px;
				}
			}
			@media (min-width: 400px) and (max-width: 599px){
				.logo{
					padding-bottom: 40px;
				}
			}
		</STYLE>
		<link href="https://fonts.googleapis.com/css?family=Montserrat:300" rel="stylesheet">
	    </head>
	    <body style="width:550px; margin:0 auto;">
	      <br>
	      <br>
	      <center><img src="http://back.lubo.com.mx/lubo/img/Lubo-Pink-compressor.png" width="120" alt="" /></center>
	      <br>
	      <p style="font-family: -apple-system, BlinkMacSystemFont, SFRegular, Helvetica Neue, Montserrat, sans-serif; text-align: center; font-size: 22px; font-weight: 400;">Solicitud de confirmación de correo</p>
	      <div class="content" style="margin-top:0px;">
		
		  <p style="font-family: -apple-system, BlinkMacSystemFont, SFRegular, Helvetica Neue, Montserrat, sans-serif;">Estimado '.$nombreCompleto.'.</p>
		
	
			  <br>
				<div class="primero">
				<p style="font-family: -apple-system, BlinkMacSystemFont, SFRegular, Helvetica Neue, Montserrat, sans-serif; line-height:23px;">Se ha solicitado recientemente la validación del correo para tu cuenta de Lubo®. Para completar el proceso, haz clic en el siguiente enlace </p>
				<br>
					<font size="3" color= "#424242" face="helvetica">
					
					<center>
					<div class="pass">
						<a href= "https://stardust.com.mx/Lubo/redirUrlProd/index.html?a=verificar&u='.$id.'">Validar mi correo electrónico</a>
					</div>
					</center>
					<br>
					<p align="center" style="font-family: Helvetica LT Std Light; font-size: 15px; color: #black; margin-left:8px;">

				 
		  <p style="font-family: -apple-system, BlinkMacSystemFont, SFRegular, Helvetica Neue, Montserrat, sans-serif; line-height:23px;">Este enlace se te fue enviado sólo para validar tu correo electrónico. Si no realizaste esta modificación <br> o si crees que alguien ha accedido a tu cuenta sin autorización, restablece<br> inmediatamente tu contraseña o contáctanos a hola@lubo.com.mx</p>
		  <br>
		  <p style="font-family: -apple-system, BlinkMacSystemFont, SFRegular, Helvetica Neue, Montserrat, sans-serif;">Atentamente</p>
		  <p style="font-family: -apple-system, BlinkMacSystemFont, SFRegular, Helvetica Neue, Montserrat, sans-serif;">El equipo de Lubo®</p>
		  <br>
		  <br>
		  <hr>
			  <p><span style="color:979797; font-size:12px;">Para mayor información sobre este correo electrónico, se puede comunicar a los siguientes teléfonos: 776 76 28870, o por medio electrónico enviando un mensaje a cj. hola@lubo.com.mx</span></p>
		<p><span style="color:979797; font-size:12px;">
		© 2020 Todos los Derechos Reservados, Startdust Inc. S.A. De C.V.</p> 
		</span></p>
			  <br>
		  <br>
	  </div>
    </body>
</html>';

			// Cabecera que especifica que es un HMTL
			$cabeceras  = 'MIME-Version: 1.0' . "\r\n";
			$cabeceras .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
			// Cabeceras adicionales
			$cabeceras .= 'From: Lubo <soporte@stardust.com.mx>' . "\r\n";
			$urlP = "http://stardust.com.mx/Lubo/email.php";
			$data = array(
				"from"=>"Lubo <soporte@stardust.com.mx>",
				"to"=>$email,
				"subject"=>$titulo,
				"message"=>$mensaje,
				"headers"=>$cabeceras
			);
			$opts = array('http' =>
			array(
				'method'  => 'POST',
				'header'  => 'Content-type: application/json',
				'content' =>  json_encode($data)));
			$context  = stream_context_create($opts);
			$res = file_get_contents($urlP, false, $context);
			$respuesta = json_decode($res);
		if ($respuesta->success){
			return '1';
		}else{
			return '0';
		}
	}
}
?>
