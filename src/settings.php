<?php
return [
    'settings' => [
        'displayErrorDetails' => true,

        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__ . '/../templates/',
        ],

        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => __DIR__ . '/../logs/app.log',
        ],

        // Configuración de mi APP
        'app_token_name'   => 'APP-TOKEN',
        'connectionString' => [
            // 'dns'  => 'mysql:host=localhost;dbname=lubo;charset=utf8', #local
            // 'dns'  => 'mysql:host=18.212.54.149;dbname=lubodev;charset=utf8', #dev
            'dns'  => 'mysql:host=54.174.116.121;dbname=lubo;charset=utf8', #prod
            'user' => 'lubo',
            'pass' => 'St@rtDu2tH0o1i007#'
        ],
        'outputBuffering'=> false
    ],
];
