<?php

$container = $app->getContainer();

// view renderer
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], Monolog\Logger::DEBUG));
    return $logger;
};

// Database
$container['db'] = function($c){
    $connectionString = $c->get('settings')['connectionString'];

    $pdo = new PDO($connectionString['dns'], $connectionString['user'], $connectionString['pass']);

    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);

    return new FluentPDO($pdo);
};

// Models
$container['model'] = function($c){
    return (object)[
        'auth' => new App\Model\AuthModel($c->db),
        'persona' => new App\Model\PersonaModel($c->db),
        'vehiculo' => new App\Model\VehiculoModel($c->db),
        'calificacion' => new App\Model\CalificacionModel($c->db),
        'devicetoken' => new App\Model\DevicetokenModel($c->db),
        'direccion' => new App\Model\DireccionModel($c->db),
        'documentos' => new App\Model\DocumentosModel($c->db),
        'detalletarifa' => new App\Model\DetalleTarifaModel($c->db),
        'historialviaje' => new App\Model\HistorialViajesModel($c->db),
        'transaccion' => new App\Model\TransaccionModel($c->db),
        'viaje' => new App\Model\ViajeModel($c->db),
        'tarifa' => new App\Model\TarifaModel($c->db),
        'tarjeta' => new App\Model\TarjetaModel($c->db),
        'efectivo' => new App\Model\EfectivoModel($c->db),
        'chat' => new App\Model\ChatModel($c->db),
        'img' => new App\Model\ImgModel($c->db),
        'registro' => new App\Model\RegistroModel($c->db),
        'conductores' => new App\Model\ConductoresModel($c->db),
        'tokenservices' => new App\Model\TokenServicesModel($c->db),
        'datapersona' => new App\Model\DataPersonaModel($c->db),
        'problemas' => new App\Model\ProblemasModel($c->db),
        'actualizacioninfo' => new App\Model\ActualizacionModel($c->db),
        'appversion' => new App\Model\AppVersionModel($c->db),
        'zona' => new App\Model\ZonaModel($c->db),
        'contactos' => new App\Model\ContactosModel($c->db),
        'administradores' => new App\Model\AdministradoresModel($c->db),
        'cron' => new App\Model\CronJob($c->db),
        'sos' => new App\Model\SosModel($c->db),
        'balance' => new App\Model\BalanceModel($c->db),
        'infobalance' => new App\Model\InfoBalanceModel($c->db),
        'infobancaria' => new App\Model\InfobancriaModel($c->db),
        'reportes' => new App\Model\ReportesModel($c->db),
        'factura' => new App\Model\FacturaModel($c->db),
        'perfilImpuestos' => new App\Model\PerfilImpuestosModel($c->db),
        'semana' => new App\Model\SemanasModel($c->db),
        'listadodata' => new App\Model\ListarDataModel($c->db)
    ];
};